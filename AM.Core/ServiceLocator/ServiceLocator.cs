﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Configuration;

namespace AM.Core.ServiceLocator
{
    public class ServiceLocator : IDisposable
    {

        #region "Private Function"
        /// <summary>
        /// Unityカスタムセクション取得
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private static UnityConfigurationSection GetUnityConfigSection()
        {
            return (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
        }

        private static UnityConfigurationSection GetUnityConfigSection(string configurationDirectory, string configName)
        {
            //'DI対象専用の構成ファイルより、unityセクションを読込
            ExeConfigurationFileMap map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = configurationDirectory + "\\" + configName + ".config";
            var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);

            return (UnityConfigurationSection)config.GetSection("unity");
        }
        #endregion

        #region "Configure"
        /// <summary>
        /// コンテナ登録
        /// </summary>
        /// <remarks></remarks>
        public static void Configure(string configurationDirectory, string configName)
        {
            //' Unityカスタムセクション取得
            UnityContainer container = new UnityContainer();
            var section = GetUnityConfigSection(configurationDirectory, configName);
            //★"File Configuration Source"で指定したEntLib.configの内容と
            //  configName("Unity")の内容が共に構成される
            foreach (ContainerElement uce in section.Containers)
            {
                //section.Configure(container, uce.Name);
                section.Configure(container);
            }

            //構成情報を元にEnterpriseLibraryのコンテナの初期化・統合
            EnterpriseLibraryContainer.Current = new UnityServiceLocator(container);
        }
        #endregion

        #region "Resolve"
        public static T Resolve<T>()
        {
            return EnterpriseLibraryContainer.Current.GetInstance<T>();
        }

        public static T Resolve<T>(string name)
        {
            return EnterpriseLibraryContainer.Current.GetInstance<T>(name);
        }

        public static object Resolve(Type t)
        {
            return EnterpriseLibraryContainer.Current.GetInstance(t);
        }

        public static object Resolve(Type t, string name)
        {
            return EnterpriseLibraryContainer.Current.GetInstance(t, name);
        }
        #endregion

        #region "IDisposable Support"
        // このコードは、破棄可能なパターンを正しく実装できるように Visual Basic によって追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(ByVal disposing As Boolean) に記述します。
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // 重複する呼び出しを検出するには
        private bool disposedValue = false;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    //_container.Dispose()
                }

                // TODO: ユーザー独自の状態を解放します (アンマネージ オブジェクト)。
                // TODO: 大きなフィールドを null に設定します。
            }
            this.disposedValue = true;
        }
        #endregion
    }
}

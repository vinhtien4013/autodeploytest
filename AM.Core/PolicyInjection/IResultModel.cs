﻿
namespace AM.Core.PolicyInjection
{
    /// <summary>
    /// AOPで使用するためのインターフェース定義
    /// </summary>
    /// <remarks>
    /// WCFサービスでの使用時も、DataMemberとしては公開はしない
    /// </remarks>
    public interface IResultModel
    {
        /// <summary>
        /// 内部制御用ステータス
        /// </summary>
        string InnerStatus { get; set; }
    }

    namespace ResultConst
    {
        /// <summary>
        /// 内部ステータス定義
        /// </summary>
        /// <remarks>
        /// AOPとの通信に使うエリア
        /// </remarks>
        public class InnerStatus
        {
            //【正常(Default値)】
            public const string Success = "000";
            //【ロールバック要求】
            public const string RequestRollBack = "999";
        }
    }
}

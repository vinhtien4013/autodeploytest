﻿using FP.Practices.EnterpriseLibrary.FPData;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using AM.Core.Util;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AM.Core.PolicyInjection
{
    /**************************************************************************/
    /// <summary>
    /// TransactionCallHandler 
    /// </summary>
    /// <remarks>
    /// Copyright(C) 2014 System Consultant Co., Ltd. All Rights Reserved.
    /// </remarks>
    /**************************************************************************/
    [ConfigurationElementType(typeof(CustomCallHandlerData))]
    public class TransactionCallHandler : ICallHandler
    {
        public int Order { get; set; }
        private string _contextName;
        private string _connectionStringName;

        /*====================================================================*/
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="contextName"></param>
        /// <param name="connectionStringName"></param>
        /*====================================================================*/
        public TransactionCallHandler(string contextName, string connectionStringName)
        {
            this._contextName = contextName;
            this._connectionStringName = connectionStringName;
        }

        /*====================================================================*/
        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="input"></param>
        /// <param name="getNext"></param>
        /// <returns></returns>
        /*====================================================================*/
        public Microsoft.Practices.Unity.InterceptionExtension.IMethodReturn Invoke(Microsoft.Practices.Unity.InterceptionExtension.IMethodInvocation input, Microsoft.Practices.Unity.InterceptionExtension.GetNextHandlerDelegate getNext)
        {
            IMethodReturn result;

            /*----------------------------------------------------------------*/
            /* Get information from MapperManager                             */
            /*----------------------------------------------------------------*/
            MapperManager mapperManager = EnterpriseLibraryContainer.Current.GetInstance<IMapperManager>() as MapperManager;
            if (mapperManager == null)
            {
                throw new ApplicationException("Can't get information from MapperManager");
            }

            string connectionStringName = mapperManager.ConnectionStringName;
            string contextName = mapperManager.ContextName;
            
            if (!(string.IsNullOrEmpty(this._contextName)))
            {
                contextName = this._contextName;
            }
            if (!(string.IsNullOrEmpty(this._connectionStringName)))
            {
                connectionStringName = this._connectionStringName;
            }

            if (string.IsNullOrEmpty(contextName))
            {
                throw new ApplicationException("ContextName doesn't exists");
            }
            if (string.IsNullOrEmpty(connectionStringName))
            {
                throw new ApplicationException("ConnectionStringName doesn't exists ");
            }

            ConnectionHelper helper = new ConnectionHelper(contextName, connectionStringName);
            helper.Connection = helper.Database.CreateConnection();

            try
            {
                SqlAzureConnectionUtil sqlAzureUtil = new SqlAzureConnectionUtil();
                sqlAzureUtil.OpenWithRetry(helper.Connection as SqlConnection);

                if (helper.Transaction == null)
                {
                    helper.Transaction = helper.Connection.BeginTransaction();
                }

                result = getNext()(input, getNext);
                
                IResultModel returnInf = result.ReturnValue as IResultModel;
                string innerStatus= "";
                if (returnInf !=null){
                        innerStatus = returnInf.InnerStatus;
                }
                if (innerStatus.Equals(ResultConst.InnerStatus.Success) && result.Exception == null)
                {
                    if (!(helper.Transaction == null))
                    {
                        helper.Transaction.Commit();
                    }
                }
                else
                {
                    if (!(helper.Transaction == null))
                    {
                        helper.Transaction.Rollback();
                    }

                    if (result.Exception != null)
                    {
                        if (result.Exception.GetType() == typeof(RollbackException))
                        {
                            result = input.CreateExceptionMethodReturn(result.Exception);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                result = input.CreateExceptionMethodReturn(ex);
            }
            finally
            {
                if (!(helper.Transaction == null))
                {
                    helper.Transaction.Dispose();
                    helper.Transaction = null;
                }

                if (!(helper.Connection == null) && helper.Connection.State == ConnectionState.Open)
                {
                    helper.Connection.Close();
                    helper.Connection.Dispose();
                    helper.Connection = null;
                }
            }
            return result;
        }
    }
}

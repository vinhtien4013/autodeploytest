﻿using Microsoft.Practices.Unity.InterceptionExtension;

namespace AM.Core.PolicyInjection
{
    /**************************************************************************/
    /// <summary>
    /// TransactionAttribute
    /// </summary>
    /// <remarks>
    /// Copyright(C) 2014 System Consultant Co., Ltd. All Rights Reserved.
    /// </remarks>
    /**************************************************************************/
    public class TransactionAttribute : HandlerAttribute
    {
        public string ContextName           { get; set; }
        public string ConnectionStringName  { get; set; }

        /*====================================================================*/
        /// <summary>
        /// ICallHandler 
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        /*====================================================================*/
        public override ICallHandler CreateHandler(Microsoft.Practices.Unity.IUnityContainer container)
        {
            return new TransactionCallHandler(this.ContextName, this.ConnectionStringName);
        }
    }
}

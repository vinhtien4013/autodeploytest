﻿using System;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;

namespace AM.Core.PolicyInjection
{
    /**************************************************************************/
    /// <summary>
    /// RollbackException
    /// </summary>
    /// <remarks>
    /// Copyright(C) 2014 System Consultant Co., Ltd. All Rights Reserved.
    /// </remarks>
    /**************************************************************************/
    [ConfigurationElementType(typeof(CustomHandlerData))]
    [Serializable()]
    public class RollbackException : ApplicationException
    {
        public string ErrorCode { get; set; }

        /*====================================================================*/
        /// <summary>
        /// GetObjectData
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        /*====================================================================*/
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        /*====================================================================*/
        /// <summary>
        /// Constructor
        /// </summary>
        /*====================================================================*/
        public RollbackException() : base() { }

        /*====================================================================*/
        /// <summary>
        /// Constructor
        /// Initializes with a specified error message.
        /// </summary>
        /// <param name="message"></param>
        /*====================================================================*/
        public RollbackException(string message) : base(message) { }

        /*====================================================================*/
        /// <summary>
        /// Constructor
        /// Initializes with a specified error message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="errorCode"></param>
        /*====================================================================*/
        public RollbackException(string message, string errorCode) : base(message)
        {
            this.ErrorCode = errorCode;
        }

        /*====================================================================*/
        /// <summary>
        /// Constructor
        /// Initializes with a specified error 
        /// message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /*====================================================================*/
        public RollbackException(string message, Exception exception) : base(message, exception) { }

        /*====================================================================*/
        /// <summary>
        /// Constructor
        /// Initializes with serialized data.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        /*====================================================================*/
        public RollbackException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

﻿using System;
using System.Linq;

namespace AM.Core.Util
{
    public class AzureUtil
    {
        /// <summary>
        /// タイムゾーンID
        /// </summary>
        /// <remarks>
        /// 適宜追加してください。
        /// </remarks>
        public class TimezoneId
        {
            public const string Japan = "Tokyo Standard Time";
            public const string VietNam = "SE Asia Standard Time";
        }

        /// <summary>
        /// 日本時間取得
        /// </summary>
        /// <returns></returns>
        /// <remarks>Public Shared ReadOnly?</remarks>
        public static DateTime JapanDateTimeNow()
        {
            return LocalDateTimeNow(AzureUtil.TimezoneId.Japan);
        }

        public static DateTime VietnamDateTimeNow()
        {
            return LocalDateTimeNow(AzureUtil.TimezoneId.VietNam);
        }

        /// <summary>
        /// ローカル時間取得
        /// </summary>
        /// <param name="timeZoneId">取得するタイムゾーンID</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DateTime LocalDateTimeNow(string timeZoneId)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now.ToUniversalTime(), timeZoneId);
        }

        /// <summary>
        /// Utcとのオフセット時間(時差：単位はHour)取得
        /// </summary>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static int GetUtcOffset(string timeZoneId)
        {
            //TimeZoneIdが明示的に指定されていない時は"日本時間"をセット
            if (string.IsNullOrEmpty(timeZoneId))
                timeZoneId = AzureUtil.TimezoneId.Japan;
            //タイムゾーン情報取得
            dynamic tzi = TimeZoneInfo.GetSystemTimeZones().Where(t => t.Id == timeZoneId);
            //Utcとの時差を返す
            return tzi.FirstOrDefault.GetUtcOffset(DateTime.Now).Hours;
        }
    }
}
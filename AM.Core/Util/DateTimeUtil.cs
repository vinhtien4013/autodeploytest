﻿using System;
using System.Configuration;

namespace AM.Core.Util
{
    public class DateTimeUtil
    {
        public const string YYYMMDD = "yyyy/MM/dd";
        public const string YYYMMDD_HHMMSS = "yyyy/MM/dd HH:mm:ss";
        public const int UP = 1;
        public const int DOWN = 2;

        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns>true or false</returns>
        public static bool IsDate(string strDate)
        {
            try
            {
                DateTime datDate = DateTime.Parse(strDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns>true or false</returns>
        public static bool IsDate(object strDate)
        {
            try
            {
                DateTime datDate = DateTime.Parse(strDate.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns>DateTime</returns>
        public static DateTime StringToDateTime(string strDate)
        {
            DateTime dateTime = DateTime.Parse(strDate);
            return dateTime;
        }
        
        /// <summary>
        /// string: 12/6/2016 10:24:44 AM
        /// </summary>
        /// <returns>12/6/2016</returns>
        public static string DateTimeToStringDate(Nullable<DateTime> dateTime)
        {
            if (dateTime != null)
            {
                return dateTime.Value.ToString(YYYMMDD);
            }
            return string.Empty;
        }

        /// <summary>
        /// Object: 12/6/2016 10:24:44 AM
        /// </summary>
        /// <returns>12/6/2016</returns>
        public static string DateTimeToStringDate(object objectDateTime)
        {
            if (IsDate(objectDateTime))
            {
                return DateTimeToStringDate(DateTime.Parse(objectDateTime.ToString()));
            }
            return string.Empty;
        }

        /// <summary>
        /// Make round minutes to 5mins for datetime
        /// </summary>
        /// <param name="datetime">date</param>
        /// <param name="Type">Rounding Type</param>
        /// <returns></returns>
        public static DateTime RoundingTime(DateTime datetime, int Type)
        {
            DateTime RoundedDate = datetime;
            var min = datetime.Minute;
            int roundmins = StringUtil.ConvertObjectToInteger32(ConfigurationManager.AppSettings["RoundMinutes"]);
            if (roundmins != 0)
            {
                int subTime = min % roundmins;
                double second = datetime.Second;

                if (subTime == 0)
                {
                    // ss : 00
                    RoundedDate = RoundedDate.AddSeconds(-second);
                }
                else
                {
                    if (Type == UP)
                    {
                        // Rounding time to up 5 minutes
                        // Ex: 07:42:37 -> 07:45:00
                        subTime = roundmins - subTime;
                        RoundedDate = RoundedDate.AddMinutes(subTime).AddSeconds(-second);
                    }
                    else
                    {
                        // Rounding time to down 5 minutes
                        // Ex: 17:03:32 -> 17:00:00
                        subTime = 0 - subTime;
                        RoundedDate = RoundedDate.AddMinutes(subTime).AddSeconds(-second);
                    }
                }
            }

            return RoundedDate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static double CalculateTwoTime(TimeSpan startTime, TimeSpan endTime)
        {
            double resultTime = 0;
            if (startTime > endTime)
                return resultTime;
            var timeDistance = endTime.Subtract(startTime);
            resultTime = timeDistance.Hours + (double)timeDistance.Minutes / 60;

            return resultTime;
        }

    }
}

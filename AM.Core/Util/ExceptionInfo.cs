﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Soap;
using System.ServiceModel;
using System.Web;
using System.Web.SessionState;
using System.Xml.Serialization;

namespace AM.Core.Util
{
    /**************************************************************************/
    /// <summary>
    /// 例外メッセージクラス
    /// </summary>
    /// <remarks>
    /// Copyright(C) 2014 System Consultant Co., Ltd. All Rights Reserved.
    /// </remarks>
    /**************************************************************************/
    public class ExceptionInfo
    {
        /*====================================================================*/
        /// <summary>
        /// 例外メッセージ取得
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /*====================================================================*/
        public static string GetExceptionMessage(Exception ex)
        {
            /*----------------------------------------------------------------*/
            /* Exception情報編集                                              */
            /*----------------------------------------------------------------*/
            List<string> listMsgEx = GetMessageFromException(ex);

            /*----------------------------------------------------------------*/
            /* Http情報編集                                                   */
            /*----------------------------------------------------------------*/
            List<string> listMsgHttp = GetMessageFromHttpContext(HttpContext.Current);

            /*----------------------------------------------------------------*/
            /* サービス情報編集                                               */
            /*----------------------------------------------------------------*/
            List<string> listMsgService = GetMessageFromService(System.ServiceModel.OperationContext.Current);

            /*----------------------------------------------------------------*/
            /* セッション情報編集                                             */
            /*----------------------------------------------------------------*/
            System.Text.StringBuilder output = new System.Text.StringBuilder();
            if (SqlAzureConnectionUtil.sessionInfoList.Count != 0 || SqlAzureConnectionUtil.sessionInfoList != null)
            {
                foreach (sessionModel session in SqlAzureConnectionUtil.sessionInfoList)
                {
                    output.AppendLine("[DatabaseNm:" + session.databaseNm + "] [ActivityId:" + session.activityId + "]");
                }
            }
            return "\r\n" + string.Join("\r\n", output.ToString()) + "\r\n" + string.Join("\r\n", ex.ToString()) + "\r\n" + string.Join("\r\n", listMsgEx) + "\r\n" + string.Join("\r\n", listMsgHttp) + "\r\n" + string.Join("\r\n", listMsgService);
        }

        /*====================================================================*/
        /// <summary>
        /// GetExceptionMessage
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        /*====================================================================*/
        public static string GetExceptionMessage(Exception ex, int num)
        {
            /*----------------------------------------------------------------*/
            /* Exception情報編集                                              */
            /*----------------------------------------------------------------*/
            List<string> listMsgEx = GetMessageFromException(ex);

            /*----------------------------------------------------------------*/
            /* Http情報編集                                                   */
            /*----------------------------------------------------------------*/
            List<string> listMsgHttp = GetMessageFromHttpContext(HttpContext.Current);

            /*----------------------------------------------------------------*/
            /* サービス情報編集                                               */
            /*----------------------------------------------------------------*/
            List<string> listMsgService = GetMessageFromService(System.ServiceModel.OperationContext.Current);

            /*----------------------------------------------------------------*/
            /* セッション情報編集                                             */
            /*----------------------------------------------------------------*/
            System.Text.StringBuilder output = new System.Text.StringBuilder();
            if (SqlAzureConnectionUtil.sessionInfoList.Count != 0 || SqlAzureConnectionUtil.sessionInfoList != null)
            {
                foreach (sessionModel session in SqlAzureConnectionUtil.sessionInfoList)
                {
                    output.AppendLine("[DatabaseNm:" + session.databaseNm + "] [ActivityId:" + session.activityId + "]");
                }
            }
            return "\r\n" + string.Join("\r\n", output.ToString()) + "\r\n" + string.Join("\r\n", "[ErrorCode:" + num.ToString() + "]") + "\r\n" + string.Join("\r\n", listMsgEx) + "\r\n" + string.Join("\r\n", listMsgHttp) + "\r\n" + string.Join("\r\n", listMsgService);
        }

        /*====================================================================*/
        /// <summary>
        /// Exceptionメッセージ取得
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        /// <remarks>
        /// InnerExceptionを潜行して編集
        /// </remarks>
        /*====================================================================*/
        public static List<string> GetMessageFromException(Exception ex)
        {
            List<string> listMsg = new List<string>();
            if (ex == null)
            {
                listMsg.Add("\r\n" + " No Exception.");
                return listMsg;
            }

            Exception currentException = ex;
            int indexException = 0;
            do
            {
                /*------------------------------------------------------------*/
                /* Exception情報取得                                          */
                /*------------------------------------------------------------*/
                listMsg.AddRange(GetMessageFromCurrentException(currentException, indexException));

                /*------------------------------------------------------------*/
                /* InnerExceptionセット                                       */
                /*------------------------------------------------------------*/
                currentException = currentException.InnerException;
                indexException += 1;
            } while ((currentException != null));
            return listMsg;
        }

        /*====================================================================*/
        /// <summary>
        /// 現在Exceptionのメッセージ取得
        /// </summary>
        /// <param name="currentException"></param>
        /// <param name="indexException"></param>
        /// <returns></returns>
        /// <remarks>
        /// InnerException以外のエラープロパティー値とStackTraceを返す
        /// </remarks>
        /*====================================================================*/
        private static List<string> GetMessageFromCurrentException(Exception currentException, int indexException)
        {
            List<string> listMsg = new List<string>();

            /*----------------------------------------------------------------*/
            /* Exceptionタイトルセット                                        */
            /*----------------------------------------------------------------*/
            listMsg.Add(string.Format("\r\n" + "{0}) Exception Information" + "\r\n", (indexException + 1).ToString()));
            listMsg.Add(string.Format("Exception Type: {0}", currentException.GetType().FullName));

            /*----------------------------------------------------------------*/
            /* Property情報セット                                             */
            /*----------------------------------------------------------------*/
            foreach (PropertyInfo propInfo in currentException.GetType().GetProperties())
            {
                if (propInfo.Name == "InnerException" | propInfo.Name == "StackTrace")
                {
                    continue;
                }

                /*------------------------------------------------------------*/
                /* ApplicationExceptionのNameValueCollectionの時              */
                /*------------------------------------------------------------*/
                if (propInfo.Name == "AdditionalInformation" & (currentException) is ApplicationException)
                {
                    if (propInfo.GetValue(currentException, null) != null)
                    {
                        /*----------------------------------------------------*/
                        /* NameValueCollectionを取得                          */
                        /*----------------------------------------------------*/
                        NameValueCollection currentAdditionalInfo = propInfo.GetValue(currentException, null) as NameValueCollection;

                        /*----------------------------------------------------*/
                        /* 値を編集                                           */
                        /*----------------------------------------------------*/
                        if (currentAdditionalInfo != null && currentAdditionalInfo.Count > 0)
                        {
                            listMsg.Add("AdditionalInformation:");
                            for (int iCnt = 0; iCnt <= currentAdditionalInfo.Count - 1; iCnt++)
                            {
                                listMsg.Add(string.Format(" -{0}: {1}", currentAdditionalInfo.GetKey(iCnt), currentAdditionalInfo[iCnt]));
                            }
                        }
                    }
                }
                else
                {
                    object propValue = propInfo.GetValue(currentException, null);
                    if (propValue == null)
                    {
                        listMsg.Add(string.Format("{0}: NULL", propInfo.Name));
                    }
                    else
                    {
                        listMsg.Add(string.Format("{0}: {1}", propInfo.Name, propValue));
                    }
                }
            }

            /*----------------------------------------------------------------*/
            /* StackTrace情報セット                                           */
            /*----------------------------------------------------------------*/
            if (currentException.StackTrace != null)
            {
                listMsg.Add("\r\n" + "StackTrace Information:");
                listMsg.Add(string.Format("{0}", currentException.StackTrace));
            }
            return listMsg;
        }

        /*====================================================================*/
        /// <summary>
        /// Httpメッセージ取得
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /*====================================================================*/
        public static List<string> GetMessageFromHttpContext(HttpContext httpContext)
        {
            List<string> listMsg = new List<string>();

            listMsg.Add("\r\n" + " [HTTP Information]");
            if (httpContext == null)
            {
                listMsg.Add(" No Information.");
                return listMsg;
            }

            HttpRequest request = httpContext.Request;
            if (request != null)
            {
                /*------------------------------------------------------------*/
                /* HTTP サーバ環境変数の出力                                  */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP ServerVariables (HTTPサーバ環境変数)]");
                listMsg.AddRange(GetListFromCollection<NameValueCollection>(request.ServerVariables));

                /*------------------------------------------------------------*/
                /* HTTP リクエストヘッダの出力                                */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Request Headers (HTTPリクエストヘッダ)]");
                listMsg.AddRange(GetListFromCollection<NameValueCollection>(request.Headers));

                /*------------------------------------------------------------*/
                /* HTTP リクエストクッキー情報の出力                          */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Request Cookies (HTTPリクエストクッキー)]");
                listMsg.AddRange(GetListFromCollectionCookie(request.Cookies));

                /*------------------------------------------------------------*/
                /* HTTP クエリ文字列の出力                                    */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Request QueryString (HTTPクエリ文字列)]");
                listMsg.AddRange(GetListFromCollection<NameValueCollection>(request.QueryString));

                /*------------------------------------------------------------*/
                /* HTTP フォームデータの出力                                  */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Request Form (HTTPフォーム)]");
                listMsg.AddRange(GetListFromCollection<NameValueCollection>(request.Form));
            }
            else
            {
                listMsg.Add("\r\n" + "[HTTP Request Information]");
                listMsg.Add("データがありません");
            }

            HttpResponse response = httpContext.Response;

            if ((response != null))
            {
                /*------------------------------------------------------------*/
                /* HTTP レスポンスステータス情報の出力                        */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Response Status (HTTPレスポンス状態)]");
                listMsg.Add(string.Format("Status : {0}", response.Status));
                listMsg.Add(string.Format("Status Code : {0}", response.StatusCode));

                /*------------------------------------------------------------*/
                /* HTTP レスポンスクッキー情報の出力                          */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Response Cookies (HTTPレスポンスクッキー)]");
                listMsg.AddRange(GetListFromCollectionCookie(response.Cookies));
            }
            else
            {
                listMsg.Add("\r\n" + "[HTTP Response Information]");
                listMsg.Add("データがありません。");
            }

            HttpSessionState session = httpContext.Session;

            if ((session != null))
            {
                /*------------------------------------------------------------*/
                /* HTTP Session内に含まれている情報の出力                     */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[HTTP Session Object (Sessionオブジェクト内の情報)]");

                foreach (string key in session.Keys)
                {
                    try
                    {
                        /*----------------------------------------------------*/
                        /* Session内には任意のオブジェクトが格納されるが、    */
                        /* その中身をテキスト化するにはToString()では不十分。 */
                        /* XmlSerializerクラスを利用して、XMLテキスト化する。 */
                        /*----------------------------------------------------*/
                        object obj = session[key];
                        string xml = null;
                        try
                        {
                            StringWriter writer = new StringWriter();
                            XmlSerializer serializer = new XmlSerializer(obj.GetType());
                            serializer.Serialize(writer, obj);
                            xml = writer.ToString();

                        }
                        catch (Exception exw)
                        {
                            MemoryStream memstream = new MemoryStream();
                            SoapFormatter formatter = new SoapFormatter();
                            formatter.Serialize(memstream, obj);
                            StreamReader reader = new StreamReader(memstream);
                            reader.BaseStream.Seek(0, SeekOrigin.Begin);
                            xml = reader.ReadToEnd();
                            string errMsg = exw.Message;
                        }
                        listMsg.Add(string.Format("{0}({1}) : " + "\r\n" + "{2}", key, obj.GetType().Name, xml));
                    }
                    catch (Exception ex)
                    {
                        string errMsg = ex.Message;
                        listMsg.Add("Session情報を出力できませんでした。");
                    }
                }
            }
            else
            {
                listMsg.Add("\r\n" + "[HTTP Session Information]");
                listMsg.Add("データがありません。");
            }
            return listMsg;
        }

        /*====================================================================*/
        /// <summary>
        /// Collectionリスト取得
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /*====================================================================*/
        private static List<string> GetListFromCollection<T1>(T1 collection) where T1 : NameValueCollection
        {
            List<string> listMsg = new List<string>();
            foreach (string key in collection.AllKeys)
            {
                listMsg.Add(string.Format("{0} : {1}", key, string.Copy(collection[key])));
            }
            return listMsg;
        }

        /*====================================================================*/
        /// <summary>
        /// GetListFromCollectionCookie
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        /*====================================================================*/
        private static List<string> GetListFromCollectionCookie(HttpCookieCollection collection)
        {
            List<string> listMsg = new List<string>();
            foreach (string key in collection.Keys)
            {
                listMsg.Add(string.Format("{0} : {1}", key, string.Copy(collection[key].Value)));
            }
            return listMsg;
        }

        /*====================================================================*/
        /// <summary>
        /// GetMessageFromService
        /// </summary>
        /// <param name="operationContext"></param>
        /// <returns></returns>
        /*====================================================================*/
        public static List<string> GetMessageFromService(OperationContext operationContext)
        {
            List<string> listMsg = new List<string>();

            listMsg.Add("\r\n" + " [Service Information]");
            if (operationContext == null)
            {
                listMsg.Add(" No Information.");
                return listMsg;
            }

            System.ServiceModel.Channels.RequestContext request = operationContext.RequestContext;
            if (request != null)
            {
                /*------------------------------------------------------------*/
                /* Service サーバ環境変数の出力                               */
                /*------------------------------------------------------------*/
                listMsg.Add("\r\n" + "[Service RequestMessage (Serviceリクエストメッセージ)]");

                /*------------------------------------------------------------*/
                /* Service リクエストヘッダの出力                             */
                /*------------------------------------------------------------*/
                listMsg.Add(request.RequestMessage.ToString());
            }
            return listMsg;
        }
    }
}
﻿using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AM.Core.Util
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonContent : HttpContent
    {
        private readonly MemoryStream _Stream = new MemoryStream();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public JsonContent(object value)
        {
            Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var jw = new JsonTextWriter(new StreamWriter(_Stream));
            jw.Formatting = Formatting.Indented;
            var serializer = new JsonSerializer();
            serializer.Serialize(jw, value);
            jw.Flush();
            _Stream.Position = 0;
        }

        /// <summary>
        /// オブジェクトのコンテンツを指定されたストリームに非同期にシリアル化します。
        /// </summary>
        /// <param name="stream">書き込み先のストリーム</param>
        /// <param name="context">関連付けられた </param>
        /// <returns>非同期操作を表すタスク オブジェクト</returns>
        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            //_Stream.CopyTo(stream);
            //var tcs = new TaskCompletionSource<object>();
            //tcs.SetResult(null);
            //return tcs.Task;
            return _Stream.CopyToAsync(stream);
        }

        /// <summary>
        /// 可能であればストリームの長さを計算します
        /// </summary>
        /// <param name="length">計算されたストリームの長さ</param>
        /// <returns>長さが計算された場合は true。それ以外の場合は false</returns>
        protected override bool TryComputeLength(out long length)
        {
            length = _Stream.Length;
            return true;
        }
    }
}

﻿using Microsoft.VisualBasic;
using System;
using System.Linq;
using System.Text;

namespace AM.Core.Util
{
    /// <summary>
    /// 文字列操作汎用クラス
    /// </summary>
    /// <remarks>
    /// UI層に依存しない文字列操作の汎用クラス
    /// </remarks>
    public class StringUtil
    {
        #region "GenerateFormatForMessage"
        /// <summary>
        /// メッセージ出力用フォーマット生成
        /// </summary>
        /// <param name="itemnm">アイテム名</param>
        /// <param name="message">メッセージ</param>
        /// <returns>
        /// フォーマット済みメッセージ([項目名]メッセージ)
        /// </returns>
        public static string GenerateFormatForMessage(string itemnm, string message)
        {
            return "[" + itemnm + "]" + message;
        }
        #endregion

        #region "ConvertDateFormat"
        /// <summary>
        /// 日付フォーマット変換
        /// </summary>
        /// <param name="ymd">yyyyMMddまたはyyyyMMddHHmm形式の文字列</param>
        /// <returns>
        /// yyyyMMdd : yyyy/MM/ddの文字列
        /// yyyyMMddHHmm : yyyy/MM/dd HH:mmの文字列
        /// </returns>
        public static string ConvertDateFormat(string ymd)
        {
            return ConvertDateFormat(ymd, "");
        }

        /// <summary>
        /// 日付フォーマット変換
        /// </summary>
        /// <param name="ymd">日付形式(yyyyMMddまたはyyyyMMddHHmm)の文字列</param>
        /// <param name="hm">時刻形式(HHmm)の文字列</param>
        /// <returns>
        /// yyyy/MM/dd HH:mmの文字列
        /// </returns>
        public static string ConvertDateFormat(string ymd, string hm)
        {
            if (string.IsNullOrEmpty(ymd) || ymd.Length <= 6)
                return string.Empty;
            System.DateTime dt = default(System.DateTime);
            string stringdt = string.Empty;

            if (!string.IsNullOrEmpty(hm) && hm.Length == 4)
            {
                // 時刻文字列が正しい形式で入力されている場合
                dt = new System.DateTime(int.Parse(ymd.Substring(0, 4)), int.Parse(ymd.Substring(4, 2)), int.Parse(ymd.Substring(6, 2)), int.Parse(hm.Substring(0, 2)), int.Parse(hm.Substring(2, 2)), 0);
                stringdt = dt.ToString("yyyy/MM/dd HH:mm");
            }
            else if (ymd.Length == 10)
            {
                // 日付文字列がyyyyMMddHHmm形式の場合
                dt = new System.DateTime(int.Parse(ymd.Substring(0, 4)), int.Parse(ymd.Substring(4, 2)), int.Parse(ymd.Substring(6, 2)), int.Parse(hm.Substring(8, 2)), int.Parse(hm.Substring(10, 2)), 0);
                stringdt = dt.ToString("yyyy/MM/dd HH:mm");
            }
            else
            {
                // 日付文字列がyyyyMMddの場合
                dt = new System.DateTime(int.Parse(ymd.Substring(0, 4)), int.Parse(ymd.Substring(4, 2)), int.Parse(ymd.Substring(6, 2)));
                stringdt = dt.ToString("yyyy/MM/dd");
            }

            return stringdt;
        }
        #endregion

        #region "ConvertYmDateFormat"
        /// <summary>
        /// 年月フォーマット変換
        /// </summary>
        /// <param name="ym">年月形式の日付文字列(yyyyMM)</param>
        /// <returns>
        /// yyyy/MMの文字列
        /// </returns>
        public static string ConvertYmDateFormat(string ym)
        {
            string strym = string.Empty;
            if ((ym != null) && ym.Length == 6)
            {
                strym = ym.Substring(0, 4) + "/" + ym.Substring(4, 2);
            }
            return strym;
        }
        #endregion

        #region "ConvertObjectToDateTime"
        /// <summary>
        /// 日付変換
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// 日付型のオブジェクト(引数が日付として妥当ではない場合はNothing)
        /// </returns>
        public static DateTime? ConvertObjectToDateTime(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return DateTime.Now;
            }
            else
            {
                if (Information.IsDate(obj))
                {
                    return Convert.ToDateTime(obj);
                }
                else
                {
                    return null;
                }
            }
        }
        public static DateTime ConvertObjectToDateTime2(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return DateTime.Now;
            }
            else
            {
                if (Information.IsDate(obj))
                {
                    return Convert.ToDateTime(obj);
                }
                else
                {
                    return DateTime.Now;
                }
            }
        }
        #endregion

        #region "ConvertObjectToInteger"
        /// <summary>
        /// Integer変換
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// Integer型の値(引数が数値として妥当ではない場合はNothing)
        /// </returns>
        public static int? ConvertObjectToInteger(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return 0;
            }
            else
            {
                if (Information.IsNumeric(obj))
                {
                    return Convert.ToInt32(obj);
                }
                else
                {
                    return null;
                }
            }
        }
        public static int ConvertObjectToInteger32(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return 0;
            }
            else
            {
                if (Information.IsNumeric(obj))
                {
                    return Convert.ToInt32(obj);
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion

        #region "ConvertObjectDecimal"
        /// <summary>
        /// Decimal変換
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// Decimal型の値(引数が数値として妥当ではない場合は0)
        /// </returns>
        public static decimal ConvertObjectToDecimal(object obj, decimal defaultVal = 0)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return defaultVal;
            }
            else
            {
                if (Information.IsNumeric(obj))
                {
                    return Convert.ToDecimal(obj);
                }
                else
                {
                    return defaultVal;
                }
            }
        }
        #endregion

        #region "ConvertIntegerToBoolean"
        /// <summary>
        /// Integer変換
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// Integer型の値(引数が数値として妥当ではない場合はNothing)
        /// </returns>
        public static bool ConvertIntegerToBool(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
                return false;

            if (Convert.ToInt32(obj) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region "ConvertObjectToBool"
        /// <summary>
        /// 文字列をBool値に変換
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// Bool型の値(True/False)
        /// </returns>
        public static bool ConvertObjectToBool(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return false;
            }
            else if (obj.GetType().Name.ToLower() == "string")
            {
                return Convert.ToBoolean((Convert.ToString(obj) == "1" ? true : false));
            }
            else
            {
                return Convert.ToBoolean(obj);
            }
        }
        #endregion

        #region "ConvertObjectToString"
        /// <summary>
        /// 文字列を文字列に変換
        /// </summary>
        /// <param name="obj">The obj.</param><returns></returns>
        public static string ConvertObjectToString(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value) || obj == null)
            {
                return "";
            }
            else
            {
                return Convert.ToString(obj);
            }
        }
        /// <summary>
        /// 文字列を文字列に変換
        /// </summary>
        /// <param name="obj">The obj.</param><returns></returns>
        public static string ConvertObjectToString(object obj, string defaultValue)
        {
            if (object.ReferenceEquals(obj, DBNull.Value) || obj == null)
            {
                return defaultValue;
            }
            else
            {
                return Convert.ToString(obj);
            }
        }
        #endregion

        #region "ConvertToNull:Null変換"
        /// <summary>
        /// Null変換
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks>
        /// DB更新時、空文字列ではなくNULLをセットするための変換関数
        /// </remarks>
        public static string ConvertToNull(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            return value;
        }
        #endregion

        #region "ConvertStringToDecimal:カンマ付き文字列の数値変換"
        /// <summary>
        /// カンマ付き文字列の数値変換
        /// </summary>
        /// <param name="value">カンマ付き文字列</param>
        /// <returns></returns>
        /// <remarks>
        /// DB更新時、カンマ付き文字列を数値項目にセットするための変換関数
        /// </remarks>
        public static Nullable<decimal> ConvertStringToDecimal(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            return Convert.ToDecimal(value.Replace(",", ""));
        }
        #endregion

        #region "ConvertDateToString:書式付き日付の文字列変換"
        /// <summary>
        /// 書式付き日付の文字列変換（2011/06/28 → 20110628）
        /// </summary>
        /// <param name="value">書式付き日付</param>
        /// <returns></returns>
        /// <remarks>
        /// DB更新時、書式付き日付を文字列にセットするための変換関数
        /// </remarks>
        public static string ConvertDateToString(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            return value.Replace("/", "").Substring(0, 8);
        }
        #endregion

        #region "ConvertTimeToString:書式付き時刻の文字列変換"
        /// <summary>
        /// 書式付き時刻の文字列変換（12:45 → 1245）
        /// </summary>
        /// <param name="value">書式付き時刻</param>
        /// <returns></returns>
        /// <remarks>
        /// DB更新時、書式付き時刻を文字列にセットするための変換関数
        /// </remarks>
        public static string ConvertTimeToString(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            return value.Replace(":", "").Substring(0, 4);
        }
        #endregion

        #region "TruncOverByte"
        /// <summary>
        /// 指定バイト長編集
        /// </summary>
        /// <param name="sStr">編集前文字列</param>
        /// <param name="iPrmMaxByte">指定バイト数</param>
        /// <remarks>最後の全角文字が指定ﾊﾞｲﾄ長を超える場合 -> 切捨て</remarks>
        /// <returns></returns>
        public static string TruncOverByte(string sStr, int iPrmMaxByte)
        {
            int iStrMaxByte = 0;
            //--文字列ﾊﾞｲﾄ数
            Encoding oEnc1 = default(Encoding);
            Encoding oEnc2 = default(Encoding);
            byte[] baArray = null;

            //--Nullﾁｪｯｸ
            if ((sStr == null))
            {
                return null;
            }
            else if (Information.IsDBNull(sStr))
            {
                return null;
            }

            oEnc1 = Encoding.GetEncoding(932);

            //--ﾊﾞｲﾄ数の算出
            iStrMaxByte = oEnc1.GetByteCount(sStr);

            //--ﾊﾞｲﾄ数による処理選択
            if (iStrMaxByte == iPrmMaxByte)
            {
                if (checkByte(sStr, iPrmMaxByte) == 1)
                {
                    baArray = oEnc1.GetBytes(sStr);
                    oEnc2 = Encoding.GetEncoding(0);
                    sStr = oEnc2.GetString(baArray, 0, iPrmMaxByte - 1);
                }
                else
                {
                    baArray = oEnc1.GetBytes(sStr);
                    oEnc2 = Encoding.GetEncoding(0);
                    sStr = oEnc2.GetString(baArray, 0, iPrmMaxByte);
                }
            }
            return sStr;

        }

        /// <summary>
        /// 指定バイト長編集
        /// </summary>
        /// <param name="sStr">文字列</param>
        /// <param name="iPosByte">指定ﾊﾞｲﾄ位置</param>
        /// <remarks>指定バイト位置の文字属性チェック</remarks>
        /// <returns>
        /// 0 :半角
        /// 1 :全角の前半部分
        /// 2 :全角の後半部分
        /// -1:ﾊﾟﾗﾒｰﾀｴﾗｰ
        /// </returns>
        private static int? checkByte(string sStr, int iPosByte)
        {

            int iCnt = 0;
            //--添字
            Encoding oEnc = default(Encoding);

            //--Nullﾁｪｯｸ
            if ((sStr == null))
            {
                return null;
            }
            else if (Information.IsDBNull(sStr))
            {
                return null;
            }

            oEnc = Encoding.GetEncoding(932);

            //--指定ﾊﾞｲﾄ位置ﾁｪｯｸ
            if (iPosByte > 0 & iPosByte <= oEnc.GetByteCount(sStr))
            {
            }
            else
            {
                return -1;
            }

            //--指定文字数の算出
            for (iCnt = 1; iCnt <= Strings.Len(sStr); iCnt++)
            {
                if (oEnc.GetByteCount(Strings.Left(sStr, iCnt)) >= iPosByte)
                {
                    break; // TODO: might not be correct. Was : Exit For
                }
            }

            //--全角／半角ﾁｪｯｸ
            if (oEnc.GetByteCount(Strings.Mid(sStr, iCnt, 1)) == 1)
            {
                return 0;
            }
            else
            {
                //--全角文字の前半／後半ﾁｪｯｸ
                if (oEnc.GetByteCount(Strings.Left(sStr, iCnt)) == iPosByte)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }

        }

        #endregion

        #region "IsMaxLengthOver"
        /// <summary>
        /// バイト数チェック
        /// </summary>
        /// <param name="str">文字列</param>
        /// <param name="maxlength">最大バイト数</param>
        /// <remarks>
        /// 文字列の長さが、最大バイト数を超えている場合、Falseを返す。超えていない場合、Trueを返す。
        /// </remarks>
        /// <returns>
        ///  True : 文字列が最大バイト数内
        ///  False: 文字列が最大バイト数をオーバー
        /// </returns>
        public static bool IsMaxLengthOver(string str, long maxlength)
        {
            int iStrMaxByte = 0;
            //--文字列ﾊﾞｲﾄ数
            Encoding oEnc1 = default(Encoding);

            //--Nullﾁｪｯｸ
            if ((str == null))
            {
                return false;
            }
            else if (Information.IsDBNull(str))
            {
                return false;
            }

            oEnc1 = Encoding.GetEncoding(932);

            //--ﾊﾞｲﾄ数の算出
            iStrMaxByte = oEnc1.GetByteCount(str);

            if (maxlength < iStrMaxByte)
            {
                return false;
            }

            return true;
        }
        #endregion

        #region "IsAlphaNumber"
        /// <summary>
        /// 英数字チェック
        /// </summary>
        /// <param name="str">文字列</param>
        /// <returns>
        /// True : 半角英数字のみの文字列
        /// False: 半角英数字以外が含まれている文字列
        /// </returns>
        public static bool IsAlphaNumber(string str)
        {
            Encoding oEnc = default(Encoding);

            //--Nullﾁｪｯｸ
            if (str == null)
            {
                return true;
            }
            else if (string.IsNullOrEmpty(str))
            {
                return true;
            }

            //--指定文字数の算出
            for (int iCnt = 0; iCnt <= str.Length - 1; iCnt++)
            {
                oEnc = Encoding.GetEncoding(932);
                // Shift-Jisで確認 (半角カナを1バイトとして扱う)

                if (oEnc.GetByteCount(str.Substring(iCnt, 1)) == 2)
                {
                    return false;
                }
                else
                {
                    oEnc = Encoding.GetEncoding(65001);
                    // UTF-8で確認（日本語は全て3バイト、UTF8でしか使用していない文字をチェック)
                    if (oEnc.GetByteCount(str.Substring(iCnt, 1)) != 1 & Strings.Asc(str.Substring(iCnt, 1)) == 63)
                    {
                        return false;
                    }

                    Strings.Asc(str.Substring(iCnt, 1));
                }
            }

            return true;
        }
        #endregion

        #region "IsJisFirstLevel"
        /// <summary>
        /// JIS第一水準チェック
        /// </summary>
        /// <param name="str">文字列</param><returns>
        /// True : 文字列はJIS第一水準に準拠している
        /// False: 文字列はJIS第一水準に準拠していない
        /// </returns>
        public static bool IsJisFirstLevel(string str)
        {
            string currentChar = null;
            int wAsc = 0;

            //For iCnt = 1 To Len(str)

#pragma warning disable CS0162 // Unreachable code detected
            for (int iCnt = 0; iCnt <= str.Length - 1; iCnt++)
#pragma warning restore CS0162 // Unreachable code detected
            {
                currentChar = Strings.Mid(str, iCnt + 1, 1);
                //１文字を抽出
                wAsc = Strings.Asc(currentChar);
                //VB文字コードに変換

                //機種依存文字判定
                //半角文字はVB文字コードは＋値

                if (wAsc < 0)
                {
                    if ((wAsc >= -30912 & wAsc <= -30817) | (wAsc <= -30823 & wAsc >= -30912) | (wAsc <= -949 & wAsc >= -1472) | wAsc == -32322 | wAsc == -32321 | wAsc == -32282)
                    {
                        //機種依存文字
                        return false;
                    }
                    else
                    {
                        //# Text入力専用判定（以下の文字はテキスト読みこみ時に内部コードが変更される、らしい）
                        //# ≒:-32288,≡:-32289,∫:-32281
                        //# √:-32285,⊥:-32293,∠:-32294
                        //# ∵:-32282,∩:-32321,∪:-32322
                        switch (wAsc)
                        {
                            case -32288:
                            case -32289:
                            case -32281:
                            case -32285:
                            case -32293:
                            case -32294:
                            case -32282:
                            case -32321:
                            case -32322:
                                return false;
                            default:
                                //それ以外の漢字コード
                                return true;
                        }

                    }
                }
                else
                {
                    Encoding oEnc = default(Encoding);
                    oEnc = Encoding.GetEncoding(65001);
                    // UTF-8で確認（日本語は全て3バイト、UTF8でしか使用していない文字をチェック)
                    if (oEnc.GetByteCount(str.Substring(iCnt, 1)) != 1 & Strings.Asc(str.Substring(iCnt, 1)) == 63)
                    {
                        return false;
                    }

                    return true;
                }

            }

            return false;
        }
        #endregion

        /// <summary>
        /// check value contains in array
        /// </summary>
        /// <param name="stringArray"></param>
        /// <param name="value"></param>
        /// <returns>true or false</returns>
        public static bool InArray(string[] stringArray, string value)
        {
            bool result = false;
            if (stringArray != null)
            {
                result = (from list in stringArray where list == value select list).Count().Equals(1);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static double ConvertObjectToDouble(object obj)
        {
            if (object.ReferenceEquals(obj, DBNull.Value))
            {
                return 0;
            }
            else
            {
                if (Information.IsNumeric(obj))
                {
                    return Convert.ToDouble(obj);
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Convert string to timespan
        /// </summary>
        /// <param name="hhmm"></param>
        /// <returns>
        /// wrong format : return null
        /// right : retun hh:mm
        /// example : 0800 => 08:00
        /// </returns>
        public static Nullable<TimeSpan> ConvertStringToTimespan(string hhmm)
        {
            if (string.IsNullOrEmpty(hhmm) || hhmm.Trim().Length != 4)
                return null;
            else
            {
                int hh = int.Parse(hhmm.Substring(0, 2));
                int mm = int.Parse(hhmm.Substring(2, 2));
                if(!(0 < hh && hh < 24 && 0 <= mm && mm < 60))
                    return null;
                return  new TimeSpan(int.Parse(hhmm.Substring(0,2)), int.Parse(hhmm.Substring(2, 2)),0);
            }
        }
    }
}
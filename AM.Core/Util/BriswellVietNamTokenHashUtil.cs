﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AM.Core.Util
{
    public class BriswellVietNamTokenHashUtil
    {
        /// <summary>
        /// Create AccessToken
        /// </summary>
        /// <param name="unixTime">unixTime</param>
        /// <param name="secretKey">Secret key</param>
        /// <returns></returns>
        public static string CreateAccessToken(string unixTime, string secretKey)
        {
            string firstHash = unixTime;
            for (int i = 0; i < 11; i++)
            {
                firstHash = GenerateSHA256String(firstHash);
            }

            string finalHash = firstHash + secretKey;
            for (int i = 0; i < 12; i++)
            {
                finalHash = GenerateSHA512String(finalHash);
            }
            return finalHash;
        }

        /// <summary>
        /// Hash an input string and return the hash
        /// </summary>
        /// <param name="inputString">input string</param>
        /// <returns></returns>
        public static string GenerateSHA256String(string inputString)
        {
            // Create a new instance of the SHA256 object.
            SHA256 sha256 = SHA256Managed.Create();
            // When overridden in a derived class, encodes all the characters in the specified string into a sequence of bytes.
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            // Computes the hash value for the specified byte array.
            byte[] hash = sha256.ComputeHash(bytes);
            // Get string from hash
            return GetStringFromHash(hash);
        }

        /// <summary>
        /// Hash an input string and return the hash
        /// </summary>
        /// <param name="inputString">intput string</param>
        /// <returns></returns>
        public static string GenerateSHA512String(string inputString)
        {
            // Create a new instance of the SHA512 object.
            SHA512 sha512 = SHA512Managed.Create();
            // When overridden in a derived class, encodes all the characters in the specified string into a sequence of bytes.
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            // Computes the hash value for the specified byte array.
            byte[] hash = sha512.ComputeHash(bytes);
            // Get string from hash
            return GetStringFromHash(hash);
        }

        /// <summary>
        /// Get string from hash
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("x2"));
            }
            return result.ToString();
        }

        /// <summary>
        /// Create AccessToken of post message to slack
        /// </summary>
        /// <param name="unixTime">unixTime</param>
        /// <param name="secretKey">Secret key</param>
        /// <param name="action">Function name in google api</param>
        /// <returns></returns>
        public static string CreateAccessTokenPostMessageSlack(string unixTime, string secretKey, string action)
        {
            string firstHash = unixTime;
            for (int i = 0; i < 11; i++)
            {
                firstHash = GenerateSHA256String(firstHash);
            }

            string finalHash = firstHash + secretKey + action;
            for (int i = 0; i < 12; i++)
            {
                finalHash = GenerateSHA512String(finalHash);
            }
            return finalHash;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AM.Core.Util
{
    /**************************************************************************/
    /// <summary>
    /// SqlAzureConnectionUtil
    /// </summary>
    /// <remarks>
    /// Copyright(C) 2014 System Consultant Co., Ltd. All Rights Reserved.
    /// </remarks>
    /**************************************************************************/
    public class SqlAzureConnectionUtil
    {
        /*--------------------------------------------------------------------*/
        /* リトライポリシー。ServiceConfiguration.cscfgから取得。             */
        /*--------------------------------------------------------------------*/
        private int sqlMaxRetries = 3;//Convert.ToInt32(RoleEnvironment.GetConfigurationSettingValue("sqlMaxRetries"));
        private int sqlRetrySleep = 3;//Convert.ToInt32(RoleEnvironment.GetConfigurationSettingValue("sqlRetrySleep"));
        private int sqlMaxSleep = 3;//Convert.ToInt32(RoleEnvironment.GetConfigurationSettingValue("sqlMaxSleep"));
        private int sqlMinSleep = 3;//Convert.ToInt32(RoleEnvironment.GetConfigurationSettingValue("sqlMinSleep"));
        private ITransientErrorDetectionStrategy transientErrorDetectionStrategy { get; set; }

        /*--------------------------------------------------------------------*/
        /* セッション取得用のIDリスト                                         */
        /*--------------------------------------------------------------------*/
        public static List<sessionModel> sessionInfoList { get; set; }

        public ITransientErrorDetectionStrategy TransientErrorDetectionStrategyService
        {
            get { return transientErrorDetectionStrategy; }
            set { transientErrorDetectionStrategy = value; }
        }

        /*====================================================================*/
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /*====================================================================*/
        public SqlAzureConnectionUtil()
        {
            sessionInfoList = new List<sessionModel>();
            if (TransientErrorDetectionStrategyService == null)
            {
                TransientErrorDetectionStrategyService = new SqlAzureTransientErrorDetectionStrategy();
            }
        }

        /*====================================================================*/
        /// <summary>
        /// OpenWithRetry
        /// </summary>
        /// <param name="conn"></param>
        /*====================================================================*/
        public void OpenWithRetry(SqlConnection conn)
        {
            for (int retryCount = 0; retryCount <= this.sqlMaxRetries; retryCount += 1)
            {
                try
                {
                    if (!(conn.State == ConnectionState.Open))
                        conn.Open();

                    Util.sessionModel model = new Util.sessionModel();
                    model.databaseNm = conn.Database;
                    model.activityId = CreateActivityId(ref conn);
                    sessionInfoList.Add(model);

                    /*--------------------------------------------------------*/
                    /* ActivityIDのログ出力                                   */
                    /*--------------------------------------------------------*/
                    LoggingUtil.WriteLog("\r\n" + string.Join("\r\n", "[DatabaseNm:" + model.databaseNm + "] [ActivityID:" + model.activityId + "]") + "\r\n" + string.Join("\r\n", "Start new DB Transaction"));
                    break;

                }
                catch (Exception ex)
                {
                    /*--------------------------------------------------------*/
                    /* Check exeption                                         */
                    /*--------------------------------------------------------*/
                    if (TransientErrorDetectionStrategyService.IsTransient(ex))
                    {
                        conn.Close();
                        SqlConnection.ClearPool(conn);
                        Util.LoggingUtil.WriteLog("CreateConnectionError:DB接続エラーが発生しました(" + (retryCount + 1).ToString() + "回目):" + Core.Util.ExceptionInfo.GetExceptionMessage(ex));
                        if (retryCount < sqlMaxRetries)
                        {
                            if (retryCount > 0)
                            {
                                int sleep = Convert.ToInt32(Math.Pow(retryCount + 1, 2.0)) * this.sqlRetrySleep;

                                if (sleep > this.sqlMaxSleep)
                                {
                                    sleep = this.sqlMaxSleep;
                                }
                                else if (sleep < this.sqlMinSleep)
                                {
                                    sleep = this.sqlMinSleep;
                                }
                                System.Threading.Thread.Sleep(sleep);
                            }
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(this.sqlMaxSleep);
                        }
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        /*====================================================================*/
        /// <summary>
        /// ActivityID取得
        /// </summary>
        /// <param name="conn">接続先コネクション</param>
        /// <remarks>
        /// 2012/11/30 作成
        /// セッショントレースIDをString型で取得
        /// ローカルDBでは取得することが出来ないため
        /// 空が返却される。
        /// </remarks>
        /*====================================================================*/
        public string CreateActivityId(ref SqlConnection conn)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT CONVERT(NVARCHAR(36), CONTEXT_INFO())";

            string rc = cmd.ExecuteScalar().ToString();
            if (string.IsNullOrEmpty(rc))
            {
                rc = "N/A";
            }
            return rc;
        }
    }

    /**************************************************************************/
    /// <summary>
    /// セッション情報格納モデル
    /// </summary>
    /**************************************************************************/
    public class sessionModel
    {
        public string databaseNm { get; set; }
        public string activityId { get; set; }
    }

    /**************************************************************************/
    /// <summary>
    /// 
    /// </summary>
    /**************************************************************************/
    public interface ITransientErrorDetectionStrategy
    {
        bool IsTransient(Exception ex);
    }
}
﻿using AM.Model.Tables;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface IAnnouncement
    {
        /// <summary>
        /// Get announcement information
        /// </summary>
        /// <param name="companyId">company_id</param>
        /// <returns></returns>
        List<Announcement> GetAnnouncement(int companyId);

        /// <summary>
        /// Delete an announcement by id
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        bool DeleteAnnouncement(Announcement model);

        /// <summary>
        /// Recover an announcement by id
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        bool RecoverAnnouncement(Announcement model);

        /// <summary>
        /// Insert/Update an announcement 
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        bool Merge(Announcement model);

        /// Get approve waiting info
        /// </summary>
        /// <param name="account_id"></param>
        /// <param name="flag_admin"></param>
        /// <returns></returns>
        List<ApproveWaitingInfo> GetApproveWaiting(int account_id, bool flag_admin, string company_id);

    } 
}

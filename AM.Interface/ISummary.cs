﻿using AM.Model;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface ISummary
    {
        /// <summary>
        /// Get Attendance Summary
        /// </summary>
        /// <param name="endDate">end date</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <param name="account_id">year</param>
        /// <returns></returns>
        List<AttendanceSummary> GetAttendanceSummary(int endDate, int month, int year, string account_id,string staff_id, int company_id);

        /// <summary>
        /// Get Attendance Summary Detail
        /// </summary>
        /// <param name="endDate">end date</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <param name="account_id">year</param>
        /// <returns></returns>
        List<AttendanceSummaryDetail> GetAttendanceSummaryDetail(int endDate, int month, int year, string account_id,string staff_id, int company_id);
    }
}

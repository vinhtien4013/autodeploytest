﻿using AM.Model;
using AM.Model.Tables;
using System;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface IWorkingtimeRecord
    {
        /// <summary>
        /// Get working time info by accountid, month
        /// </summary>
        /// <param name="account_id">account_id</param>
        /// <param name="month">month</param>
        /// <returns></returns>
        List<WorkingTimeResponse> GetWorkingTime(int account_id, DateTime month,Company company_info);

        /// <summary>
        /// Get working time info by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        WorkingtimeRecord GetdataById(int id);

        /// <summary>
        ///  Get working time info by working date,account_id
        /// </summary>
        /// <param name="datetime">register date</param>
        /// <param name="account_id">account id</param>
        /// <returns></returns>
        WorkingtimeRecord GetWorkTimeByAccount(DateTime datetime, int account_id);

        /// <summary>
        /// Insert a new working time record
        /// </summary>
        /// <param name="workingTimeRecord"></param>
        /// <returns></returns>
        int Insert(WorkingtimeRecord workingTimeRecord);

        /// <summary>
        /// Update a exist working time record
        /// </summary>
        /// <param name="workingTimeRecord"></param>
        /// <returns></returns>
        bool Update(WorkingtimeRecord workingTimeRecord);

        /// <summary>
        /// Get list of check attendance
        /// </summary>
        /// <param name="register_date">register_date</param>
        /// <param name="company_id">company_id</param>
        /// <returns></returns>
        List<CheckAttendanceResponse> GetCheckAttendance(string register_date, string company_id);

        /// <summary>
        /// Update working time record going
        /// </summary>
        /// <param name="dateNow">DateTime.Now</param>
        /// <param name="account_id">account_id</param>
        /// <returns></returns>
        int UpdateWorkingTimeGoing(DateTime dateNow, string actualTime, int account_id, Company company);

        /// <summary>
        /// Update working time record leaving
        /// </summary>
        /// <param name="dateNow">DateTime.Now</param>
        /// <param name="account_id">account_id</param>
        /// <param name="int overtime_record_id">int overtime_record_id</param>
        /// <returns></returns>
        int UpdateWorkingTimeLeaving(DateTime dateNow, TimeSpan actualTime, int account_id, string restTime, int overtime_record_id);

        /// <summary>
        /// Update working time after approved
        /// </summary>
        /// <param name="requsetManag"></param>
        /// <returns></returns>
        bool UpdateWorkingAfterApproved(AccountPaidHoliday requsetManag, string expectStartTime, string expectEndTime);

        /// <summary>
        /// Update working time record after deleted
        /// </summary>
        /// <param name="account_paid_holiday_id">account_paid_holiday_id</param>
        /// <param name="updated_date">updated_date</param>
        /// <param name="updated_account_id">updated_account_id</param>
        /// <returns></returns>
        bool UpdateWorkingAfterDeleted(int account_paid_holiday_id,DateTime updated_date, int updated_account_id);

        /// <summary>
        /// Check the working time record be approved or not 
        /// </summary>
        /// <param name="id">workingtime_record.id</param>
        /// <returns></returns>
        bool IsApprovedWorkTime(int id);

        /// <summary>
        /// Update working time record when have rest request via Slack
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="restTime"></param>
        /// <param name="updatedDate"></param>
        /// <param name="expectStartTime"></param>
        /// <param name="expectEndTime"></param>
        /// <returns></returns>
        bool UpdateAdditionalResttime(int accountId, double restTime, string updatedDate, string expectStartTime,string expectEndTime);

        /// <summary>
        /// Update working time record when have change/ late request via Slack
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expectStartTime"></param>
        /// <param name="updatedDate"></param>
        /// <returns></returns>
        bool UpdateChangeOrLateTime(int updatedAccountId, string expectEndTime,string expectStartTime, string updatedDate);

        /// <summary>
        /// Check the working time is selfcheck or not
        /// </summary>
        /// <param name="id">working time record id</param>
        /// <returns></returns>
        bool IsSelfCheck(int id);

        /// <summary>
        /// Delete working time record
        /// </summary>
        /// <param name="workingTimeRecord"></param>
        /// <returns></returns>
        bool Delete(WorkingtimeRecord workingTimeRecord);

        /// <summary>
        /// Get staff working status
        /// </summary>
        /// <param name="set_of_id">string id</param>
        /// <param name="range_from">range from</param>
        /// <param name="range_to">range to</param>
        /// <returns></returns>
        List<StaffWKStatusResponse> GetStaffWKStatus(string set_of_id, int range,Company company);
    }
}

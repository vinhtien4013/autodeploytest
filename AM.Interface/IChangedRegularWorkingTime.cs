﻿using AM.Model.Tables;
using System;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface  IChangedRegularWorkingTime
    {
        List<ChangedRegularWorkingTime> GetChangedRegularWorkingTime(int companyId);

        bool Insert(ChangedRegularWorkingTime model);

        bool Update(ChangedRegularWorkingTime model);

        bool Delete(ChangedRegularWorkingTime model);

        ChangedRegularWorkingTime GetRegularWorkTimeByAccount(int accountId, DateTime date);

        ChangedRegularWorkingTime GetExpectTime(int accountId, DateTime date, Company company);
    }
}

﻿using AM.Model;
using AM.Model.Tables;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface IFingerPrint
    {
        List<FingerPrintResponse> FingerPrintLst(string company_id);

        bool Insert(FingerPrint model);

        bool checkExistFingerId(int fingerPrintId);

        bool DeleteFingerPrint (int fingerPrintId);
    }
}

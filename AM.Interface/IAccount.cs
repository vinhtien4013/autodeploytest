﻿using AM.Model;
using AM.Model.Tables;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface IAccount
    {
        /// <summary>
        /// Get list account by company
        /// </summary>
        /// <param name="company_id"></param>
        /// <returns></returns>
        List<EditAccountModel> GetAccount(string company_id);

        /// <summary>
        ///  Get list member by email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        MemberInfoModel GetMemberInfoByGoogle(string email, string token);

        /// <summary>
        /// Get list member by login manual
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        MemberInfoManualModel GetMemberInfoByManual(string email, string password, string token);

        /// <summary>
        /// Get memberInfo by slackUserId
        /// </summary>
        /// <param name="slack_user_id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        MemberInfoModel GetMemberInfoBySlackUserId(string slack_user_id, string token);

        /// <summary>
        ///  Search account by condition
        /// </summary>
        /// <param name="searchInput"></param>
        /// <returns></returns>
        List<EditAccountModel> GetAccountByCondition(ParameterSearchAccount searchInput);

        /// <summary>
        ///  Insert a new account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        int Insert(Account account);

        /// <summary>
        /// Update account when edit finger print
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        bool Update(Account account);

        /// <summary>
        /// Delete account with finger print
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        bool Delete(Account account);

        /// <summary>
        /// Update account language
        /// </summary>
        /// <param name="id"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        bool UpdateAccountLanguage(int id, string language);

        /// <summary>
        /// Get account by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        MemberInfoModel GetDataById(int id);

        /// <summary>
        /// Get data by finger print id
        /// </summary>
        /// <param name="fingerprint_id">fingerprint_id</param>
        /// <returns></returns>
        MemberInfoModel GetDataByFingerPrintId(int fingerprint_id, string token);

        /// <summary>
        /// Check the staff is responsible staff or not
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        bool IsResponsibleStaff(string accountId, string companyId);

        /// <summary>
        /// Update password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        bool UpdateAccountPassword(string password, int id,int updated_account_id);

        /// <summary>
        /// Get account by person_id (face api)
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        MemberInfoModel GetDataByPersonId(string personId,string token);

        /// <summary>
        /// Get list account use face image
        /// </summary>
        /// <returns></returns>
        List<Account> ListAccountImages();
    }
}

﻿using AM.Model.Tables;

namespace AM.Interface
{
    public interface IWorkingovertimeRecord
    {
        /// <summary>
        /// Insert working overtime
        /// </summary>
        /// <param name="overtime_record_id">overtime record id</param>
        /// <returns></returns>
        int Insert(WorkingovertimeRecord model);

        /// <summary>
        /// Update working overtime
        /// </summary>
        /// <param name="overtime_record_id">overtime record id</param>
        /// <returns></returns>
        bool Update(WorkingovertimeRecord model);

        /// <summary>
        /// The overtime was in existence
        /// </summary>
        /// <param name="overtime_record_id">overtime record id</param>
        /// <returns></returns>
        bool IsExist(int overtime_record_id);
    }
}

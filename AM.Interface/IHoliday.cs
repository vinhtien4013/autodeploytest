﻿using AM.Model.Tables;
using System;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface IHoliday
    {
        List<Holiday> GetHolidayByYear(int year, int companyId);

        Holiday GetHolidayById(int id);

        bool Insert(Holiday model);

        bool Update(Holiday model);

        bool Delete(Holiday model);

        bool IsHoliday(DateTime date, int companyId);
    }
}

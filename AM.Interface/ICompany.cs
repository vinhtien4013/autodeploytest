﻿using AM.Model.Tables;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface ICompany
    {
        /// <summary>
        /// Get list company
        /// </summary>
        /// <returns></returns>
        List<Company> GetCompany ();

        /// <summary>
        /// Get company information by token
        /// </summary>
        /// <returns></returns>
        Company GetCompanyByCondition(string token, string id,string slack_token);

        /// <summary>
        /// Get company information by slack_token
        /// </summary>
        /// <returns></returns>
        Company GetCompanyBySlackInfo(string slack_token);

        /// <summary>
        /// delete a company
        /// </summary>
        /// <param name="model">Company model</param>
        /// <returns></returns>
        bool DeleteCompany(Company model);

        /// <summary>
        /// update a company
        /// </summary>
        /// <param name="model">Company model</param>
        /// <returns></returns>
        bool UpdateCompany(Company model);

        /// <summary>
        /// insert a company
        /// </summary>
        /// <param name="model">Company model</param>
        /// <returns></returns>
        int InsertCompany(Company model);

    }
}

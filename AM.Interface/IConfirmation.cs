﻿using AM.Model;
using AM.Model.Tables;
using System.Collections.Generic;

namespace AM.Interface
{
    /// <summary>
    /// Interface confirmation
    /// </summary>
    public interface IConfirmation
    {
        /// <summary>
        /// Update Confirmation table
        /// </summary>
        /// <param name="model">Confirmation table</param>
        /// <returns></returns>
        bool UpdateCheckStatus(Confirmation model);

        /// <summary>
        /// Update Paid Holiday Check Status
        /// </summary>
        /// <param name="model">Confirmation table</param>
        /// <returns></returns>
        bool UpdatePaidHolidayCheckStatus(Confirmation model, string expectStartTime, string expectEndTime);

        /// <summary>
        /// Insert a new record to table confirmation
        /// </summary>
        /// <param name="Confirmation">Confirmation table</param>
        /// <returns></returns>
        bool Insert(Confirmation Confirmation);

        /// <summary>
        /// Update a exist record in table confirmation
        /// </summary>
        /// <param name="account_id">Account id</param>
        /// <returns></returns>
        bool Update(int account_id);

        /// <summary>
        /// Check the request has already be approved or not
        /// </summary>
        /// <param name="id">application_id</param>
        /// <returns></returns>
        bool checkHasApproved(int id);

        /// <summary>
        /// Get list of notes when reject
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        List<ConfirmationResponse> GetNotesWhenReject(string companyId);

        /// <summary>
        /// Get max application sequence
        /// </summary>
        /// <param name="application_id">application_id</param>
        /// <returns></returns>
        int GetMaxApplicationSequence(int application_id);

        /// <summary>
        /// Get confirmation by application_id
        /// </summary>
        /// <param name="application_id"></param>
        /// <param name="application_form_id"></param>
        /// <returns></returns>
        List<Confirmation> GetConfirmationByApplicationId(int application_id, int application_form_id);

        /// <summary>
        /// Get list of notes when reject
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        List<ConfirmationResponse> GetCheckNotesWhenReject(string companyId);
    }
}

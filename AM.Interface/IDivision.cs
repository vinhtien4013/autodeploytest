using AM.Model.Tables;
using AM.Model;
using System.Collections.Generic;
using AM.Model.api.Brw;

namespace AM.Interface
{
    public interface IDivision
    {
        List<Division> GetDivisionListIdAndName(string company_id);

        /// <summary>
        /// Get list of division
        /// </summary>
        /// <param name="leftFather"></param>
        /// <returns></returns>
        List<DivisionResponse> GetDivison(int? leftFather,int company_id);

        /// <summary>
        /// Add new division
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int AddDivision(Division model);

        /// <summary>
        /// Update new division
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateDivision(Division model);

        /// <summary>
        /// Delete division
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool DeleteDivision(int group_id,int account_id,int company_id);


        /// <summary>
        /// Get all division in company
        /// </summary>
        /// <returns></returns>
        List<DivisionMenu> GetDivisionByCompanyId(int company_id);

        /// <summary>
        /// Check division before delete
        /// </summary>
        /// <returns></returns>
        int CheckDeleteDivision(int id);

        /// <summary>
        /// Get Group Data
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        List<group_data> GetGroupData(string companyId);

        /// <summary>
        /// Get list approve account by division id
        /// </summary>
        /// <param name="divisionId">division id</param>
        /// <returns></returns>
        List<Division> GetListCheckAccount(int divisionId);

        /// <summary>
        /// Check exist div_code
        /// </summary>
        /// <param name="div_code"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        bool CheckExistDivCode(string div_code,int company_id,string id);
    }
}

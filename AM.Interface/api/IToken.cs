﻿using AM.Model;

namespace AM.Interface
{
    public interface IToken
    {
        AttendanceToken GetToken(long unixDateTimes);
    }
}

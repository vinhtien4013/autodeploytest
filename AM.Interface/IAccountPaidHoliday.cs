﻿using AM.Model;
using AM.Model.Tables;
using System;
using System.Collections.Generic;

namespace AM.Interface
{
    public interface IAccountPaidHoliday
    {
        double GetRemainPaidHolidayForAcc(int account_id);

        List<PaidHolidayResponse> GetPaidHoliday(string account_id, string staff_id, string companyId);

        int Insert(AccountPaidHoliday accPaidHolidayInfo);

        bool MonthlyAddition(int updateAccount, string companyId);

        int CheckMonthlyAddition(string companyId);

        List<PaidHolidayRequestManageResponse> GetRequestManage(string id, string account_id, string date_from, string date_to, string approve_status);

        List<CheckPaidHolidayResponse> GetPaidHolidayCheckRequest(string id, string accountId, string applyFrom, string applyTo, string status, string companyId);

        bool Update(AccountPaidHoliday model);

        bool Delete(AccountPaidHoliday model);

        AccountPaidHoliday GetRequestManageById(int id);

        int CheckApplication(int account_id, DateTime application_date, string isAprrove_flg = null);

        List<AccountPaidHoliday> GetListPaidHolidayDetail(string account_id);

        bool UpdatePaidHolidayDetail(AccountPaidHoliday model);

        bool DeleteMonthly();

        AccountPaidHoliday GetRequestByAccount(int accountId, DateTime applicationDate);
    }
}

﻿using System.Threading.Tasks;
namespace AM.Interface
{
    public interface IEmailService
    {
        string SendEmail(string toEmail, string subject, string content);
    }
    
}

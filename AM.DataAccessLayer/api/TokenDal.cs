﻿using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.api;
using System;
using System.Configuration;

namespace AM.DataAccessLayer
{
    public class TokenDal : IToken
    {
        private AttendanceToken brwToken = new AttendanceToken();
        private readonly AttendanceSettings attendanceSettings;

        public TokenDal(AttendanceSettings attendanceSettings)
        {
            this.attendanceSettings = attendanceSettings;
        }

        /// <summary>
        /// Get new access token
        /// </summary>
        /// <returns></returns>
        public AttendanceToken GetToken(long unixDateTime)
        {
            if (!this.brwToken.IsValidAndNotExpiring)
            {
                this.brwToken = this.GetNewToken(unixDateTime);
            }
            return brwToken;
        }

        /// <summary>
        /// Get new access token for google sheets
        /// </summary>
        /// <returns></returns>
        private AttendanceToken GetNewToken(long unixDateTime)
        {
            var token = new AttendanceToken();
            var dateTime = DateTime.UtcNow;
            // Initializes a new instance of the System.DateTimeOffset structure using the specified System.DateTime value.
            var dateTimeOffset = new DateTimeOffset(dateTime);
            // The number of seconds that have elapsed since 1970-01-01T00:00:00Z.
            //var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();

            token.UnixTime = unixDateTime;
            token.AccessToken = BriswellVietNamTokenHashUtil.CreateAccessToken(unixDateTime.ToString(), ConfigurationManager.AppSettings["SecretKeyToken"]);
            return token;
        }

        /// <summary>
        /// Get new access token for google sheets
        /// </summary>
        /// <returns></returns>
        public AttendanceToken GetBriswelVietNamToken()
        {
            if (!this.brwToken.IsValidAndNotExpiring)
            {
                this.brwToken = this.GetNewBriswelVietNamToken();
            }
            return brwToken;
        }

        /// <summary>
        /// Get new access token for google sheets
        /// </summary>
        /// <returns></returns>
        private AttendanceToken GetNewBriswelVietNamToken()
        {
            var token = new AttendanceToken();
            var dateTime = DateTime.UtcNow;
            // Initializes a new instance of the System.DateTimeOffset structure using the specified System.DateTime value.
            var dateTimeOffset = new DateTimeOffset(dateTime);
            // The number of seconds that have elapsed since 1970-01-01T00:00:00Z.
            var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();

            token.UnixTime = unixDateTime;
            token.AccessToken = BriswellVietNamTokenHashUtil.CreateAccessToken(unixDateTime.ToString(), this.attendanceSettings.SecretKeyToken);
            return token;
        }

        /// <summary>
        /// Get new access token of post message to slack
        /// </summary>
        /// <returns></returns>
        private AttendanceToken GetNewTokenOfPostMessageSlack(string action)
        {
            var token = new AttendanceToken();
            var dateTime = DateTime.UtcNow;

            // Initializes a new instance of the System.DateTimeOffset structure using the specified System.DateTime value.
            var dateTimeOffset = new DateTimeOffset(dateTime);

            // The number of seconds that have elapsed since 1970-01-01T00:00:00Z.
            var unixDateTime = dateTimeOffset.ToUnixTimeSeconds().ToString();

            token.UnixTime = long.Parse(unixDateTime.ToString());
            token.AccessToken = BriswellVietNamTokenHashUtil.CreateAccessTokenPostMessageSlack(unixDateTime.ToString()
                , this.attendanceSettings.SecretKeyTokenPostMessageSlack, action);
            return token;
        }

        /// <summary>
        /// Get new access token use in post message to slack
        /// </summary>
        /// <param name="action">Function name in google api</param>
        /// <returns></returns>
        public AttendanceToken GetTokenPostMessageSlack(string action)
        {
            if (!this.brwToken.IsValidAndNotExpiring)
            {
                this.brwToken = this.GetNewTokenOfPostMessageSlack(action);
            }
            return brwToken;
        }
    }
}

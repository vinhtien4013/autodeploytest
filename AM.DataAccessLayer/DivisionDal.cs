﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model.Tables;
using AM.Model;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using AM.Model.api.Brw;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace AM.DataAccessLayer
{
    public class DivisionDal : IDivision
    {
        /// <summary>
        /// Get division with company id
        /// </summary>
        /// <param name="company_id">company id</param>
        /// <returns></returns>
        public List<Division> GetDivisionListIdAndName(string company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", company_id);
            return mapper.QueryForList<Hashtable, Division>("Division", "GetDivisionListIdAndName", ht);
        }

        /// <summary>
        /// Get division by company id and div range from
        /// </summary>
        /// <param name="leftFather">div range from</param>
        /// <param name="company_id">company id</param>
        /// <returns></returns>
        public List<DivisionResponse> GetDivison(int? leftFather, int company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("div_range_from", leftFather);
            ht.Add("company_id", company_id);
            return mapper.QueryForList<Hashtable, DivisionResponse>("Division", "GetDivision", ht);
        }

        /// <summary>
        /// Insert new division
        /// </summary>
        /// <param name="model">Division table model</param>
        /// <returns></returns>
        public int AddDivision(Division model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Division>("Division", "Insert", model));
        }

        /// <summary>
        /// Update division
        /// </summary>
        /// <param name="model">Division table model</param>
        /// <returns></returns>
        public bool UpdateDivision(Division model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            int iRc = mapper.Update<Division>("Division", "Update", model);
            return iRc > 0;
        }

        /// <summary>
        /// Delete division
        /// </summary>
        /// <param name="id">division id</param>
        /// <param name="account_id">deleted account id</param>
        /// <param name="company_id">company id</param>
        /// <returns></returns>
        public bool DeleteDivision(int id,int account_id,int company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            ht.Add("deleted_account_id", account_id);
            ht.Add("deleted_date", DateTime.Now);
            ht.Add("company_id", company_id);

            int iRc = mapper.Update<Hashtable>("Division", "Delete", ht);
            return iRc > 0;
        }

        /// <summary>
        /// Get all division by company id
        /// </summary>
        /// <param name="company_id">company id</param>
        /// <returns></returns>
        public List<DivisionMenu> GetDivisionByCompanyId(int company_id)
        {
           
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", company_id);
            return mapper.QueryForList<Hashtable, DivisionMenu>("Division", "GetDivisionByCompanyId", ht);
        }

        /// <summary>
        /// Check before delete division 
        /// </summary>
        /// <param name="id">division id</param>
        /// <returns>
        /// 1: Division had staff
        /// 2: Division had child division
        /// 3: out of above
        /// </returns>
        public int CheckDeleteDivision(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            return StringUtil.ConvertObjectToInteger32 (mapper.QueryForScalar<Hashtable>("Division", "CheckDeleteDivision", ht).ToString());
        }

        /// <summary>
        /// Get group data
        /// </summary>
        /// <param name="companyId">company_id</param>
        /// <returns></returns>
        public List<group_data> GetGroupData(string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            Hashtable ht = new Hashtable();
            ht.Add("company_id", companyId);

            return mapper.QueryForList<Hashtable, group_data>("Division", "GetGroupData", ht);
        }

        /// <summary>
        /// Get list approve account by division id
        /// </summary>
        /// <param name="divisionId">division id</param>
        /// <returns></returns>
        public List<Division> GetListCheckAccount(int divisionId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            IRowMapper<Division> rowMapper = MapBuilder<Division>.MapNoProperties()
               .MapByName(p => p.id)
               .MapByName(p => p.responsible_staff_id)
               .MapByName(p => p.admin_div_flg)
              .Build();
            Hashtable ht = new Hashtable();
            ht.Add("division_id", divisionId);

            return mapper.QueryForList<Hashtable, Division>("Division", "GetListCheckAccount", ht, rowMapper);
        }

        public bool CheckExistDivCode(string div_code, int company_id,string id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            Hashtable ht = new Hashtable();
            ht.Add("company_id", company_id);
            ht.Add("div_code", div_code);
            ht.Add("id", id);
            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("Division", "CheckExistDivCode", ht)) > 0 ;

        }
    }
}

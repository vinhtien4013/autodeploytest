﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;
using System.Collections.Generic;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class ConfirmationDal : IConfirmation
    {
        /// <summary>
        /// Update Confirmation table
        /// </summary>
        /// <param name="model">Confirmation table</param>
        /// <returns></returns>
        public bool UpdateCheckStatus(Confirmation model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            int iRc = mapper.Update<Confirmation>("Confirmation", "UpdateCheckStatus", model);
            return iRc > 0;
        }

        /// <summary>
        /// Update Paid Holiday Check Status
        /// </summary>
        /// <param name="model">Confirmation table</param>
        /// <returns></returns>
        public bool UpdatePaidHolidayCheckStatus(Confirmation model, string expectStartTime, string expectEndTime)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            ht.Add("application_id",model.application_id);
            ht.Add("account_id", model.account_id);
            ht.Add("application_form_id", model.application_form_id);
            ht.Add("check_sequence", model.check_sequence);
            ht.Add("confirmed_datetime", model.confirmed_datetime.ToString());
            ht.Add("check_status", model.check_status);
            ht.Add("updated_account_id", model.updated_account_id);
            ht.Add("updated_date", model.updated_date);
            ht.Add("reason", model.reason);
            //ht.Add("default_start_time", CONST_VALUE.DEFAULT_START_TIME);
            //ht.Add("default_end_time", CONST_VALUE.DEFAULT_END_TIME);
            ht.Add("default_start_time", expectStartTime);
            ht.Add("default_end_time", expectEndTime);
            ht.Add("off_half_morning", (int)PaidHolidayTypeEnum.HalfDayMorning);
            ht.Add("off_half_afternoon", (int)PaidHolidayTypeEnum.HalfDayAfternoon);
            ht.Add("default_max_end_workingtime", CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME.ToString());

            int iRc = mapper.Update<Hashtable>("Confirmation", "UpdatePaidHolidayCheckStatus", ht);
            return iRc > 0;
        }

        /// <summary>
        /// Insert a new record to table confirmation
        /// </summary>
        /// <param name="Confirmation">Confirmation table</param>
        /// <returns></returns>
        public bool Insert(Confirmation Confirmation)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            int iRc = mapper.Insert<Confirmation>("Confirmation", "Merge", Confirmation);
            return iRc > 0;
        }

        /// <summary>
        /// Update a exist record in table confirmation
        /// </summary>
        /// <param name="account_id">Account id</param>
        /// <returns></returns>
        public bool Update(int account_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", account_id);

            return mapper.Update<Hashtable>("Confirmation", "Update", ht) > 0;
        }

        /// <summary>
        /// Check the request has already be approved or not
        /// </summary>
        /// <param name="id">application_id</param>
        /// <returns></returns>
        public bool checkHasApproved(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);

            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("Confirmation", "GetApprovedInfo", ht).ToString()) > 0;
        }

        /// <summary>
        /// Get list of notes when reject
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        public List<ConfirmationResponse> GetNotesWhenReject(string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", companyId);
            return mapper.QueryForList<Hashtable,ConfirmationResponse>("Confirmation", "GetNotesWhenReject",ht);
        }

        /// <summary>
        /// Get max application sequence
        /// </summary>
        /// <param name="application_id">application_id</param>
        /// <returns></returns>
        public int GetMaxApplicationSequence(int application_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("application_id", application_id);

            var application_sequence = mapper.QueryForScalar<Hashtable>("Confirmation", "GetMaxApplicationSequence", ht);
            if (application_sequence != null)
            {
                return StringUtil.ConvertObjectToInteger32(application_sequence.ToString());
            }
            else
            {
                // If insert the first time then set default max application_sequence = 1
                return 1;
            }
        }

        /// <summary>
        /// Get list conformation by application_id
        /// </summary>
        /// <param name="application_id"></param>
        /// <param name="application_form_id"></param>
        /// <returns></returns>
        public List<Confirmation> GetConfirmationByApplicationId(int application_id, int application_form_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("application_id", application_id);
            ht.Add("application_form_id", application_form_id);
            return mapper.QueryForList<Hashtable, Confirmation>("Confirmation", "GetConfirmationByApplicationId", ht);
        }

        /// <summary>
        /// Get list check of notes when reject
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        public List<ConfirmationResponse> GetCheckNotesWhenReject(string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", companyId);

            return mapper.QueryForList<Hashtable,ConfirmationResponse>("Confirmation", "GetCheckNotesWhenReject",ht);
        }
    }
}

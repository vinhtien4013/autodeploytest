﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;
using System.Collections.Generic;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class FingerPrintDal : IFingerPrint
    {
        /// <summary>
        /// Get List Finger Print
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public List<FingerPrintResponse> FingerPrintLst(string company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();            
            ht.Add("company_id", company_id);            
            return mapper.QueryForList<Hashtable, FingerPrintResponse>("FingerPrint", "GetFingerPrintList", ht);
        }

        /// <summary>
        ///  Regist a new finger print id
        /// </summary>
        /// <param name="model">FingerPrintModel</param>
        /// <returns></returns>
        public bool Insert(FingerPrint model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return mapper.Insert<FingerPrint>("FingerPrint","Insert", model) > 0;
        }

        /// <summary>
        /// check Finger Print id exist or not
        /// </summary>
        /// <param name="fingerPrintId">fingerPrintId</param>
        /// <returns></returns>
        public bool checkExistFingerId(int fingerPrintId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            ht.Add("id", fingerPrintId);

            return StringUtil.ConvertObjectToInteger(mapper.QueryForScalar<Hashtable >("FingerPrint", "checkExistFingerId", ht)) > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fingerPrintId"></param>
        /// <returns></returns>
        public bool DeleteFingerPrint(int fingerPrintId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            ht.Add("id", fingerPrintId);
            ht.Add("deleted_date", AzureUtil.VietnamDateTimeNow());
            ht.Add("deleted_account_id", CONST_VALUE.SERVER_ACCOUNT);

            return mapper.Update<Hashtable>("FingerPrint", "DeleteFingerPrint", ht) > 0;
        }
    }
}

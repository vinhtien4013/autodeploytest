﻿using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;
using System.Collections.Generic;

namespace AM.DataAccessLayer
{
    public class AnnouncementDal : IAnnouncement
    {
        /// <summary>
        /// Get announcement information
        /// </summary>
        /// <param name="companyId">company_id</param>
        /// <returns></returns>
        public List<Announcement> GetAnnouncement(int companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", companyId);
            return mapper.QueryForList<Hashtable,Announcement>("Announcement", "GetAnnouncement", ht);
        }

        /// <summary>
        /// Delete an announcement by id
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        public bool DeleteAnnouncement(Announcement model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            
            return mapper.Delete<Announcement>("Announcement", "Delete", model) >= 0;

        }

        /// <summary>
        /// Recover an announcement by id
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        public bool RecoverAnnouncement(Announcement model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            return mapper.Delete<Announcement>("Announcement", "Recover", model) >= 0;

        }

        /// <summary>
        /// Insert/Update an announcement 
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        public bool Merge(Announcement model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            return mapper.Insert<Announcement>("Announcement", "Merge", model) > 0;
        }

        /// <summary>
        /// Get list approve waiting by account id
        /// </summary>
        /// <param name="account_id">account_id</param>
        /// <param name="flag_admin">admin flag</param>
        /// <returns></returns>
        public List<ApproveWaitingInfo> GetApproveWaiting(int account_id, bool flag_admin, string company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", account_id);
            if (flag_admin)
                ht.Add("admin", flag_admin);
            ht.Add("company_id", company_id);
            return mapper.QueryForList<Hashtable, ApproveWaitingInfo>("Announcement", "GetApproveWaiting", ht);
        }
    }
}

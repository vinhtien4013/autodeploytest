﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;

namespace AM.DataAccessLayer
{
    public class WorkingovertimeRecordDal : IWorkingovertimeRecord
    {
        /// <summary>
        /// Insert working overtime
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(WorkingovertimeRecord model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<WorkingovertimeRecord>("WorkingovertimeRecord", "Insert", model));
        }

        /// <summary>
        /// Update working overtime
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(WorkingovertimeRecord model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", model.id);
            ht.Add("total_over_timework", model.total_over_timework);
            ht.Add("updated_date", model.updated_date);
            ht.Add("updated_account_id", model.updated_account_id);
            return mapper.Update<Hashtable>("WorkingovertimeRecord", "Update", ht) > 0;
        }

        /// <summary>
        /// The overtime was in existence
        /// </summary>
        /// <param name="overtime_record_id"></param>
        /// <returns></returns>
        public bool IsExist(int overtime_record_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", overtime_record_id.ToString());
            var result = StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("WorkingovertimeRecord", "IsExist", ht));
            return result > 0;
        }
    }
}

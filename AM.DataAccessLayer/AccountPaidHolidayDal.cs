﻿using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using AM.Core.Util;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class AccountPaidHolidayDal : IAccountPaidHoliday
    {
        public double GetRemainPaidHolidayForAcc(int account_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", account_id);

            return StringUtil.ConvertObjectToDouble(mapper.QueryForScalar<Hashtable>("AccountPaidHoliday", "GetRemainPaidHolidayForAcc", ht));
        }

        /// <summary>
        /// Get List Account Paid Holiday
        /// </summary>
        /// <param name="account_id">Account login id</param>
        /// <param name="staff_id">staff id</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        public List<PaidHolidayResponse> GetPaidHoliday(string account_id, string staff_id, string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", account_id);
            ht.Add("staff_id", staff_id);
            ht.Add("company_id", companyId);

            return mapper.QueryForList<Hashtable, PaidHolidayResponse>("AccountPaidHoliday", "GetPaidHoliday", ht);
        }

        /// <summary>
        /// Check already add monthly or not
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        public int CheckMonthlyAddition(string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", companyId);
            return Convert.ToInt32(mapper.QueryForScalar<Hashtable>("AccountPaidHoliday", "GetListMonthlyAddition",ht).ToString());
        }


        /// <summary>
        /// Insert data to account_paid_holiday
        /// </summary>
        /// <param name="AccPaidHoliday"></param>
        /// <returns></returns>
        public int Insert(AccountPaidHoliday AccPaidHoliday)
        {

            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<AccountPaidHoliday>("AccountPaidHoliday", "Insert", AccPaidHoliday));
        }

        /// <summary>
        /// Insert data to account_paid_holiday
        /// </summary>
        /// <param name="AccPaidHoliday"></param>
        /// <returns></returns>
        public bool MonthlyAddition(int updateAccount, string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("updated_account_id", updateAccount);
            ht.Add("processing_status", (byte)ProcessingStatusEnum.AddMonthPaidHoliday);
            ht.Add("company_id", companyId);

            int iRc  = mapper.Insert<Hashtable>("AccountPaidHoliday", "MonthlyAddition", ht);

            return iRc > 0;
        }

        /// <summary>
        /// Get list Request Manage
        /// </summary>
        /// <param name="account_id">id of user login</param>
        /// <param name="date_from">data from in view</param>
        /// <param name="date_to">date to in view</param>
        /// <returns></returns>
        public List<PaidHolidayRequestManageResponse> GetRequestManage(string id, string account_id, string date_from, string date_to, string approve_status)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            
            if (string.IsNullOrEmpty(id))
                ht.Add("id", DBNull.Value);
            else
                ht.Add("id", id);

            if (string.IsNullOrEmpty(account_id))
                ht.Add("account_id", DBNull.Value);
            else
                ht.Add("account_id", account_id);

            if (string.IsNullOrEmpty(date_from))
                ht.Add("date_from", DBNull.Value);
            else
                ht.Add("date_from", date_from);

            if (string.IsNullOrEmpty(date_to))
                ht.Add("date_to", DBNull.Value);
            else
                ht.Add("date_to", date_to);

            if (string.IsNullOrEmpty(approve_status))
                ht.Add("approve_status", DBNull.Value);
            else
                ht.Add("approve_status", approve_status);
            return mapper.QueryForList<Hashtable, PaidHolidayRequestManageResponse>("AccountPaidHoliday", "GetRequestManage", ht);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accountId"></param>
        /// <param name="applyFrom"></param>
        /// <param name="applyTo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<CheckPaidHolidayResponse> GetPaidHolidayCheckRequest(string id, string accountId, string applyFrom, string applyTo, string status, string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            
            ht.Add("id", id);
            ht.Add("account_id", accountId);
            ht.Add("date_from", applyFrom);
            ht.Add("date_to", applyTo);
            ht.Add("approve_status", status);
            ht.Add("company_id", companyId);

            return mapper.QueryForList<Hashtable, CheckPaidHolidayResponse>("AccountPaidHoliday", "GetPaidHolidayCheckRequest", ht);
        }

        /// <summary>
        /// Update an exit record
        /// </summary>
        /// <param name="requsetManage"></param>
        /// <returns></returns>
        public bool Update(AccountPaidHoliday requsetManage)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<AccountPaidHoliday>("AccountPaidHoliday", "Update", requsetManage);
            return iRc > 0;
        }

        /// <summary>
        /// Delete an exit record
        /// </summary>
        /// <param name="requestManage"></param>
        /// <returns></returns>
        public bool Delete(AccountPaidHoliday requestManage)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<AccountPaidHoliday>("AccountPaidHoliday", "Delete", requestManage);
            return iRc > 0;
        }

        /// <summary>
        /// Get request manage by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>AccountPaidHoliday</returns>
        public AccountPaidHoliday GetRequestManageById(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            return mapper.QueryForObject<Hashtable, AccountPaidHoliday>("AccountPaidHoliday", "GetRequestManageById", ht);
        }

        /// <summary>
        /// Count application num with account_id, application_date
        /// </summary>
        /// <param name="account_id"></param>
        /// <param name="application_date"></param>
        /// <returns></returns>
        public int CheckApplication(int account_id, DateTime application_date,string isAprrove_flg = null)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", account_id);
            ht.Add("application_date", application_date.ToString("yyyy/MM/dd"));
            ht.Add("isAprrove_flg", isAprrove_flg);

            return Convert.ToInt32(mapper.QueryForScalar<Hashtable>("AccountPaidHoliday", "GetListApplication", ht).ToString());
        }

        /// <summary>
        /// Get list Paid Holiday Detail
        /// </summary>
        /// <param name="account_id">id of user login</param>
        /// <returns></returns>
        public List<AccountPaidHoliday> GetListPaidHolidayDetail(string account_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            
            if (string.IsNullOrEmpty(account_id))
                ht.Add("account_id", DBNull.Value);
            else
                ht.Add("account_id", account_id);
            
            return mapper.QueryForList<Hashtable, AccountPaidHoliday>("AccountPaidHoliday", "GetListPaidHolidayDetail", ht);
        }

        /// <summary>
        /// Update an exit record
        /// </summary>
        /// <param name="requsetManage"></param>
        /// <returns></returns>
        public bool UpdatePaidHolidayDetail(AccountPaidHoliday requsetManage)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<AccountPaidHoliday>("AccountPaidHoliday", "UpdatePaidHolidayDetail", requsetManage);
            return iRc > 0;
        }

        /// <summary>
        /// Delete all record on this month
        /// </summary>
        /// <param name="requestManage"></param>
        /// <returns></returns>
        public bool DeleteMonthly()
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Delete<AccountPaidHoliday>("AccountPaidHoliday", "DeleteMonthly", null);
            return iRc >= 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="applicationDate"></param>
        /// <returns></returns>
        public AccountPaidHoliday GetRequestByAccount(int accountId, DateTime applicationDate)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            ht.Add("account_id", accountId);
            ht.Add("application_date", applicationDate.ToString("yyyy/MM/dd"));

            return mapper.QueryForObject<Hashtable, AccountPaidHoliday>("AccountPaidHoliday", "GetRequestByAccount", ht);
        }

    }
}

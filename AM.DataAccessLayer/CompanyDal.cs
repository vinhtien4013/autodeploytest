﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;
using System.Collections.Generic;

namespace AM.DataAccessLayer
{
    public class CompanyDal : ICompany
    {
        /// <summary>
        /// Get list company
        /// </summary>
        /// <returns></returns>
        public List<Company> GetCompany()
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return mapper.QueryForList<Company>("Company", "GetCompany");
        }

        /// <summary>
        /// Get company information by token
        /// </summary>
        /// <returns></returns>
        public Company GetCompanyByCondition(string token, string id,string slack_token)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("token", token);
            ht.Add("id", id);
            ht.Add("slack_token", slack_token);
            return mapper.QueryForObject<Hashtable, Company>("Company", "GetCompany",ht);
        } 

        /// <summary>
        /// Get company information by slack_token
        /// </summary>
        /// <returns></returns>
        public Company GetCompanyBySlackInfo(string slack_token)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("slack_token", slack_token);
            return mapper.QueryForObject<Hashtable, Company>("Company", "GetCompany", ht);
        }

        /// <summary>
        /// Delete a company
        /// </summary>
        /// <param name="model">Company model</param>
        /// <returns></returns>
        public bool DeleteCompany(Company model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return mapper.Delete<Company>("Company", "DeleteCompany", model) >= 0;
        }
      
        /// <summary>
        /// Update a company
        /// </summary>
        /// <param name="model">Company model</param>
        /// <returns></returns>
        public bool UpdateCompany(Company model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", model.id);
            ht.Add("company_name", model.company_name);
            ht.Add("company_token", model.company_token);
            ht.Add("admin_company_flg", model.admin_company_flg);
            ht.Add("valid_date", model.valid_date);
            ht.Add("description", model.description);
            ht.Add("updated_date", model.updated_date);
            ht.Add("updated_account_id", model.updated_account_id);
            ht.Add("slack_flg", model.slack_flg);
            ht.Add("start_working_time", model.start_working_time.ToString());
            ht.Add("end_working_time", model.end_working_time.ToString());
            ht.Add("start_lunch_time", model.start_lunch_time.ToString());
            ht.Add("end_lunch_time", model.end_lunch_time.ToString());
            ht.Add("uncounted_time", model.uncounted_time);
            ht.Add("slack_token", model.slack_token);
            ht.Add("slack_team_id", model.slack_team_id);
            ht.Add("slack_team_domain", model.slack_team_domain);
            ht.Add("attendance_channel", model.attendance_channel);
            ht.Add("announcement_channel", model.announcement_channel);
            ht.Add("attendance_command", model.attendance_command);
            ht.Add("company_logo", model.company_logo);
            ht.Add("time_zone_id", model.time_zone_id);
            ht.Add("paid_holiday_monthly_flg", model.paid_holiday_monthly_flg);
            ht.Add("slack_webhook_url", model.slack_webhook_url);
            ht.Add("post_message_username", model.post_message_username);
            ht.Add("login_by", model.login_by);
            ht.Add("face_flg", model.face_flg);
            ht.Add("face_folder", model.face_folder);
            ht.Add("min_workingtime", model.min_workingtime);
            return mapper.Update<Hashtable>("Company", "UpdateCompany", ht) >= 0;
        }
   
        /// <summary>
        /// Insert a new company
        /// </summary>
        /// <param name="model">Company model</param>
        /// <returns></returns>
        public int InsertCompany(Company model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_name", model.company_name);
            ht.Add("company_token", model.company_token);
            ht.Add("admin_company_flg", model.admin_company_flg);
            ht.Add("valid_date", model.valid_date);
            ht.Add("description", model.description);
            ht.Add("created_date", model.updated_date);
            ht.Add("created_account_id", model.updated_account_id);
            ht.Add("updated_date", model.updated_date);
            ht.Add("updated_account_id", model.updated_account_id);
            ht.Add("slack_flg", model.slack_flg);
            ht.Add("start_working_time", model.start_working_time.ToString());
            ht.Add("end_working_time", model.end_working_time.ToString());
            ht.Add("start_lunch_time", model.start_lunch_time.ToString());
            ht.Add("end_lunch_time", model.end_lunch_time.ToString());
            ht.Add("uncounted_time", model.uncounted_time);
            ht.Add("slack_token", model.slack_token);
            ht.Add("slack_team_id", model.slack_team_id);
            ht.Add("slack_team_domain", model.slack_team_domain);
            ht.Add("attendance_channel", model.attendance_channel);
            ht.Add("announcement_channel", model.announcement_channel);
            ht.Add("attendance_command", model.attendance_command);
            ht.Add("company_logo", model.company_logo);
            ht.Add("time_zone_id", model.time_zone_id);
            ht.Add("paid_holiday_monthly_flg", model.paid_holiday_monthly_flg);
            ht.Add("slack_webhook_url", model.slack_webhook_url);
            ht.Add("post_message_username", model.post_message_username);
            ht.Add("login_by", model.login_by);
            ht.Add("face_flg", model.face_flg);
            ht.Add("face_folder", model.face_folder);
            ht.Add("min_workingtime", model.min_workingtime);
            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("Company", "InsertCompany", ht));
        }

    }
}

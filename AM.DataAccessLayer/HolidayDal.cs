﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AM.DataAccessLayer
{
    public class HolidayDal : IHoliday
    {
        /// <summary>
        /// Get List Holiday By Year
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="companyId">company_id</param>
        /// <returns></returns>
        public List<Holiday> GetHolidayByYear(int year, int companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("year", year);
            ht.Add("company_id", companyId);

            return mapper.QueryForList<Hashtable, Holiday>("Holiday", "GetHolidayByYear", ht);
        }

        /// <summary>
        /// Get holiday by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>Holiday</returns>
        public Holiday GetHolidayById(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            return mapper.QueryForObject<Hashtable, Holiday>("Holiday", "GetHolidayById", ht);
        }

        /// <summary>
        /// Insert a new record to Holiday
        /// </summary>
        /// <param name="holiday">Holiday</param>
        /// <returns></returns>
        public bool Insert(Holiday holiday)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Insert<Holiday>("Holiday", "Insert", holiday);
            return iRc > 0;
        }

        /// <summary>
        /// Update an exit record
        /// </summary>
        /// <param name="holiday"></param>
        /// <returns></returns>
        public bool Update(Holiday holiday)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<Holiday>("Holiday", "Update", holiday);
            return iRc > 0;
        }

        /// <summary>
        /// Delete an exit record
        /// </summary>
        /// <param name="holiday"></param>
        /// <returns></returns>
        public bool Delete(Holiday holiday)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<Holiday>("Holiday", "Delete", holiday);
            return iRc > 0;
        }

        /// <summary>
        /// Check
        /// </summary>
        /// <param name="date"></param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        public bool IsHoliday(DateTime date, int companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("date", date);
            ht.Add("company_id", companyId);

            return StringUtil.ConvertObjectToBool(mapper.QueryForScalar<Hashtable>("Holiday", "IsHoliday", ht));
        }
    }
}   

﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class WorkingtimeRecordDal : IWorkingtimeRecord
    {
        /// <summary>
        /// Get list Working time by Year
        /// </summary>
        /// <param name="account_id">account_id</param>
        /// <param name="Month_Year">date</param>
        /// <returns></returns>
        public List<WorkingTimeResponse> GetWorkingTime(int account_id, DateTime Month_Year,Company company_info)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            TimeSpan tmpTime = new TimeSpan(0, 0, 0);
            Hashtable ht = new Hashtable();
            ht.Add("account_id", account_id);
            ht.Add("year", Month_Year.Year);
            ht.Add("month", Month_Year.Month);
            //ht.Add("expect_start_time", CONST_VALUE.DEFAULT_START_TIME);
            //ht.Add("expect_end_time", CONST_VALUE.DEFAULT_END_TIME);
            ht.Add("expect_start_time", company_info.start_working_time.ToString());
            ht.Add("expect_end_time", company_info.end_working_time.ToString());
            //ht.Add("full_rest_time", CONST_VALUE.DEFAULT_REST_TIME);
            var timeSpan = Math.Round((company_info.end_lunch_time - company_info.start_lunch_time).TotalHours,2);
            ht.Add("full_rest_time", timeSpan.ToString());
            ht.Add("half_rest_time", CONST_VALUE.HALF_REST_TIME);
            ht.Add("none_work_time", StringUtil.ConvertObjectToString(tmpTime));
            ht.Add("off_half_morning", (int)PaidHolidayTypeEnum.HalfDayMorning);
            ht.Add("off_half_afternoon", (int)PaidHolidayTypeEnum.HalfDayAfternoon);
            return mapper.QueryForList<Hashtable, WorkingTimeResponse>("WorkingtimeRecord", "GetWorkingTime", ht);
        }

        /// <summary>
        /// Get Data by Id to check exist
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public WorkingtimeRecord GetdataById(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            IRowMapper<WorkingtimeRecord> rowMapper = MapBuilder<WorkingtimeRecord>.MapNoProperties()
                .MapByName(p => p.id)
                .MapByName(p => p.account_id)
                .MapByName(p => p.register_date)
                .MapByName(p => p.account_paid_holiday_id)
                .MapByName(p => p.expect_start_time)
                .MapByName(p => p.expect_end_time)
                .MapByName(p => p.device_start_time)
                .MapByName(p => p.device_end_time)
                .MapByName(p => p.actual_start_time)
                .MapByName(p => p.actual_end_time)
                .MapByName(p => p.rest_time)
                .MapByName(p => p.additional_rest_time)
                .MapByName(p => p.overtime_record_id)
                .MapByName(p => p.created_date)
                .MapByName(p => p.created_account_id)
                .MapByName(p => p.updated_date)
                .MapByName(p => p.updated_account_id)
                .MapByName(p => p.deleted_date)
                .MapByName(p => p.deleted_account_id)
               .Build();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            return mapper.QueryForObject<Hashtable,WorkingtimeRecord>("WorkingtimeRecord", "GetWorkTimeByAccount", ht, rowMapper);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="account_id"></param>
        /// <returns></returns>
        public WorkingtimeRecord GetWorkTimeByAccount(DateTime datetime, int account_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            IRowMapper<WorkingtimeRecord> rowMapper = MapBuilder<WorkingtimeRecord>.MapNoProperties()
                .MapByName(p => p.id)
                .MapByName(p => p.device_start_time)
                .MapByName(p => p.actual_start_time)
                .MapByName(p => p.device_end_time)
                .MapByName(p => p.actual_end_time)
                .MapByName(p => p.expect_end_time)
                .MapByName(p => p.expect_start_time)
                .MapByName(p => p.rest_time)
                .MapByName(p => p.additional_rest_time)
                .Build();

            Hashtable ht = new Hashtable();
            ht.Add("datetime", datetime.ToString("yyyy/MM/dd"));
            ht.Add("account_id", account_id);

            return mapper.QueryForObject<Hashtable, WorkingtimeRecord>("WorkingtimeRecord", "GetWorkTimeByAccount", ht, rowMapper);
        }

        /// <summary>
        /// Insert a new record for WorkingTimeRecord
        /// </summary>
        /// <param name="workingTimeRecord"></param>
        /// <returns></returns>
        public int Insert(WorkingtimeRecord workingTimeRecord)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            
            ht.Add("register_date", workingTimeRecord.register_date);
            ht.Add("account_id", workingTimeRecord.account_id);
            ht.Add("device_start_time", workingTimeRecord.device_start_time.ToString());
            ht.Add("device_end_time", workingTimeRecord.device_end_time.ToString());
            ht.Add("actual_start_time", workingTimeRecord.actual_start_time.ToString());
            ht.Add("actual_end_time", workingTimeRecord.actual_end_time.ToString());
            ht.Add("rest_time", workingTimeRecord.rest_time);
            ht.Add("additional_rest_time", workingTimeRecord.additional_rest_time);
            ht.Add("overtime_record_id", workingTimeRecord.overtime_record_id);
            ht.Add("created_date", workingTimeRecord.created_date);
            ht.Add("created_account_id", workingTimeRecord.created_account_id);
            ht.Add("updated_date", workingTimeRecord.updated_date);
            ht.Add("updated_account_id", workingTimeRecord.updated_account_id);
            //ht.Add("expect_start_time", CONST_VALUE.DEFAULT_START_TIME);
            //ht.Add("expect_end_time", CONST_VALUE.DEFAULT_END_TIME);
            ht.Add("expect_start_time", workingTimeRecord.expect_start_time.ToString());
            ht.Add("expect_end_time", workingTimeRecord.expect_end_time.ToString());

            //int iRc = ServiceLocator.Resolve<IMapperManager>().Insert<Hashtable>("WorkingtimeRecord", "Insert", ht);
            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("WorkingtimeRecord", "Insert", ht).ToString());            
        }

        /// <summary>
        ///  Update a new record for WorkingTimeRecord
        /// </summary>
        /// <param name="workingTimeRecord"></param>
        /// <returns></returns>
        public bool Update(WorkingtimeRecord workingTimeRecord)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", workingTimeRecord.id);
            ht.Add("account_id", workingTimeRecord.account_id);
            ht.Add("register_date", workingTimeRecord.register_date);
            ht.Add("account_paid_holiday_id", workingTimeRecord.account_paid_holiday_id);
            ht.Add("expect_start_time", workingTimeRecord.expect_start_time.ToString());
            ht.Add("expect_end_time", workingTimeRecord.expect_end_time.ToString());
            ht.Add("device_start_time", workingTimeRecord.device_start_time.ToString());
            ht.Add("device_end_time", workingTimeRecord.device_end_time.ToString());
            ht.Add("actual_start_time", workingTimeRecord.actual_start_time.ToString());
            ht.Add("actual_end_time", workingTimeRecord.actual_end_time.ToString());
            ht.Add("rest_time", workingTimeRecord.rest_time);
            ht.Add("additional_rest_time", workingTimeRecord.additional_rest_time);
            ht.Add("overtime_record_id", workingTimeRecord.overtime_record_id);
            ht.Add("updated_date", workingTimeRecord.updated_date);
            ht.Add("updated_account_id", workingTimeRecord.updated_account_id);
            ht.Add("deleted_date", workingTimeRecord.deleted_date);
            ht.Add("deleted_account_id", workingTimeRecord.deleted_account_id);

            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<Hashtable>("WorkingtimeRecord", "Update", ht);
            return iRc > 0;
        }

        /// <summary>
        /// Get information for check attendance 
        /// </summary>
        /// <param name="register_date">register_date</param>
        /// <param name="company_id">account_id</param>
        /// <returns></returns>
        public List<CheckAttendanceResponse> GetCheckAttendance(string register_date, string company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("register_date", register_date);
            ht.Add("company_id", company_id);
            
            return mapper.QueryForList<Hashtable, CheckAttendanceResponse>("WorkingtimeRecord", "GetCheckAttendance", ht);
        }

        /// <summary>
        /// Update working time record going
        /// </summary>
        /// <param name="dateNow">DateTime.Now</param>
        /// <param name="account_id">account_id</param>
        /// <returns></returns>
        public int UpdateWorkingTimeGoing(DateTime dateNow, string actualTime, int account_id,Company company)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("working_datetime_now", dateNow.ToString("MM/dd/yyyy HH:mm:ss"));
            ht.Add("working_account_id", account_id);
            ht.Add("actual_start_time", actualTime);
            ht.Add("expect_start_time", company.start_working_time.ToString());
            ht.Add("expect_end_time", company.end_working_time.ToString());
            var timeSpan = Math.Round((company.end_lunch_time - company.start_lunch_time).TotalHours, 2);
            ht.Add("full_rest_time", timeSpan.ToString());

            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("WorkingtimeRecord", "UpdateWorkingTimeGoing", ht).ToString());
        }

        /// <summary>
        /// Update working time record leaving
        /// </summary>
        /// <param name="dateNow">DateTime.Now</param>
        /// <param name="account_id">account_id</param>
        /// <param name="overtime_record_id">overtime_record_id</param>
        /// <returns></returns>
        public int UpdateWorkingTimeLeaving(DateTime dateNow,TimeSpan actualTime, int account_id, string restTime, int overtime_record_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("working_datetime_now", dateNow.ToString("MM/dd/yyyy HH:mm:ss"));
            ht.Add("working_account_id", account_id);
            ht.Add("actual_end_time", actualTime.ToString());
            ht.Add("rest_time", restTime);
            if (overtime_record_id == 0)
                ht.Add("overtime_record_id", null);
            else
                ht.Add("overtime_record_id", overtime_record_id);

            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("WorkingtimeRecord", "UpdateWorkingTimeLeaving", ht).ToString());
        }

        /// <summary>
        /// Update working time record after approved
        /// <param name="requsetManag"></param>
        /// </summary>
        /// <returns></returns>
        public bool UpdateWorkingAfterApproved(AccountPaidHoliday requsetManag, string expectStartTime, string expectEndTime)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            ht.Add("id", requsetManag.id);
            ht.Add("updated_account_id", requsetManag.updated_account_id);
            ht.Add("updated_date", requsetManag.updated_date);
            //ht.Add("default_start_time", CONST_VALUE.DEFAULT_START_TIME);
            //ht.Add("default_end_time", CONST_VALUE.DEFAULT_END_TIME);
            ht.Add("default_start_time", expectStartTime);
            ht.Add("default_end_time", expectEndTime);
            ht.Add("off_half_morning", (int)PaidHolidayTypeEnum.HalfDayMorning);
            ht.Add("off_half_afternoon", (int)PaidHolidayTypeEnum.HalfDayAfternoon);

            return mapper.Update<Hashtable>("WorkingtimeRecord", "UpdateWorkingAfterApproved", ht) >= 0;
        }

        /// <summary>
        /// Update working time record after deleted
        /// </summary>
        /// <param name="account_paid_holiday_id">account_paid_holiday_id</param>
        /// <param name="updated_date">updated_date</param>
        /// <param name="updated_account_id">updated_account_id</param>
        /// <returns></returns>
        public bool UpdateWorkingAfterDeleted(int account_paid_holiday_id,DateTime updated_date,int updated_account_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_paid_holiday_id", account_paid_holiday_id);
            ht.Add("updated_date", updated_date);
            ht.Add("updated_account_id", updated_account_id);
            return mapper.Update<Hashtable>("WorkingtimeRecord", "UpdateWorkingAfterDeleted", ht) >= 0;
        }

        /// <summary>
        /// Check the working time record be approved or not 
        /// </summary>
        /// <param name="id">workingtime_record.id</param>
        /// <returns></returns>
        public bool IsApprovedWorkTime(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id",id);
            return StringUtil.ConvertObjectToInteger(mapper.QueryForScalar<Hashtable>("WorkingtimeRecord", "IsApprovedWorkTime", ht)) > 0 ;
        }

        /// <summary>
        /// Update working time record when have rest request via Slack
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="restTime"></param>
        /// <param name="updatedDate"></param>
        /// <param name="expectEndTime"></param>
        /// <returns></returns>
        public bool UpdateAdditionalResttime(int accountId, double restTime, string updatedDate, string expectStartTime, string expectEndTime)
        {
            var memberInfo = ServiceLocator.Resolve<IAccount>().GetDataById(accountId);
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", accountId);
            ht.Add("register_date", updatedDate);
            //ht.Add("expect_start_time", CONST_VALUE.DEFAULT_START_TIME);
            //ht.Add("expect_end_time", CONST_VALUE.DEFAULT_END_TIME);
            ht.Add("expect_start_time", expectStartTime);
            ht.Add("expect_end_time", expectEndTime);
            ht.Add("device_start_time", null);
            ht.Add("device_end_time", null);
            ht.Add("actual_start_time", null);
            ht.Add("actual_end_time", null);
            ht.Add("rest_time", 0);
            ht.Add("additional_rest_time", restTime);
            ht.Add("overtime_record_id", null);
            ht.Add("created_date", AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id));
            ht.Add("created_account_id", accountId);
            ht.Add("updated_account_id", accountId);
            ht.Add("updated_date", AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id));
            
            return mapper.Update<Hashtable>("WorkingtimeRecord", "UpdateAdditionalResttime", ht) > 0;
        }

        /// <summary>
        /// Update working time record when have change/ late request via Slack
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expectStartTime"></param>
        /// <param name="updatedDate"></param>
        /// <returns></returns>
        public bool UpdateChangeOrLateTime(int updatedAccountId, string expectStartTime, string expectEndTime, string updatedDate)
        {
            var memberInfo = ServiceLocator.Resolve<IAccount>().GetDataById(updatedAccountId);
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("register_date", updatedDate);
            ht.Add("account_id", updatedAccountId);
            ht.Add("device_start_time", null);
            ht.Add("device_end_time", null);
            ht.Add("actual_start_time", null);
            ht.Add("actual_end_time", null);
            ht.Add("rest_time", 0);
            ht.Add("additional_rest_time",0);
            ht.Add("overtime_record_id", null);
            ht.Add("created_date", AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id).ToString("yyyy/MM/dd"));
            ht.Add("created_account_id", updatedAccountId);
            ht.Add("expect_start_time", expectStartTime);
            ht.Add("expect_end_time", expectEndTime);
            ht.Add("updated_account_id", updatedAccountId);
            ht.Add("updated_date", AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id).ToString("yyyy/MM/dd"));

            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("WorkingtimeRecord", "UpdateChangeOrLateTime", ht)) > 0;
        }

        /// <summary>
        /// Check the working time is selfcheck or not
        /// </summary>
        /// <param name="id">working time record id</param>
        /// <returns></returns>
        public bool IsSelfCheck(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);

            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Hashtable>("WorkingtimeRecord", "IsSelfCheck", ht)) > 0;
        }


        /// <summary>
        /// Delete working time record
        /// </summary>
        /// <param name="workingTimeRecord"></param>
        /// <returns></returns>
        public bool Delete(WorkingtimeRecord workingTimeRecord)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return mapper.Update<WorkingtimeRecord>("WorkingtimeRecord", "Delete", workingTimeRecord) >= 0;
        }

        /// <summary>
        /// Get staff working status
        /// </summary>
        /// <param name="set_of_id">string id</param>
        /// <param name="range_from">range from</param>
        /// <param name="range_to">range to</param>
        /// <returns></returns>
        public List<StaffWKStatusResponse> GetStaffWKStatus(string set_of_id, int range,Company company)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            TimeSpan tmpTime = new TimeSpan(0, 0, 0);
            ht.Add("set_of_id", set_of_id);
            ht.Add("range", range);
            ht.Add("day", AzureUtil.LocalDateTimeNow(company.time_zone_id).Day);
            ht.Add("month", AzureUtil.LocalDateTimeNow(company.time_zone_id).Month);
            ht.Add("year", AzureUtil.LocalDateTimeNow(company.time_zone_id).Year);
            ht.Add("company_id", company.id);
            ht.Add("expect_start_time", company.start_working_time.ToString());
            ht.Add("expect_end_time", company.end_working_time.ToString());
            ht.Add("none_work_time", StringUtil.ConvertObjectToString(tmpTime));
            return mapper.QueryForList<Hashtable,StaffWKStatusResponse>("WorkingtimeRecord", "GetStaffWKStatus", ht);
        }
    }
}

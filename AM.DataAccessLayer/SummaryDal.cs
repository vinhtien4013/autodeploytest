﻿using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;
using System.Collections.Generic;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class SummaryDal : ISummary
    {
        /// <summary>
        /// Get data Attendance summary
        /// </summary>
        /// <param name="endDate">end date</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <param name="id">account_id</param>
        /// <returns></returns>
        public List<AttendanceSummary> GetAttendanceSummary(int endDate, int month, int year,string account_id,string staff_id, int company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("month", month);
            ht.Add("dateEnd", endDate);
            ht.Add("year", year);
            ht.Add("id", account_id);
            ht.Add("staff_id", staff_id);
            ht.Add("company_id", company_id);

            return mapper.QueryForList<Hashtable, AttendanceSummary>("AttendanceSummary", "GetAttendanceSummary", ht);
        }

        /// <summary>
        /// Get Attendance Summary Detail
        /// </summary>
        /// <param name="endDate">end date</param>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <param name="id">account_id</param>
        /// <returns></returns>
        public List<AttendanceSummaryDetail> GetAttendanceSummaryDetail(int endDate, int month, int year, string account_id,string staff_id, int company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("month", month);
            ht.Add("dateEnd", endDate);
            ht.Add("year", year);
            ht.Add("fulldayNoSalary", DATE_FLG.FulldayOffNoSalary);
            ht.Add("halfdayNoSalary", DATE_FLG.HalfdayOffNoSalary);
            ht.Add("workingDate", DATE_FLG.FulldayWorking);
            ht.Add("fulldayHaveSalary", DATE_FLG.FulldayOffHaveSalary);
            ht.Add("halfdayHaveSalary", DATE_FLG.HalfdayOffHaveSalary);
            ht.Add("weekenDate", DATE_FLG.Weekendate);
            ht.Add("FulldayWorking", DATE_FLG.FulldayWorking);
            ht.Add("holidayDate", DATE_FLG.Holidaydate);
            ht.Add("id", account_id);
            ht.Add("staff_id", staff_id);
            ht.Add("company_id", company_id);

            return mapper.QueryForList<Hashtable, AttendanceSummaryDetail>("AttendanceSummary", "GetAttendanceSummaryDetail", ht);
        }
    }
}

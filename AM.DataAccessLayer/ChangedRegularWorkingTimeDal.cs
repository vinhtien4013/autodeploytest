﻿using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using System.Collections;
using System.Collections.Generic;
using System;
using Microsoft.Practices.EnterpriseLibrary.Data;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class ChangedRegularWorkingTimeDal : IChangedRegularWorkingTime
    {
        /// <summary>
        /// Get list information in table changed_regular_working_time
        /// </summary>
        /// <returns>List modal ChangedRegularWorkingTime</returns>
        public List<ChangedRegularWorkingTime> GetChangedRegularWorkingTime(int companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", companyId);
            return mapper.QueryForList<Hashtable,ChangedRegularWorkingTime>("ChangedRegularWorkingTime", "GetChangedRegularWorkingTime",ht);
        }

        /// <summary>
        /// Register a new line changed_regular_working_time
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Insert(ChangedRegularWorkingTime model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", model.account_id);
            ht.Add("start_time", model.start_time.ToString());
            ht.Add("end_time", model.end_time.ToString());
            ht.Add("reason", model.reason);
            ht.Add("rest_time", model.rest_time);
            ht.Add("mon_flg", model.mon_flg);
            ht.Add("tue_flg", model.tue_flg);
            ht.Add("wed_flg", model.wed_flg);
            ht.Add("thu_flg", model.thu_flg);
            ht.Add("fri_flg", model.fri_flg);
            ht.Add("lunch_time_from", model.lunch_time_from.ToString());
            ht.Add("lunch_time_to", model.lunch_time_to.ToString());
            ht.Add("apply_start_date", model.apply_start_date);
            ht.Add("apply_end_date", model.apply_end_date);
            ht.Add("created_date", model.created_date);
            ht.Add("created_account_id", model.created_account_id);
            ht.Add("updated_date", model.updated_date);
            ht.Add("updated_account_id", model.updated_account_id);

            return mapper.Insert<Hashtable>("ChangedRegularWorkingTime", "Insert", ht)> 0;
        }

        /// <summary>
        /// Update a existed line changed_regular_working_time
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(ChangedRegularWorkingTime model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", model.id);
            ht.Add("account_id", model.account_id);
            ht.Add("start_time", model.start_time.ToString());
            ht.Add("end_time", model.end_time.ToString());
            ht.Add("reason", model.reason);
            ht.Add("rest_time", model.rest_time);
            ht.Add("mon_flg", model.mon_flg);
            ht.Add("tue_flg", model.tue_flg);
            ht.Add("wed_flg", model.wed_flg);
            ht.Add("thu_flg", model.thu_flg);
            ht.Add("fri_flg", model.fri_flg);
            ht.Add("lunch_time_from", model.lunch_time_from.ToString());
            ht.Add("lunch_time_to", model.lunch_time_to.ToString());
            ht.Add("apply_start_date", model.apply_start_date);
            ht.Add("apply_end_date", model.apply_end_date);
            ht.Add("updated_date", model.updated_date);
            ht.Add("updated_account_id", model.updated_account_id);

            return mapper.Update<Hashtable>("ChangedRegularWorkingTime", "Update", ht) > 0;
        }

        /// <summary>
        /// Delete a existed line changed_regular_working_time
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Delete(ChangedRegularWorkingTime model)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            return mapper.Update<ChangedRegularWorkingTime>("ChangedRegularWorkingTime", "Delete", model) > 0;
        }

        public ChangedRegularWorkingTime GetRegularWorkTimeByAccount(int accountId, DateTime date)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable  ht = new Hashtable();
            ht.Add("account_id", accountId);
            ht.Add("date", date.ToString("yyyy/MM/dd"));

            return mapper.QueryForObject<Hashtable, ChangedRegularWorkingTime>("ChangedRegularWorkingTime", "GetRegularWorkTimeByAccount", ht);
        }

        /// <summary>
        /// Get expect start time/ expect end time
        /// </summary>
        /// <param name="account_id">account id</param>
        /// <param name="date">date</param>
        /// <returns></returns>
        public ChangedRegularWorkingTime GetExpectTime(int accountId, DateTime date, Company company)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            IRowMapper<ChangedRegularWorkingTime> rowMapper = MapBuilder<ChangedRegularWorkingTime>.MapNoProperties()
                .MapByName(p => p.start_time)
                .MapByName(p => p.end_time)
                .Build();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", accountId);
            ht.Add("date", date);
            ht.Add("expect_start_time", company.start_working_time.ToString());
            ht.Add("expect_end_time", company.end_working_time.ToString());

            return mapper.QueryForObject<Hashtable, ChangedRegularWorkingTime>("ChangedRegularWorkingTime", "GetExpectTime", ht, rowMapper);
        }

    }
}

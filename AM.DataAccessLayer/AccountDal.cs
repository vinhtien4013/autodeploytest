﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.Tables;
using FP.Practices.EnterpriseLibrary.FPDataMapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using static AM.Model.Const.ConstType;

namespace AM.DataAccessLayer
{
    public class AccountDal : IAccount
    {
        /// <summary>
        /// Get list account by company
        /// </summary>
        /// <param name="company_id">Company ID</param>
        /// <returns>List account by company</returns>
        public List<EditAccountModel> GetAccount(string company_id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("company_id", company_id);
            return mapper.QueryForList<Hashtable, EditAccountModel>("Account", "GetAccountByCondition", ht);
        }
        /// <summary>
        /// Get list member by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>List account</returns>
        public MemberInfoModel GetMemberInfoByGoogle(string email, string token)
        {
            var dtnow = DateTime.Now;
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("email", email);
            ht.Add("token", token);
            return mapper.QueryForObject<Hashtable, MemberInfoModel>("Account", "GetMemberInfo", ht);
        }


        /// <summary>
        /// Get memberInfo by slackUserId
        /// </summary>
        /// <param name="slack_user_id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public MemberInfoModel GetMemberInfoBySlackUserId(string slack_user_id, string token)
        {
            var dtnow = DateTime.Now;
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("slack_user_id", slack_user_id);
            ht.Add("token", token);
            return mapper.QueryForObject<Hashtable, MemberInfoModel>("Account", "GetMemberInfo", ht);
        }

        /// <summary>
        /// Get list member by login manual
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>List account</returns>
        public MemberInfoManualModel GetMemberInfoByManual(string email, string password,string token)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("password", password);
            ht.Add("email", email);
            ht.Add("token", token);
            return mapper.QueryForObject<Hashtable, MemberInfoManualModel>("Account", "GetMemberInfoByManual", ht);
        }

        /// <summary>
        /// Insert a new account
        /// </summary>
        /// <param name="account">accountmodel</param>
        /// <returns></returns>
        public int Insert(Account account)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            return StringUtil.ConvertObjectToInteger32(mapper.QueryForScalar<Account>("Account", "Insert", account));
        }

        /// <summary>
        /// Update account when edit finger print
        /// </summary>
        /// <param name="account"></param>
        /// <returns>List account</returns>
        public bool Update(Account account)
        {
            int iRc = ServiceLocator.Resolve<IMapperManager>().Update<Account>("Account", "Update", account);
            return iRc > 0;
        }

        /// <summary>
        /// Update account language
        /// </summary>
        /// <param name="id"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public bool UpdateAccountLanguage(int id, string language)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            ht.Add("language", language);

            return mapper.Update<Hashtable>("Account", "UpdateAccountLanguage", ht) > 0;
        }
        
        /// <summary>
        /// Update password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        public bool UpdateAccountPassword(string password,int id,int updated_account_id )
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("password", password);
            ht.Add("id", id);
            ht.Add("updated_date", AzureUtil.VietnamDateTimeNow());
            ht.Add("updated_account_id", updated_account_id);
            int iRc = mapper.Update<Hashtable>("Account", "UpdateAccountPassword", ht);
            return iRc > 0;
        }

        /// <summary>
        /// Delete account with finger print
        /// </summary>
        /// <param name="requestManage"></param>
        /// <returns></returns>
        public bool Delete(Account account)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();

            ht.Add("id", account.id);
            ht.Add("fingerprint_id", account.fingerprint_id);
            ht.Add("deleted_date", AzureUtil.VietnamDateTimeNow());
            ht.Add("deleted_account_id", CONST_VALUE.SERVER_ACCOUNT);

            return mapper.Update<Hashtable>("Account", "Delete", ht) > 0;
        }

        /// <summary>
        /// Get account by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public MemberInfoModel GetDataById(int id)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("id", id);
            return mapper.QueryForObject<Hashtable, MemberInfoModel>("Account", "GetDataById", ht);
        }

        /// <summary>
        /// Get account by fingerprint_id
        /// </summary>
        /// <param name="fingerprint_id">fingerprint_id</param>
        /// <returns></returns>
        public MemberInfoModel GetDataByFingerPrintId(int fingerprint_id, string token)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("fingerprint_id", fingerprint_id);
            ht.Add("token", token);
            return mapper.QueryForObject<Hashtable, MemberInfoModel>("Account", "GetMemberInfo", ht);
        }

        /// <summary>
        /// Search account by condition
        /// </summary>
        /// <param name="searchInput">ParameterSearchAccount model</param>
        /// <returns></returns>
        public List<EditAccountModel> GetAccountByCondition(ParameterSearchAccount searchInput)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();

            Hashtable ht = new Hashtable();
            ht.Add("company_id", searchInput.company_id);
            ht.Add("fingerprint_id", searchInput.search_Finger_ID);
            ht.Add("division_id", searchInput.search_Division_Name);
            ht.Add("staff_name", searchInput.search_Staff_Name);
            ht.Add("staff_id", searchInput.search_Staff_ID);
            return mapper.QueryForList<Hashtable, EditAccountModel>("Account", "GetAccountByCondition", ht);
        }

        /// <summary>
        /// Check the staff is responsible staff or not
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        public bool IsResponsibleStaff(string accountId, string companyId)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("account_id", accountId);
            ht.Add("company_id", companyId);

            return StringUtil.ConvertObjectToInteger(mapper.QueryForScalar<Hashtable>("Account", "IsResponsibleStaff", ht)) > 0;
        }

        /// <summary>
        /// Get account by person_id (face api)
        /// </summary>
        /// <param name="person_id">person_id</param>
        /// <returns></returns>
        public MemberInfoModel GetDataByPersonId(string person_id,string token)
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            Hashtable ht = new Hashtable();
            ht.Add("person_id", person_id);
            ht.Add("token", token);
            return mapper.QueryForObject<Hashtable, MemberInfoModel>("Account", "GetMemberInfo", ht);
        }

        /// <summary>
        /// Get account list that alr existed face_image
        /// </summary>
        /// <returns></returns>
        public List<Account> ListAccountImages()
        {
            IMapperManager mapper = ServiceLocator.Resolve<IMapperManager>();
            IRowMapper<Account> rowMapper = MapBuilder<Account>.MapNoProperties()
                .MapByName(p => p.id)
                .MapByName(p => p.staff_id)
                .MapByName(p => p.staff_name)
                .MapByName(p => p.face_image)
               .Build();

            return mapper.QueryForList<Hashtable, Account>("Account", "GetFaceImgFromAccount", null, rowMapper);
        }
    }
}

﻿namespace AM.Model.Common
{
    public class DataPostMessageToSlack
    {
        public string text { get; set; }
        public string channel { get; set; }
        public string username { get; set; }
        public string icon_url { get; set; }
    }
}

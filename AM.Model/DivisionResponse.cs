﻿namespace AM.Model
{
    public class DivisionResponse
    {
        public int id { get; set; }
        public int div_range_from { get; set; }
        public int div_range_to { get; set; }
        public string div_name { get; set; }
        public int responsible_staff_id { get; set; }
        public string admin_div_flg { get; set; }
        public string description { get; set; }
        public int count_member { get; set; }
        public string email { get; set; }
        public string div_code { get; set; }
    }

    public class DivisionMenu
    {
        public int id { get; set; }
        public int div_range_from { get; set; }
        public int div_range_to { get; set; }
        public string div_name { get; set; }
        public string div_parent_name { get; set; }
        public string div_code { get; set; }
    }
}

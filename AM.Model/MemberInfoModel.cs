﻿using AM.Model.Tables;

namespace AM.Model
{
    public class MemberInfoModel : Account
    {
        /// <summary>
        /// admin_flag:bit
        /// </summary>
        public bool company_admin_flg { get; set; }

        /// <summary>
        /// admin_flag:int
        /// </summary>
        public int company_id { get; set; }

        /// <summary>
        /// company_token:string
        /// </summary>
        public string company_token { get; set; }

        /// <summary>
        /// company_token:string
        /// </summary>
        public string time_zone_id { get; set; }
    }

    public class MemberInfoManualModel 
    {
        
        /// <summary>
        /// Password
        /// </summary>
        public string password { get; set; }
    }

}

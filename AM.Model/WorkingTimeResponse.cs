﻿using System;

namespace AM.Model
{
    public class WorkingTimeResponse
    {
        /// <summary>
        /// date
        /// </summary>
        public DateTime date { get; set; }
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// account_id
        /// </summary>
        public int account_id { get; set; }
        /// <summary>
        /// register_date
        /// </summary>
        public DateTime register_date { get; set; }
        /// <summary>
        /// account_paid_holiday_id
        /// </summary>
        public int account_paid_holiday_id { get; set; }
        /// <summary>
        /// expect_start_time
        /// </summary>
        public TimeSpan expect_start_time { get; set; }
        /// <summary>
        /// expect_end_time:time
        /// </summary>
        public TimeSpan expect_end_time { get; set; }
        /// <summary>
        /// device_start_time:time
        /// </summary>
        public TimeSpan device_start_time { get; set; }
        /// <summary>
        /// device_end_time:time
        /// </summary>
        public TimeSpan device_end_time { get; set; }
        /// <summary>
        /// actual_start_time:time
        /// </summary>
        public TimeSpan actual_start_time { get; set; }
        /// <summary>
        /// actual_end_time:time
        /// </summary>
        public TimeSpan actual_end_time { get; set; }
        /// <summary>
        /// rest_time:float
        /// </summary>
        public double rest_time { get; set; }
        /// <summary>
        /// additional_rest_time:float
        /// </summary>
        public double additional_rest_time { get; set; }
        /// <summary>
        /// overtime_record_id:int
        /// </summary>
        public int overtime_record_id { get; set; }
        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }
        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }
        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }
        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }
        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }
        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }
        ///<summary>
        /// national_flg
        /// </summary>
        public bool national_day { get; set; }
        /// <summary>
        /// registor_days
        /// </summary>
        public double registor_days { get; set; }
        /// <summary>
        /// time over
        /// </summary>
        public double total_over_time_work { get; set; }
        /// <summary>
        /// date name
        /// </summary>
        public string date_name { get; set; }
        /// <summary>
        /// Is approved
        /// </summary>
        public bool is_approved { get; set; }
        /// <summary>
        /// Is Self Check
        /// </summary>
        public string is_selfcheck { get; set; }

        /// <summary>
        /// Is Reject Selfcheck
        /// </summary>
        public bool is_reject_selfcheck { get; set; }
    }
}

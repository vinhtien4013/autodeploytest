﻿using System;
using System.Configuration;

namespace AM.Model.api
{
    public class AttendanceSettings
    {
        private static string briswellVietNamGoogleAddress = ConfigurationManager.AppSettings["BriswellVietNamGoogleSite"];
        private static string briswellVietNamGroup = String.Format("{0}{1}", briswellVietNamGoogleAddress, ConfigurationManager.AppSettings["BriswellVietNamGroup"]);
        private static string briswellVietNamAcccount = String.Format("{0}{1}", briswellVietNamGoogleAddress, ConfigurationManager.AppSettings["BriswellVietNamAccount"]);
        private static string secretKeyToken = ConfigurationManager.AppSettings["SecretKeyToken"];
        private static string secretKeyTokenPostMessageSlack = ConfigurationManager.AppSettings["SecretKeyTokenPostMessageSlack"];

        /// <summary> Get post message to slack url</summary>
        public static string GetPostMessageToSlackUrl = ConfigurationManager.AppSettings["PostMessageToSlack"];
        /// <summary> Message check in (going)</summary>
        public static string MessageCheckInSlack = ConfigurationManager.AppSettings["MessageGoing"];
        /// <summary> Message check out (leaving)</summary>
        public static string MessageCheckOutSlack = ConfigurationManager.AppSettings["MessageLeaving"];
        /// <summary> Channel</summary>
        public static string ChannelSlack = ConfigurationManager.AppSettings["Channel"].ToString();
        /// <summary> User name</summary>
        public static string UserNameSlack = ConfigurationManager.AppSettings["Username"].ToString();
        /// <summary> Icon url</summary>
        public static string IconUrlSlack = ConfigurationManager.AppSettings["IconUrl"].ToString();
        /// <summary> Announcement Message</summary>
        public static string AnnouncementMessage = ConfigurationManager.AppSettings["AnnouncementMessage"].ToString();
        /// <summary> Announcement Chanel</summary>
        public static string AnnouncementChanel = ConfigurationManager.AppSettings["AnnouncementChanel"].ToString();
        /// <summary> API Face API ENDPOINT</summary>
        public static string apimEndPoint = ConfigurationManager.AppSettings["EndPointFaceAPI"];
        /// <summary> Key for Face API Azure</summary>
        public static string apimKey = ConfigurationManager.AppSettings["FaceAPIKey"];
        /// <summary> String start of each Person group by ENV</summary>
        public static string FaceEnv = ConfigurationManager.AppSettings["FaceEnv"];
        public string BriswellVietNamGroup { get; set; }
        public string BriswellVietNamAcccount { get; set; }
        public string SecretKeyToken { get; set; }
        public string SecretKeyTokenPostMessageSlack { get; set; }

        /// <summary> String start of each Person group by ENV</summary>
        public static string awsAccessKEy = ConfigurationManager.AppSettings["awsAccessKeyID"];
        /// <summary> String start of each Person group by ENV</summary>
        public static string awsSecretKEy = ConfigurationManager.AppSettings["awsSecretKey"];
        public AttendanceSettings()
        {
            BriswellVietNamGroup = briswellVietNamGroup;
            BriswellVietNamAcccount = briswellVietNamAcccount;
            SecretKeyToken = secretKeyToken;
            SecretKeyTokenPostMessageSlack = secretKeyTokenPostMessageSlack;
        }
    }
}

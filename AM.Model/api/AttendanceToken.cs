﻿using System;

namespace AM.Model
{
    /// <summary>
    /// Attendance Token
    /// </summary>
    public class AttendanceToken
    {
        /// <summary>
        /// Access Token
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        ///  unix timestamp
        /// </summary>
        public long UnixTime { get; set; }

        public bool IsValidAndNotExpiring
        {
            get
            {
                var dateTime = DateTime.UtcNow;
                // Initializes a new instance of the System.DateTimeOffset structure using the specified System.DateTime value.
                var dateTimeOffset = new DateTimeOffset(dateTime);
                // The number of seconds that have elapsed since 1970-01-01T00:00:00Z.
                var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();

                return !String.IsNullOrEmpty(this.AccessToken) &&
                (this.UnixTime < (unixDateTime - 60) || unixDateTime + 60 < this.UnixTime);
            }
        }
    }
}

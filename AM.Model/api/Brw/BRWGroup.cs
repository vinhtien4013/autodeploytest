﻿using System.Collections.Generic;

namespace AM.Model.api.Brw
{
    public class BRWGroup
    {
        public BRWGroup()
        {
            data = new List<group_data>();
        }
        public int status { get; set; }
        public string message { get; set; }
        public int maxcount { get; set; }
        public List<group_data> data { get; set; }
    }

    public class group_data
    {
        public string group_id { get; set; }
        public string group_name { get; set; }
        public string super_group_id { get; set; }
        public string group_level { get; set; }
        public string admin_div_flg { get; set; }
        public string responsible_person_staff_id { get; set; }
    }
}

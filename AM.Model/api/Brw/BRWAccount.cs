﻿using System.Collections.Generic;

namespace AM.Model.api.Brw
{
    public class BRWAccount
    {
        public BRWAccount()
        {
            data = new List<staff_data>();
        }
        public int status { get; set; }
        public string message { get; set; }
        public int maxcount { get; set; }
        public List<staff_data> data { get; set; }
    }

    public class staff_data
    {
        public string staff_id { get; set; }
        public string staff_name { get; set; }
        public int group_id { get; set; }
    }
}

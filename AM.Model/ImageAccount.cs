﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AM.Model
{
    public class ImageAccount
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }
        /// <summary>
        /// staff_id:varchar(12)
        /// </summary>
        public string staff_id { get; set; }

        /// <summary>
        /// staff_name:varchar(255)
        /// </summary>
        public string staff_name { get; set; }

        /// <summary>
        /// face_image: varchar(128)
        /// </summary>
        public string face_image { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace AM.Model
{
    public class SlackModel
    {

        public SlackModel()
        {
            TimeFromTo = new List<TimeFromto>();
        }
        /// <summary>
        /// account id
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// register date (in case: register paid holiday, change regular workingtime, register additional rest time)
        /// </summary>
        public Nullable<DateTime> RegisterDate { get; set; }

        /// <summary>
        /// register late time in the current date
        /// </summary>
        public Nullable<TimeSpan> LateTime { get; set; }

        /// <summary>
        /// reason
        /// </summary>
        public string reason { get; set; }

        /// <summary>
        /// login user nam
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// command type : determined by text
        /// 1 : register account paid holiday(All day)
        /// 2 : register account paid holiday(Half morning)
        /// 3 : register account paid holiday(Half afternoon)
        /// 4 : be late in day
        /// 5 : change working time in an specific
        /// 6 : register additional rest time in working time
        /// 7 : help
        /// </summary>
        public int CommandType { get; set; }

        //public List<string> ListItem { get; set; }

        /// <summary>
        /// register expect start time 
        /// </summary>
        public TimeSpan? ExpectStartTime{ get; set; }

        /// <summary>
        /// register expect end time
        /// </summary>
        public TimeSpan? ExpectEndTime { get; set; }

        /// <summary>
        /// total rest hours
        /// </summary>
        public double RestHours { get; set; }

        /// <summary>
        /// list time from and time to 
        /// </summary>
        public List<TimeFromto>TimeFromTo { get; set; }

        /// <summary>
        /// sex
        /// 0: Male
        /// 1: Female
        /// </summary>
        public string UserSex { get; set; }
    }

    public class SlackResponse
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SlackResponse()
        {
            attachments = new List<HelperContent>();
        }

        public SlackResponse(string ResponseType,string Text)
        {
            response_type = ResponseType;
            text = Text;
            attachments = new List<HelperContent>();
        }

        /// <summary>
        /// Slack response type
        /// in_channel/ephemeral
        /// </summary>
        public string response_type { get; set; }

        /// <summary>
        /// response message text
        /// </summary>
        public string text { get; set; }

        /// <summary>
        /// attachments 
        /// </summary>
        public List<HelperContent> attachments { get; set; }
    }

    public class HelperContent
    {
        public string text { get; set; }
    }

    public class TimeFromto
    {
        //Time From
        public TimeSpan? TimeFrom { get; set; }

        //Time To
        public TimeSpan? TimeTo { get; set; }
    }
}

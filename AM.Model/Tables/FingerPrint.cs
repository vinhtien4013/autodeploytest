﻿using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class FingerPrint
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// finger_data
        /// </summary>
        public string finger_data { get; set; }

        /// <summary>
        /// salt
        /// </summary>
        public string salt { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }
    }
}

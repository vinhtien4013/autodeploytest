﻿using System;

namespace AM.Model.Tables
{
    public class Division
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }
        /// <summary>
        /// div_name:nvarchar(MAX)
        /// </summary>
        public string div_name { get; set; }
        /// <summary>
        /// div_range_from: int
        /// </summary>
        public int div_range_from { get; set; }
        /// <summary>
        /// div_range_to:int
        /// </summary>
        public int div_range_to { get; set; }

        /// <summary>
        /// responsible_staff_id:int
        /// </summary>
        public int responsible_staff_id { get; set; }

        /// <summary>
        /// admin_div_flg:bit
        /// </summary>
        public bool admin_div_flg { get; set; }

        /// <summary>
        /// description:nvarchar(max)
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// company_id:int
        /// </summary>
        public int company_id { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public DateTime? deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public Nullable<int> deleted_account_id { get; set; }

        /// <summary>
        /// div_range_parent
        /// </summary>
        public int div_range_parent { get; set; }

        /// <summary>
        /// modify mode
        /// </summary>
        public string modify_mode { get; set; }

        /// <summary>
        /// group id
        /// </summary>
        public int group_id { get; set; }
        
        /// <summary>
        /// division code
        /// </summary>
        public string div_code { get; set; }
    }
}

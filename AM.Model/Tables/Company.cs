﻿using System;
using System.Xml.Linq;

namespace AM.Model.Tables
{
    /// <summary>
    /// Table Company
    /// </summary>
    public class Company
    {
        /// <summary>
        /// id: int
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// company token: nvarchar(250)
        /// </summary>
        public string company_token { get; set; }

        /// <summary>
        /// company name: nvarchar(255)
        /// </summary>
        public string company_name { get; set; }

        /// <summary>
        /// admin company flag: bit
        /// </summary>
        public bool admin_company_flg { get; set; }

        /// <summary>
        /// valid date: Datetime
        /// </summary>
        public Nullable<DateTime> valid_date { get; set; }

        /// <summary>
        /// description: nvarchar(255)
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// created date: Datetime
        /// </summary>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created account id: int
        /// </summary>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated date: Datetime
        /// </summary>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated account id:int
        /// </summary>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted date: Datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted account id: int
        /// </summary>
        public Nullable<int> deleted_account_id { get; set; }

        /// <summary>
        /// slack using flag
        /// </summary>
        public bool slack_flg { get; set; }

        /// <summary>
        /// start_working_time
        /// </summary>
        public TimeSpan start_working_time { get; set; }

        /// <summary>
        /// end_working_time
        /// </summary>
        public TimeSpan end_working_time { get; set; }

        /// <summary>
        /// start_lunch_time
        /// </summary>
        public TimeSpan start_lunch_time { get; set; }

        /// <summary>
        /// end_lunch_time
        /// </summary>
        public TimeSpan end_lunch_time { get; set; }

        /// <summary>
        /// uncounted_time
        /// </summary>
        public int uncounted_time { get; set; }

        /// <summary>
        /// slack_token
        /// </summary>
        public string slack_token { get; set; }

        /// <summary>
        /// slack_team_id
        /// </summary>
        public string slack_team_id { get; set; }

        /// <summary>
        /// slack_team_domain
        /// </summary>
        public string slack_team_domain { get; set; }

        /// <summary>
        /// attendance_channel
        /// </summary>
        public string attendance_channel { get; set; }

        /// <summary>
        /// announcement_channel
        /// </summary>
        public string announcement_channel { get; set; }

        /// <summary>
        /// attendance_command
        /// </summary>
        public string attendance_command { get; set; }

        /// <summary>
        /// company_logo
        /// </summary>
        public string company_logo { get; set; }

        /// <summary>
        /// time_zone_id
        /// </summary>
        public string time_zone_id { get; set; }

        /// <summary>
        /// paid_holiday_monthly_flg
        /// </summary>
        public bool paid_holiday_monthly_flg { get; set; }

        /// <summary>
        /// slack_webhook_url
        /// </summary>
        public string slack_webhook_url { get; set; }

        /// <summary>
        /// post_message_username
        /// </summary>
        public string post_message_username { get; set; }

        /// <summary>
        /// login_by
        /// 0 : login by manual
        /// 1: login with google
        /// </summary>
        public int login_by { get; set; }

        ///<summary>
        /// face using flag
        /// </summary>
        public bool face_flg { get; set; }

        /// <summary>
        /// face folder
        /// </summary>
        public string face_folder { get; set; }

        /// <summary>
        /// min working time
        /// </summary>
        public int min_workingtime { get; set; }
    }
}

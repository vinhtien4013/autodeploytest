﻿using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class Account
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// fingerprint_id:int
        /// </summary>
        public Nullable<int> fingerprint_id { get; set; }

        /// <summary>
        /// staff_id:varchar(12)
        /// </summary>
        public string staff_id { get; set; }

        /// <summary>
        /// staff_name:varchar(255)
        /// </summary>
        public string staff_name { get; set; }

        /// <summary>
        /// sex 
        /// </summary>
        public string sex { get; set; }


        /// <summary>
        /// division_id:int
        /// </summary>
        public int division_id { get; set; }

        /// <summary>
        /// email:varchar(128)
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// admin_flag:bit
        /// </summary>
        public bool admin_flg { get; set; }

        /// <summary>
        /// joined_date:date
        /// </summary>
        public Nullable<DateTime> joined_date { get; set; }

        /// <summary>
        /// resign_date:date
        /// </summary>
        public Nullable<DateTime> resign_date { get; set; }

        /// <summary>
        /// face_image: varchar(128)
        /// </summary>
        public string face_image { get; set; }

        /// <summary>
        /// person_id: varchar(128)
        /// </summary>
        public string person_id { get; set; }

        /// <summary>
        /// persisted_face_id: varchar(128)
        /// </summary>
        public string persisted_face_id { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

        /// <summary>
        /// language
        /// </summary>
        public string language { get; set; }

        /// <summary>
        /// password
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Slack user id : varchar(20)
        /// </summary>
        public string slack_user_id { get; set; }
    }
}

﻿using AM.Core.Util;
using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class WorkingtimeRecord
    {
        public WorkingtimeRecord()
        {
            deleted_date = null;
            updated_date = AzureUtil.LocalDateTimeNow(AzureUtil.TimezoneId.VietNam);
            overtime_record = new WorkingovertimeRecord();
        }

        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }
        /// <summary>
        /// account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int account_id { get; set; }
        /// <summary>
        /// register_date:date
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime register_date { get; set; }
        /// <summary>
        /// account_paid_holiday_id:int
        /// </summary>
        public int? account_paid_holiday_id { get; set; }

        /// <summary>
        /// expect_start_time:time
        /// </summary>  
        public TimeSpan expect_start_time { get; set; }
        /// <summary>
        /// expect_end_time:time
        /// </summary>
        public TimeSpan expect_end_time { get; set; }
        /// <summary>
        /// device_start_time:time
        /// </summary>
        public TimeSpan device_start_time { get; set; }
        /// <summary>
        /// device_end_time:time
        /// </summary>
        public TimeSpan device_end_time { get; set; }
        /// <summary>
        /// actual_start_time:time
        /// </summary>
        public TimeSpan actual_start_time { get; set; }
        /// <summary>
        /// actual_end_time:time
        /// </summary>
        public TimeSpan actual_end_time { get; set; }
        /// <summary>
        /// rest_time:float
        /// </summary>
        public double rest_time { get; set; }
        /// <summary>
        /// additional_rest_time:float
        /// </summary>
        public Nullable<double> additional_rest_time { get; set; }
        /// <summary>
        /// overtime_record_id:int
        /// </summary>
        public Nullable<int> overtime_record_id { get; set; }
        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }
        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }
        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }
        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }
        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }
        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

        /// <summary>
        /// Working Overtime Record
        /// </summary>
        public WorkingovertimeRecord overtime_record { get; set; }

        /// <summary>
        /// Is reject selfcheck
        /// </summary>
        public bool is_reject_selfcheck { get; set; } = false;
    }
}

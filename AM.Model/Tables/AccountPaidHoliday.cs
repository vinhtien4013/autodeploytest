﻿using AM.Core.Util;
﻿using AM.Core.ServiceLocator;
using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class AccountPaidHoliday
    {
        public AccountPaidHoliday()
        {
        }

        public AccountPaidHoliday(int account_Id)
        {
            this.account_id = account_Id;
            //application_datetime = AzureUtil.VietnamDateTimeNow();
            //created_date = AzureUtil.VietnamDateTimeNow();
            //updated_date = AzureUtil.VietnamDateTimeNow();
            deleted_date = null;
        }
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints: PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int account_id { get; set; }

        /// <summary>
        /// application_datetime:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime application_datetime { get; set; }

        /// <summary>
        /// registor_days:float(1)
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public double registor_days { get; set; }

        /// <summary>
        /// processing_status:tinyint
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public byte processing_status { get; set; }

        /// <summary>
        /// conpensatory_holiday_flg:bit
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public bool conpensatory_holiday_flg { get; set; }

        /// <summary>
        /// reason_txt:varchar(100)
        /// </summary>
        public string reason_txt { get; set; }
        
        /// <summary>
        /// paid_holiday_type: int?
        /// </summary>
        public int? paid_holiday_type { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

    }
}

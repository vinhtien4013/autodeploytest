﻿using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class Confirmation
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// application_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int application_id { get; set; }

        /// <summary>
        /// application_form_id:tinyint
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public byte application_form_id { get; set; }

        /// <summary>
        /// confirmed_datetime:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public Nullable<DateTime> confirmed_datetime { get; set; }

        /// <summary>
        /// account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int account_id { get; set; }

        /// <summary>
        /// check_sequence:tinyint
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public byte check_sequence { get; set; }

        /// <summary>
        /// check_status:bit --> int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int check_status { get; set; }

        /// <summary>
        /// reason:text
        /// </summary>
        public string reason { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

        /// <summary>
        /// Application sequence
        /// </summary>
        public int application_sequence { get; set; } = 1;
    }
}

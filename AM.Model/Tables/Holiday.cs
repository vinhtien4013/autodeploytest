﻿using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class Holiday
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints: PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// holiday_date_from:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime holiday_date_from { get; set; }

        /// <summary>
        /// holiday_date_to:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime holiday_date_to { get; set; }

        /// <summary>
        /// holiday_name:varchar(50)
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public string holiday_name { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

        /// <summary>
        /// company_id: int
        /// </summary>
        public int company_id { get; set; }
    }
}

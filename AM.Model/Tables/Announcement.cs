﻿using System;

namespace AM.Model.Tables
{
    public class Announcement
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// title
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// created date
        /// </summary>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created account id
        /// </summary>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated date
        /// </summary>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated account id
        /// </summary>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted date
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted account id
        /// </summary>
        public Nullable<int> deleted_account_id { get; set; }

        /// <summary>
        /// Email 
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// post date
        /// </summary>
        public Nullable<DateTime> post_date { get; set; }
        
        /// <summary>
        /// draft flag
        /// </summary>
        public bool draft_flg { get; set; }

        /// <summary>
        /// company_id: int
        /// </summary>
        public int company_id { get; set; }
    }

    public class ApproveWaitingInfo
    {
        /// <summary>
        /// account id
        /// </summary>
        public int account_id { get; set; }

        /// <summary>
        /// register day
        /// </summary>
        public Nullable<DateTime> register_date { get; set; }

        /// <summary>
        /// type Message
        /// 1 : seft check
        /// 2 : check for member
        /// 3 : wait check
        /// </summary>
        public int type_message { get;set; }

        /// <summary>
        /// application form id
        /// </summary>
        public int application_form_id { get; set; }
        
        /// <summary>
        /// Email 
        /// </summary>
        public string email { get; set; }
    }
}

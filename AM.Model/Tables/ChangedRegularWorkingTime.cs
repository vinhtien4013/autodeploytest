﻿using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class ChangedRegularWorkingTime
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int account_id { get; set; }

        /// <summary>
        /// start_time:time
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public TimeSpan start_time { get; set; }

        /// <summary>
        /// end_time:time
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public TimeSpan end_time { get; set; }

        /// <summary>
        /// reason:nvarchar(100)
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public string reason { get; set; }

        /// <summary>
        /// rest_time:float(2)
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public double rest_time { get; set; }

        /// <summary>
        /// changed Monday flag : bit
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public bool mon_flg { get; set; }

        /// <summary>
        /// changed Tuesday flag : bit
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public bool tue_flg { get; set; }

        /// <summary>
        /// changed Wednesday flag : bit
        /// </summary>  
        /// <remarks>NOT NULL</remarks>
        public bool wed_flg { get; set; }

        /// <summary>
        /// changed Thursday flag : bit
        /// </summary>
        ///  <remarks>NOT NULL</remarks>
        public bool thu_flg { get; set; }

        /// <summary>
        /// changed Friday flag : bit
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public bool fri_flg { get; set; }

        /// <summary>
        /// lunch start time: time
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public TimeSpan lunch_time_from { get; set; }

        /// <summary>
        /// lunch end time: time
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public TimeSpan lunch_time_to { get; set; }

        /// <summary>
        /// apply start date : datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime apply_start_date { get; set; }

        /// <summary>
        /// apply end date : datetime
        /// </summary>
        public DateTime? apply_end_date { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

        /// <summary>
        /// Staff id
        /// </summary>
        public string staff_id { get; set; }
    }
}

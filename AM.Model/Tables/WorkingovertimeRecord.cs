﻿using System;

namespace AM.Model.Tables
{
    [Serializable]
    public class WorkingovertimeRecord
    {
        /// <summary>
        /// id:int
        /// </summary>
        /// <remarks>Constraints : PRIMARY KEY NOT NULL</remarks>
        public int id { get; set; }

        /// <summary>
        /// expect_start_over_time:time
        /// </summary>
        public string expect_start_over_time { get; set; }

        /// <summary>
        /// expect_end_over_time:time
        /// </summary>
        public string expect_end_over_time { get; set; }

        /// <summary>
        /// expect_total_over_time:float(2)
        /// </summary>
        public double expect_total_over_time { get; set; }

        /// <summary>
        /// total_over_timework:float(2)
        /// </summary>
        public double? total_over_timework { get; set; }

        /// <summary>
        /// decrease_time:float(2)
        /// </summary>
        public double decrease_time{ get; set; }

        /// <summary>
        /// compensatory_holiday:float(1)
        /// </summary>
        public double compensatory_holiday{ get; set; }

        /// <summary>
        /// overworking_time:float(2)
        /// </summary>
        public double overworking_time { get; set; }

        /// <summary>
        /// normal_overworking_time:float(2)
        /// </summary>
        public double normal_overworking_time { get; set; }

        /// <summary>
        /// holiday_overworking_time:float(2)
        /// </summary>
        public double holiday_overworking_time { get; set; }

        /// <summary>
        /// task_number_txt:text
        /// </summary>
        public string task_number_txt { get; set; }

        /// <summary>
        /// reason_txt:text
        /// </summary>
        public string reason_txt { get; set; }

        /// <summary>
        /// lateapplying_reason_text:text
        /// </summary>
        public string lateapplying_reason_text { get; set; }

        /// <summary>
        /// gap_reason:text
        /// </summary>
        public string gap_reason { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        /// <remarks>NOT NULL</remarks>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

    }
}

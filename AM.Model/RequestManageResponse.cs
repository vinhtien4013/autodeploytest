﻿using AM.Model.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AM.Model
{
    public class RequestManageResponse : AccountPaidHoliday
    {
        public string staff_id { get; set; }
    }
}

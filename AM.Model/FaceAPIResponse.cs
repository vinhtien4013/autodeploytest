﻿using AM.Model.Tables;
using System.Collections.Generic;
using System.Net;

namespace AM.Model
{
    public class FaceAPIResponse
    {
        /// <summary>
        /// error code return
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// status code return
        /// </summary>
        public string statusCode { get; set; }
        /// <summary>
        /// message error
        /// </summary>
        public string message { get; set; }
    }

    public class FaceDetectResponse
    {
        /// <summary>
        /// faceId response
        /// </summary>
        public string faceId { get; set; }
        public HttpStatusCode status { get; set; }
        public FaceAPIResponse error { get; set; }
    }

    public class FaceIdentifyResponse
    {
        /// <summary>
        /// personId response
        /// </summary>
        public string faceId { get; set; }
        public HttpStatusCode status { get; set; }
        public FaceAPIResponse error { get; set; }
        public List<CandidateIdentify> candidates { get; set; }
    }
    public class CandidateIdentify
    {
        public string personId { get; set; }
        public double confidence { get; set; }
    }

    public class FaceIdentifyRequest
    {
        public string personGroupId { get; set; }
        public List<string> faceIds { get; set; }
        public int maxNumOfCandidatesReturned { get; set; }
        public double confidenceThreshold { get; set; }
    }

    public class MessageRegistorResult
    {
        public Account account{ get; set; }
        public string message { get; set; }
    }

    public class CreatePersonResponse
    {
        /// <summary>
        /// personId return from api
        /// </summary>
        public string personId { get; set; }
        /// <summary>
        /// error object
        /// </summary>
        public ErrorResponseFaceAPI error { get; set; }
    }

    public class ErrorResponseFaceAPI
    {
        /// <summary>
        /// error code return
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// status code return
        /// </summary>
        public HttpStatusCode statusCode { get; set; }
        /// <summary>
        /// message error
        /// </summary>
        public string message { get; set; }
    }

    public class AddFaceResponse
    {
        /// <summary>
        /// persistedFaceId return from api
        /// </summary>
        public string persistedFaceId { get; set; }
        /// <summary>
        /// error object
        /// </summary>
        public ErrorResponseFaceAPI error { get; set; }
    }

    public class FaceAPIAddFaceBody
    {
        /// <summary>
        /// name
        /// </summary>
        public string url { get; set; }
    }

    public class FaceAPICreatePersonBody
    {
        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }
    }
    public class FaceAPICreatePersonGroupBody
    {
        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }
    }

    public class TrainFaceResponse
    {
        /// <summary>
        /// error object
        /// </summary>
        public ErrorResponseFaceAPI error { get; set; }
    }

    public class CommonResponseNoneBody
    {
        /// <summary>
        /// error object
        /// </summary>
        public ErrorResponseFaceAPI error { get; set; }
    }

    public class GroupListInfo
    {
        public string personGroupId { get; set; }
        public string name { get; set; }
        public string userData { get; set; }
        public string recognitionModel { get; set; }
    }

    public class GetListResponse
    {
        public List<GroupListInfo> list { get; set; }
        public CommonResponseNoneBody error { get; set; }
    }

    public class ProcessCompareGroupCompany
    {
        public HttpStatusCode StatusCode { get; set; }
        public string message { get; set; }
        public List<Company> inValidLstCompany { get; set; }
    }
}

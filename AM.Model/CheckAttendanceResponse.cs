﻿using System;

namespace AM.Model
{
    public class CheckAttendanceResponse
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; } // int
        
        /// <summary>
        /// staff_id
        /// </summary>
        public string staff_id { get; set; } // string

        /// <summary>
        /// paid_holiday_type
        /// </summary>
        public string paid_holiday_type { get; set; } // int
        
        /// <summary>
        /// expect_start_time
        /// </summary>
        public TimeSpan expect_start_time { get; set; } // time(0),>

        /// <summary>
        /// expect_end_time
        /// </summary>
        public TimeSpan expect_end_time { get; set; } // time(0),>
        
        /// <summary>
        /// actual_start_time
        /// </summary>
        public TimeSpan actual_start_time { get; set; } // time(0),>

        /// <summary>
        /// actual_end_time
        /// </summary>
        public TimeSpan actual_end_time { get; set; } // time(0),>

        /// <summary>
        /// rest_time
        /// </summary>
        public float rest_time { get; set; } // float,>

        /// <summary>
        /// over_time
        /// </summary>
        public float over_time { get; set; } // float,>

        /// <summary>
        /// working_hour
        /// </summary>
        public float working_hour { get; set; } // float,>

        /// <summary>
        /// Staff self check
        /// </summary>
        public int account_check_1 { get; set; }

        /// <summary>
        /// Staff first check
        /// </summary>
        public int account_check_2 { get; set; }

        /// <summary>
        /// Staff second check
        /// </summary>
        public int account_check_3 { get; set; }

        /// <summary>
        /// Staff third check
        /// </summary>
        public int account_check_4 { get; set; }

        /// <summary>
        /// Staff admin check
        /// </summary>
        public int account_check_5 { get; set; }

        /// <summary>
        /// Check 1
        /// </summary>
        public int Check_1 { get; set; }

        /// <summary>
        /// Check 2
        /// </summary>
        public int check_2 { get; set; }

        /// <summary>
        /// Check 3
        /// </summary>
        public int check_3 { get; set; }

        /// <summary>
        /// Check 4
        /// </summary>
        public int check_4 { get; set; }

        /// <summary>
        /// Check 5
        /// </summary>
        public int check_5 { get; set; }

        /// <summary>
        /// additional_rest_time
        /// </summary>
        public float additional_rest_time { get; set; }

        /// <summary>
        /// device_start_time
        /// </summary>
        public TimeSpan device_start_time { get; set; } // time(0),>

        /// <summary>
        /// device_end_time
        /// </summary>
        public TimeSpan device_end_time { get; set; } // time(0),>

        /// <summary>
        /// Is approved
        /// </summary>
        public bool is_approved { get; set; }

        /// <summary>
        /// Email 1
        /// </summary>
        public string email_1 { get; set; }

        /// <summary>
        /// Email 2
        /// </summary>
        public string email_2 { get; set; }

        /// <summary>
        /// Email 3
        /// </summary>
        public string email_3 { get; set; }

        /// <summary>
        /// Email 4
        /// </summary>
        public string email_4 { get; set; }

        /// <summary>
        /// Email 5
        /// </summary>
        public string email_5 { get; set; }

        /// <summary>
        /// Application sequence
        /// </summary>
        public int application_sequence { get; set; }

        /// <summary>
        /// max check sequence
        /// </summary>
        public int last_sequence { get; set; }
    }
}

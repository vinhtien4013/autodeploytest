﻿namespace AM.Model
{
    public class ParameterSearchAccount
    {
        /// <summary>
        /// company_id:string
        /// </summary>
        public string company_id { get; set; }
        /// <summary>
        /// search_Finger_ID:string
        /// </summary>
        public string search_Finger_ID { get; set; }
        /// <summary>
        /// search_Division_Name:string
        /// </summary>
        public string search_Division_Name { get; set; }
        /// <summary>
        /// search_Staff_ID:string
        /// </summary>
        public string search_Staff_ID { get; set; }
        /// <summary>
        /// search_Staff_Name:string
        /// </summary>
        public string search_Staff_Name { get; set; }
    }
}

﻿
namespace AM.Model.Const
{
    public class SLACT_CONST
    {
        /// <summary>
        /// Token
        /// </summary>
        public static string TOKEN = "token";

        /// <summary>
        /// Slack channel name 
        /// </summary>
        public static string CHANNELNM = "channel_name";

        /// <summary>
        /// Slack user name
        /// </summary>
        public static string USERNM = "user_name";

        /// <summary>
        /// Content in command
        /// </summary>
        public static string TEXT = "text";

        /// <summary>
        /// manual text
        /// </summary>
        public static string MANUAL_TEXT = "You could use one of them below to register to Attendance System\n 1. /bw [off/offm/offa] [yyyymmdd] [(reason)]\n 2. /bw [late] [hhmm] [(reason)]\n 3. /bw [change] [yyyymmdd] from [hhmm] to [hhmm] [(reason)]\n 4. /bw [rest] [yyyymmdd] from [hhmm] to [hhmm], from [hhmm] to [hhmm] [(reason)]";

        /// <summary>
        /// detail manual case 1
        /// </summary>
        public static string DETAIL_MANUAL_1 = "About case 1, Users use paid off or not is automatically calculated. If they have paid holiday, the API decreases it. If not so, it is no paid holiday.";

        /// <summary>
        /// detail manual case 2
        /// </summary>
        public static string DETAIL_MANUAL_2 = "About case 2, User use this message is in a hurry, like riding a motorbike. No need to push the date. Only push time and reason.";

        /// <summary>
        /// detail manual case 3
        /// </summary>
        public static string DETAIL_MANUAL_3 = "About case 3, User use it for changing working time instead of talking to their leaders.";

        /// <summary>
        /// detail manual case 4
        /// </summary>
        public static string DETAIL_MANUAL_4 = "About case 4, User use it to get additional rest time instead of talking to their leaders. You can use rest command (from...to) maximum 3 times.";

        /// <summary>
        /// domain  mail
        /// </summary>
        public static string MAIL_DOMAIN = "@briswell-vn.com";
    }

    public class SLACK_RESPONSE_TYP
    {
        /// <summary>
        /// ephemeral : be seen by yourself
        /// </summary>
        public static string EPHEMERAL = "ephemeral";

        /// <summary>
        /// in_channel : be seen by all people in channel
        /// </summary>
        public static string INCHANNEL = "in_channel";
    }

    /// <summary>
    /// command type : determined by text
    /// 1 : register account paid holiday
    /// 2 : be late in day
    /// 3 : change working time in an specific
    /// 4 : register additional rest time in working time
    /// 5 : help
    /// </summary>
    public enum COMMANDTYP
    {
        OFFALLDAY = 1,
        OFFHALFMORNING = 2,
        OFFHALFAFTER = 3,
        LATE = 4,
        CHANGE = 5,
        REST = 6,
        HELP = 7
    }

    public class SLACK_COMMAND_TYP
    {
        /// <summary>
        /// off all day
        /// </summary>
        public static string OFFALLDAY = "off";

        /// <summary>
        /// off morning
        /// </summary>
        public static string OFFHALFMORNING = "offm";

        /// <summary>
        /// off afternoon
        /// </summary>
        public static string OFFHALFAFTERNOON = "offa";

        /// <summary>
        /// late
        /// </summary>
        public static string LATE = "late";

        /// <summary>
        /// change regular workingtime
        /// </summary>
        public static string CHANGEREGULAR = "change";

        /// <summary>
        /// add rest time
        /// </summary>
        public static string REST = "rest";

        /// <summary>
        /// help command
        /// </summary>
        public static string HELP = "help";
    }
}

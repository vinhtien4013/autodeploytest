﻿namespace AM.Model.Const
{
    /**************************************************************************/
    /// <summary>
    /// 定数定義【正規表現】
    /// </summary>
    /// <remarks>
    /// Copyright(C) 2014 System Consultant Co., Ltd. All Rights Reserved.
    /// </remarks>
    /**************************************************************************/
    public class REGEX
    {
        /// <summary>
        /// 数値チェック用正規表現
        /// </summary>
        public const string NUMBER_REGEX = "^[0-9]+$";

        /// <summary>
        /// 半角英数チェック用正規表現
        /// </summary>
        public const string HALF_CHAR_REGEX = "^[a-zA-Z0-9]+$";

        /// <summary>
        /// 半角英数記号チェック用正規表現
        /// </summary>
        public const string HALF_CHAR_WITH_SYMBOL_REGEX = "^[a-zA-Z0-9!-/:-@\\[-`{-~]+$";

        /// <summary>
        ///全角カナチェック用正規表現 
        /// </summary>
        public const string FULL_CHAR_KANA_REGEX = "^[ァ-ヶー0-9]+$";

        /// <summary>
        ///全角カナチェック用正規表現 (not including digits)
        /// </summary>
        public const string FULL_CHAR_KANA_NOT_DIGITS_REGEX = "^[ァ-ヶー]+$";

        /// <summary>
        /// かな/カナのみチェック用正規表現
        /// </summary>
        public const string FULL_CHAR_HIRAGANA_KANA_NOT_DIGITS_REGEX = "^[ぁ-んァ-ンー]+$";

        /// <summary>
        /// 半角カナチェック用正規表現 
        /// </summary>
        public const string HALF_CHAR_KANA_REGEX = "^[ｦ-ﾝﾞﾟ]+$";

        /// <summary>
        ///カタカナ名称チェック用正規表現 
        /// </summary>
        public const string KANA_NAME_REGEX = "^[ァ-ヶーｦ-ﾝﾞﾟa-zA-Z!-&(-/:;=?@[-`{-~ ']+$";

        /// <summary>
        ///漢字名称チェック用正規表現 
        /// </summary>
        public const string KANJI_NAME_REGEX = "^[ァ-ヶーｦ-ﾝﾞﾟa-zA-Z!-&(-/:;=?@[-`{-~]+$";

        /// <summary>
        /// メールアドレスチェック用正規表現
        /// "^[a-zA-Z0-9_-]+(?:\\.[a-zA-Z0-9_-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$"
        /// "@"^[\x20-\x7D]*@([\x20-\x7E]+\.)+[\x20-\x7E]+$"
        /// </summary>
        public const string MAIL_REGEX = "^[A-Za-z0-9_\\.\\-\\/\\?\\!\\%\\&\\~\\#\\$\\'\\*\\=\\+\\^\\`\\{\\|\\}\\+]+@([A-Za-z0-9_\\-]+\\.)+[A-Za-z0-9_\\-]+$";

        /// <summary>
        /// パスワードチェック用正規表現
        /// 
        /// ＜JOBNETパスワードポリシー＞
        /// ・1つ以上の英字を含む（大文字でも小文字でもOK）
        /// ・1つ以上の数字を含む
        /// ・記号を利用可能
        /// ・利用可能な記号は   !#$%&()=~|`{+*}?_^@[:]./
        /// ・8文字以上16文字以内
        /// </summary>
        public const string PASSWORD_REGEX = "^(?=.*[a-zA-Z])(?=.*?[0-9])[a-zA-Z0-9!#\\$%&()=~\\|`{\\+\\*}\\?_\\^@\\[:\\]\\./]{8,16}$";

        /// <summary>
        /// URL用正規表現
        /// </summary>
        public const string URL_REGEX = "^(https?|ftp)(:\\/\\/[-_.!~*\\'()a-zA-Z0-9;\\/?:\\@&=+\\$,%#]+)$";

        /// <summary>
        /// 請求年月正規表現
        /// </summary>
        public const string BILLING_YEARS_REGEX = "^(19|20)[0-9]{2}/(0[1-9]|1[0-2])$";

        /// <summary>
        /// 電話番号正規表現
        /// </summary>
        public const string PHONE_REGEX = "^[0-9-+]+$";

        /// <summary>
        /// ゆうちょ記号情報チェック用正規表現
        /// </summary>
        public const string JAPAN_POST_BANK_KIGOU_REGEX = "^1[0-9]+0$";

        /// <summary>
        /// ゆうちょ番号情報チェック用正規表現
        /// </summary>
        public const string JAPAN_POST_BANK_BANGOU_REGEX = "^[0-9]+1$";

        /// <summary>
        /// 口座名義チェック用正規表現
        /// </summary>
        public const string KOUZA_MEIGI_REGEX = @"^[ァ-ヶA-Z。-゜\d\s\(\)\.\-]*$";

        /// <summary>
        /// 数字が含まれていないかチェックする正規表現
        /// </summary>
        public const string NOT_NUM_REGEX = "^[^0-9０-９]+$";

        /// <summary>
        /// 
        /// </summary>
        public const string ROMANM_REGEX = "^[a-zA-Z0-9 ]+$";

    }
}

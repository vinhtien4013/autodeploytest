﻿namespace AM.Model.Const
{
    public class ConstType
    {
        public class CONST_VALUE
        {
            /// <summary>
            /// Off work all days
            /// </summary>
            public const int ALL = 1;
            /// <summary>
            /// Off work haft the morning
            /// </summary>
            public const int HALF_MORNING = 2;
            /// <summary>
            /// Off work haft the afternoon
            /// </summary>
            public const int HALF_AFTERNOON = 3;
            /// <summary>
            /// Registor all days
            /// </summary>
            public const double REGISTOR_ALL = 1;
            /// <summary>
            /// Registor half days
            /// </summary>
            public const double REGISTOR_HALF = 0.5;
            /// <summary>
            /// Access Token
            /// </summary>
            public const string ACCESS_TOKEN = "AccessToken";
            /// <summary>
            /// Access Time
            /// </summary>
            public const string ACCESS_TIME = "AccessTime";
            /// <summary>
            /// Name of action to post message to Slack
            /// </summary>
            public const string ACTION_NAME = "postMessageToSlackBWV";
            /// <summary>
            /// Content-type: application/json
            /// </summary>
            public const string CONTENT_TYPE = "application/json";
            /// <summary>
            /// Server Account Id
            /// </summary>
            public const int SERVER_ACCOUNT = 1;
            /// <summary>
            /// Default Expect Start Time
            /// </summary>
            public const string DEFAULT_START_TIME ="08:00";
            /// <summary>
            /// Default HALF TIME NEED TO WORK
            /// </summary>
            public const string DEFAULT_HALF_TIME_WORK = "04:00";
            /// <summary>
            /// Default Expect End Time
            /// </summary>
            public const string DEFAULT_END_TIME = "17:00";
            /// <summary>
            /// Default Rest Time
            /// </summary>
            public const string DEFAULT_REST_TIME = "1";
            /// <summary>
            /// Default Mid Time
            /// </summary>
            public const string DEFAULT_MID_TIME = "13:00";
            /// <summary>
            /// Default haft Rest Time
            /// </summary>
            public const string HALF_REST_TIME = "0";

            /// <summary>
            /// Default Slack time out
            /// </summary>
            public const int SLACK_TIME_OUT = 10;

            /// <summary>
            /// Default Lated start time can change morning
            /// </summary>
            public const string LATED_START_TIME_MORNING = "09:00";

            /// <summary>
            /// Default Lated start time can change afternoon
            /// </summary>
            public const string LATED_START_TIME_AFTERNOON = "14:00";

            /// <summary>
            /// Default 00:00:00
            /// </summary>
            public const string NONE_TIME = "00:00:00";

            /// <summary>
            /// Default start rest time
            /// </summary>
            public const string DEFAULT_START_RESTTIME = "11:30";

            /// <summary>
            /// Default end rest time
            /// </summary>
            public const string DEFAULT_END_RESTTIME = "12:30";

            /// <summary>
            /// Default max end working time
            /// </summary>
            public const string DEFAULT_MAX_END_WORKINGTIME = "18:00";
            
            /// <summary>
            /// Default min start working time
            /// </summary>
            public const string DEFAULT_MIN_START_WORKINGTIME = "07:30";

            /// <summary>
            /// Default session company value 1 
            /// </summary>
            public const string SESSION_COMPANY_DEFAULT = "1";

            /// <summary>
            /// Default division
            /// </summary>
            public const string DEFAULT_DIVISION = "Default Devision";

            /// <summary>
            /// Default staff name
            /// </summary>
            public const string DEFAULT_STAFF_NAME = "Default Account";

            /// <summary>
            /// Default staff id
            /// </summary>
            public const string DEFAULT_STAFF_ID = "00000";

            /// <summary>
            /// DEFAULT_DIV_CODE
            /// </summary>
            public const string DEFAULT_DIV_CODE = "MNG0001";

            /// Default header for Face Azure API
            /// </summary>
            public const string Face_API_KEY = "Ocp-Apim-Subscription-Key";

            /// <summary>
            /// Default header for Face Azure API
            /// </summary>
            public const string BINARY_CONTENT_TYPE = "application/octet-stream";

            /// <summary>
            /// Default max num return of candidate
            /// </summary>
            public const int MAX_NUM_CANDIDATE_RETURN = 1;

            /// <summary>
            /// Default value confidence
            /// </summary>
            public const double CONFIDENCE_THRESHOLD = 0.7;

            public const string COMPANY_TOKEN = "CompanyToken";

            /// <summary>
            /// Value Code for response delete face that can skip
            /// </summary>
            public const string ErrorDeleteCodeCanSkip = "PersistedFaceNotFound";

            /// <summary>
            /// default language
            /// </summary>
            public const string DEFAULT_LANGUAGE = "en";

            /// <summary>
            /// Edit a row
            /// </summary>
            public const string EDIT_ROW = "1";

            /// <summary>
            /// Add a row
            /// </summary>
            public const string ADD_ROW = "2";

            /// <summary>
            /// Delete a row
            /// </summary>
            public const string DEL_ROW = "3";

            /// <summary>
            /// Status post message slack successfully
            /// </summary>
            public const string STATUS_SLACK_SUCCESS = "ok";

            /// <summary>
            /// Alpha character
            /// </summary>
            public const char ALPHA = '@';
            /// <summary>
            /// Received Request type
            /// </summary>
            public const int RECEIVED_REQUEST = 0;

            /// <summary>
            /// Response Request type
            /// </summary>
            public const int RESPONSE_REQUEST = 1;
        }

        #region Processing status
        public enum ProcessingStatusEnum
        {
            /// <summary>
            /// Use paid holiday
            /// </summary>
            UsePaidHoliday = 0,
            /// <summary>
            /// Add paid holiday
            /// </summary>
            AddPaidHoliday = 1,
            /// <summary>
            /// Add monthly paid holiday
            /// </summary>
            AddMonthPaidHoliday = 2
        }
        #endregion

        #region Paid holiday type
        public enum PaidHolidayTypeEnum
        {
            /// <summary>
            /// All day
            /// </summary>
            AllDay = 1,
            /// <summary>
            /// Half-day morning
            /// </summary>
            HalfDayMorning = 2,
            /// <summary>
            /// Half-day afternoon
            /// </summary>
            HalfDayAfternoon = 3
        }
        #endregion

        #region Rounding Time Type
        public enum RoundingTimeTypeEnum
        {
            /// <summary>
            /// Up to 5 minutes
            /// </summary>
            UP = 1,
            /// <summary>
            /// Low to 5 minutes
            /// </summary>
            DOWN = 2
        }
        #endregion

        #region Check in or check out
        public enum CheckInOutEnum
        {
            /// <summary>
            /// Check in (going)
            /// </summary>
            CheckIn = 1,
            /// <summary>
            /// Check out (leaving)
            /// </summary>
            CheckOut = 2
        }
        #endregion

        #region Confirmation Type
        public enum ConfirmationType
        {
            /// <summary>
            /// Confirm for working time
            /// </summary>
            WorkingTime = 0,
            /// <summary>
            /// Confirm for working over time
            /// </summary>
            WorkingOverTime = 1,
            /// <summary>
            /// Confirm for account paid holiday
            /// </summary>
            PaidHoliday = 2
        }
        #endregion

        #region CheckStatus
        public enum CheckStatus
        {
            /// <summary>
            /// Need Check
            /// </summary>
            NeedCheck = 0,
            /// <summary>
            /// Agreed
            /// </summary>
            Agreed = 1,
            /// <summary>
            /// Reject
            /// </summary>
            Reject = 2,
            /// <summary>
            /// Pending
            /// </summary>
            Pending = 3,
            /// <summary>
            /// Skipped
            /// </summary>
            Skipped = 4
        }
        #endregion

        #region "Admin_div_flg"
        public class ADMIN_DIV_FLG
        {
            /// <summary>
            /// in admin group
            /// </summary>
            public const string IN_ADMIN_GROUP = "1";

            /// <summary>
            /// not in admin group
            /// </summary>
            public const string NOTIN_ADMIN_GROUP = "0";
        }
        #endregion

        #region "Date_Flg"
        public class DATE_FLG
        {
            /// 0 : Full-day none Paid holiday date
            public const double FulldayOffNoSalary = 0;
            /// 0,5 : Half-day none paid holiday date
            public const double HalfdayOffNoSalary = 0.5;
            /// 1: full-day working date
            public const double FulldayWorking = 1;
            /// 2: full-day paid holiday date / holiday date
            public const double FulldayOffHaveSalary = 2;
            /// 3: haft-day paid holiday date
            public const double HalfdayOffHaveSalary = 3;
            /// 4: weekend date
            public const double Weekendate = 4;
            /// 5: holiday date
            public const double Holidaydate = 5;

        }
        #endregion

        #region "Typed_Deleted"
        public class TYPE_DELETE
        {
            /// recover
            public const string Recover = "Recover";

            /// deleted
            public const string Deleted = "Deleted";


        }
        #endregion

        #region "Pattern"
        public class Pattern
        {
            //Pattern register change working time
            public const string PatternRegisterChange = @"^change (?<date>\d{8}) from (?<timeFrom>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) to (?<timeTo>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) (?<reason>\w.+)";

            //Pattern register paid holiday request
            public const string PatternRegisterOff = @"^(off|offa|offm) (?<date>\d{8}) (?<reason>\w.+)";

            //Pattern register late
            public const string PatternRegisterLate = @"^late (?<timeLate>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) (?<reason>\w.+)";

            //Pattern register rest time
            public const string PatternRegisterRest = @"^rest (?<date>\d{8}) from (?<timeFrom>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) to (?<timeTo>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5)))([' ']|([','] from (?<timeFrom>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) to (?<timeTo>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5)))([' ']|([','] from (?<timeFrom>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) to (?<timeTo>([0-1]\d[0-5](0|5)|2[0-3][0-5](0|5))) ))))(?<reason>\w.+)";

            //Pattern help
            public const string PatternHelp = @"^help$";

        }
        #endregion

        #region "Paid holiday type string"
        public class PaidHolidayType
        {
            /// <summary>
            /// All day
            /// </summary>
            public const string AllDay = "all day";
            /// <summary>
            /// Half-day morning
            /// </summary>
            public const string HalfDayMorning = "morning";
            /// <summary>
            /// Half-day afternoon
            /// </summary>
            public const string HalfDayAfternoon = "afternoon";

        }
        #endregion

        #region "Approved flag"
        public class APPROVE_FLG
        {
            /// <summary>
            /// the request have been approved
            /// </summary>
            public const string FLG_ON = "1";

            /// <summary>
            /// the request have not yet approved
            /// </summary>
            public const string FLG_OFF = "0";
        }
        #endregion

        #region Login
        public class LOGIN_TYPE
        {
            /// <summary>
            /// All login method
            /// </summary>
            public const int ALL_LOGIN_METHOD = 0;
            /// <summary>
            /// Login manual
            /// </summary>
            public const int MANUAL_LOGIN = 1;
            /// <summary>
            /// Login by google
            /// </summary>
            public const int GOOGLE_LOGIN = 2;
            /// <summary>
            /// Login by finger
            /// </summary>
            public const int FINGER_LOGIN = 3;
            /// <summary>
            /// Forgot pass
            /// </summary>
            public const int FORGOT_PASS = 4;
        }
        #endregion

        #region "Pass check"
        public class CheckExistDivision
        {
            /// <summary>
            /// Check Exits division done no error
            /// </summary>
            public const string Pass = "Pass";
        }
        #endregion

        #region "modify mode"
        public class ModifyMode
        {
            /// <summary>
            /// Update mode
            /// </summary>
            public const string UpdateMode = "1";

            /// <summary>
            /// InsertMode
            /// </summary>
            public const string InSertMode = "2";
            
            /// <summary>
            /// DeleteMode
            /// </summary>
            public const string DeleteMode = "3";
        }
        #endregion

        #region "Sex"
        /// <summary>
        /// sex
        /// </summary>
        public class SEX
        {
            /// <summary>
            /// sex: Male
            /// </summary>
            public const string MALE = "0";

            /// <summary>
            /// sex: Female
            /// </summary>
            public const string FEMALE = "1";
        }
        #endregion

        #region "PossesiveNoun"
        /// <summary>
        /// POSSESIVENOUN
        /// </summary>
        public class POSSESIVENOUN
        {
            /// <summary>
            /// her
            /// </summary>
            public const string HER = "her";

            /// <summary>
            /// his
            /// </summary>
            public const string HIS = "his";
        };
        #endregion
    }
}

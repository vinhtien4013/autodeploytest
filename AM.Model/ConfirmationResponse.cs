﻿using System;

namespace AM.Model
{
    /// <summary>
    /// Confirmation response class
    /// </summary>
    public class ConfirmationResponse
    {
        /// <summary>
        /// Application id
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Account id
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Check status
        /// </summary>
        public int CheckStatus { get; set; }

        /// <summary>
        /// Reason
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Application Sequence
        /// </summary>
        public int ApplicationSequence { get; set; }

        /// <summary>
        /// Check Sequence
        /// </summary>
        public int CheckSequence { get; set; }
    }
}

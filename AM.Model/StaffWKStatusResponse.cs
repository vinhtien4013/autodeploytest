﻿using System;

namespace AM.Model
{
    public class StaffWKStatusResponse
    {
        /// <summary>account id</summary>
        public string account_id { get; set; }
        /// <summary>staff id</summary>
        public string staff_id { get; set; }
        /// <summary>staff name</summary>
        public string staff_name { get; set; }
        /// <summary>division name </summary>
        public string division_name { get; set; }
        /// <summary> date </summary>
        public DateTime date { get; set; }
        /// <summary> expect start time </summary>
        public TimeSpan expect_start_time { get; set; }
        /// <summary> expect end time </summary>
        public TimeSpan expect_end_time { get; set; }
        /// <summary> actual start time </summary>
        public TimeSpan actual_start_time { get; set; }
        /// <summary> actual end time </summary>
        public TimeSpan actual_end_time { get; set; }
        /// <summary> paid_holiday_type 0 1 2 3</summary>
        public int paid_holiday_type { get; set; }
        /// <summary>national_date</summary>
        public bool national_date { get; set; }
        /// <summary>place remote work</summary>
        public int place { get; set; }
    }
}

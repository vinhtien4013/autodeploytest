﻿using AM.Model.Tables;

namespace AM.Model
{
    public class EditAccountModel : Account
    {
        /// <summary>
        /// division_name:nvarchar(MAX)
        /// </summary>
        public string division_name { get; set; }
    }
}

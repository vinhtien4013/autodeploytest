﻿using System;

namespace AM.Model
{
    public class CheckPaidHolidayResponse
    {
        /// <summary>
        /// id:int
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// account_id:int
        /// </summary>
        public int account_id { get; set; }

        /// <summary>
        /// application_datetime:datetime
        /// </summary>
        public DateTime application_datetime { get; set; }

        /// <summary>
        /// registor_days:float(1)
        /// </summary>
        public double registor_days { get; set; }

        /// <summary>
        /// processing_status:tinyint
        /// </summary>
        public byte processing_status { get; set; }

        /// <summary>
        /// conpensatory_holiday_flg:bit
        /// </summary>
        public bool conpensatory_holiday_flg { get; set; }

        /// <summary>
        /// reason_txt:varchar(100)
        /// </summary>
        public string reason_txt { get; set; }

        /// <summary>
        /// paid_holiday_type: int?
        /// </summary>
        public int? paid_holiday_type { get; set; }

        /// <summary>
        /// status: int
        /// </summary>
        public int approve_status { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }

        /// <summary>
        /// account_check_id_1: int
        /// </summary>
        public int? account_check_1 { get; set; }

        /// <summary>
        ///  account_check_id_2: int
        /// </summary>
        public int? account_check_2 { get; set; }

        /// <summary>
        /// account_check_id_3: int
        /// </summary>
        public int? account_check_3 { get; set; }

        /// <summary>
        ///  account_check_id_4: int
        /// </summary>
        public int? account_check_4 { get; set; }

        /// <summary>
        ///  account_check_id_5: int
        /// </summary>
        public int? account_check_5 { get; set; }

        /// <summary>
        /// check_1:int
        /// </summary>
        public int? check_1 { get; set; }


        /// <summary>
        ///  check_2:int
        /// </summary>
        public int? check_2 { get; set; }

        /// <summary>
        ///  check_3:int
        /// </summary>
        public int? check_3 { get; set; }

        /// <summary>
        ///  check_4:int
        /// </summary>
        public int? check_4 { get; set; }

        /// <summary>
        ///  check_5:int
        /// </summary>
        public int? check_5 { get; set; }

        /// <summary>
        /// max_sequence : int
        /// </summary>
        public int max_sequence { get; set; }

        /// <summary>
        /// email_1 : string
        /// </summary>
        public string email_1 { get; set; }

        /// <summary>
        /// email_2 : string
        /// </summary>
        public string email_2 { get; set; }

        /// <summary>
        /// email_3 : string
        /// </summary>
        public string email_3 { get; set; }

        /// <summary>
        /// email_4 : string
        /// </summary>
        public string email_4 { get; set; }

        /// <summary>
        /// email_5 : string
        /// </summary>
        public string email_5 { get; set; }
    }
}

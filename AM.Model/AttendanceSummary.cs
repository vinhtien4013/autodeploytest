﻿
using System;

namespace AM.Model
{
    public class AttendanceSummary
    {
        /// <summary>account_id</summary>
        public int Accountid { get; set; }

        /// <summary>staff_id</summary>
        public string Staffid { get; set; }

        /// <summary>full-day None Paid holiday Date Number </summary>
        public int Fulldayoff { get; set; }

        /// <summary>half-day None Paid holiday Date Number</summary>
        public int Halfdayoff { get; set; }

        /// <summary>full-day Paid holiday Date Number</summary>
        public int Fulldaypaidholiday { get; set; }

        /// <summary>half-day Paid holiday Date Number</summary>
        public int Halfdaypaidholiday { get; set; }

        /// <summary>absent time</summary>
        public double Absenttime { get; set; }

        /// <summary>over time</summary>
        public double Overtime { get; set; }

        /// <summary>holiday number</summary>
        public int HolidayNum { get; set; }

        /// <summary>weekend day number</summary>
        public int WeekendNum { get; set; }
    }

    public class AttendanceSummaryDetail
    {
        /// <summary>account_id</summary>
        public int Accountid { get; set; }

        /// <summary>staff_id</summary>
        public string Staffid { get; set; }

        /// <summary>date</summary>
        public DateTimeOffset Workdate { get; set; }

        /// <summary>Date name</summary>
        public string Shortdatename { get; set; }

        /// <summary>Date Flag</summary>
        /// 0 : Full-day none Paid holiday date
        /// 0,5 : Half-day none paid holiday date
        /// 1: full-day working date
        /// 2: full-day paid holiday date 
        /// 3: haft-day paid holiday date
        /// 4: weekend date
        /// 5: holiday date
        public float Dateflg { get; set; }

        /// <summary>absent time</summary>
        public double Absenttime { get; set; }

        /// <summary>over time</summary>
        public double Overtime { get; set; }

        /// <summary>
        /// 0: need check
        /// 1: approved
        /// 2: rejected
        /// </summary>
        public int Approveflg { get; set; }

        /// <summary>
        /// true : forgot punch in/ punch out
        /// false : punched in/ punched out
        /// </summary>
        public bool Notpunchinout { get; set; }
    }
}

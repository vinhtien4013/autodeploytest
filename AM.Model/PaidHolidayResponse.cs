﻿using System;
using System.Collections.Generic;

namespace AM.Model
{
    public class PaidHolidayResponse
    {
        /// <summary>
        /// account_id:int
        /// </summary>
        public int account_id { get; set; }

        /// <summary>
        /// staff_id:string
        /// </summary>
        public string staff_id { get; set; }

        /// <summary>
        /// total_paid_holiday : double
        /// </summary>
        public double total_paid_holiday { get; set; }
    }
    public class PaidHolidayRequestManageResponse
    {
        /// <summary>
        /// id:int
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// account_id:int
        /// </summary>
        public int account_id { get; set; }

        /// <summary>
        /// application_datetime:datetime
        /// </summary>
        public DateTime application_datetime { get; set; }

        /// <summary>
        /// registor_days:float(1)
        /// </summary>
        public double registor_days { get; set; }

        /// <summary>
        /// processing_status:tinyint
        /// </summary>
        public byte processing_status { get; set; }

        /// <summary>
        /// conpensatory_holiday_flg:bit
        /// </summary>
        public bool conpensatory_holiday_flg { get; set; }

        /// <summary>
        /// reason_txt:varchar(100)
        /// </summary>
        public string reason_txt { get; set; }

        /// <summary>
        /// paid_holiday_type: int?
        /// </summary>
        public int? paid_holiday_type { get; set; }

        /// <summary>
        /// status: int
        /// </summary>
        public int approve_status { get; set; }

        /// <summary>
        /// created_date:datetime
        /// </summary>
        public DateTime created_date { get; set; }

        /// <summary>
        /// created_account_id:int
        /// </summary>
        public int created_account_id { get; set; }

        /// <summary>
        /// updated_date:datetime
        /// </summary>
        public DateTime updated_date { get; set; }

        /// <summary>
        /// updated_account_id:int
        /// </summary>
        public int updated_account_id { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// deleted_account_id:int
        /// </summary>
        public int? deleted_account_id { get; set; }
    }

    public class RequestManageInputInfo
    {
        public RequestManageInputInfo()
        {
            requestManageList = new List<RequestManageInputDetail>();
        }
        public List<RequestManageInputDetail> requestManageList { get; set; }
    }

    public class RequestManageInputDetail
    {
        /// <summary>
        /// id : int
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// account_id: int
        /// </summary>
        public int account_id { get; set; }

        /// <summary>
        /// updated_account_id: int
        /// </summary>
        public int updated_account_id { get; set; }

        /// <summary>
        /// created_account_id: int
        /// </summary>
        public int created_account_id { get; set; }

        /// <summary>
        /// application_datetime : Datetime
        /// </summary>
        public DateTime application_datetime { get; set; }

        /// <summary>
        /// paid_holiday_type: int
        /// Allow null
        /// </summary>
        public int? paid_holiday_type { get; set; }

        /// <summary>
        /// reason_txt : string
        /// </summary>
        public string reason_txt { get; set; }

        /// <summary>
        /// deleted_date:datetime
        /// </summary>
        public Nullable<DateTime> deleted_date { get; set; }

        /// <summary>
        /// oldId : previous id
        /// Case there was existed a request before
        /// </summary>
        public string oldId { get; set; }
    }
}

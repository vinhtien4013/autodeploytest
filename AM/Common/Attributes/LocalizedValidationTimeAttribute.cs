
using System;

namespace AM.Common.Attributes
{
    /// <summary>
    /// LocalizedRegularExpressionAttributeを時刻(HH:mm)検証用に拡張したクラス
    /// 内部で正規表現検証コントロールを使用しています。
    /// </summary>
    /// <remarks></remarks>
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public sealed class LocalizedValidationTimeAttribute : LocalizedRegularExpressionAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>
        /// ・時刻の形式は"HH:mm"です。
        /// ・時刻検証用の正規表現を内部で自動作成します。
        /// </remarks>
        public LocalizedValidationTimeAttribute() : base("([0-1][0-9]|2[0-3]):([0-5][0-9])")
        {
        }
    }
}
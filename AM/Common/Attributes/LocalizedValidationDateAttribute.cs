using AM.Core.Util;
using AM.Common.ConstValue;
using System.Web;
using System;
using System.ComponentModel.DataAnnotations;

namespace AM.Common.Attributes
{
    /// <summary>
    /// 日付検証クラス
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public sealed class LocalizedValidationDateAttribute : ValidationAttribute
    {
        #region "定数"
        /// <summary>
        /// 検証タイプ
        /// </summary>
        private const string _validationType = "localizeddate";
        #endregion

        #region "コンストラクタ"
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>
        /// ・エラーメッセージは固定で"GlobalMessage"の"ErrMsgInputRegularExpression"を使用します。
        /// ・日付の形式は"yyyy/MM/dd"です。
        /// ・日付検証用の正規表現を内部で自動作成します。
        /// ・有効な日付は1900-2099までです。
        /// </remarks>
        public LocalizedValidationDateAttribute() : base(_validationType)
        {
            //エラーメッセージセット({0}を正しく入力してください。)
            string resourceKey = "ErrMsgInputRegularExpression";
            this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.GlobalMessage, resourceKey), resourceKey);
        }
        #endregion

        #region "IsValid: 日付検証"
        /// <summary>
        /// 日付検証
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public override bool IsValid(object value)
        {
            //未入力はOK
            if (value == null)
                return true;
            //日付以外はNG
            if (DateTimeUtil.IsDate(value))
                return true;
            return false;
        }
        #endregion
    }
}
﻿using System.Web;
using System.ComponentModel.DataAnnotations;
using AM.Core.Util;
using AM.Common.ConstValue;
using System;

namespace AM.Common.Attributes
{
    /// <summary>
    /// ErrorMessageをリソースから取得するように拡張したクラス
    /// </summary>
    /// <remarks></remarks>
    [System.AttributeUsage(AttributeTargets.All, Inherited = true)]
    public sealed class LocalizedRequiredAttribute : RequiredAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>
        /// エラーメッセージは固定で"GlobalMessage"の"ErrMsgInputMust"を使用します。
        /// </remarks>
        public LocalizedRequiredAttribute() : base()
        {
            string resourceKey = "ErrMsgInputMust";
            this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.GlobalMessage, resourceKey), resourceKey);
        }
    }
}
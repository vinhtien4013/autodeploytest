﻿using AM.Common.ConstValue;
using AM.Core.Util;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Web;

namespace AM.Common.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.All, Inherited = true)]
    public sealed class LocalizedCompareAttribute : CompareAttribute
    {
        private string resourceKey;
        public string ResourceKey
        {
            get { return resourceKey; }
        }
        public LocalizedCompareAttribute(string otherproperty, string resourceKey) : base(otherproperty)
        {
            this.resourceKey = resourceKey;
            this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.SlackMessage, resourceKey), resourceKey);
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using AM.Core.Util;
using AM.Common.ConstValue;

namespace AM.Common.Attributes
{
    /// <summary>
    /// ErrorMessageをリソースから取得するように拡張したクラス
    /// </summary>
    /// <remarks>
    /// 日付型のチェックなどが必要な時は適宜コンストラクタを増やしてください
    /// </remarks>
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = true)]
    public sealed class LocalizedRangeAttribute : RangeAttribute
    {
        public LocalizedRangeAttribute(double minimum, double maximum) :  base(minimum, maximum)
        {
            string resourceKey = "ErrMsgInputRange";
            string msg = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.GlobalMessage, resourceKey), resourceKey);
            this.ErrorMessage = msg.Replace("$minimum$", minimum.ToString()).Replace("$maximum$", maximum.ToString());
        }
    }
}
﻿using System.Web;
using System.ComponentModel.DataAnnotations;
using AM.Core.Util;
using AM.Common.ConstValue;
using System;

namespace AM.Common.Attributes
{
    /// <summary>
    /// ErrorMessageをリソースから取得するように拡張したクラス
    /// </summary>
    /// <remarks></remarks>
    [System.AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public sealed class LocalizedStringLengthAttribute : StringLengthAttribute
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>
        /// エラーメッセージは固定で"GlobalMessage"の"ErrMsgInputLengthOver"を使用します。
        /// </remarks>
        public LocalizedStringLengthAttribute(int maximumLength, int minimumLength) : base(maximumLength)
        {
            string resourceKey = "";
            this.MinimumLength = minimumLength;
            if (this.MinimumLength == maximumLength)
            {
                resourceKey = "ErrMsgInputLengthNotSame";
            }
            else
            {
                resourceKey = "ErrMsgInputLengthShortOrOver";
            }
            
            this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.GlobalMessage, resourceKey), resourceKey);
        }

        public LocalizedStringLengthAttribute(int maximumLength) : base(maximumLength)
        {
            string resourceKey = "ErrMsgInputLengthOver";
            this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.GlobalMessage, resourceKey), resourceKey);
        }
    }
}
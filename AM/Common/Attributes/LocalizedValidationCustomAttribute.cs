﻿using System;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using AM.Core.Util;
using AM.Common.Util;

namespace AM.Common.Attributes
{
    /// <summary>
    /// カスタム検証クラス
    /// </summary>
    /// <remarks></remarks>
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public sealed class LocalizedValidationCustomAttribute : ValidationAttribute
    {

        #region "定数"
        /// <summary>
        /// 検証タイプ(タグのAttributeに埋め込まれ、jQueryのvalidation構成に仕様されます)
        /// </summary>
        /// <remarks>
        /// タグ出力例(検証タイプにはエラーメッセージがセットされます)
        /// ＜input data-val="true" data-val-localizedcustom="発注日時の大小を正しく入力してください。"  ･･･ /＞
        /// </remarks>

        private const string _validationType = "localizedcustom";
        /// <summary>
        /// jQueryのvalidation構成に使用される引数(クライアント検証ファンクション名)
        /// </summary>
        /// <remarks>
        /// タグ出力例(引数"clientfuncname"にはClientFuncNameがセットされます)
        /// ＜input data-val="true" data-val-localizedcustom="発注日時の大小を正しく入力してください。" 
        ///                         data-val-localizedcustom-clientfuncname="$.fn.CM030010.checkOrderDate" ･･･ /＞
        /// </remarks>
        #endregion
        public const string ValidationParameterKeyClientFuncName = "clientfuncname";

        #region "Public Enum ResourceMode: リソース取得モード"
        /// <summary>
        /// リソース取得モード
        /// </summary>
        /// <remarks></remarks>
        public enum ResourceMode
        {
            Local,
            Global
        }
        #endregion

        #region "Properties"

        #region "ClientFuncName: クライアント検証ファンクション名(jQuery)"
        /// <summary>
        /// クライアント検証ファンクション名(jQuery)
        /// </summary>
        /// <remarks></remarks>
        private string _clientFuncName;
        public string ClientFuncName
        {
            get { return this._clientFuncName; }
        }
        #endregion

        private Type _serverMethodClass;
        public Type ServerMethodClass
        {
            get { return this._serverMethodClass; }
        }

        #region "ServerMethodClass: サーバー検証クラス"
        /// <summary>
        /// サーバー検証クラス
        /// </summary>
        /// <remarks></remarks>
        private Type _resourceManagerProvider;
        public Type resourceManagerProvider
        {
            get { return this._resourceManagerProvider; }
        }

        private string _resourceKey;
        public string resourceKey
        {
            get
            {
                return this._resourceKey;
            }
        }

        private string _resourcePath;
        public string resourcePath
        {
            get
            {
                return this._resourcePath;
            }
        }
        #endregion

        private ResourceMode _globalLocalMode;
        public ResourceMode globalLocalMode
        {
            get { return this._globalLocalMode; }
        }

        #region "ServerMethodName: サーバー検証ファンクション名"
        /// <summary>
        /// サーバー検証ファンクション名
        /// </summary>
        /// <remarks></remarks>
        private string _serverMethodName;
        public string ServerMethodName
        {
            get { return this._serverMethodName; }
        }

        #endregion

        #endregion

        #region "コンストラクタ"
        /// <summary>
        /// コンストラクタ(リソースクラスからメッセージ取得)
        /// </summary>
        /// <param name="clientFuncName">クライアント側検証ファンクション名(jQuery)</param>
        /// <param name="serverMethodClass">サーバー側検証クラス</param>
        /// <param name="serverMethodName">サーバー側検証メソッド名</param>
        /// <param name="resourceManagerProvider">カラム名取得用のリソースクラス</param>
        /// <param name="resourceKey">カラム名取得用のリソースキー</param>
        /// <remarks></remarks>
        public LocalizedValidationCustomAttribute(string clientFuncName, Type serverMethodClass, string serverMethodName, Type resourceManagerProvider, string resourceKey, ResourceMode globalLocalMode) : base(_validationType)
        {
            //共通プロパティー初期化
            this.InitializeProperties(clientFuncName, serverMethodClass, serverMethodName, resourceKey, resourceManagerProvider, globalLocalMode);
            //メッセージセット
            this.ErrorMessage = ResourceUtil.GetNameFromResource(resourceManagerProvider, resourceKey);
        }

        /// <summary>
        /// コンストラクタ(リソースファイルからメッセージ取得)
        /// </summary>
        /// <param name="clientFuncName">クライアント側検証ファンクション名(jQuery)</param>
        /// <param name="serverMethodClass">サーバー側検証クラス</param>
        /// <param name="serverMethodName">サーバー側検証メソッド名</param>
        /// <param name="resourcePath">リソースファイルパス</param>
        /// <param name="resourceKey">カラム名取得用のリソースキー</param>
        /// <param name="resourceMode">リソースがローカルかグローバルか</param>
        /// <remarks></remarks>
        public LocalizedValidationCustomAttribute(string clientFuncName, Type serverMethodClass, string serverMethodName, string resourcePath, string resourceKey, Type resourceManagerProvider, ResourceMode globalLocalMode) : base(_validationType)
        {
            //共通プロパティー初期化
            this.InitializeProperties(clientFuncName, serverMethodClass, serverMethodName, resourceKey, resourceManagerProvider, globalLocalMode);
            //メッセージセット
            if (globalLocalMode == ResourceMode.Local)
            {
                this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetLocalResourceObject(resourcePath, resourceKey), resourceKey);
            }
            else
            {
                this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(resourcePath, resourceKey), resourceKey);
            }
        }

        /// <summary>
        /// 共通プロパティー初期化
        /// </summary>
        private void InitializeProperties(string clientFuncName, Type serverMethodClass, string serverMethodName, string resourceKey, Type resourceManagerProvider, ResourceMode globalLocalMode)
        {
            this._clientFuncName = clientFuncName;
            this._serverMethodClass = serverMethodClass;
            this._serverMethodName = serverMethodName;
            this._resourceKey = resourceKey;
            this._resourceManagerProvider = resourceManagerProvider;
            this._globalLocalMode = globalLocalMode;
            this._resourcePath = resourcePath;

        }
        #endregion

        #region "IsValid: サーバー側検証"
        /// <summary>
        /// サーバー側検証
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //サーバー側検証メソッドコール
            MethodInfo serverMethod = this.ServerMethodClass.GetMethod(this.ServerMethodName);
            return (ValidationResult)serverMethod.Invoke(null, new object[] { value, validationContext, this });
        }
        #endregion

        #region "SetValidationParameters: 検証引数設定"
        /// <summary>
        /// 検証引数設定
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static ModelClientValidationRule SetValidationParameters(ModelClientValidationRule rule, string ClientFuncName)
        {
            rule.ValidationParameters.Add("clientfuncname", ClientFuncName);
            return rule;
        }
        #endregion

    }
}
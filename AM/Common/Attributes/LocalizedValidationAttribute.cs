using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AM.Common.Attributes
{
    /// <summary>
    /// ErrorMessageをリソースから取得するように拡張した検証属性の基底クラス
    /// </summary>
    /// <remarks></remarks>
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public sealed class LocalizedValidationAttribute : ValidationAttribute
	{
		#region "Properties"

		#region "ValidationType: 検証タイプ"
		/// <summary>
		/// 検証タイプ
		/// </summary>
		/// <remarks></remarks>
		private string _validationType;
		public string ValidationType {
			get { return this._validationType; }
		}
		#endregion

		#endregion

		#region "コンストラクタ"
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="validationType"></param>
		/// <remarks></remarks>
		public LocalizedValidationAttribute(string validationType)
		{
			this._validationType = validationType;
		}
		#endregion

		#region "IClientValidatableインターフェース: クライアント検証情報設定"
		/// <summary>
		/// クライアント検証情報設定
		/// </summary>
		/// <param name="metadata"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata)
		{
			//検証情報作成
			ModelClientValidationRule rule = new ModelClientValidationRule {
				ErrorMessage = this.FormatErrorMessage(metadata.DisplayName),
				ValidationType = this.ValidationType
			};

			//検証引数セット
			rule = SetValidationParameters(rule);

			//検証情報追加
			List<ModelClientValidationRule> rules = new List<ModelClientValidationRule>();
			rules.Add(rule);
			return rules;
		}

		/// <summary>
		/// 検証引数セット(引数を拡張クラスがカスタマイズするために切り出した処理)
		/// </summary>
		/// <param name="rule"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static ModelClientValidationRule SetValidationParameters(ModelClientValidationRule rule)
		{
			return rule;
		}
        #endregion

    }

}

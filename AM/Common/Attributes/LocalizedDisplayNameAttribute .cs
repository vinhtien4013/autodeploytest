using System;
using System.ComponentModel;
using System.Web;
using AM.Core.Util;
using AM.Common.Util;

namespace AM.Common.Attributes
{
    /// <summary>
    /// DisplayNameAttributeをリソースから取得するように拡張したクラス
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Class, Inherited=true)]
    public sealed class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        #region "private fields"
        /// <summary>
        /// リソース取得モード
        /// </summary>
        /// <remarks></remarks>
        private enum ResourceMode
        {
            None,
            Class,
            Local,
            Global
        }

        //リソース情報
        private ResourceMode _resourceModeType = ResourceMode.None;

        #endregion

        #region "Public Property IsLocalResource: ローカルリソースか否か"
        /// <summary>
        /// ローカルリソースか否か
        /// </summary>
        /// <value>
        /// True: LocalResource 
        /// False:GlobalResource
        /// </value>
        public bool IsLocalResource { get; set; }
        #endregion

        #region "Overrides Property DisplayName: 表示名称"
   
        /// <summary>
        /// ResourceMode設定
        /// </summary>
        /// <remarks></remarks>
        private void SetResourceMode()
        {
            if (this._resourceModeType == ResourceMode.None)
            {
                if (this.IsLocalResource)
                {
                    this._resourceModeType = ResourceMode.Local;
                }
                else
                {
                    this._resourceModeType = ResourceMode.Global;
                }
            }
        }
        #endregion

    }
}

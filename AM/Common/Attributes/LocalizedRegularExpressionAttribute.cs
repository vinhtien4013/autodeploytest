using System.ComponentModel.DataAnnotations;
using System.Web;
using AM.Core.Util;
using AM.Common.ConstValue;

namespace AM.Common.Attributes
{
    /// <summary>
    /// ErrorMessageをリソースから取得するように拡張したクラス
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.All, Inherited=true)]
    public sealed class LocalizedRegularExpressionAttribute : RegularExpressionAttribute
	{
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="pattern"></param>
		/// <remarks>
		/// エラーメッセージは固定で"GlobalMessage"の"ErrMsgInputRegularExpression"を使用します。
		/// </remarks>
		public LocalizedRegularExpressionAttribute(string pattern) : base(pattern)
		{
            string resourceKey = "ErrMsgInputRegularExpression";

            this.ErrorMessage = StringUtil.ConvertObjectToString(HttpContext.GetGlobalResourceObject(GlobalResources.GlobalMessage, resourceKey), resourceKey);
		}
	}
}
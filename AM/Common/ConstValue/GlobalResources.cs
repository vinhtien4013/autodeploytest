namespace AM.Common.ConstValue
{
	public sealed class GlobalResources
	{
		//メッセージ定義リソース名
		public const string GlobalMessage = "GlobalMessage";
        public const string GlobalError = "Error";
        public const string SlackMessage = "SlackMessage";

        private GlobalResources()
        {
        }
    }
}
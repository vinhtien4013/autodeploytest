using System;
using System.Reflection;
using System.Resources;

namespace AM.Common.Util
{
	/// <summary>
	/// リソースアクセスクラス
	/// </summary>
	/// <remarks></remarks>
	public sealed class ResourceUtil
	{
        private ResourceUtil()
        {
        }
        #region "GetNameFromResource: リソース取得"
        /// <summary>
        /// リソース取得
        /// </summary>
        /// <param name="resourceManagerProvider"></param>
        /// <param name="resourceKey"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string GetNameFromResource(Type resourceManagerProvider, string resourceKey)
		{
			//リソースマネージャー取得
			ResourceManager resourceManager = GetResourceManager(resourceManagerProvider);

			//リソース文字列取得
			if (resourceManager != null) {
				return resourceManager.GetString(resourceKey);
			}

			return resourceKey;
		}
		#endregion

		#region "GetResourceManager: リソースマネージャー取得"
		/// <summary>
		/// リソースマネージャー取得
		/// </summary>
		/// <param name="resourceManagerProvider"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static ResourceManager GetResourceManager(Type resourceManagerProvider)
		{
			//リソースマネージャー取得
			foreach (PropertyInfo propInfo in resourceManagerProvider.GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)) {
				if (propInfo.PropertyType == typeof(ResourceManager)) {
					return (ResourceManager)propInfo.GetValue(null, null);
				}
			}
			return null;
		}
		#endregion

	}

}
﻿using AM.Common.Attributes;
using System;
using System.Collections.Generic;

namespace AM.Models
{
    public class CompanyInput
    {
        public CompanyInput()
        {
            CompanyLst = new List<CompanyDetail>();
        }

        public IReadOnlyList<CompanyDetail> CompanyLst { get; set; }
    }

    public class CompanyDetail
    {
        /// <summary>
        /// company id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// company token
        /// </summary>
        public string CompanyToken { get; set; }

        /// <summary>
        /// company name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// admin company flag
        /// </summary>
        public bool AdminFlg { get; set; }

        /// <summary>
        /// company valid date
        /// </summary>
        public Nullable<DateTime> ValidDate { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// updated account id
        /// </summary>
        public int UpdatedAccount { get; set; }

        /// <summary>
        /// deleted date
        /// </summary>
        /// If this company be deleted => set current date to this field
        public Nullable<DateTime> DeletedDate { get; set; }

        /// <summary>
        /// slack flag
        /// </summary>
        public bool SlackFlg { get; set; }

        /// <summary>
        /// StartWorkingTime
        /// </summary>
        public TimeSpan StartWorkingTime { get; set; }

        /// <summary>
        /// EndWorkingTime
        /// </summary>
        public TimeSpan EndWorkingTime { get; set; }

        /// <summary>
        /// StartLunchTime
        /// </summary>
        public TimeSpan StartLunchTime { get; set; }

        /// <summary>
        /// EndLunchTime
        /// </summary>
        public TimeSpan EndLunchTime { get; set; }

        /// <summary>
        /// UncountedTime
        /// </summary>
        public int UncountedTime { get; set; }

        /// <summary>
        /// SlackToken
        /// </summary>
        public string SlackToken { get; set; }

        /// <summary>
        /// SlackTeamId
        /// </summary>
        public string SlackTeamId { get; set; }

        /// <summary>
        /// SlackTeamDomain
        /// </summary>
        public string SlackTeamDomain { get; set; }

        /// <summary>
        /// AttendanceChannel
        /// </summary>
        public string AttendanceChannel { get; set; }

        /// <summary>
        /// AnnouncementChannel
        /// </summary>
        public string AnnouncementChannel { get; set; }

        /// <summary>
        /// AttendanceCommand
        /// </summary>
        public string AttendanceCommand { get; set; }

        /// <summary>
        /// CompanyLogo
        /// </summary>
        public string CompanyLogo { get; set; }

        /// <summary>
        /// TimeZoneId
        /// </summary>
        public string TimeZoneId { get; set; }

        /// <summary>
        /// PaidHolidayMonthlyFlg
        /// </summary>
        public bool PaidHolidayMonthlyFlg { get; set; }

        /// <summary>
        /// SlackWebHookUrl
        /// </summary>
        public string SlackWebHookUrl { get; set; }

        /// <summary>
        /// PostMessageUsername
        /// </summary>
        public string PostMessageUsername { get; set; }

        /// <summary>
        /// login_by
        /// </summary>
        public int LoginBy { get; set; }
        ///<summary>
        /// face flag
        /// </summary>
        public bool FaceFlg { get; set; }

        /// <summary>
        /// face folder
        /// </summary>
        public string FaceFolder { get; set; }

        /// <summary>
        /// Min Working Time
        /// </summary>
        public int MinWorkingTime { get; set; }
    }

    public class TokenInput
    {
        [LocalizedRequired()]
        public string CompanyToken { get; set; }
    }
    public class ConditionInputCompany
    {
        public string id { get; set; }
    }
}
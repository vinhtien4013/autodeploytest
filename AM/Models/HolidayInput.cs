﻿using AM.Model.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AM.Models
{
    public class HolidayInput
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int year { get; set; }

        [Required]
        public int companyId { get; set; }
    }

    public class ParameterRegist 
    {
        public ParameterRegist()
        {
            holidaylist = new List<HolidayInputForRegist>();
        }
        public IReadOnlyList<HolidayInputForRegist> holidaylist { get; set; }

    }

    public class HolidayInputForRegist
    {
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int updated_account_id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int created_account_id { get; set; }
        
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime holiday_date_from { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime holiday_date_to { get; set; }

        [Required]
        public string holiday_name { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> deleted_date { get; set; }

        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int company_id { get; set; }

    }

    public class HolidayInputForDelete
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }
    }

}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AM.Models
{
    public class CheckPaidHolidayRequestInput
    {
        public CheckPaidHolidayRequestInput()
        {
            lstCheck = new Collection<confLst>();
        }
        
        public int applicationId { get; set; }

        public int updatedAccountId { get; set; }

        public int maxSequence { get; set; }

        public IReadOnlyList<confLst> lstCheck { get; set; }
    }

    public class confLst
    {
        public int checkAcc { get; set; }

        public int checkSequence { get; set; }

        public int checkStt { get; set; }

        public string reason { get; set; }
    }
}
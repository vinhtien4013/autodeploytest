﻿using AM.Model.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AM.Models
{
    public class AccountInputForUpdate
    {
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int id { get; set; }

        public string email { get; set;}

        public string staff_id { get; set; }

        public string staff_name { get; set; }

        public Nullable<int> fingerprint_id { get; set; }
        public string sex { get; set; }
        public bool admin_flg { get; set; }
        public int division_id { get; set; }
        public string face_image { get; set; }
        public string person_id { get; set; }
        public string persisted_face_id { get; set; }
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int updated_account_id { get; set; }

        public Nullable<DateTime> deleted_date { get; set; }
        public string password { get; set; }
        public string slack_user_id { get; set; }
        public string language { get; set; }

    }

    public class AccountInputForDelete
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int fingerprint_id { get; set; }
    }

    public class ParameterEditAccount
    {
        public ParameterEditAccount()
        {
            editAccountLst = new List<AccountInputForUpdate>();
        }
        public IReadOnlyList<AccountInputForUpdate> editAccountLst { get; set; }
    }

    public class ParameterSearch
    {
        public string company_id { get; set; }

        public string search_Finger_ID { get; set; }

        public string search_Division_Name { get; set; }

        public string search_Staff_ID { get; set; }

        public string search_Staff_Name { get; set; }
    }

    public class CheckResponsibleInput {
        [Required]
        public string AccountId { get; set; }

        [Required]
        public string CompanyId { get; set; }

    }

    /// <summary>
    /// Update language class
    /// </summary>
    public class UpdateAccountLanguageInput
    {
        /// <summary>
        /// language
        /// </summary>
        public string language { get; set; }

        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }
    }
}
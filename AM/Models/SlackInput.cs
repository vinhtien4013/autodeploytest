﻿using AM.Common.Attributes;
using System;
using System.Configuration;

namespace AM.Models
{
    public class SlackInput
    {

        
        /// <summary>
        /// slack token
        /// </summary>
        //[Required]
        [LocalizedRequired]
        public string token { get; set; }

        /// <summary>
        /// team id
        /// </summary>
        [LocalizedRequired]
        public string team_id { get; set; }

        /// <summary>
        /// team domain
        /// </summary>
        [LocalizedRequired]
        public string team_domain { get; set; }

        /// <summary>
        /// Slack channel name
        /// </summary>
        //[Required]
        [LocalizedRequired]
        public string channel_name { get; set; }

        /// <summary>
        /// login user nam
        /// </summary>
        //[Required]
        [LocalizedRequired]
        public string user_id { get; set; }

        /// <summary>
        /// command
        /// </summary>
        [LocalizedRequired]
        public string command { get; set; }

        /// <summary>
        /// text
        /// </summary>
        //[Required]
        [LocalizedRequired]
        public string text { get; set; }

        /// <summary>
        /// command type : determined by text
        /// 1 : register account paid holiday
        /// 2 : be late in day
        /// 3 : change working time in an specific
        /// 4 : register additional rest time in working time
        /// 5 : help
        /// </summary>
        public int CommandType { get; set; }

        public Uri response_url { get; set; }
    }
}
﻿using AM.Model.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AM.Models
{
    public class WorkingTimeInput
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }

        [DataType(DataType.DateTime)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime register_date { get; set; }
        public Nullable<int> range { get; set; }
    }

    public class MemberInfoInput
    {
        //[Required]
        //[RegularExpression(REGEX.MAIL_REGEX)]
        public string email { get; set; }

        public string finger_id { get; set; }

        public string token { get; set; }

        public int login_by { get; set; }

        public string password { get; set; }
    }

    public class UpdateWorkingTimeInput
    {
        public UpdateWorkingTimeInput()
        {
            Data_detail = new List<DetailInput>();
        }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int updated_account_id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int company_id { get; set; }

        public IReadOnlyList<DetailInput> Data_detail { get; set; }
    }

    public class DetailInput
    {
        /// <summary>
        /// Record id
        /// </summary>
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public Nullable<int> id { get; set; }

        /// <summary>
        /// Working date
        /// </summary>
        [DataType(DataType.DateTime)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime working_date { get; set; }

        /// <summary>
        /// Paid holiday
        /// </summary>
        public Nullable<int> paid_holiday { get; set; }

        /// <summary>
        /// Start working time
        /// </summary>
        [DataType(DataType.Time)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan expect_start_time { get; set; }

        /// <summary>
        /// Start working time
        /// </summary>
        [DataType(DataType.Time)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan expect_end_time { get; set; }

        /// <summary>
        /// Start working time
        /// </summary>
        [DataType(DataType.Time)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan start_working_time { get; set; }

        /// <summary>
        /// End working time
        /// </summary>
        [DataType(DataType.Time)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan end_working_time { get; set; }

        /// <summary>
        /// Rest time
        /// </summary>
        public Nullable<double> rest_time { get; set; }

        /// <summary>
        /// Over time
        /// </summary>
        public Nullable<double> over_time { get; set; }

        /// <summary>
        /// Additional rest time
        /// </summary>
        public Nullable<double> additional_rest_time { get; set; }

        /// <summary>
        /// Is reject self check
        /// </summary>
        public bool is_reject_selfcheck { get; set; }

        /// <summary>
        /// Modify
        /// </summary>
        public string modify { get; set; }
    }

    public class CheckAttendanceInput
    {
        [Required]
        public string register_date { get; set; }

        [Required]
        public string company_id { get; set; }
    }

    public class UpdateCheckAttendanceInput
    {
        [Required]
        public int application_id { get; set; }

        [Required]
        public int updated_account_id { get; set; }

        public DateTime updated_date { get; set; }

        /// <summary>
        /// Application sequence
        /// </summary>
        public int application_sequence { get; set; }

        public IReadOnlyList<UpdateCheckAttendance> Data_detail { get; set; }
    }
    public class UpdateCheckAttendance
    {
        [Required]
        public int account_id { get; set; }

        [Required]
        public byte check_sequence { get; set; }

        [Required]
        public int check_status { get; set; }

        /// <summary>
        /// Reason
        /// </summary>
        public string reason { get; set; }
    }

    public class WorkingInput
    {
        public int account_id { get; set; }

        public int fingerprint_id { get; set; }
        public string face_image { get; set; }
    }

    public class CompanyInputParam
    {
        [Required]
        public string CompanyId { get; set; }
    }

    /// <summary>
    /// Update acccount password model
    /// </summary>
    public class UpdateAccountPasswordModel
    {
        public string token { get; set; }
        public string email { get; set; }
    }

    public class ChangeAccountPassword
    {
        /// <summary>
        /// Password
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Account id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Updated account id
        /// </summary>
        public int updated_account_id { get; set; }

    }

    public class GetStaffWKInput
    {
        /// <summary>
        /// set of id
        /// </summary>
        [Required]
        public string account_id { get; set; }
        /// <summary>
        /// range
        /// </summary>
        [Required]
        public int range { get; set; }
        
    }
}
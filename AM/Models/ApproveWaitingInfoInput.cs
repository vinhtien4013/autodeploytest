﻿namespace AM.Models
{
    public class ApproveWaitingInfoInput
    {
        /// <summary>
        /// account_id
        /// </summary>
        public int account_id { get; set; }

        /// <summary>
        /// flg_admin
        /// </summary>
        public bool flg_admin { get; set; }

        /// <summary>
        /// company_id
        /// </summary>
        public string company_id { get; set; }
    }
}
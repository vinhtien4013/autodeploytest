﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AM.Models
{
    public class GetAllDivisionInput
    {
        public int company_id { get; set; }
    }
    public class GetDivisionInput
    {
        public int? div_range_from { get; set; }
        public int company_id { get; set; }
    }
    public class DivisionInputDivision
    {
        public int div_range_parent { get; set; }
        public string div_name { get; set; }
        public int responsible_staff_id { get; set; }
        //public string admin_div_flg { get; set; }
        public bool admin_div_flg { get; set; }

        public string description { get; set; }
        public int company_id { get; set; }
        public int account_id{ get; set; }
        public string modify_mode { get; set; }
        public int group_id { get; set; }
        public string div_code { get; set; }
    }
    public class CheckDeleteInput
    {
        public int id { get; set; }
    }
}
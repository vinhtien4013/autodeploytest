﻿using AM.Model.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AM.Models
{
    public class FingerPrintInput
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int FingerPrintId { get; set; }

        public string account_id { get; set; }

        public string finger_data { get; set; }

        public string salt { get; set; }
    }
}
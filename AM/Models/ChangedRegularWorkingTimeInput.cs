﻿using AM.BusinessLayer.AccountService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.Const;
using AM.Model.ConstData;
using AM.Model.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static AM.BusinessLayer.BaseService;

//[assembly: CLSCompliant(true)]
namespace AM.Models
{
    public class ChangedRegularWorkingTimeInput
    {
        public ChangedRegularWorkingTimeInput()
        {
            changeWorkingTimeDetail = new List<ChangedWorkingTimeDetail>();
        }

        public int updated_account_id { get; set; }

        public IReadOnlyList<ChangedWorkingTimeDetail> changeWorkingTimeDetail { get; set; }

//#pragma warning disable CS3001 // Argument type is not CLS-compliant
        [CLSCompliant(false)]
        public ResultInfo ConvertToModel(ref List<ChangedRegularWorkingTime> modelLst)
//#pragma warning restore CS3001 // Argument type is not CLS-compliant
        {
            ResultInfo resultInfo = new ResultInfo()
            {
                Status = ResultStatus.RegistFailed,
                Message = Resources.MessageInfo.E0041
            };
            modelLst = new List<ChangedRegularWorkingTime>();

            try
            {
                Nullable<int> tmpAccount = null;
                if (this.changeWorkingTimeDetail == null || this.changeWorkingTimeDetail.Count <= 0)
                {
                    return resultInfo;
                }
                var memberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(changeWorkingTimeDetail[0].account_id);
                DateTime nowTime = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);
                foreach (var detail in this.changeWorkingTimeDetail)
                {
                    double restTime = DateTimeUtil.CalculateTwoTime(detail.lunch_time_from, detail.lunch_time_to);
                    var model = new ChangedRegularWorkingTime()
                    {
                        id = detail.id,
                        account_id = detail.account_id,
                        start_time = detail.start_time,
                        end_time = detail.end_time,
                        rest_time = restTime,
                        reason = detail.reason,
                        mon_flg = detail.mon_flg,
                        tue_flg = detail.tue_flg,
                        wed_flg = detail.wed_flg,
                        thu_flg = detail.thu_flg,
                        fri_flg = detail.fri_flg,
                        lunch_time_from = detail.lunch_time_from,
                        lunch_time_to = detail.lunch_time_to,
                        apply_start_date = detail.apply_start_date,
                        apply_end_date = detail.apply_end_date,
                        created_account_id = this.updated_account_id,
                        created_date = nowTime,
                        updated_account_id = this.updated_account_id,
                        updated_date = nowTime,
                        deleted_date = detail.deleted_date,
                        deleted_account_id = (detail.deleted_date.HasValue) ? this.updated_account_id : tmpAccount
                    };

                    modelLst.Add(model);
                }

                resultInfo.Status = ResultStatus.NSuccess;
                return resultInfo;
            }
            catch(Exception)
            {
                throw;
            }
        }
    }

    public class ChangedWorkingTimeDetail
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan start_time { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan end_time { get; set; }

        [Required]
        public string reason { get; set; }

        [Required]
        public bool mon_flg { get; set; }

        [Required]
        public bool tue_flg { get; set; }

        [Required]
        public bool wed_flg { get; set; }

        [Required]
        public bool thu_flg { get; set; }

        [Required]
        public bool fri_flg { get; set; }
        
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan lunch_time_from { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan lunch_time_to { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime apply_start_date { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> apply_end_date { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> deleted_date { get; set; }
    }

    public class ParamInput
    {
        [Required]
        public int companyId { get; set; }
    }
    
}
﻿using AM.Model.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AM.Models
{
    public class PaidHolidayInput
    {
        public PaidHolidayInput()
        {
            Data_detail = new List<PaidHolidayDetail>();
        }

        [RegularExpression(REGEX.NUMBER_REGEX)]
        public Nullable<int> account_id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int updated_account_id { get; set; }

        //[Required]
        //public double registor_days { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public byte processing_status { get; set; }

        [Required]
        public string company_id { get; set; }

        public IReadOnlyList<PaidHolidayDetail> Data_detail { get; set; }
    }

    public class PaidHolidayDetail
    {
        /// <summary>
        /// id
        /// </summary>
        public Nullable<int> id { get; set; }
        /// <summary>
        /// account_id
        /// </summary>
        public Nullable<int> account_id { get; set; }
        /// <summary>
        /// application_datetime
        /// </summary>
        public DateTime application_datetime { get; set; }
        /// <summary>
        /// registor_days
        /// </summary>
        public double registor_days { get; set; }
        /// <summary>
        /// reason_txt
        /// </summary>
        public string reason_txt { get; set; }
        /// <summary>
        /// deleted_date
        /// </summary>
        public DateTime? deleted_date { get; set; }
        /// <summary>
        /// deleted_account_id
        /// </summary>
        public int? deleted_account_id { get; set; }
    }

    public class RequestManageInput
    {
        //[RegularExpression(REGEX.NUMBER_REGEX)]
        public string id { get; set; }
        public string account_id { get; set; }

        public string date_from { get; set; }

        public string date_to { get; set; }

        public string approve_status { get; set; }

        public string company_id { get; set; }
    }

    public class RequestManageById
    {
        public string id { get; set; }
    }

    public class ParameterRegistRequestManage
    {
        public ParameterRegistRequestManage()
        {
            requestManageList = new List<RequestManageInputForRegist>();
        }
        public IReadOnlyList<RequestManageInputForRegist> requestManageList { get; set; }
    }

    public class RequestManageInputForRegist
    {
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int updated_account_id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int created_account_id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime application_datetime { get; set; }

        public int? paid_holiday_type { get; set; }

        public string reason_txt { get; set; }

        /// <summary>
        /// Deleted date
        /// </summary>
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? deleted_date { get; set; }

        /// <summary>
        /// oldId : previous id
        /// Case there was existed a request before
        /// </summary>
        public string oldId { get; set; }
    }

    public class RequestManageInputForDelete
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int id { get; set; }

        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }
    }
    public class Inputforcheck
    {
        [Required]
        [RegularExpression(REGEX.NUMBER_REGEX)]
        public int account_id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime application_datetime { get; set; }

        [Required]
        public int applicationType { get; set; }
    }

    public class ListPaidHolidayInput
    {
        /// <summary>
        /// account_id
        /// </summary>
        public string account_id { get; set; }

        /// <summary>
        /// Staff_id
        /// </summary>
        public string staff_id { get; set; }

        /// <summary>
        /// Company_id
        /// </summary>
        public string company_id { get; set; }
    }

}
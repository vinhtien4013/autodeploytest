﻿using System;

namespace AM.Models
{
    public class AttendanceSummaryInput
    {

        /// <summary>
        /// Date input
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// account_id
        /// </summary>
        public string account_id { get; set; }

        /// <summary>
        /// Staff_id
        /// </summary>
        public string staff_id { get; set; }

        /// <summary>
        /// Company_id
        /// </summary>
        public int company_id { get; set; }
    }
}
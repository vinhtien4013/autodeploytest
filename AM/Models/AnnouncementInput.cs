﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AM.Models
{
    public class AnnouncementInput
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int UpdatedAccountId { get; set; }

        public string Email { get; set; }

        public bool FlgDraft { get; set; }

        public DateTime PostDate { get; set; }

        public string TypeDelete { get; set; }

        public bool FlgPostSlack { get; set; }

        public int CompanyId { get; set; }
    }
}
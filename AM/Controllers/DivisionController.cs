﻿using AM.BusinessLayer;
using AM.BusinessLayer.DivisionService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/Division")]
    public class DivisionController : ApiController
    {
        // GET: api/Division/GetDivisionList
        [HttpGet]
        [Route("GetDivisionListIdAndName")]
        public HttpResponseMessage GetDivisionListIdAndName(string company_id)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //write log
                var data = new
                {
                    company_id = company_id
                };
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetDivisionListIdAndName",JsonConvert.SerializeObject(data));
                try
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    DivisionService service = ServiceLocator.Resolve<DivisionService>();
                    var sumData = service.GetDivisionListIdAndName(company_id);

                    if (sumData == null)
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.OK;
                        // return a list attendance summary of all staff in the specificed month
                        response.Content = new JsonContent(sumData);
                    }
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetDivisionListIdAndName", JsonConvert.SerializeObject(sumData));
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Division/GetDivisionByCompanyId
        [HttpPost]
        [Route("GetDivisionByCompanyId")]
        public HttpResponseMessage GetDivisionByCompanyId([FromBody] GetAllDivisionInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                try
                {
                    //write log
                    LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST , "GetDivisionByCompanyId",JsonConvert.SerializeObject(data));
                    HttpResponseMessage response = new HttpResponseMessage();
                    DivisionService service = ServiceLocator.Resolve<DivisionService>();
                    var sumData = service.GetDivisionByCompanyId(data.company_id);

                    if (sumData == null)
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.OK;
                        // return a list attendance summary of all staff in the specificed month
                        response.Content = new JsonContent(sumData);
                    }
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetDivisionByCompanyId", JsonConvert.SerializeObject(sumData));
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Division/GetDivision
        [HttpPost]
        [Route("GetDivision")]
        public HttpResponseMessage GetDivision([FromBody] GetDivisionInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                try
                 {
                    //write log
                    LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetDivision",JsonConvert.SerializeObject(data));
                    HttpResponseMessage response = new HttpResponseMessage();
                    DivisionService service = ServiceLocator.Resolve<DivisionService>();
                    var sumData = service.GetDivison(data.div_range_from,data.company_id);

                    if (sumData == null)
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.OK;
                        // return a list attendance summary of all staff in the specificed month
                        response.Content = new JsonContent(sumData);
                    }
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetDivision", JsonConvert.SerializeObject(sumData));
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Division/UpdateDivision
        [HttpPost]
        [Route("UpdateDivision")]
        public HttpResponseMessage UpdateDivision([FromBody] Collection<DivisionInputDivision> lstdata)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(lstdata);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateDivision",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        DivisionService service = ServiceLocator.Resolve<DivisionService>();
                        List<Division> divisionLst = new List<Division>();
                        //get time zone by company 
                        var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, lstdata[0].company_id.ToString(),null);
                        foreach (var data in lstdata)
                        {

                            divisionLst.Add(new Division()
                            {
                                div_range_parent = data.div_range_parent,
                                div_name = data.div_name,
                                responsible_staff_id = data.responsible_staff_id,
                                admin_div_flg = data.admin_div_flg,
                                description = data.description,
                                created_account_id = data.account_id,
                                created_date = AzureUtil.LocalDateTimeNow(company.time_zone_id),
                                updated_date = AzureUtil.LocalDateTimeNow(company.time_zone_id),
                                updated_account_id = data.account_id,
                                company_id = data.company_id,
                                modify_mode = data.modify_mode,
                                group_id = data.group_id,
                                div_code = data.div_code

                            }) ;
                        }
                       
                        var resultInfo = service.UpdateDivision(divisionLst);
                        //write log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateDivision", resultInfo.Message);
                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);

                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpPost]
        [Route("CheckDeleteDivision")]
        public HttpResponseMessage CheckDeleteDivision([FromBody] CheckDeleteInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                
                try
                {
                    //write log
                    LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "CheckDeleteDivsion",JsonConvert.SerializeObject(data));
                    HttpResponseMessage response = new HttpResponseMessage();
                    DivisionService service = ServiceLocator.Resolve<DivisionService>();
                    var sumData = service.CheckDeleteDivision(data.id);

                    if (sumData == 0)
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.OK;
                        // return a list attendance summary of all staff in the specificed month
                        response.Content = new JsonContent(sumData);
                    }
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "CheckDeleteDivsion", JsonConvert.SerializeObject(sumData));
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Division/GetGroupData
        [HttpPost]
        [Route("GetGroupData")]
        public HttpResponseMessage GetGroupData(GetAllDivisionInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                try
                {
                    //write log
                    LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetGroupData",JsonConvert.SerializeObject(data));
                    HttpResponseMessage response = new HttpResponseMessage();
                    DivisionService service = ServiceLocator.Resolve<DivisionService>();
                    var sumData = service.GetGroupData(data.company_id.ToString());

                    if (sumData == null)
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.OK;
                        // return a list attendance summary of all staff in the specificed month
                        response.Content = new JsonContent(sumData);
                    }
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetGroupData", JsonConvert.SerializeObject(sumData));
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }
    }
}

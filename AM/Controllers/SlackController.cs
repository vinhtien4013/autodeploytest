﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.BusinessLayer.SlackService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model;
using AM.Model.Const;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/Slack")]
    public class SlackController : ApiController
    {
        // POST: api/Slack/RegisterAttendanceViaSlack
        [HttpPost]
        [Route("RegisterAttendanceViaSlack")]
        public  HttpResponseMessage RegisterAttendanceViaSlack ([FromBody] SlackInput data)
        {
            try
            {
                SlackResponse messResponse = new SlackResponse();
                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "RegisterAttendanceViaSlack", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    var tokenCancel = new CancellationTokenSource();

                    try
                    {
                        SlackService service = ServiceLocator.Resolve<SlackService>();
                        SlackModel model = new SlackModel();
                        //int companyId = StringUtil.ConvertObjectToInteger32(ConstType.CONST_VALUE.SESSION_COMPANY_DEFAULT);
                        var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null,null,data.token);
                        if (company == null || !company.slack_flg)
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = MessageInfo.E0017;
                            return new HttpResponseMessage()
                            {
                                StatusCode = HttpStatusCode.OK,
                                Content = new JsonContent(messResponse)
                            };
                        }
                       
                        if (!data.channel_name.Equals(company.attendance_channel))
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = MessageInfo.E0018;
                            return new HttpResponseMessage()
                            {
                                StatusCode = HttpStatusCode.OK,
                                Content = new JsonContent(messResponse)
                            };
                        }
                        int companyId = company.id;
                        bool result = ConvertToModel( data, ref model, ref messResponse, ref companyId);

                        //writie log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "RegisterAttendanceViaSlack", JsonConvert.SerializeObject(messResponse));
                        if (result)
                        {
                            new System.Threading.Tasks.Task( () => 
                             {
                                //insert to db code;
                                messResponse =  service.RegisterAttendanceViaSlack(model, company);
                                var webClient = new WebClient();
                                var json = JsonConvert.SerializeObject(messResponse);
                                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                                webClient.Encoding = System.Text.Encoding.UTF8;
                                webClient.UploadString(data.response_url, json);
                                 tokenCancel.Cancel();
                             }, tokenCancel.Token).Start();
                            
                            response.StatusCode = HttpStatusCode.OK;
                            //response.Content = new JsonContent(JsonConvert.DeserializeObject(JsonConvert.SerializeObject(messResponse)));
                            response.Content = new JsonContent(messResponse);
                            return response;
                        }

                        response.StatusCode = HttpStatusCode.OK;
                        response.Content = new JsonContent(messResponse);

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                    finally
                    {
                        tokenCancel.Dispose();
                    }
                }
                else
                {
                    var errors = ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage);
                    LoggingUtil.WriteLog(JsonConvert.SerializeObject(errors));
                    messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    messResponse.text = errors.FirstOrDefault();
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content = new JsonContent(messResponse)
                    };
                }

            }
            catch(HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="model"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private static bool ConvertToModel(SlackInput data, ref SlackModel model, ref SlackResponse response, ref int companyId)
        {
            model = new SlackModel();
            response = new SlackResponse(SLACK_RESPONSE_TYP.EPHEMERAL, MessageInfo.I0053);
            //var tmpEmail = string.Concat(data.user_name, SLACT_CONST.MAIL_DOMAIN);
            AccountService accService = ServiceLocator.Resolve<AccountService>();

            MemberInfoModel memberInfo = null;
            try
            {
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyBySlackInfo(data.token);
                
                ////Check token company and slack mode 
                //if (company == null )
                //{
                //    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                //    response.text = Resources.MessageInfo.E0019;
                //    return false;
                //}
                ////Check channel name
                //else if (company.attendance_channel != data.channel_name)
                //{
                //    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                //    response.text = MessageInfo.E0018;
                //    return false;
                //}
                //Check company command
                if (company.attendance_command != data.command)
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = String.Format(MessageInfo.E0028,company.attendance_command);
                    return false;
                }
                //Check user_id
                else
                {
                    memberInfo = accService.GetMemberInfoBySlackUserId(data.user_id, company.company_token);
                    if (memberInfo == null)
                    {
                        response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        response.text = MessageInfo.E0019;
                        return false;
                    }
                }
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(memberInfo.language);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(memberInfo.language);
                model.AccountId = memberInfo.id;
                //model.UserName = memberInfo.email.Split(CONST_VALUE.ALPHA)[0].ToString();
                model.UserName = memberInfo.staff_name;
                model.UserSex = memberInfo.sex;
                companyId = memberInfo.company_id;

                int firstSpaceIndex = data.text.IndexOf(' ');
                var firstchar = "";
                if (firstSpaceIndex == -1)
                    firstchar = data.text;
                else
                    firstchar = data.text.Substring(0, firstSpaceIndex);

                if (firstchar.Equals(SLACK_COMMAND_TYP.OFFALLDAY) || firstchar.Equals(SLACK_COMMAND_TYP.OFFHALFMORNING) || firstchar.Equals(SLACK_COMMAND_TYP.OFFHALFAFTERNOON))
                {
                    return CheckOffFormat(firstchar, data, ref model, ref response);
                }
                else if (firstchar.Equals(SLACK_COMMAND_TYP.LATE))
                {
                    return CheckLateFormat(data, ref model, ref response);
                }
                else if (firstchar.Equals(SLACK_COMMAND_TYP.CHANGEREGULAR))
                {
                    return CheckChangeregularFormat(data, ref model, ref response);
                }
                else if (firstchar.Equals(SLACK_COMMAND_TYP.REST))
                {
                    return CheckRestFormat(data, company, ref model, ref response);
                }
                else if (firstchar.Equals(SLACK_COMMAND_TYP.HELP))
                {
                    // help
                    Regex rx = new Regex(ConstType.Pattern.PatternHelp, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    Match item = rx.Match(data.text);

                    if (item.Length == 0)
                    {
                        response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        response.text = String.Format(MessageInfo.E0028, data.command);
                        return false;
                    }
                    model.CommandType = (int)COMMANDTYP.HELP;
                }
                else
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = String.Format(MessageInfo.E0028, data.command);
                    return false;
                }
            }
            catch(ArgumentException ex )
            {
                LoggingUtil.WriteExceptionLog(String.Format(MessageInfo.I0052, ex.Message));
                return false;

            }
            catch (TimeoutException ex)
            {

                LoggingUtil.WriteExceptionLog(String.Format(MessageInfo.I0052, ex.Message));
                return false;

            }

            return true;
        }

        /// <summary>
        /// Check off format input text
        /// </summary>
        /// <param name="firstchar">
        /// to define account paid holiday type: 
        /// Off Half Morning/ Off Half Afternoon/ Off All day
        /// </param>
        /// <param name="data">data input</param>
        /// <param name="model">model</param>
        /// <param name="response">response</param>
        /// <returns></returns>
        private static bool CheckOffFormat(string firstchar, SlackInput data, ref SlackModel model, ref SlackResponse response)
        {
            try
            {
                Regex rx = new Regex(ConstType.Pattern.PatternRegisterOff, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                Match item = rx.Match(data.text);
                string date = item.Groups["date"].ToString();
                DateTime tmpDate;
                if (!DateTime.TryParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out tmpDate) || item.Length == 0)
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = String.Format(MessageInfo.E0028, data.command);
                    return false;
                }
                model.reason = item.Groups["reason"].ToString();
                model.RegisterDate = tmpDate;
                model.CommandType = (firstchar.Equals(SLACK_COMMAND_TYP.OFFALLDAY) ? (int)COMMANDTYP.OFFALLDAY : ((firstchar.Equals(SLACK_COMMAND_TYP.OFFHALFMORNING) ? (int)COMMANDTYP.OFFHALFMORNING : (int)COMMANDTYP.OFFHALFAFTER)));
            }
            catch (ArgumentException ex)
            {
                LoggingUtil.WriteExceptionLog(String.Format(MessageInfo.I0052, ex.Message));
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check late format input text
        /// </summary>
        /// <param name="data">data input</param>
        /// <param name="model">model</param>
        /// <param name="response">response</param>
        /// <returns></returns>
        private static bool CheckLateFormat(SlackInput data, ref SlackModel model, ref SlackResponse response)
        {
            try
            {
                Regex rx = new Regex(ConstType.Pattern.PatternRegisterLate, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                Match item = rx.Match(data.text);
                string date = item.Groups["timeLate"].ToString();

                var tmpTime = StringUtil.ConvertStringToTimespan(date);
                if (tmpTime == null || item.Length == 0)
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = String.Format(MessageInfo.E0028, data.command);
                    return false;
                }

                model.reason = item.Groups["reason"].ToString();
                model.LateTime = tmpTime;
                model.CommandType = (int)COMMANDTYP.LATE;
            }
            catch(ArgumentException ex)
            {
                LoggingUtil.WriteExceptionLog(String.Format(MessageInfo.I0052, ex.Message));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check rest format input text
        /// </summary>
        /// <param name="data">data input</param>
        /// <param name="model">model</param>
        /// <param name="response">response</param>
        /// <returns></returns>
        private static bool CheckRestFormat(SlackInput data, Company company, ref SlackModel model, ref SlackResponse response)
        {
            try
            {
                Regex rx = new Regex(ConstType.Pattern.PatternRegisterRest, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                Match itemLst = rx.Match(data.text);
                string date = itemLst.Groups["date"].ToString();
                string reason = itemLst.Groups["reason"].ToString();
                var timeFromLst = new List<TimeSpan?>();
                var timeToLst = new List<TimeSpan?>();
                DateTime tmpDate;
                double hourRest = 0;
                if (itemLst.Length == 0 || !DateTime.TryParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out tmpDate))
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = String.Format(MessageInfo.E0028, data.command);
                    return false;
                }

                //Get time from and check format
                foreach (Capture item in itemLst.Groups["timeFrom"].Captures)
                {
                    var tmpTime = StringUtil.ConvertStringToTimespan(item.ToString());
                    if (tmpTime == null)
                    {
                        response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        response.text = String.Format(MessageInfo.E0028, data.command);
                        return false;
                    }
                    timeFromLst.Add(tmpTime);
                }

                //Get time to and check format
                foreach (Capture item in itemLst.Groups["timeTo"].Captures)
                {
                    var tmpTime = StringUtil.ConvertStringToTimespan(item.ToString());
                    if (tmpTime == null)
                    {
                        response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        response.text = String.Format(MessageInfo.E0028, data.command);
                        return false;
                    }
                    timeToLst.Add(tmpTime);
                }

                //Get total rest hour
                for (int i = 0; i < timeToLst.Count(); i++)
                {

                    double tmpHourRest = (timeToLst[i].Value - timeFromLst[i].Value).TotalHours;

                    //Time From < time to
                    if (tmpHourRest <= 0)
                    {
                        response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        response.text = String.Format(MessageInfo.E0028, data.command);
                        return false;
                    }

                    //if(!(timeFromLst[i] >= TimeSpan.Parse(ConstType.CONST_VALUE.DEFAULT_END_RESTTIME) || timeToLst[i] <= TimeSpan.Parse(ConstType.CONST_VALUE.DEFAULT_START_RESTTIME)))
                    if (!(timeFromLst[i] >= company.end_lunch_time || timeToLst[i] <= company.start_lunch_time))
                    {
                        response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        response.text = String.Format(Resources.MessageInfo.E0032, company.start_lunch_time.ToString(@"hh\:mm"), company.end_lunch_time.ToString(@"hh\:mm"));
                        return false;
                    }

                    if ((i + 1) < timeToLst.Count())
                    {
                        if (!((timeToLst[i] > timeFromLst[i + 1] & timeToLst[i + 1] < timeFromLst[i]) | (timeToLst[i] < timeFromLst[i + 1] & timeToLst[i + 1] > timeFromLst[i])))
                        {
                            response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            response.text = String.Format(MessageInfo.E0028, data.command);
                            return false;
                        }

                    }
                    model.TimeFromTo.Add(new TimeFromto { TimeFrom = timeFromLst[i], TimeTo = timeToLst[i] });
                    hourRest += tmpHourRest;

                }

                //Total rest time  > 8
                if (hourRest > 8)
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = Resources.MessageInfo.E0035;
                    return false;
                }

                model.RegisterDate = tmpDate;
                model.reason = reason;
                model.RestHours = hourRest;
                model.CommandType = (int)COMMANDTYP.REST;
            }
            catch(ArgumentException ex)
            {
                LoggingUtil.WriteExceptionLog(String.Format(MessageInfo.I0052, ex.Message));
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check change regular format input text
        /// </summary>
        /// <param name="data">data input</param>
        /// <param name="model">model</param>
        /// <param name="response">response</param>
        /// <returns></returns>
        private static bool CheckChangeregularFormat(SlackInput data, ref SlackModel model, ref SlackResponse response)
        {
            try
            {
                TimeSpan defaultMinStartTime;
                TimeSpan defaultMaxEndTime;
                var check1 = TimeSpan.TryParse(ConstType.CONST_VALUE.DEFAULT_MIN_START_WORKINGTIME, out defaultMinStartTime);
                var check2 = TimeSpan.TryParse(ConstType.CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME, out defaultMaxEndTime);
                if (!(check1 && check2))
                {
                    return false;
                }
                Regex rx = new Regex(ConstType.Pattern.PatternRegisterChange, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                Match item = rx.Match(data.text);
                string date = item.Groups["date"].ToString();
                TimeSpan? timeFrom = StringUtil.ConvertStringToTimespan(item.Groups["timeFrom"].ToString());
                TimeSpan? timeTo = StringUtil.ConvertStringToTimespan(item.Groups["timeTo"].ToString());
                string reason = item.Groups["reason"].ToString();
                DateTime tmpDate;
                if (item.Length == 0 || !DateTime.TryParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out tmpDate) || (timeTo.Value - timeFrom.Value).TotalHours < 0)
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = String.Format(MessageInfo.E0028, data.command);
                    return false;
                }

                // Check Time from < 07:30 or Time to > 18:00
                if (TimeSpan.Compare(timeFrom.Value, defaultMinStartTime) < 0 || TimeSpan.Compare(timeTo.Value, defaultMaxEndTime) > 0)
                {
                    response.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    response.text = Resources.MessageInfo.E0029;
                    return false;
                }

                model.RegisterDate = tmpDate;
                model.reason = reason;
                model.ExpectStartTime = timeFrom;
                model.ExpectEndTime = timeTo;
                model.CommandType = (int)COMMANDTYP.CHANGE;
            }
            catch (ArgumentException ex)
            {
                LoggingUtil.WriteExceptionLog(String.Format(MessageInfo.I0052, ex.Message));
                return false;
            }

            return true;
        }
    }
    
}
﻿using AM.BusinessLayer;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/ChangedRegularWorkingTime")]
    public class ChangedRegularWorkingTimeController : ApiController
    {
        // POST: api/ChangedRegularWorkingTime/GetChangedRegularWorkingTime
        [HttpPost]
        [Route("GetChangedRegularWorkingTime")]
        public HttpResponseMessage LoadListChangedRegularWorkingTime([FromBody]ParamInput param)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.Path, null);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetChangeRegularWorkingTime", JsonConvert.SerializeObject(param));

                try
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    ChangedRegularWorkingTimeService service = ServiceLocator.Resolve<ChangedRegularWorkingTimeService>();
                    // Get information in table changed_regular_working_time
                    var list = service.GetChangedRegularWorkingTime(param.companyId);
                    if (list == null || list.Count == 0)
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.OK;
                        // return a list holiday by year
                        response.Content = new JsonContent(list);
                    }
                    //Write log
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetChangeRegularWorkingTime", JsonConvert.SerializeObject(list));
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/ChangedRegularWorkingTime/UpdateChangedRegularWorkingTime
        [HttpPost]
        [Route("UpdateChangedRegularWorkingTime")]
        public HttpResponseMessage UpdateChangedRegularWorkingTime([FromBody] ChangedRegularWorkingTimeInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateChangedRegularWorkingTime", jsonStr);

                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {

                        HttpResponseMessage response = new HttpResponseMessage();
                        var modelList = new List<ChangedRegularWorkingTime>();
                        ChangedRegularWorkingTimeService service = ServiceLocator.Resolve<ChangedRegularWorkingTimeService>();
                        
                        var resultInfo = data.ConvertToModel(ref modelList);
                        if (resultInfo.Status != ResultStatus.NSuccess)
                        {
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateChangedRegularWorkingTime", resultInfo.Message);
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                        else
                        {
                            // Update changed regular working time record
                            resultInfo = service.UpdateChangedWorkingTime(modelList);
                            if(resultInfo.Status != ResultStatus.RegistSuccess)
                            {
                                LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateChangedRegularWorkingTime", resultInfo.Message);
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }

                            response.StatusCode = HttpStatusCode.OK;
                        }
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }

                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
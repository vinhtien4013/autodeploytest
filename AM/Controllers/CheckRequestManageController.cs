﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/CheckRequestManage")]
    public class CheckRequestManageController : ApiController
    {
        // POST: api/CheckRequestManage/GetPaidHolidayCheckRequest
        [HttpPost]
        [Route("GetPaidHolidayCheckRequest")]
        public HttpResponseMessage GetPaidHolidayCheckRequest([FromBody]RequestManageInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetPaidHolidayCheckRequest", JsonConvert.SerializeObject(data));
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayRequestService service = ServiceLocator.Resolve<PaidHolidayRequestService>();
                        var list = service.GetPaidHolidayCheckRequest(data.id, data.account_id, data.date_from, data.date_to, data.approve_status, data.company_id);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list paid holiday
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetPaidHolidayCheckRequest",JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        // POST: api/CheckRequestManage/UpdatePaidHolidayCheck
        [HttpPost]
        [Route("UpdatePaidHolidayCheck")]
        public HttpResponseMessage UpdatePaidHolidayCheck([FromBody]Collection<CheckPaidHolidayRequestInput> data)
        {
            try
            {
                // Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //Check token company
                if (!ServiceLocator.Resolve<CommonService>().CheckCompanyToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0037;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // find the company by token
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(Request.Headers.GetValues(CONST_VALUE.COMPANY_TOKEN).First(), null, null);
                if (company == null)
                {
                    var errors = Resources.MessageInfo.E0038;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                //write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdatePaidHolidayCheck",JsonConvert.SerializeObject(data));
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayRequestService service = ServiceLocator.Resolve<PaidHolidayRequestService>();
                        var listConfirm = ConvertData(data);
                        if (listConfirm == null || listConfirm.Count == 0)
                        {
                            var errors = string.Format(Resources.MessageInfo.E0008,"Confirmation");
                            LoggingUtil.WriteLog(errors);
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                        }

                        var resultInfo = service.UpdatePaidHolidayCheck(listConfirm, company);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdatePaidHolidayCheck", resultInfo.Message);
                        if (resultInfo.Status != ResultStatus.EditSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);                            
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        /// <summary>
        /// Convert input data to list confirmation
        /// </summary>
        /// <param name="data">input data</param>
        /// <returns></returns>
        private static Collection<Confirmation> ConvertData(Collection<CheckPaidHolidayRequestInput> data)
        {
            Collection<Confirmation> lstConfirm = new Collection<Confirmation>();
            Confirmation confirmItem;
            //find the company
            var getMemberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(data[0].updatedAccountId);
            var dateNow = AzureUtil.LocalDateTimeNow(getMemberInfo.time_zone_id);
            if (data != null && data.Count > 0)
            {
                foreach (var item in data)
                {
                    if (item.lstCheck != null && item.lstCheck.Count > 0)
                    {
                        foreach (var itemCheck in item.lstCheck)
                        {
                            confirmItem = new Confirmation
                            {
                                application_id = item.applicationId,
                                application_form_id = (byte)ConfirmationType.PaidHoliday,
                                confirmed_datetime = dateNow,
                                account_id = itemCheck.checkAcc,
                                check_status = itemCheck.checkStt,
                                check_sequence = (byte)itemCheck.checkSequence,
                                application_sequence = 1,
                                reason = itemCheck.reason,
                                updated_date = dateNow,
                                updated_account_id = item.updatedAccountId
                            };

                            lstConfirm.Add(confirmItem);
                        }
                    }
                } 
            }

            return lstConfirm;
        }
    }
}
﻿using AM.BusinessLayer;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.Const;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static AM.BusinessLayer.BaseService;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/Announcement")]
    public class AnnouncementController : ApiController
    {
        // POST: api/Announcement/GetAnnouncement
        [HttpPost]
        [Route("GetAnnouncement")]
        public HttpResponseMessage LoadAnnouncementInfo([FromBody]AnnouncementInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetAnnouncement", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AnnouncementService service = ServiceLocator.Resolve<AnnouncementService>();

                        var listAnnouncement = service.GetAnnouncement(data.CompanyId);

                        if (listAnnouncement == null || listAnnouncement.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list announcement
                            response.Content = new JsonContent(listAnnouncement);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetAnnouncement", JsonConvert.SerializeObject(listAnnouncement));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Announcement/DeleteAnnouncement
        [HttpPost]
        [Route("DeleteAnnouncement")]
        public HttpResponseMessage DeleteAnnouncement([FromBody]AnnouncementInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "DeleteAnnouncement", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AnnouncementService service = ServiceLocator.Resolve<AnnouncementService>();
                        var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, data.CompanyId.ToString(),null);
                        DateTime now = AzureUtil.LocalDateTimeNow(company.time_zone_id);
                        
                        if (data == null)
                        {
                            response.StatusCode = HttpStatusCode.BadRequest;
                            return response;
                        }

                        // ====================
                        // Update into database
                        // ====================
                        // create a model
                        Announcement model = new Announcement()
                        {
                            id = data.Id,
                            title = data.Title,
                            description = data.Description,
                            created_date = now,
                            created_account_id = data.UpdatedAccountId,
                            updated_date = now,
                            updated_account_id = data.UpdatedAccountId,
                            deleted_date = (data.TypeDelete == ConstType.TYPE_DELETE.Recover)? new Nullable<DateTime>() :now,
                            deleted_account_id = (data.TypeDelete == ConstType.TYPE_DELETE.Recover) ? new Nullable<int>() : data.UpdatedAccountId,
                        };
                        // delete announcement
                        ResultInfo resultInfo = new ResultInfo();
                        if (data.TypeDelete == ConstType.TYPE_DELETE.Recover)
                        {
                             resultInfo = service.RecoverAnnouncement(model);
                        }
                        else
                             resultInfo = service.DeleteAnnouncement(model);
                        // check result
                        if (resultInfo.Status != ResultStatus.DeleteSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "DeleteAnnouncement", resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        response.StatusCode = HttpStatusCode.OK;
                        //writelog
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "DeleteAnnouncement", null);
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        // POST: api/Announcement/UpdateAnnouncement
        [HttpPost]
        [Route("UpdateAnnouncement")]
        public async Task<HttpResponseMessage> UpdateAnnouncement([FromBody]AnnouncementInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateAnnouncement", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AnnouncementService service = ServiceLocator.Resolve<AnnouncementService>();
                        var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, data.CompanyId.ToString(),null);
                        DateTime now = AzureUtil.LocalDateTimeNow(company.time_zone_id);

                        if (data == null)
                        {
                            response.StatusCode = HttpStatusCode.BadRequest;
                            return response;
                        }

                        // ====================
                        // Update into database
                        // ====================
                        // create a model
                        Announcement model = new Announcement()
                        {
                            id = data.Id,
                            title = data.Title,
                            description = data.Description,
                            created_date = now,
                            created_account_id = data.UpdatedAccountId,
                            updated_date = now,
                            updated_account_id = data.UpdatedAccountId,
                            draft_flg = data.FlgDraft,
                            post_date = data.FlgDraft ? new Nullable<DateTime>() : now,
                            deleted_date = null,
                            deleted_account_id = null,
                            company_id = data.CompanyId
                        };
                        // Update/Insert announcement
                        var resultInfo = service.Merge(model);
                        // check result
                        if (resultInfo.Status != ResultStatus.EditSuccess)
                        {
                            // Write Log Message
                            LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        // Get company information
                        var companyInfo = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, data.CompanyId.ToString(),null);
                        
                        // Post to slack when company use slack and check post to flag
                        if (companyInfo != null && companyInfo.slack_flg && data.FlgPostSlack)
                        {
                            // Call api post message to slack
                            resultInfo = await service.PostMessageToSlack(data.Email, data.Title, companyInfo);

                            if (resultInfo.Status != ResultStatus.RegistSuccess)
                            {
                                // Write Log Message
                                LoggingUtil.WriteLog(resultInfo.Message);

                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                        }
                        //write log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateAnnouncement", resultInfo.Message);
                        //response.StatusCode = HttpStatusCode.OK;
                        return Request.CreateErrorResponse(HttpStatusCode.OK, resultInfo.Message);
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Announcement/GetAnnouncementApproved
        [HttpPost]
        [Route("GetApproveWaiting")]
        public HttpResponseMessage GetApproveWaiting([FromBody]ApproveWaitingInfoInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetApproveWaiting", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AnnouncementService service = ServiceLocator.Resolve<AnnouncementService>();
                        var listAnnouncementApporved = service.GetApproveWaiting(data.account_id, data.flg_admin, data.company_id);
                        if (listAnnouncementApporved == null || listAnnouncementApporved.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list announcement
                            response.Content = new JsonContent(listAnnouncementApporved);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetApproveWaiting", JsonConvert.SerializeObject(listAnnouncementApporved));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}

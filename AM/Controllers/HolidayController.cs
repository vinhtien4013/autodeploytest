﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

[assembly: CLSCompliant(true)]
namespace AM.Controllers
{
    [RoutePrefix("api/Holiday")]
    public class HolidayController : ApiController
    {
        // POST: api/Holiday/GetHolidayByYear
        [HttpPost]
        [Route("GetHolidayByYear")]
        public HttpResponseMessage GetHolidayByYear([FromBody] HolidayInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetHolidayByYear",JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        HolidayService service = ServiceLocator.Resolve<HolidayService>();
                        var list = service.GetHolidayByYear(data.year, data.companyId);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list holiday by year
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetHolidayByYear", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Holiday/RegisterHoliday
        [HttpPost]
        [Route("RegisterHoliday")]
        public HttpResponseMessage RegisterHoliday([FromBody] ParameterRegist data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "RegisterHoliday",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        HolidayService service = ServiceLocator.Resolve<HolidayService>();
                        List<Holiday> holidayLst = new List<Holiday>();

                        bool result  = ConvertFromInputToModel(ref holidayLst, data);
                        if(result == false)
                        {
                            // Write log
                            LoggingUtil.WriteLog(string.Format(Resources.MessageInfo.E0003, "holidayLst"));
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(Resources.MessageInfo.E0003, "holidayLst"));
                        }

                        var resultInfo = service.UpdateHoliday(holidayLst);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "RegisterHoliday", resultInfo.Message);
                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);

                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Holiday/DeleteHoliday
        [HttpPost]
        [Route("DeleteHoliday")]
        public HttpResponseMessage DeleteHoliday([FromBody] HolidayInputForDelete data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "DeleteHoliday",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        HolidayService service = ServiceLocator.Resolve<HolidayService>();
                        var memberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(data.account_id);
                        var nowTime = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);
                        Holiday holidayInfo = new Holiday()
                        {
                            id = data.id,
                            updated_account_id = data.account_id,
                            updated_date = nowTime,
                            deleted_account_id = data.account_id,
                            deleted_date = nowTime
                        };

                        var resultInfo = service.DeleteHoliday(holidayInfo);
                        //write log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "DeleteHoliday",resultInfo.Message);
                        if (resultInfo.Status != ResultStatus.DeleteSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);

                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

#pragma warning disable CS3001 // Argument type is not CLS-compliant
        /// <summary>
        /// Convert data from list input to model
        /// </summary>
        /// <param name="holidayLst">List holiday for register</param>
        /// <param name="data"> data input</param>
        /// <returns></returns>
        public static bool ConvertFromInputToModel(ref List<Holiday> holidayLst, ParameterRegist data)
#pragma warning restore CS3001 // Argument type is not CLS-compliant
        {
            if (data.holidaylist.Count == 0)
            {
                return false;
            }
            else
            {
                holidayLst = new List<Holiday>();
                Nullable<int> tmpAcc = null;
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null,data.holidaylist[0].company_id.ToString(),null);
                var nowTime = AzureUtil.LocalDateTimeNow(company.time_zone_id);
                for (int i = 0; i < data.holidaylist.Count; i++)
                {
                    var holiday = data.holidaylist[i];
                    var newHoliday = new Holiday() { 
                        id = holiday.id,
                        holiday_date_from = holiday.holiday_date_from,
                        holiday_date_to = holiday.holiday_date_to,
                        holiday_name = holiday.holiday_name,
                        created_date = nowTime,
                        created_account_id = holiday.created_account_id,
                        updated_account_id = holiday.updated_account_id,
                        updated_date = nowTime,
                        deleted_account_id = holiday.deleted_date.HasValue ? holiday.created_account_id : tmpAcc,
                        deleted_date = holiday.deleted_date,
                        company_id = holiday.company_id
                    };

                holidayLst.Add(newHoliday);
                }

                return true;
            }
        }

    }
}

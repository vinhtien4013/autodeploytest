﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model;
using AM.Model.api;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static AM.BusinessLayer.BaseService;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/WorkingTime")]
    public class WorkingTimeController : ApiController
    {
        // POST: api/WorkingTime/GetWorkingTime
        [HttpPost]
        [Route("GetWorkingTime")]
        public HttpResponseMessage GetWorkingTime([FromBody]WorkingTimeInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetWorkingTime", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        WorkingRecordService service = ServiceLocator.Resolve<WorkingRecordService>();
                        var accountByID = ServiceLocator.Resolve<AccountService>().GetDataById(data.account_id);
                        var company_info = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, accountByID.company_id.ToString(),null);
                        var list = service.GetWorkingTime(data.account_id, data.register_date,company_info);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list workingtimerecord get by account_id and month
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetWorkingTime", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/WorkingTime/UpdateWorkingTime
        [HttpPost]
        [Route("UpdateWorkingTime")]
        public HttpResponseMessage UpdateWorkingTime([FromBody] UpdateWorkingTimeInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateWorkingTime",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        WorkingRecordService service = ServiceLocator.Resolve<WorkingRecordService>();
                        List<WorkingtimeRecord> listInfo = new List<WorkingtimeRecord>();

                        var result = ConvertToModel(data, ref listInfo);

                        if (result == false)
                        {
                            // Write log
                            LoggingUtil.WriteLog(string.Format(Resources.MessageInfo.E0003, "data"));
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(Resources.MessageInfo.E0003, "data"));
                        }

                        var resultInfo = service.UpdateWorkingTime(listInfo, data.company_id);
                        //write log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateWorkingTime", resultInfo.Message);
                        if (resultInfo.Status != ResultStatus.EditSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);

                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/WorkingTime/GetCheckAttendance
        /// <summary>
        /// Get list of check attendance
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCheckAttendance")]
        public HttpResponseMessage GetCheckAttendance([FromBody]CheckAttendanceInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetCheckAttendance",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        WorkingRecordService service = ServiceLocator.Resolve<WorkingRecordService>();
                        var list = service.GetCheckAttendance(data.register_date, data.company_id);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetCheckAttendance", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/WorkingTime/UpdateWorkingTimeGoing
        [HttpPost]
        [Route("UpdateWorkingTimeGoing")]
        public async Task<HttpResponseMessage> UpdateWorkingTimeGoing([FromBody] WorkingInput data)
        {
            return await CheckInOut(CheckInOutEnum.CheckIn, data);
        }
        
        // POST: api/WorkingTime/UpdateWorkingTimeLeaving
        [HttpPost]
        [Route("UpdateWorkingTimeLeaving")]
        public async Task<HttpResponseMessage> UpdateWorkingTimeLeaving([FromBody] WorkingInput data)
        {
            return await CheckInOut(CheckInOutEnum.CheckOut, data);
        }

        // POST: api/WorkingTime/GetWorkingTimeByAccount
        [HttpPost]
        [Route("GetWorkingTimeByAccount")]
        public HttpResponseMessage GetWorkingTimeByAccount([FromBody] WorkingInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                
                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetWorkingTimeByAccount",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        WorkingRecordService service = ServiceLocator.Resolve<WorkingRecordService>();

                        // Check input data
                        if (data == null)
                        {
                            response.StatusCode = HttpStatusCode.BadRequest;
                        }
                        else
                        {
                            //get time zone
                            var memberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(data.account_id);
                            DateTime dateNow = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);
                            // Get working time record information
                            var workingTimeInfo = service.GetWorkTimeByAccount(dateNow, data.account_id);

                            if (workingTimeInfo == null)
                            {
                                workingTimeInfo = new WorkingtimeRecord();
                            }

                            response.StatusCode = HttpStatusCode.OK;

                            // return working time information
                            response.Content = new JsonContent(workingTimeInfo);
                            //write log
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetWorkingTimeByAccount", JsonConvert.SerializeObject(workingTimeInfo));
                        }
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/WorkingTime/GetStaffWorkingStatus
        [HttpPost]
        [Route("GetStaffWorkingStatus")]
        public HttpResponseMessage GetStaffWorkingStatus([FromBody] GetStaffWKInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //Check token company
                if (!ServiceLocator.Resolve<CommonService>().CheckCompanyToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0037;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // find the company by token
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(Request.Headers.GetValues(CONST_VALUE.COMPANY_TOKEN).First(), null, null);
                if (company == null)
                {
                    var errors = Resources.MessageInfo.E0038;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetStaffWorkingStatus", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();

                        // Check input data
                        if (data == null)
                        {
                            response.StatusCode = HttpStatusCode.BadRequest;
                        }
                        else
                        {
                            var result = new List<StaffWKStatusResponse>();
                            result = ServiceLocator.Resolve<WorkingRecordService>().GetStaffWKStatus(data.account_id,data.range,company);
                            //write log
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetStaffWorkingStatus", JsonConvert.SerializeObject(result));
                            if (result == null)
                            {
                                response.StatusCode = HttpStatusCode.NoContent;
                                return response;
                            }
                            response.StatusCode = HttpStatusCode.OK;
                            // return staff wk status
                            response.Content = new JsonContent(result);                            
                        }
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        /// <summary>
        /// Check in or check out
        /// </summary>
        /// <param name="checkInOut">1: Check in, 2: check out</param>
        /// <param name="data">data para</param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> CheckInOut(CheckInOutEnum checkInOut, WorkingInput data)
        {
            try
            {
                //Compare token 
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //Check token company
                if (!ServiceLocator.Resolve<CommonService>().CheckCompanyToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0037;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // find the company by token
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(Request.Headers.GetValues(CONST_VALUE.COMPANY_TOKEN).First(), null,null);
                if (company == null)
                {
                    var errors = Resources.MessageInfo.E0038;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // write log
                var apiName = checkInOut == CheckInOutEnum.CheckIn ? "UpdateWorkingTimeGoing" : "UpdateWorkingTimeLeaving";
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, apiName, jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    WorkingRecordService service = ServiceLocator.Resolve<WorkingRecordService>();
                    AccountService serviceAcc = ServiceLocator.Resolve<AccountService>();
                    var multiInOutFlg = StringUtil.ConvertObjectToBool(ConfigurationManager.AppSettings["MultiInOutFlg"].ToString());
                    TimeSpan tmpTime = new TimeSpan(0, 0, 0);

                    // Account id login
                    var account_id = 0;

                    // Staff id login
                    var staff_id = string.Empty;

                    // Staff name
                    var staff_name = string.Empty;

                    // Division_id
                    int divisionId = 0;

                    // Company_id
                    int companyId = company.id;

                    //face_image
                    var face_image = string.Empty;

                    //company token
                    var token = Request.Headers.GetValues(CONST_VALUE.COMPANY_TOKEN).First();
                    // DateTime.Now
                    DateTime dateNow = AzureUtil.LocalDateTimeNow(company.time_zone_id);

                    // Check data null
                    if (data == null)
                    {
                        response.StatusCode = HttpStatusCode.BadRequest;
                        return response;
                    }
                    MemberInfoModel memberInfo = new MemberInfoModel();
                    if (data.fingerprint_id != 0) // Check login by finger print
                    {
                        // Get account object by finger print id
                        memberInfo = serviceAcc.GetDataByFingerPrintId(data.fingerprint_id);

                    }
                    if (data.account_id != 0) // Login by mail account(on web)
                    {
                        memberInfo = serviceAcc.GetDataById(data.account_id);
                        account_id = data.account_id;
                    }

                    if (!string.IsNullOrEmpty(data.face_image))
                    {
                        //Call face api to detect
                        var detectResult = await serviceAcc.FaceDetectByImg(data.face_image);
                        if (detectResult.Count != CONST_VALUE.MAX_NUM_CANDIDATE_RETURN)
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, Resources.MessageInfo.I0025);
                            return Request.CreateErrorResponse(HttpStatusCode.NoContent, Resources.MessageInfo.I0025);
                        }
                        if (string.IsNullOrEmpty(detectResult.First().faceId))
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, detectResult.First().error.message);
                            return Request.CreateErrorResponse(detectResult.First().status, detectResult.First().error.message);
                        }
                        else
                        {
                            face_image = detectResult.First().faceId;
                        }
                        //call face identify
                        var identifyResult = await serviceAcc.IdentifyFaceByFaceId(face_image, AttendanceSettings.FaceEnv + companyId.ToString());
                        // if error
                        if (identifyResult.Count == 0)
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, Resources.MessageInfo.I0025);
                            return Request.CreateErrorResponse(identifyResult.First().status, Resources.MessageInfo.I0025);
                        }
                        if (identifyResult.First().error != null)
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, identifyResult.First().error.message);
                            return Request.CreateErrorResponse(identifyResult.First().status, identifyResult.First().error.message);
                        }
                        //if success but have no candidate return
                        if (identifyResult.First().candidates.Count == 0)
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, Resources.MessageInfo.I0025);
                            return Request.CreateErrorResponse(HttpStatusCode.NoContent, Resources.MessageInfo.I0025);
                        }
                        //find account if have personid from candidate
                        memberInfo = serviceAcc.GetDataByPersonId(identifyResult.First().candidates.First().personId,token);
                        if (memberInfo == null)
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, Resources.MessageInfo.I0025);
                            return Request.CreateErrorResponse(HttpStatusCode.NoContent, Resources.MessageInfo.I0025);
                        }
                    }

                    if (memberInfo == null || memberInfo.id == 0)
                    {
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, Resources.MessageInfo.I0013);
                        return Request.CreateErrorResponse(HttpStatusCode.NoContent, Resources.MessageInfo.I0013);
                    }

                    account_id = memberInfo.id;
                    staff_id = memberInfo.staff_id;
                    staff_name = memberInfo.staff_name;
                    divisionId = memberInfo.division_id;
                    companyId = memberInfo.company_id;

                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(memberInfo.language);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(memberInfo.language);

                    // Get staff name
                    //var staff_name = serviceAcc.GetStaffName(staff_id);
                    //var staff_name = MemoryCacheHelper.GetValue("staff_name" + staff_id);
                    //if (staff_name == null )
                    //{
                    //    var cache_staff_name = serviceAcc.GetStaffName(staff_id);
                    //    staff_name = cache_staff_name;
                    //    // for example get token from database and put grabbed token
                    //    // again in memCacher Cache
                    //    MemoryCacheHelper.Add("staff_name" + staff_id, new List<string>() { cache_staff_name }, DateTimeOffset.UtcNow.AddDays(30));
                    //}
                    //else
                    //{
                    //    List<string> collection = new List<string>((IEnumerable<string>)staff_name);
                    //    staff_name = collection.FirstOrDefault().ToString();
                    //    if(string.IsNullOrEmpty(collection.FirstOrDefault().ToString()))
                    //    {
                    //        var cache_staff_name = serviceAcc.GetStaffName(staff_id);
                    //        staff_name = cache_staff_name;
                    //        // for example get token from database and put grabbed token
                    //        // again in memCacher Cache
                    //        MemoryCacheHelper.Delete("staff_name" + staff_id);
                    //        MemoryCacheHelper.Add("staff_name" + staff_id, new List<string>() { cache_staff_name }, DateTimeOffset.UtcNow.AddDays(30));
                    //    }
                    //}

                    var resultInfo = new ResultInfo();
                    var workTimeInfo = service.GetWorkTimeByAccount(dateNow, account_id);


                    // Check in (going)
                    if (checkInOut == CheckInOutEnum.CheckIn)
                    {
                        if (!multiInOutFlg)
                        {
                            if (workTimeInfo != null && TimeSpan.Compare(workTimeInfo.actual_start_time , tmpTime) != 0)
                            {
                                resultInfo.Message = Resources.MessageInfo.E0011;
                                // Write Log Message
                                //LoggingUtil.WriteLog(resultInfo.Message);
                                LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName,resultInfo.Message);
                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                        }
                        
                        //if(workTimeInfo != null && TimeSpan.Compare(workTimeInfo.device_end_time, tmpTime) != 0)
                        //{
                        //    resultInfo.Message = ErrorMessages.AldreadyCheckOut;
                        //    // Write Log Message
                        //    LoggingUtil.WriteLog(resultInfo.Message);

                        //    // Log Error
                        //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        //}

                        // Regist check in infomation for staff
                        resultInfo = service.RegisterCheckinInfo(account_id, dateNow, company);
                    }
                    else // Check out (leaving)
                    {
                        IList<int> lstCheckAccId = new List<int>();
                        // Check don't have data before click leaving
                        if (workTimeInfo == null || workTimeInfo.actual_start_time == null || TimeSpan.Compare(workTimeInfo.actual_start_time, tmpTime) == 0)
                        {
                            resultInfo.Message = Resources.MessageInfo.E0012;
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                        if (!multiInOutFlg)
                        {
                            if (TimeSpan.Compare(workTimeInfo.device_end_time, tmpTime) != 0)
                            {
                                resultInfo.Message = Resources.MessageInfo.E0013;
                                // Write Log Message
                                //LoggingUtil.WriteLog(resultInfo.Message);
                                LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, resultInfo.Message);
                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                            //get min working time from company
                            var min_wkt = new TimeSpan(00,company.min_workingtime,00);
                            if (!service.CheckTimeLeaving(dateNow, workTimeInfo.actual_start_time + min_wkt))
                            {
                                resultInfo.Message = string.Format(Resources.MessageInfo.E0014,company.min_workingtime);
                                // Write Log Message
                                //LoggingUtil.WriteLog(resultInfo.Message);
                                LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, resultInfo.Message);
                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                        }

                        if (TimeSpan.Compare(workTimeInfo.device_end_time, tmpTime) == 0)
                        {
                            // Get list account id for confimation
                             lstCheckAccId = serviceAcc.GetListCheckAccountId(account_id, divisionId).Distinct().ToList();
                        }

                        // Update leaving time of staff
                        resultInfo = service.UpdateCheckOutInfo(account_id, workTimeInfo,lstCheckAccId, company);
                    }

                    if (resultInfo.Status != ResultStatus.RegistSuccess)
                    {
                        // Write Log Message
                        //LoggingUtil.WriteLog(resultInfo.Message);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, resultInfo.Message);
                        // Log Error
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                    }


                    // Post to slack when company use slack
                    if (company.slack_flg)
                    {
                        if (String.IsNullOrEmpty(StringUtil.ConvertObjectToString(staff_name)))
                            staff_name = staff_id;
                        // Call api post message to slack
                        resultInfo = await service.PostMessageToSlack(checkInOut, dateNow, StringUtil.ConvertObjectToString(staff_name),company);

                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);                           
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, apiName, resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                    }

                    //Write log
                    LoggingUtil.WriteLog(resultInfo.Message);

                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new JsonContent(new {
                        staff_id = staff_id,
                        staff_name = staff_name,
                        time = DateTimeUtil.RoundingTime(dateNow, StringUtil.ConvertObjectToInteger32(checkInOut)).ToString("yyyy-MM-dd HH:mm:ss"),
                        message = resultInfo.Message 
                    });

                    return response;
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

#pragma warning disable CS3001 // Argument type is not CLS-compliant
        /// <summary>
        /// Convert Data from Request to Model Table
        /// </summary>
        /// <param name="data">data from Request</param>
        /// <param name="listInfo">List data model</param>
        /// <returns></returns>
        public static bool ConvertToModel(UpdateWorkingTimeInput data, ref List<WorkingtimeRecord> listInfo)
#pragma warning restore CS3001 // Argument type is not CLS-compliant
        {
            if (data.Data_detail == null || data.Data_detail.Count == 0)
            {
                return false;
            }
            else
            {
                WorkingRecordService service = ServiceLocator.Resolve<WorkingRecordService>();
                listInfo = new List<WorkingtimeRecord>();
                DetailInput detail_input = new DetailInput();
                var memberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(data.updated_account_id);
                DateTime nowTime = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);

                for (int i = 0; i < data.Data_detail.Count; i++)
                {
                    WorkingtimeRecord detail_info = new WorkingtimeRecord();
                    detail_input = data.Data_detail[i];

                    var id = (detail_input.id == null || detail_input.id.Equals(string.Empty)) ? 0 : Convert.ToInt32(detail_input.id);
                    if (id != 0)
                    {
                        // Check exist in DB
                        var existdata = service.GetDataById(id);
                        if (existdata != null)
                        {
                            detail_info = existdata;
                            if(detail_input.modify == CONST_VALUE.DEL_ROW)
                            {
                                detail_info.deleted_date = nowTime;
                                detail_info.deleted_account_id = data.updated_account_id;
                            }    
                        }    
                            
                        else id = 0;
                    } 

                    detail_info.id = id;
                    detail_info.account_id = data.account_id;
                    detail_info.register_date = detail_input.working_date;
                    detail_info.expect_start_time = detail_input.expect_start_time;
                    detail_info.expect_end_time = detail_input.expect_end_time;
                    detail_info.actual_start_time = detail_input.start_working_time;
                    detail_info.actual_end_time = detail_input.end_working_time;
                    detail_info.rest_time = detail_input.rest_time.Value;
                    detail_info.created_date = nowTime;
                    detail_info.created_account_id = data.updated_account_id;
                    detail_info.updated_date = nowTime;
                    detail_info.updated_account_id = data.updated_account_id;
                    detail_info.additional_rest_time = (detail_input.additional_rest_time.Value == 0) ? null : detail_input.additional_rest_time;
                    detail_info.overtime_record.total_over_timework = detail_input.over_time;
                    detail_info.is_reject_selfcheck = detail_input.is_reject_selfcheck;

                    listInfo.Add(detail_info);
                }

                return true;
            }
        }        
    }
}
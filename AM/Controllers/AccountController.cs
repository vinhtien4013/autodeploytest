﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.BusinessLayer.AWSService;
using AM.BusinessLayer.FaceAzureService;
using AM.BusinessLayer.FingerPrintService;
using AM.BusinessLayer.SendGrid;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model;
using AM.Model.api;
using AM.Model.Const;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        // POST: api/Account/GetMemberInfo
        [HttpPost]
        [Route("GetMemberInfo")]
        public  HttpResponseMessage GetMemberInfo([FromBody]MemberInfoInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // check data input
                if (data == null)
                {
                    var errors = string.Format(Resources.MessageInfo.E0001,"data");
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetMemberInfo",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = ServiceLocator.Resolve<AccountService>();
                        MemberInfoModel accountinfo;
                        // get data by email
                        if (data.login_by == LOGIN_TYPE.MANUAL_LOGIN)
                        {
                            MemberInfoManualModel accountManaulInfo = service.GetMemberInfoByManual(data.email, BCrypt.Net.BCrypt.HashPassword(data.password.ToString()), data.token);
                            if (accountManaulInfo != null)
                            {
                                accountinfo = BCrypt.Net.BCrypt.Verify(data.password, accountManaulInfo.password) ? service.GetMemberInfoByGoogle(data.email, data.token) : null;
                            }
                            else
                                accountinfo = null;
                            
                        }
                        else if (data.login_by == LOGIN_TYPE.GOOGLE_LOGIN)
                        {
                            accountinfo = service.GetMemberInfoByGoogle(data.email, data.token);
                        }
                        else 
                        {
                            // get data by fingerprint_id
                            accountinfo = service.GetDataByFingerPrintId(StringUtil.ConvertObjectToInteger32(data.finger_id), data.token);
                        }

                        if (accountinfo == null)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list account by email
                            response.Content = new JsonContent(accountinfo);
                        }
                        //Write log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetMemberInfo", JsonConvert.SerializeObject(accountinfo));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // GET: api/Account/GetAccount?comany_id=
        //with param company_id
        [HttpGet]
        [Route("GetAccount")]
        public HttpResponseMessage LoadAccountList(string company_id)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        // write log
                        //var jsonStr = JsonConvert.SerializeObject(data);
                        //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                        var data = new
                        {
                            company_id = company_id
                        };
                        LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetAccount", JsonConvert.SerializeObject(data));
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = new AccountService();
                        var listAccount = service.GetAccount(company_id);
                        if (listAccount == null || listAccount.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list account by email
                            response.Content = new JsonContent(listAccount.OrderBy(a => a.division_id));
                        }
                        ///write log
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetAccount", JsonConvert.SerializeObject(listAccount));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET: api/Account/GetAccount
        //without param
        [HttpGet]
        [Route("GetAccount")]
        public HttpResponseMessage LoadAccountList()
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetAccount", null);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();

                        AccountService service = ServiceLocator.Resolve<AccountService>();
                        var listAccount = service.GetAccount(ConstType.CONST_VALUE.SESSION_COMPANY_DEFAULT);

                        if (listAccount == null || listAccount.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list account by email
                            response.Content = new JsonContent(listAccount.OrderBy(a => a.division_id));
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetAccount", JsonConvert.SerializeObject(listAccount));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Account/GetAccountByCondition
        [HttpPost]
        [Route("GetAccountByCondition")]
        public HttpResponseMessage GetAccountByCondition([FromBody]ParameterSearch searchInput)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // write log
                var jsonStr = JsonConvert.SerializeObject(searchInput);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetAccountByCondition", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {                       
                        //response
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = new AccountService();
                        //convert to AM.Model.ParameterSearchAccount
                        var convertInput = new ParameterSearchAccount()
                        {
                            company_id = searchInput.company_id,
                            search_Division_Name = searchInput.search_Division_Name,
                            search_Staff_ID = searchInput.search_Staff_ID,
                            search_Finger_ID = searchInput.search_Finger_ID,
                            search_Staff_Name = searchInput.search_Staff_Name
                        };
                        //get list account by search input
                        var listAccount = service.GetAccountByCondition(convertInput);
                        if (listAccount == null || listAccount.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list account by search input
                            response.Content = new JsonContent(listAccount);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetAccountByCondition", JsonConvert.SerializeObject(listAccount));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        // POST: api/Account/UpdateAccount
        [HttpPost]
        [Route("UpdateAccount")]
        public async Task<HttpResponseMessage> UpdateAccount([FromBody]ParameterEditAccount data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //Check token company
                if (!ServiceLocator.Resolve<CommonService>().CheckCompanyToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0037;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // find the company by token
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(Request.Headers.GetValues(CONST_VALUE.COMPANY_TOKEN).First(),null,null);
                if (company == null)
                {
                    var errors = Resources.MessageInfo.E0038;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateAccount", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = ServiceLocator.Resolve<AccountService>();
                        int company_id = company.id;
                        var result = await ConvertData(data,company);
                        if (!string.IsNullOrEmpty(result.First().message))
                        {
                            // Write log
                            //LoggingUtil.WriteLog(string.Format(ErrorMessages.CantConvertData, "editAccountLst"));
                            LoggingUtil.WriteLog(result.First().message);
                            var staff_error = !string.IsNullOrEmpty(result.First().account.staff_id) ? result.First().account.staff_id : "";
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, staff_error + ": " + result.First().message);
                        }
                        else
                        {
                            var train = await ServiceLocator.Resolve<FaceAPIAzureService>().TrainingFace(AttendanceSettings.FaceEnv + company_id);
                            if (train != null)
                            {
                                LoggingUtil.WriteLog(train.error.message);
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, train.error.message);
                            }
                        }
                        var lstUpdateAccount = new List<Account>();
                        foreach (var item in result)
                        {
                            lstUpdateAccount.Add(item.account);
                        }
                        var resultInfo = service.UpdateAccountInfo(lstUpdateAccount);
                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {

                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateAccount", resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        foreach (var accountDetail in lstUpdateAccount)
                        {
                            if ((accountDetail.id != 0 && !string.IsNullOrEmpty(accountDetail.password)) || accountDetail.id == 0)
                            {
                                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(accountDetail.language);
                                Thread.CurrentThread.CurrentUICulture = new CultureInfo(accountDetail.language);
                                SendGridService sendGridService = ServiceLocator.Resolve<SendGridService>();
                                sendGridService.SendEmail(accountDetail.email, MessageInfo.I0038, string.Format(MessageInfo.I0039,"<strong>" + accountDetail.password + "</strong>"));

                            }
                        }
                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);                            
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateAccount", resultInfo.Message);
                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }
        // POST: api/Account/UpdateAccountLanguage
        [HttpPost]
        [Route("UpdateAccountLanguage")]
        public  HttpResponseMessage UpdateAccountLanguage([FromBody] UpdateAccountLanguageInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateAccountLanguage", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = ServiceLocator.Resolve<AccountService>();
                        var resultInfo = service.UpdateAccountLanguage(data.id, data.language);
                        if (resultInfo.Status == ResultStatus.RegistSuccess)
                        {
                            response.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            // Write Log Message
                            LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateAccountLanguage", null);
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Account/SendPassword
        [HttpPost]
        [Route("SendPassword")]
        public  HttpResponseMessage SendPassword([FromBody] UpdateAccountPasswordModel data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "SendPassword", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = ServiceLocator.Resolve<AccountService>();
                        MemberInfoModel accountinfo;
                        accountinfo = service.GetMemberInfoByGoogle(data.email, data.token);
                        if (accountinfo != null)
                        {
                            string password = CreateRanDomPassword(8);
                            string hash = BCrypt.Net.BCrypt.HashString(password);
                            var resultInfo = service.UpdateAccountPassword(hash, accountinfo.id,accountinfo.id);
                            if (resultInfo.Status == ResultStatus.RegistSuccess)
                            {
                                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(accountinfo.language);
                                Thread.CurrentThread.CurrentUICulture = new CultureInfo(accountinfo.language);
                                SendGridService sendGridService = ServiceLocator.Resolve<SendGridService>();
                                sendGridService.SendEmail(accountinfo.email, MessageInfo.I0038, string.Format(MessageInfo.I0039, "<strong>" + password + "</strong>"));
                            }
                            else
                            {
                                // Write Log Message
                                LoggingUtil.WriteLog(resultInfo.Message);
                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                        }
                        if (accountinfo == null)
                        {
                            var errors = Resources.MessageInfo.I0013;
                            LoggingUtil.WriteLog(errors);
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "SendPassword", null);
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Account/UpdateAccountPassword
        [HttpPost]
        [Route("UpdateAccountPassword")]
        public HttpResponseMessage UpdateAccountPassword([FromBody] ChangeAccountPassword data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                
                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateAccountPassword", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = ServiceLocator.Resolve<AccountService>();
                        string hash = BCrypt.Net.BCrypt.HashString(data.password.ToString());
                        var resultInfo = service.UpdateAccountPassword(hash, data.id,data.updated_account_id);

                        if (resultInfo.Status == ResultStatus.RegistSuccess)
                        {
                            response.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            // Write Log Message
                            LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateAccountPassword", null);
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        // POST: api/Account/DeleteAccount
        [HttpPost]
        [Route("DeleteAccount")]
        public HttpResponseMessage DeleteAccount([FromBody]AccountInputForDelete data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "DeleteAccount", jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        FingerPrintService Fingerservice = ServiceLocator.Resolve<FingerPrintService>();
                        AccountService AccountService = ServiceLocator.Resolve<AccountService>();

                        // Delete finger print id
                        var resultInfo = Fingerservice.DeleteFingerPrint(data.fingerprint_id);
                        if (resultInfo.Status != ResultStatus.DeleteSuccess)
                        {
                            // Write Log Message
                            LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                        else
                        {
                            // Delete Account
                            resultInfo = AccountService.DeleteAccount(data.fingerprint_id);
                            if (resultInfo.Status != ResultStatus.DeleteSuccess)
                            {
                                // Write Log Message
                                LoggingUtil.WriteLog(resultInfo.Message);
                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "DeleteAccount", resultInfo.Message);
                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Account/IsResponsibleStaff
        [HttpPost]
        [Route("IsResponsibleStaff")]
        public HttpResponseMessage IsResponsibleStaff([FromBody]CheckResponsibleInput data)
        {

            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //LoggingUtil.WriteLog(Resources.MessageInfo.E0039);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "IsResponsibleStaff", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {

                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        AccountService service = ServiceLocator.Resolve<AccountService>();

                        var result = service.IsResponsibleStaff(data.AccountId, data.CompanyId);

                        response.StatusCode = HttpStatusCode.OK;
                        // true : Is response staff
                        // false : Is not response staff
                        response.Content = new JsonContent(new
                        {
                            result = result
                        });
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "IsResponsibleStaff", result.ToString());
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }


        // GET: api/Account/ClearCacheMemory
        [HttpGet]
        [Route("ClearCacheMemory")]
        public HttpResponseMessage ClearCacheMemory()
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //WriteLog
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "ClearCacheMemory", null);
                try
                {
                    HttpResponseMessage response = new HttpResponseMessage();

                    // Delete finger print id
                    var resultInfo = ServiceLocator.Resolve<CommonService>().ClearCacheMemoryHelper();
                    if (resultInfo.Status != ResultStatus.DeleteSuccess)
                    {
                        // Write Log Message
                        LoggingUtil.WriteLog(resultInfo.Message);
                        // Log Error
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                    }

                    //Write log
                    //LoggingUtil.WriteLog(resultInfo.Message);
                    LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "ClearCacheMemory", resultInfo.Message);
                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new JsonContent(new
                    {
                        message = resultInfo.Message
                    });
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // DELETE : api/Account/DeleteUnnecessaryFaceImage
        [HttpDelete]
        [Route("DeleteUnnecessaryFaceImage")]
        public HttpResponseMessage DeleteUnnecessaryFaceImage()
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "DeleteUnnecessaryFaceImage", null);
                try
                {
                    //response
                    HttpResponseMessage response = new HttpResponseMessage();
                    var result = ServiceLocator.Resolve<AWSS3Service>().MultiObjectDelete();
                    if (!result)
                    {
                        response.StatusCode = HttpStatusCode.BadRequest;
                        response.Content = new JsonContent(new
                        {
                            Message = MessageInfo.I0040
                        });
                    }
                    //Write log
                    //LoggingUtil.WriteLog("");
                    response.StatusCode = HttpStatusCode.OK;
                    return response;
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }                
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Convert data to List of account
        /// </summary>
        /// <param name="data"></param>
        /// <param name="company"></param>
        /// <returns></returns>
        private static async Task<List<MessageRegistorResult>> ConvertData(ParameterEditAccount data,Company company)
        {
            List<MessageRegistorResult> lstResultAccount = new List<MessageRegistorResult>();            
            AccountService service = ServiceLocator.Resolve<AccountService>();            
            if (data == null || data.editAccountLst == null || data.editAccountLst.Count == 0)
            {
                var result = new MessageRegistorResult();
                result.message = string.Format(Resources.MessageInfo.E0008,"editAccountLst");
                lstResultAccount.Clear();
                lstResultAccount.Add(result);
                return lstResultAccount;
            }
            //Get time now
            DateTime nowTime = AzureUtil.LocalDateTimeNow(company.time_zone_id);
            //Loop over all list params
            for (int i = 0; i < data.editAccountLst.Count; i++)
            {
                var result = new MessageRegistorResult();
                var account = data.editAccountLst[i];
                var existData = new Account();
                //flag for checking if face url is changed or not , false means not change
                bool changedFaceURL = false;
                bool deleteFaceFlag = false;
                // In case id != 0 (update exist account)
                if (account.id != 0)
                {
                    // Check exist in DB
                    existData = service.GetDataById(account.id);
                    if (existData == null)
                    {
                        result.account.staff_id = account.staff_id;
                        result.message = Resources.MessageInfo.I0013;
                        lstResultAccount.Clear();
                        lstResultAccount.Add(result);
                        return lstResultAccount;
                    }
                    if (string.IsNullOrEmpty(existData.face_image) && !string.IsNullOrEmpty(account.face_image))
                    {
                        //not have image then just add face
                        changedFaceURL = true;
                        deleteFaceFlag = false;
                    }
                    if (!string.IsNullOrEmpty(existData.face_image) && !string.IsNullOrEmpty(account.face_image))
                    {
                        var compareImgName = GetValidStrFromURL(account.face_image);
                        if (!existData.face_image.Equals(compareImgName))
                        {
                            // face ulr is changed , need to delete old face
                            changedFaceURL = true;
                            deleteFaceFlag = true;
                        }
                    }
                }

                var editAccount = new Account()
                {
                    id = account.id,
                    email = account.email,
                    staff_id = account.staff_id,
                    staff_name = account.staff_name,
                    sex = account.sex,
                    division_id = account.division_id,
                    fingerprint_id = account.fingerprint_id,
                    admin_flg = account.admin_flg,
                    joined_date = nowTime,
                    resign_date = nowTime,
                    face_image = string.IsNullOrEmpty(account.face_image) ? null : account.face_image,
                    person_id = string.IsNullOrEmpty(account.person_id) ? null : account.person_id,
                    persisted_face_id = string.IsNullOrEmpty(account.persisted_face_id) ? null: account.persisted_face_id,
                    created_date = nowTime,
                    created_account_id = account.updated_account_id,
                    updated_date = nowTime,
                    updated_account_id = account.updated_account_id,
                    deleted_date = account.deleted_date,
                    password = account.password,
                    slack_user_id = account.slack_user_id,
                    language = account.language
                };
                if (account.deleted_date == null)
                {
                    editAccount.deleted_account_id = null;
                    if (company.face_flg)
                    {
                        var updated_account = await FaceProcess(editAccount, company.id, changedFaceURL, deleteFaceFlag);
                        if (!string.IsNullOrEmpty(updated_account.message))
                        {
                            result = updated_account;
                            lstResultAccount.Clear();
                            lstResultAccount.Add(result);
                            return lstResultAccount;
                        }
                        editAccount = updated_account.account;
                    }
                }
                else
                {
                    editAccount.deleted_account_id = account.updated_account_id;
                    editAccount.updated_date = existData.updated_date;
                    editAccount.updated_account_id = existData.updated_account_id;
                }
                editAccount.face_image = string.IsNullOrEmpty(editAccount.face_image) ? null : GetValidStrFromURL(editAccount.face_image);
                result.account = editAccount;
                //Add to list
                lstResultAccount.Add(result);
            }

            return lstResultAccount;
        }


        /// <summary>
        /// Face process for account list params
        /// </summary>
        /// <param name="account">Account model</param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        private static async Task<MessageRegistorResult> FaceProcess(Account account, int company_id, bool urlChanged,bool deleteFaceFlag)
        {
            //result
            var result = new MessageRegistorResult
            {
                account = account,
                message = null
            };
            //format group face by env
            var formatGroupId = AttendanceSettings.FaceEnv + company_id;
            AccountService service = ServiceLocator.Resolve<AccountService>();
            FaceAPIAzureService face_service = ServiceLocator.Resolve<FaceAPIAzureService>();
            // check if is null person_id then create person id
            if (string.IsNullOrEmpty(account.person_id))
            {
                var personIdFromAPI = await face_service.CreateNewPerson(formatGroupId, account.staff_name);
                account.person_id = personIdFromAPI.personId;
                if (string.IsNullOrEmpty(account.person_id))
                {
                    result.message = face_service.GetFirstMessageFromFaceAPI(personIdFromAPI.error.message);
                    return result;
                }
            }
            //Check if have face_image and this is ADD new OR Change url value
            if (!string.IsNullOrEmpty(account.face_image) && (account.id == 0 || urlChanged))
            {               
                // if the account have full value face properties, then delete old face first
                if (!string.IsNullOrEmpty(account.person_id) && !string.IsNullOrEmpty(account.persisted_face_id) && deleteFaceFlag)
                {
                    var deleteFace = await face_service.PersonGroupDeleteFace(formatGroupId, account.person_id, account.persisted_face_id);
                    if (deleteFace != null)
                    {
                        if (deleteFace.error.statusCode != HttpStatusCode.NotFound || !deleteFace.error.code.Equals(CONST_VALUE.ErrorDeleteCodeCanSkip))
                        {
                            result.message = face_service.GetFirstMessageFromFaceAPI(deleteFace.error.message);
                            return result;
                        }
                    }
                }         
                // add face processing
                var persistedFromAPI = await face_service.AddFace(formatGroupId, account.person_id, account.face_image);
                account.persisted_face_id = persistedFromAPI.persistedFaceId;
                if (string.IsNullOrEmpty(persistedFromAPI.persistedFaceId))
                {
                    result.message = face_service.GetFirstMessageFromFaceAPI(persistedFromAPI.error.message);
                    return result;
                }
            }
            return result;
        }

        /// <summary>
        /// Get image name from URL AWS S3
        /// </summary>
        /// <param name="url">url</param>
        /// <returns>Image name</returns>
        private static string GetValidStrFromURL(string url)
        {
            var result = string.Empty;
            var split = url.Split('?');
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i].ToString().Contains("."))
                {
                    var arr = split[i].Split('/');
                    result = arr[arr.Length - 1];
                }
            }
            return result;
        }
        public static string CreateRanDomPassword(int length)
        {
            const string lower = "abcdefghijklmnopqrstuvwxyz";
            const string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string number = "1234567890";
            const string special = "!@#$%^&*";

            var middle = length / 2;
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                if (middle == length)
                {
                    res.Append(number[rnd.Next(number.Length)]);
                }
                else if (middle - 1 == length)
                {
                    res.Append(special[rnd.Next(special.Length)]);
                }
                else
                {
                    if (length % 2 == 0)
                    {
                        res.Append(lower[rnd.Next(lower.Length)]);
                    }
                    else
                    {
                        res.Append(upper[rnd.Next(upper.Length)]);
                    }
                }
            }
            return res.ToString();
        }
    }
}

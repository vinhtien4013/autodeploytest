﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.BusinessLayer.ConfirmationService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/Confirmation")]
    public class ConfirmationController : ApiController
    {
        // POST: api/Confirmation/UpdateCheckAttendance
        [HttpPost]
        [Route("UpdateCheckAttendance")]
        public HttpResponseMessage UpdateCheckAttendance([FromBody]Collection<UpdateCheckAttendanceInput> data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateCheckAttendance",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        ConfirmationService service = ServiceLocator.Resolve<ConfirmationService>();
                        List<Confirmation> listInfo = new List<Confirmation>();

                        var result = ConvertToModel(data, ref listInfo);

                        if (result == false)
                        {
                            // Write log
                            //LoggingUtil.WriteLog(string.Format(Resources.MessageInfo.E0003, "data"));
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateCheckAttendance", string.Format(Resources.MessageInfo.E0003, "data"));
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(Resources.MessageInfo.E0003, "data"));
                        }

                        // ====================
                        // Update into database
                        // ====================
                        foreach (var item in listInfo)
                        {
                            // Update check status
                            var resultInfo = service.UpdateCheckStatus(item);
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateCheckAttendance", resultInfo.Message);
                            if (resultInfo.Status != ResultStatus.EditSuccess)
                            {
                                // Write Log Message
                                //LoggingUtil.WriteLog(resultInfo.Message);
                                // Log Error
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                            }
                        }

                        response.StatusCode = HttpStatusCode.OK;
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        /// <summary>
        /// Convert Data from Request to Model Table
        /// </summary>
        /// <param name="data">data from Request</param>
        /// <param name="listInfo">List data model</param>
        /// <returns></returns>
        [CLSCompliant(false)]
        public static bool ConvertToModel(Collection<UpdateCheckAttendanceInput> data, ref List<Confirmation> listInfo)
        {
            // Check null
            if (data == null || data.Count == 0)
            {
                return false;
            }
            else
            {
                Confirmation confirm_info;
                listInfo = new List<Confirmation>();
                UpdateCheckAttendanceInput infoInput;
                //find time zone
                var memberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(data[0].updated_account_id);
                var dateNow = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);

                for (int i = 0; i < data.Count; i++)
                {
                    infoInput = data[i];
                    
                    for (int j = 0; j < infoInput.Data_detail.Count; j++)
                    {
                        // Create object confirmation
                        confirm_info = new Confirmation
                        {
                            application_id = infoInput.application_id,
                            application_form_id = (byte)ConfirmationType.WorkingTime,
                            confirmed_datetime = dateNow,
                            account_id = infoInput.Data_detail[j].account_id,
                            check_status = infoInput.Data_detail[j].check_status,
                            check_sequence = infoInput.Data_detail[j].check_sequence,
                            updated_account_id = infoInput.updated_account_id,
                            updated_date = dateNow,
                            reason = infoInput.Data_detail[j].reason,
                            application_sequence = infoInput.application_sequence
                        };

                        // Add to list confirmation
                        listInfo.Add(confirm_info);
                    }
                }
                return true;
            }
        }

        // POST: api/Confirmation/GetNotesWhenReject
        [HttpPost]
        [Route("GetNotesWhenReject")]
        public HttpResponseMessage ListNotesWhenReject([FromBody]CompanyInputParam data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.Path, null);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetNotesWhenReject", JsonConvert.SerializeObject(data));
                HttpResponseMessage response = new HttpResponseMessage();
                ConfirmationService service = ServiceLocator.Resolve<ConfirmationService>();

                // Get list of notes when reject
                var list = service.GetNotesWhenReject(data.CompanyId);
                if (list == null || list.Count == 0)
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new JsonContent(list);
                }
                //write log
                LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetNotesWhenReject", JsonConvert.SerializeObject(list));
                // Return
                return response;
            }
            catch (HttpException ex)
            {
                // Write log
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Confirmation/GetCheckNotesWhenReject
        [HttpPost]
        [Route("GetCheckNotesWhenReject")]
        public HttpResponseMessage ListCheckNotesWhenReject([FromBody]CompanyInputParam data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.Path, null);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetCheckNotesWhenReject",JsonConvert.SerializeObject(data));
                HttpResponseMessage response = new HttpResponseMessage();
                ConfirmationService service = ServiceLocator.Resolve<ConfirmationService>();

                // Get list of notes when reject
                var list = service.GetCheckNotesWhenReject(data.CompanyId);
                if (list == null || list.Count == 0)
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new JsonContent(list);
                }
                LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetCheckNotesWhenReject", JsonConvert.SerializeObject(list));
                // Return
                return response;
            }
            catch (HttpException ex)
            {
                // Write log
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
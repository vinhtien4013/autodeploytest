﻿using AM.BusinessLayer;
using AM.BusinessLayer.AttendanceSummaryService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/AttendanceSummary")]
    public class AttendanceSummaryController : ApiController
    {
        //GET: api/AttendanceSummary/GetAttendanceSummary
       [HttpPost]
       [Route("GetAttendanceSummary")]
        public HttpResponseMessage GetAttendanceSummary([FromBody]AttendanceSummaryInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                
                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetAttendanceSummary", JsonConvert.SerializeObject(data));

                try
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    AttendanceSummaryService service = ServiceLocator.Resolve<AttendanceSummaryService>();
                    var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, data.company_id.ToString(),null);
                    var now = AzureUtil.LocalDateTimeNow(company.time_zone_id);

                    if (data == null || (data.Date.Month > now.Month && data.Date.Year == now.Year) || (data.Date.Year > now.Year) )
                    {
                        var errors = string.Format(Resources.MessageInfo.E0003,"data");
                        LoggingUtil.WriteLog(errors);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                    }
                    else
                    {
                        var sumData = service.GetAttendanceSummary(data.Date.Month, data.Date.Year,data.account_id,data.staff_id,data.company_id);

                        if (sumData == null)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list attendance summary of all staff in the specificed month
                            response.Content = new JsonContent(sumData);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetAttendanceSummary", JsonConvert.SerializeObject(sumData));
                        return response;
                    }
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }

        //GET: api/AttendanceSummary/GetAttendanceSummaryDetail
        [HttpPost]
        [Route("GetAttendanceSummaryDetail")]
        public HttpResponseMessage GetAttendanceSummaryDetail([FromBody]AttendanceSummaryInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetAttendanceSummaryDetail", jsonStr);

                try
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    AttendanceSummaryService service = ServiceLocator.Resolve<AttendanceSummaryService>();
                    var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, data.company_id.ToString(),null);
                    var now = AzureUtil.LocalDateTimeNow(company.time_zone_id);
                    
                    // check input month > the current month
                    if (data == null || (data.Date.Month > now.Month && data.Date.Year == now.Year) || (data.Date.Year > now.Year))
                    {
                        var errors = string.Format(Resources.MessageInfo.E0003, "data");
                        LoggingUtil.WriteLog(errors);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                    }
                    else
                    {
                        var sumData = service.GetAttendanceSummaryDetail(data.Date.Month, data.Date.Year,data.account_id,data.staff_id, data.company_id);

                        if (sumData == null)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list attendance summary detail of all staff in the specificed month
                            response.Content = new JsonContent(sumData);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetAttendanceSummaryDetail", JsonConvert.SerializeObject(sumData));
                        return response;
                    }
                }
                catch (HttpException ex)
                {
                    LoggingUtil.WriteExceptionLog(ex.Message);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                throw;
            }
        }
    }
}
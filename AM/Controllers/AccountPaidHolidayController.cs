﻿using AM.BusinessLayer;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.BusinessLayer.BaseService;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/AccountPaidHoliday")]
    public class AccountPaidHolidayController : ApiController
    {
        // POST: api/AccountPaidHoliday/GetPaidHoliday
        [HttpPost]
        [Route("GetPaidHoliday")]
        public HttpResponseMessage LoadListPaidHoliday([FromBody]ListPaidHolidayInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetPaidHoliday", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();
                        var list = service.GetPaidHoliday(data.account_id, data.staff_id, data.company_id);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list paid holiday
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetPaidHoliday", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/AccountPaidHoliday/UpdatePaidHoliday
        [HttpPost]
        [Route("UpdatePaidHoliday")]
        public HttpResponseMessage UpdatePaidHoliday([FromBody]PaidHolidayInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdatePaidHoliday", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();
                        List<AccountPaidHoliday> listInfo = new List<AccountPaidHoliday>();
                        var resultInfo = new ResultInfo();

                        if (data.processing_status == (byte)ProcessingStatusEnum.AddMonthPaidHoliday)
                        {
                            resultInfo = service.UpdateMonthlyAddition(data.updated_account_id,data.company_id);
                        }
                        else
                        {
                            var result = ConvertToModel(data, ref listInfo);

                            if (result == false)
                            {
                                // Write log
                                LoggingUtil.WriteLog(string.Format(Resources.MessageInfo.E0030,"data"));
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(Resources.MessageInfo.E0030, "data"));
                            }

                             resultInfo = service.UpdateAccountPaidHoliday(listInfo, data.processing_status);
                        }
                        if (resultInfo.Status != ResultStatus.EditSuccess)
                        {
                            // Write Log Message
                            LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdatePaidHoliday", resultInfo.Message);
                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        // GET: api/AccountPaidHoliday/CheckMonthlyAddition
        [HttpGet]
        [Route("CheckMonthlyAddition")]
        public HttpResponseMessage CheckMonthlyAddition(string companyId)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //LoggingUtil.WriteLog(Resources.MessageInfo.E0040);
                var data = new
                {
                    companyId = companyId
                };
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "CheckMonthlyAddition", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {

                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();

                        var result = (service.CheckMonthlyAddition(companyId) > 0) ? true : false;

                        response.StatusCode = HttpStatusCode.OK;
                        // true: already add the monthly day
                        // false : not yet add monthly day
                        response.Content = new JsonContent(new
                        {
                            result = result
                        });
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "CheckMonthlyAddition", result.ToString());
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        // GET: api/AccountPaidHoliday/GetRequestManage
        [HttpPost]
        [Route("GetRequestManage")]
        public HttpResponseMessage GetRequestManage([FromBody]RequestManageInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                //Writelog
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetRequestManage", JsonConvert.SerializeObject(data));
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();
                        var list = service.GetRequestManage(data.id, data.account_id, data.date_from, data.date_to, data.approve_status);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list paid holiday
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetRequestManage", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        // POST: api/AccountPaidHoliday/RegisterRequestManage
        [HttpPost]
        [Route("RegisterRequestManage")]
        public HttpResponseMessage RegisterRequestManage([FromBody] ParameterRegistRequestManage data)
        {
            try
            {
                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "RegisterRequestManage", JsonConvert.SerializeObject(data));

                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                //Check token company
                if (!ServiceLocator.Resolve<CommonService>().CheckCompanyToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0037;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // find the company by token
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(Request.Headers.GetValues(CONST_VALUE.COMPANY_TOKEN).First(), null, null);
                if (company == null)
                {
                    var errors = Resources.MessageInfo.E0038;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
               
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();
                        RequestManageInputInfo requestdata = new RequestManageInputInfo();

                        // get number new Request
                        var newRequestNum = data.requestManageList.Where(r => r.id == 0).ToList().Count;
                       
                        // Check new request number more than the remain paid holiday
                        if (newRequestNum > 0)
                        {
                            double remainPaidHoliday = service.GetRemainPaidHolidayForAcc(data.requestManageList[0].account_id);
                            if(!CheckAuthRequest(data, remainPaidHoliday))
                            {
                                var errors = Resources.MessageInfo.E0007_Code;
                                LoggingUtil.WriteLog(errors);
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                            }
                        }

                        ConvertData(ref requestdata, data);
                        var resultInfo = service.RegisterRequestManage(requestdata, company);
                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {
                            // Write Log Message
                            LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "RegisterRequestManage", resultInfo.Message);
                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/AccountPaidHoliday/CheckExistApplication
        [HttpPost]
        [Route("CheckExistApplication")]
        public HttpResponseMessage CheckExistApplication(Inputforcheck data)
        {

            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                //LoggingUtil.WriteLog(Resources.MessageInfo.E0039);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "CheckExistApplication", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {

                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();
                        var objResult = new
                        {
                            result = false,
                            oldId = 0,
                            oldType = 0
                        };
                        var requestHoliday = service.GetRequestByAccount(data.account_id, data.application_datetime);
                        if(requestHoliday != null && (requestHoliday.paid_holiday_type == CONST_VALUE.ALL || (requestHoliday.paid_holiday_type!= null && data.applicationType == CONST_VALUE.ALL) || requestHoliday.paid_holiday_type == data.applicationType))
                        {
                            objResult = new
                            {
                                result = true,
                                oldId = 0,
                                oldType = 0
                            };
                        }
                        else
                        {
                            if (requestHoliday != null && requestHoliday.paid_holiday_type != null)
                            {
                                objResult = new
                                {
                                    result = false,
                                    oldId = requestHoliday.id,
                                    oldType = requestHoliday.paid_holiday_type.Value
                                };
                            }
                        }

                        //result = (service.HasApplication(data.account_id, data.application_datetime) > 0) ? true : false;

                        response.StatusCode = HttpStatusCode.OK;
                        // true : already had application in that datetime
                        // false : not yet had application in that datetime
                        response.Content = new JsonContent(objResult);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "CheckExistApplication", JsonConvert.SerializeObject(objResult));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET: api/AccountPaidHoliday/GetListPaidHolidayDetail
        [HttpGet]
        [Route("GetListPaidHolidayDetail")]
        public HttpResponseMessage LoadListPaidHolidayDetail(string account_id)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //Writelog
                var data = new
                {
                    account_id = account_id
                };
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetListPaidHolidayDetail", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();
                        var list = service.GetListPaidHolidayDetail(account_id);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list paid holiday
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetListPaidHolidayDetail", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/AccountPaidHoliday/DeleteMonthly
        [HttpPost]
        [Route("DeleteMonthly")]
        public HttpResponseMessage DeleteMonthly()
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //Write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "DeleteMonthly", null);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        PaidHolidayService service = ServiceLocator.Resolve<PaidHolidayService>();

                        var result = service.DeleteMonthly();

                        response.StatusCode = HttpStatusCode.OK;
                        response.Content = new JsonContent(new
                        {
                            result = result
                        });
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "DeleteMonthly", JsonConvert.SerializeObject(result.ToString()));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

#pragma warning disable CS3001 // Argument type is not CLS-compliant
        /// <summary>
        /// Convert data for update account_paid_holiday
        /// </summary>
        /// <param name="data"></param>
        /// <param name="listInfo">list account paid holiday</param>
        /// <returns></returns>
        public static bool ConvertToModel(PaidHolidayInput data, ref List<AccountPaidHoliday> listInfo)
#pragma warning restore CS3001 // Argument type is not CLS-compliant
        {
            if (data == null)
                return false;
            // Add paid holiday
            else
            {

                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, data.company_id.ToString(),null);
                DateTime now = AzureUtil.LocalDateTimeNow(company.time_zone_id);
                foreach (var itemData in data.Data_detail)
                {
                    AccountPaidHoliday accPaidHoliday = new AccountPaidHoliday(Convert.ToInt32(itemData.account_id));
                    accPaidHoliday.id = (int)itemData.id;
                    accPaidHoliday.account_id = Convert.ToInt32(itemData.account_id);
                    accPaidHoliday.application_datetime = itemData.application_datetime;
                    accPaidHoliday.registor_days = itemData.registor_days;
                    accPaidHoliday.created_account_id = data.updated_account_id;
                    accPaidHoliday.updated_account_id = data.updated_account_id;
                    accPaidHoliday.processing_status = data.processing_status;
                    accPaidHoliday.reason_txt = itemData.reason_txt;
                    accPaidHoliday.paid_holiday_type = null;
                    accPaidHoliday.deleted_date = itemData.deleted_date;
                    accPaidHoliday.deleted_account_id = itemData.deleted_account_id;
                    accPaidHoliday.application_datetime = now;
                    accPaidHoliday.created_date = now;
                    accPaidHoliday.updated_date = now;
                    listInfo.Add(accPaidHoliday);
                }
            }

            return true;
        }

#pragma warning disable CS3001 // Argument type is not CLS-compliant
        /// <summary>
        /// Convert data to regist request
        /// </summary>
        /// <param name="requestdata"></param>
        /// <param name="data"></param>
        public static void ConvertData(ref RequestManageInputInfo requestdata, ParameterRegistRequestManage data)
#pragma warning restore CS3001 // Argument type is not CLS-compliant
        {
            if (data.requestManageList.Count > 0)
            {
                List<RequestManageInputDetail> LstDetail = new List<RequestManageInputDetail>();
                for (int i = 0; i < data.requestManageList.Count; i++)
                {
                    var request = data.requestManageList[i];
                    var _requestdetail = new RequestManageInputDetail();

                    _requestdetail.account_id = request.account_id;
                    _requestdetail.application_datetime = request.application_datetime;
                    _requestdetail.created_account_id = request.created_account_id;
                    _requestdetail.id = request.id;
                    _requestdetail.paid_holiday_type = request.paid_holiday_type;
                    _requestdetail.reason_txt = request.reason_txt;
                    _requestdetail.updated_account_id = request.updated_account_id;
                    _requestdetail.deleted_date = request.deleted_date;
                    _requestdetail.oldId = request.oldId;

                    LstDetail.Add(_requestdetail);
                }
                requestdata.requestManageList.AddRange(LstDetail);
            }
            else { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="remainPaidHoliday"></param>
        /// <returns></returns>
        public static bool CheckAuthRequest(ParameterRegistRequestManage data, double remainPaidHoliday)
        {
            var newRequest = data.requestManageList.Where(r => r.id == 0).ToList();
            var deleteRequest = data.requestManageList.Where(r => r.deleted_date != null).ToList();
            double result = 0;

            double newRegistDay = newRequest.Where(r => r.paid_holiday_type == CONST_VALUE.ALL).ToList().Count * CONST_VALUE.REGISTOR_ALL + newRequest.Where(r => r.paid_holiday_type != CONST_VALUE.ALL).ToList().Count * CONST_VALUE.REGISTOR_HALF;
            double deleteDay = deleteRequest.Where(r => r.paid_holiday_type == CONST_VALUE.ALL).ToList().Count * CONST_VALUE.REGISTOR_ALL + deleteRequest.Where(r => r.paid_holiday_type != CONST_VALUE.ALL).ToList().Count * CONST_VALUE.REGISTOR_HALF;

            result = remainPaidHoliday + deleteDay - newRegistDay;

            return result >= 0;
        }
    }
}
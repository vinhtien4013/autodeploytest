﻿using AM.BusinessLayer;
using AM.BusinessLayer.FingerPrintService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/FingerPrint")]
    public class FingerPrintController : ApiController
    {
        // GET: api/FingerPrint/GetFingerPrint?company_id=
        //with param company_id
        [HttpGet]
        [Route("GetFingerPrint")]
        public HttpResponseMessage LoadListFingerPrint(string company_id)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //write log
                var data = new { company_id = company_id };
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetFingerPrint",JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        FingerPrintService service = ServiceLocator.Resolve<FingerPrintService>();
                        var list = service.GetFingerPrint(company_id);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list finger print
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetFingerPrint", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET: api/FingerPrint/GetFingerPrint
        [HttpGet]
        [Route("GetFingerPrint")]
        public HttpResponseMessage LoadListFingerPrint()
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetFingerPrint",null);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        FingerPrintService service = ServiceLocator.Resolve<FingerPrintService>();
                        var list = service.GetFingerPrint(CONST_VALUE.SESSION_COMPANY_DEFAULT);
                        if (list == null || list.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list finger print
                            response.Content = new JsonContent(list);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetFingerPrint", JsonConvert.SerializeObject(list));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/FingerPrint/RegisterFingerAccount
        [HttpPost]
        [Route("RegisterFingerAccount")]
        public HttpResponseMessage RegisterFingerPrint([FromBody] FingerPrintInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }
                //write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "RegisterFingerAccount", JsonConvert.SerializeObject(data));
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        FingerPrintService service = ServiceLocator.Resolve<FingerPrintService>();
                        DateTime nowTime = AzureUtil.VietnamDateTimeNow();

                        if(data == null)
                        {
                            response.StatusCode = HttpStatusCode.BadRequest;
                            return response;
                        }

                        FingerPrint model = new FingerPrint()
                        {
                            id = data.FingerPrintId,
                            finger_data = data.finger_data,
                            salt = data.salt,
                            created_date = nowTime,
                            created_account_id = CONST_VALUE.SERVER_ACCOUNT,
                            updated_date = nowTime,
                            updated_account_id = CONST_VALUE.SERVER_ACCOUNT,
                            deleted_date = null,
                            deleted_account_id = null
                        };

                        var resultInfo = service.InsertFingerPrint(model,data.account_id);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "RegisterFingerAccount", resultInfo.Message);
                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }

                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}

﻿using AM.BusinessLayer;
using AM.BusinessLayer.AccountService;
using AM.BusinessLayer.FaceAzureService;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model;
using AM.Model.api;
using AM.Model.ConstData;
using AM.Model.Tables;
using AM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static AM.Model.Const.ConstType;

namespace AM.Controllers
{
    [RoutePrefix("api/Company")]
    public class CompanyController : ApiController
    {
        // GET: api/Company/GetCompany 
        [HttpGet]
        [Route("GetCompany")]
        public HttpResponseMessage LoadListCompany()
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetCompany", null);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        CompanyService service = ServiceLocator.Resolve<CompanyService>();
                        var listCompany = service.GetCompany();
                        if (listCompany == null || listCompany.Count == 0)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return a list company
                            response.Content = new JsonContent(listCompany);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetCompany",JsonConvert.SerializeObject(listCompany));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Company/GetCompanyByToken 
        [HttpPost]
        [Route("GetCompanyByToken")]
        public HttpResponseMessage LoadCompanyByToken([FromBody]TokenInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetCompanyByToken",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        CompanyService service = ServiceLocator.Resolve<CompanyService>();
                        var company = service.GetCompanyByCondition(data.CompanyToken,null,null);
                        if (company == null)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return company information
                            response.Content = new JsonContent(company);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetCompanyByToken", JsonConvert.SerializeObject(company));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Company/UpdateCompany
        [HttpPost]
        [Route("UpdateCompany")]
        public async Task<HttpResponseMessage> UpdateCompany([FromBody] CompanyInput data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "UpdateCompany",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        CompanyService service = ServiceLocator.Resolve<CompanyService>();
                        List<Company> companyLst = new List<Company>();

                        ConvertData(ref companyLst, data);
                        var resultInfo = service.UpdateCompany(companyLst);
                        if (resultInfo.Status != ResultStatus.RegistSuccess)
                        {
                            // Write Log Message
                            //LoggingUtil.WriteLog(resultInfo.Message);
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateCompany", resultInfo.Message);
                            // Log Error
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultInfo.Message);
                        }
                        //call compare group
                        var resultCompare = await CompareCompanyGroup();
                        if (!string.IsNullOrEmpty(resultCompare.message))
                        {
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateCompany", resultCompare.message);
                            return Request.CreateErrorResponse(HttpStatusCode.OK, resultCompare.message);
                        }                       
                        //CALL CREATE PERSON GROUP
                        var listError = new List<string>();
                        var listRollBack = new List<Company>();
                        foreach (var item in resultCompare.inValidLstCompany)
                        {
                            // persongroupID = Env + id 
                            var formatIDByEnv = AttendanceSettings.FaceEnv + item.id;
                            var createPersonGroup = await ServiceLocator.Resolve<FaceAPIAzureService>().CreatePersonGroup(formatIDByEnv, item.company_name);
                            if (createPersonGroup != null)
                            {
                                resultInfo.Message = string.IsNullOrEmpty(createPersonGroup.error.message) ? resultInfo.Message : createPersonGroup.error.message;
                                listError.Add(resultInfo.Message);
                                //return face_flg to false
                                item.face_flg = false;
                                listRollBack.Add(item);
                            }
                        }
                        if (listError.Count > 0)
                        {
                            var rollbackFaceFlg = service.UpdateCompany(listRollBack);
                            var err = string.Empty;
                            foreach (var item in listError)
                            {
                                err += item + "\n";
                            }
                            LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateCompany", err);
                            return Request.CreateErrorResponse(HttpStatusCode.OK,err.Remove(err.Length -1));
                        }

                        //Write log
                        //LoggingUtil.WriteLog(resultInfo.Message);
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "UpdateCompany", resultInfo.Message);

                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // POST: api/Company/GetCompanyByCondition
        [HttpPost]
        [Route("GetCompanyByCondition")]
        public HttpResponseMessage GetCompanyByCondition([FromBody]ConditionInputCompany data)
        {
            try
            {
                //Compare token
                if (!ServiceLocator.Resolve<CommonService>().CompareToken(Request.Headers))
                {
                    var errors = Resources.MessageInfo.E0010;
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, errors);
                }

                // write log
                var jsonStr = JsonConvert.SerializeObject(data);
                //LoggingUtil.WriteAMLog(HttpContext.Current.Request.UserHostAddress, jsonStr);
                LoggingUtil.LogMessage(CONST_VALUE.RECEIVED_REQUEST, "GetCompanyByCondition",jsonStr);
                // check validate
                if (ModelState.IsValid)
                {
                    try
                    {
                        HttpResponseMessage response = new HttpResponseMessage();
                        CompanyService service = ServiceLocator.Resolve<CompanyService>();
                        var company = service.GetCompanyByCondition(null,data.id,null);
                        if (company == null)
                        {
                            response.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.OK;
                            // return company information
                            response.Content = new JsonContent(company);
                        }
                        LoggingUtil.LogMessage(CONST_VALUE.RESPONSE_REQUEST, "GetCompanyByCondition",JsonConvert.SerializeObject(company));
                        return response;
                    }
                    catch (HttpException ex)
                    {
                        LoggingUtil.WriteExceptionLog(ex.Message);
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
                else
                {
                    string errors = JsonConvert.SerializeObject(ModelState.Values.SelectMany(state => state.Errors).Select(error => error.ErrorMessage));
                    LoggingUtil.WriteLog(errors);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (HttpException ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

#pragma warning disable CS3001
        /// <summary>
        /// Convert data from input to list company model for update
        /// </summary>
        /// <param name="companyLst">list Company model return</param>
        /// <param name="data">data input</param>
        /// <returns></returns>
        private static bool ConvertData(ref List<Company> companyLst, CompanyInput data)
        {
            companyLst = new List<Company>();
            if(data.CompanyLst.Count > 0)
            {
                int lstCount = data.CompanyLst.Count;
                //get time zone by create or updated account
                var memberInfo = ServiceLocator.Resolve<AccountService>().GetDataById(data.CompanyLst[0].UpdatedAccount);
                DateTime nowDate = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);
                var paramCompany = new CompanyDetail();
                for (int index = 0; index < lstCount; index++)
                {
                    paramCompany = data.CompanyLst[index];

                    var company = new Company()
                    {
                        id = paramCompany.Id,
                        company_token = paramCompany.CompanyToken,
                        company_name = paramCompany.CompanyName,
                        admin_company_flg = paramCompany.AdminFlg,
                        valid_date = paramCompany.ValidDate,
                        description = string.IsNullOrEmpty(paramCompany.Description) ? null : paramCompany.Description,
                        created_account_id = paramCompany.UpdatedAccount,
                        created_date = nowDate,
                        updated_account_id = paramCompany.UpdatedAccount,
                        updated_date = nowDate,
                        deleted_account_id = paramCompany.UpdatedAccount,
                        deleted_date = paramCompany.DeletedDate,
                        slack_flg = paramCompany.SlackFlg,
                        start_working_time = paramCompany.StartWorkingTime,
                        end_working_time = paramCompany.EndWorkingTime,
                        start_lunch_time = paramCompany.StartLunchTime,
                        end_lunch_time = paramCompany.EndLunchTime,
                        uncounted_time = paramCompany.UncountedTime,
                        slack_token = string.IsNullOrEmpty(paramCompany.SlackToken) ? null : paramCompany.SlackToken,
                        slack_team_id = string.IsNullOrEmpty(paramCompany.SlackTeamId) ? null : paramCompany.SlackTeamId,
                        slack_team_domain = string.IsNullOrEmpty(paramCompany.SlackTeamDomain) ? null : paramCompany.SlackTeamDomain,
                        attendance_channel = string.IsNullOrEmpty(paramCompany.AttendanceChannel) ? null : paramCompany.AttendanceChannel,
                        announcement_channel = string.IsNullOrEmpty(paramCompany.AttendanceChannel) ? null : paramCompany.AnnouncementChannel,
                        attendance_command = string.IsNullOrEmpty(paramCompany.AttendanceChannel) ? null : paramCompany.AttendanceCommand,
                        company_logo = string.IsNullOrEmpty(paramCompany.CompanyLogo) ? null : paramCompany.CompanyLogo,
                        time_zone_id = paramCompany.TimeZoneId,
                        paid_holiday_monthly_flg = paramCompany.PaidHolidayMonthlyFlg,
                        slack_webhook_url = string.IsNullOrEmpty(paramCompany.SlackWebHookUrl) ? null : paramCompany.SlackWebHookUrl,
                        post_message_username = string.IsNullOrEmpty(paramCompany.PostMessageUsername) ? null : paramCompany.PostMessageUsername,
                        login_by = paramCompany.LoginBy,
                        face_flg = paramCompany.FaceFlg,
                        face_folder = string.IsNullOrEmpty(paramCompany.FaceFolder) ? null : paramCompany.FaceFolder,
                        min_workingtime = paramCompany.MinWorkingTime
                    };
                    companyLst.Add(company);
                }
            }

            return true;
        }

        /// <summary>
        /// Compare company id with group id from face API, return company that have not created personGroupId
        /// </summary>
        /// <returns></returns>
#pragma warning disable CS3002 // Return type is not CLS-compliant
        public async Task<ProcessCompareGroupCompany> CompareCompanyGroup()
#pragma warning restore CS3002 // Return type is not CLS-compliant
        {
            var service = ServiceLocator.Resolve<CompanyService>();
            //Get ALL COMPANY
            var allCompanyLst = service.GetCompany().Where(s =>s.face_flg).ToList();
            var result = new ProcessCompareGroupCompany()
            {
                StatusCode = HttpStatusCode.OK,
                message = string.Empty,
                inValidLstCompany = allCompanyLst
            };
           
            if (allCompanyLst.Count < 1)
            {
                result.StatusCode = HttpStatusCode.OK;
                return result;
            }
            //get all lst valid person group id from API FACE
            var validGroupLst = await ServiceLocator.Resolve<FaceAPIAzureService>().GetGroupList();
            if (validGroupLst.error != null)
            {
                if (!string.IsNullOrEmpty(validGroupLst.error.error.message))
                {
                    result.StatusCode = validGroupLst.error.error.statusCode;
                    result.message = validGroupLst.error.error.message;
                    return result;
                }
            }
            var listPersonGroupFormatByEnv = validGroupLst.list.Where(s=>s.personGroupId.StartsWith(AttendanceSettings.FaceEnv)).ToList();
            if (listPersonGroupFormatByEnv.Count > 0)
            {
                //return invalid group lst company
                result.inValidLstCompany = allCompanyLst.Where(x => !listPersonGroupFormatByEnv.Any(s => s.personGroupId.Equals(AttendanceSettings.FaceEnv + x.id.ToString()))).ToList();
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}
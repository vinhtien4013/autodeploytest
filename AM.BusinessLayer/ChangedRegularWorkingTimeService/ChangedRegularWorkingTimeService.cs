﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AM.BusinessLayer
{
    public class ChangedRegularWorkingTimeService : BaseService
    {
        /// <summary>
        /// Get list information in table changed_regular_working_time
        /// </summary>
        /// <param name="companyId">company_id</param>
        /// <returns>List model ChangedRegularWorkingTime</returns>
        [LogCallHandler(BeforeMessage = "Start get list changed regular working time information processing",
             AfterMessage = "Finish get list changed regular working time information processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<ChangedRegularWorkingTime> GetChangedRegularWorkingTime(int companyId)
        {
            return ServiceLocator.Resolve<IChangedRegularWorkingTime>().GetChangedRegularWorkingTime(companyId);
        }

        /// <summary>
        /// Register/Update/Delete changed regular working time 
        /// </summary>
        /// <param name="modelList">List model of changed_regular_working_time</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register/update/delete changed regular working time processing",
             AfterMessage = "Finish register/update/delete changed regular working time processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateChangedWorkingTime(List<ChangedRegularWorkingTime> modelList)
        {
            ResultInfo resultInfo = new ResultInfo()
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0006
            };

            var changedRegularWorkingTimeDal = ServiceLocator.Resolve<IChangedRegularWorkingTime>();

            for(int index = 0; index < modelList.Count; index ++)
            {
                var model = modelList[index];

                if (model.id == 0)
                {
                    if (!changedRegularWorkingTimeDal.Insert(model))
                    {
                        resultInfo.Message = string.Format(InfoMessage.I0050, model.account_id);
                        return resultInfo;
                    }
                }
                else if (model.deleted_date == null )
                {
                    if (!changedRegularWorkingTimeDal.Update(model))
                    {
                        resultInfo.Message = string.Format(InfoMessage.I0051, model.account_id);
                        return resultInfo;
                    }
                }
                else if (!changedRegularWorkingTimeDal.Delete(model))
                {
                    resultInfo.Message = string.Format(InfoMessage.I0052, model.account_id);
                    return resultInfo;
                }
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0053;

            return resultInfo;
        }

        /// <summary>
        /// Get regular working time by account id
        /// </summary>
        /// <param name="account_id"></param>
        /// <param name="date"></param>
        /// <returns> 
        /// regular working time of account
        /// default : 08:00 - 17:00
        /// </returns>
        [LogCallHandler(BeforeMessage = "Start get regular working time by account id processing",
            AfterMessage = "Finish get regular working time by account id processing",
            Severity = TraceEventType.Information,
            Categories = new string[] { "General" },
            IncludeParameters = false)]
        public ChangedRegularWorkingTime GetRegularWorkTimeByAccount(int account_id, DateTime date)
        {
            return ServiceLocator.Resolve<IChangedRegularWorkingTime>().GetRegularWorkTimeByAccount(account_id, date);
        }

        /// <summary>
        /// Get expect start time/ expect end time
        /// </summary>
        /// <param name="account_id">account id</param>
        /// <param name="date">date</param>
        /// <returns></returns>
        public ChangedRegularWorkingTime GetExpectTime(int account_id, DateTime date, Company company)
        {
            return ServiceLocator.Resolve<IChangedRegularWorkingTime>().GetExpectTime(account_id, date, company);
        }
    }
}

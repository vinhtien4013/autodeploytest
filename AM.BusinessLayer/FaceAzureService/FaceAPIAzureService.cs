﻿using AM.Model;
using AM.Model.api;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer.FaceAzureService
{
    public class FaceAPIAzureService : BaseService
    {
        /// <summary>
        /// Create Person ID via Face API Azure
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="staff_name"></param>
        /// <returns></returns>
        [LogCallHandler(
           BeforeMessage = "Start of create new person processing",
           AfterMessage = "End of create new person processing",
           Severity = TraceEventType.Information,
           Categories = new string[] { "General" },
           IncludeParameters = false)]
        public async Task<CreatePersonResponse> CreateNewPerson(string groupId, string staff_name)
        {
            CreatePersonResponse value = new CreatePersonResponse();

            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            //Add content
            var request_body = new FaceAPICreatePersonBody
            {
                name = staff_name
            };
            var json = JsonConvert.SerializeObject(request_body);
            var content = new StringContent(json, Encoding.UTF8, CONST_VALUE.CONTENT_TYPE);
            // Call api create person ID
            Uri apim = new Uri(AttendanceSettings.apimEndPoint + "/persongroups/" + groupId + "/persons");
            var res = await client.PostAsync(apim, content);
            // if success return personId            
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            value = JsonConvert.DeserializeObject<CreatePersonResponse>(jsonRes);
            // Return
            return value;
        }

        /// <summary>
        /// Add face to Person ID via Face API Azure
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="personId"></param>
        /// <param name="face_image"></param>
        /// <returns></returns>
        [LogCallHandler(
           BeforeMessage = "Start of add face processing",
           AfterMessage = "End of add face processing",
           Severity = TraceEventType.Information,
           Categories = new string[] { "General" },
           IncludeParameters = false)]
        public async Task<AddFaceResponse> AddFace(string groupId, string personId, string face_image)
        {
            AddFaceResponse value = new AddFaceResponse();

            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            //Add content
            var request_body = new FaceAPIAddFaceBody
            {
                url = face_image
            };
            var json = JsonConvert.SerializeObject(request_body);
            var content = new StringContent(json, Encoding.UTF8, CONST_VALUE.CONTENT_TYPE);
            // Call api create person ID
            var apiUrl = AttendanceSettings.apimEndPoint + "/persongroups/" + groupId + "/persons/" + personId + "/persistedFaces";
            Uri apim = new Uri(apiUrl);
            var res = await client.PostAsync(apim, content);
            // if success return personId           
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            value = JsonConvert.DeserializeObject<AddFaceResponse>(jsonRes);

            // Return
            return value;
        }

        /// <summary>
        /// Train face 
        /// </summary>
        /// <param name="groupId"></param>    
        /// <returns></returns>
        [LogCallHandler(
           BeforeMessage = "Start of trainning face processing",
           AfterMessage = "End of trainning face processing",
           Severity = TraceEventType.Information,
           Categories = new string[] { "General" },
           IncludeParameters = false)]
        public async Task<TrainFaceResponse> TrainingFace(string groupId)
        {
            TrainFaceResponse value = new TrainFaceResponse();
            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            // Call api create person ID
            var apiUrl = AttendanceSettings.apimEndPoint + "/persongroups/" + groupId + "/train";
            Uri apim = new Uri(apiUrl);
            var res = await client.PostAsync(apim, null);
            // if success return personId    
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            value = JsonConvert.DeserializeObject<TrainFaceResponse>(jsonRes);
            // Return
            return value;
        }

        /// <summary>
        /// Create person group 
        /// </summary>
        /// <param name="groupId"></param>    
        /// <param name="company_name"></param>  
        /// <returns></returns>
        [LogCallHandler(
           BeforeMessage = "Start of trainning face processing",
           AfterMessage = "End of trainning face processing",
           Severity = TraceEventType.Information,
           Categories = new string[] { "General" },
           IncludeParameters = false)]
        public async Task<CommonResponseNoneBody> CreatePersonGroup(string groupId,string company_name)
        {
            CommonResponseNoneBody value = new CommonResponseNoneBody();
            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);      
            //Add content
            var request_body = new FaceAPICreatePersonGroupBody
            {
                name = company_name
            };
            var json = JsonConvert.SerializeObject(request_body);
            var content = new StringContent(json, Encoding.UTF8, CONST_VALUE.CONTENT_TYPE);
            // Call api create person ID
            var apiUrl = AttendanceSettings.apimEndPoint + "/persongroups/"+ groupId;
            Uri apim = new Uri(apiUrl);
            var res = await client.PutAsync(apim, content);
            // if success return null bobdy    
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            value = JsonConvert.DeserializeObject<CommonResponseNoneBody>(jsonRes);
            if (res.StatusCode == System.Net.HttpStatusCode.Conflict || res.StatusCode == System.Net.HttpStatusCode.OK)
            {
                value = null;
                return value;
            }
            if (value != null)
            {
                value.error.message = string.IsNullOrEmpty(value.error.message) ? value.error.message : company_name + ":" + value.error.message;
            }
            // Return
            return value;
        }

        /// <summary>
        /// Get all person group
        /// </summary>          
        /// <returns></returns>
        [LogCallHandler(
           BeforeMessage = "Start of get all peprson group processing",
           AfterMessage = "End of get all peprson group processing",
           Severity = TraceEventType.Information,
           Categories = new string[] { "General" },
           IncludeParameters = false)]
        public async Task<GetListResponse> GetGroupList()
        {
            GetListResponse value = new GetListResponse();
            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            // Call api create person ID
            var apiUrl = AttendanceSettings.apimEndPoint + "/persongroups";
            var res = await client.GetAsync(apiUrl);
            // if success return list info    
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            if (res.IsSuccessStatusCode)
            {
                var result = JsonConvert.DeserializeObject<List<GroupListInfo>>(jsonRes);
                value.list = result;
            }
            else
            {
                var result = JsonConvert.DeserializeObject<CommonResponseNoneBody>(jsonRes);
                value.error = result;
                value.error.error.statusCode = res.StatusCode;
            }
            // Return
            return value;
        }

        /// <summary>
        /// get first sentence of error from Face API
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public string GetFirstMessageFromFaceAPI(string errMessage)
        {
            string[] firstMessage = errMessage.Split(new Char[] { '.' });
            return firstMessage[0];

        }


        /// <summary>
        /// Delete face in person group
        /// </summary>
        /// <param name="groupId"></param>    
        /// <returns></returns>
        [LogCallHandler(
           BeforeMessage = "Start of deleting face processing",
           AfterMessage = "End of deleting face processing",
           Severity = TraceEventType.Information,
           Categories = new string[] { "General" },
           IncludeParameters = false)]
        public async Task<CommonResponseNoneBody> PersonGroupDeleteFace(string groupId, string person_id , string persistedFaceId)
        {
            CommonResponseNoneBody value = new CommonResponseNoneBody();
            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            // Call api create person ID
            var apiUrl = AttendanceSettings.apimEndPoint + "/persongroups/" + groupId + "/persons/" + person_id + "/persistedFaces/" + persistedFaceId;
            Uri apim = new Uri(apiUrl);
            var res = await client.DeleteAsync(apim);
            // if success return non-content     
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            value = JsonConvert.DeserializeObject<CommonResponseNoneBody>(jsonRes);
            if (value != null)
            {
                // catch 404 err
                value.error.statusCode = res.StatusCode;
            }
            // Return
            return value;
        }
    }
}

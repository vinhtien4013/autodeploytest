﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.DataAccessLayer;
using AM.Model;
using AM.Model.api;
using AM.Model.api.Brw;
using AM.Model.ConstData;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer
{
    public class CommonService : BaseService
    {
        private static AttendanceSettings settings;
        private static TokenDal tokenDal;

        #region "Tokens"
        /// <summary>
        /// Get new token
        /// </summary>
        /// <param name="unixDateTime"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get new token processing",
                      AfterMessage = "Finish get new token processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public AttendanceToken GetToken(long unixDateTime)
        {
            var result = new AttendanceToken();
            try
            {
                var brwToken = ServiceLocator.Resolve<TokenDal>().GetToken(unixDateTime);
                if (brwToken == null)
                {
                    LoggingUtil.WriteLog(ErrorMessage.E0009);
                    return result;
                }
                return brwToken;
            }
            catch (Exception ex)
            {
                LoggingUtil.WriteLog(ex.Message);
                return result;
            }
        }

        /// <summary>
        /// Function compare token
        /// </summary>
        /// <param name="headers">Store parameters</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get compare token processing",
                      AfterMessage = "Finish get compare token processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public bool CompareToken(HttpRequestHeaders headers)
        {

            if (!headers.Contains(CONST_VALUE.ACCESS_TOKEN))
            {
                var errors = ErrorMessage.E0009;
                LoggingUtil.WriteLog(errors);
                return false;
            }

            if (!headers.Contains(CONST_VALUE.ACCESS_TIME))
            {
                var errors = "Requested api : The AccessTime not exists";
                LoggingUtil.WriteLog(errors);
                return false;
            }

            var accessToken = headers.GetValues(CONST_VALUE.ACCESS_TOKEN).First();
            var accessTime = long.Parse(headers.GetValues(CONST_VALUE.ACCESS_TIME).First());

            var token = this.GetToken(accessTime).AccessToken;
            //Compare two token
            if (!accessToken.Equals(token))
            {
                LoggingUtil.WriteLog(ErrorMessage.E0010);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Function check token company
        /// </summary>
        /// <param name="headers">Store parameters</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get company token processing",
                      AfterMessage = "Finish get company token processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public bool CheckCompanyToken(HttpRequestHeaders headers)
        {
           
            if (!headers.Contains(CONST_VALUE.COMPANY_TOKEN))
            {
                var errors = ErrorMessage.E0037;
                LoggingUtil.WriteLog(errors);
                return false;
            }
            //else return true           
            return true;
        }
        #endregion

        #region Accounts

        /// <summary>
        /// Get parent account by group id
        /// </summary>
        /// <param name="group_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get parent account by group id processing",
                      AfterMessage = "Finish get parent account by group id processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public List<group_data> GetAllAccountBySuperGroupId(string group_id, string companyId)
        {
            BRWGroup groups = new BRWGroup();
            //Get group briswell
            groups = this.GetBriswellVietNamGroup(companyId);
            //Get all super group by group_id of account login
            var listGroup = this.GetAccountbySuperGroupId(groups, group_id).ToList();

            return listGroup;
        }

        /// <summary>
        /// Get all super group by group_id
        /// </summary>
        /// <param name="groups">object of groups</param>
        /// <param name="member">object account of login</param>
        /// <returns>object of group_data</returns>
        [LogCallHandler(BeforeMessage = "Start Get all super group by group_id processing",
                      AfterMessage = "Finish Get all super group by group_id processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public IEnumerable<group_data> GetAccountbySuperGroupId(BRWGroup groups, string group_id)
        {
            var results = Enumerable.Range(0, groups.data.Count).Select(i => new List<group_data>()).ToList();
            results[0].Add
            (
                groups.data.Single(x => x.group_id == group_id)
            );
            for (int level = 1; level <= groups.data.Count; level++)
            {
                var range = results[level - 1].SelectMany
                    (
                        x => groups.data.Where(y => y.group_id == x.super_group_id)
                    );

                if (range.ToList().Count == 0) break;
                results[level].AddRange
                (
                    range
                );
            }
            return results.SelectMany(x => x);
        }

        /// <summary>
        /// Group information acquisition process
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start group information acquisition process",
                      AfterMessage = "Finish group information acquisition process",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public BRWGroup GetBriswellVietNamGroup(string companyId)
        {
            try
            { 
                return ServiceLocator.Resolve<DivisionService.DivisionService>().GetGroupData(companyId);
                //settings = new AttendanceSettings();
                // Member information acquisition process
                //return this.GetBriswellVietNamInfo<BRWGroup>(settings.BriswellVietNamGroup);
            }
            catch (Exception ex)
            {
                LoggingUtil.WriteLog(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Member information acquisition process
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url">Url</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start member information acquisition process",
                      AfterMessage = "Finish member information acquisition process",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        private T GetBriswellVietNamInfo<T>(string url)
        {
            T result = default(T);
            try
            {
                settings = new AttendanceSettings();
                tokenDal = new TokenDal(settings);
                var brwToken = tokenDal.GetBriswelVietNamToken();
                Uri apiRequestUri = new Uri(string.Format(string.Concat(url, "&token={0}&time={1}"), brwToken.AccessToken, brwToken.UnixTime));
                //request briswell vietnam account
                using (var webClient = new TimedWebClient())
                {
                    // Optionally specify an encoding for uploading and downloading strings.
                    webClient.Encoding = System.Text.Encoding.UTF8;

                    // Downloads the requested resource as a System.String. The resource to download is specified as a System.Uri.
                    var json = webClient.DownloadString(apiRequestUri);

                    // Deserializes the JSON to a .NET object.
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }
            catch (Exception ex)
            {
                LoggingUtil.WriteLog(ex.Message);
                return result;
            }
        }

        /// <summary>
        /// Account information acquisition process
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start account information acquisition process",
                      AfterMessage = "Finish account information acquisition process",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public BRWAccount GetBriswellVietNamAccount(string companyId)
        {
            try
            {
                //settings = new AttendanceSettings();
                // Member information acquisition process
                //return this.GetBriswellVietNamInfo<BRWAccount>(settings.BriswellVietNamAcccount);
                var listAccount = ServiceLocator.Resolve<AccountService.AccountService>().GetAccount(companyId);
                BRWAccount lstAccount = new BRWAccount();
                lstAccount.status = 200;

                if (listAccount != null)
                {
                    foreach (var account in listAccount)
                    {
                        staff_data staffData = new staff_data()
                        {
                            staff_id = account.staff_id,
                            staff_name = account.staff_name,
                            group_id = StringUtil.ConvertObjectToInteger32(account.division_id)
                        };

                        lstAccount.data.Add(staffData);
                    }
                }

                return lstAccount;
            }
            catch (Exception ex)
            {
                LoggingUtil.WriteLog(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Clear cache memory helper
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start clear cache memory helper process",
                      AfterMessage = "Finish clear cache memory helper process",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public ResultInfo ClearCacheMemoryHelper()
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DataNotExists,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0017
            };

            try
            {
                List<string> cacheKeys = MemoryCache.Default.Select(kvp => kvp.Key).ToList();
                foreach (string cacheKey in cacheKeys)
                {
                    MemoryCache.Default.Remove(cacheKey);
                }

                resultInfo.Status = ResultStatus.DeleteSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0016;
            }
            catch (Exception ex)
            {
                resultInfo.Status = ResultStatus.DataNotExists;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack;
                resultInfo.Message = ex.Message;
            }

            return resultInfo;
           
        }

        #endregion
    }
}

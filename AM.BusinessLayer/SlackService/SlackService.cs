﻿using AM.BusinessLayer.AccountService;
using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Model;
using AM.Model.Const;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer.SlackService
{
    public class SlackService : BaseService
    {
        /// <summary>
        /// Register attendance via Slack
        /// </summary>
        /// <param name="model"></param>
        /// <param name="messResponse"></param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register attendance via Slack processing",
                      AfterMessage = "Finish register attendance via Slack processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        [Transaction()]
        public SlackResponse RegisterAttendanceViaSlack(SlackModel model, Company company)
        {
            SlackResponse messResponse = new SlackResponse();
            ResultInfo result = new ResultInfo();
            PaidHolidayService paidHolidayService = ServiceLocator.Resolve<PaidHolidayService>();
            HolidayService holidayService = ServiceLocator.Resolve<HolidayService>();
            //var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, company.id.ToString(),null);
            var currentDate = AzureUtil.LocalDateTimeNow(company.time_zone_id);

            try
            {
                var possessiveNoun = (model.UserSex == SEX.MALE) ? InfoMessage.I0070 : InfoMessage.I0071;
                if (model.CommandType == (int)COMMANDTYP.OFFALLDAY || model.CommandType == (int)COMMANDTYP.OFFHALFMORNING || model.CommandType == (int)COMMANDTYP.OFFHALFAFTER)
                {
                    var accountPaidHoliday = paidHolidayService.GetRequestByAccount(model.AccountId, model.RegisterDate.Value);

                    if (model.RegisterDate.Value.DayOfWeek == DayOfWeek.Saturday || model.RegisterDate.Value.DayOfWeek == DayOfWeek.Sunday || holidayService.IsHoliday(model.RegisterDate.Value, company.id))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0022;
                        return messResponse;
                    }
                    //else if(paidHolidayService.HasApplication(model.AccountId,model.RegisterDate.Value) > 0)
                    else if(accountPaidHoliday != null && accountPaidHoliday.id != 0 && (model.CommandType == CONST_VALUE.ALL || accountPaidHoliday.paid_holiday_type == CONST_VALUE.ALL || accountPaidHoliday.paid_holiday_type == model.CommandType))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0021;
                        return messResponse;
                    }
                    else
                    {
                        // Changespec 20200714- Task #70251 
                            WorkingRecordService workTimeService = ServiceLocator.Resolve<WorkingRecordService>();
                        //    var workTimeInfo = workTimeService.GetWorkTimeByAccount(model.RegisterDate.Value, model.AccountId);
                        //    if (workTimeInfo != null && workTimeInfo.actual_start_time != null && workTimeInfo.actual_start_time != new TimeSpan(0, 0, 0))
                        //    {
                        //        if (model.CommandType == (int)COMMANDTYP.OFFALLDAY)
                        //        {
                        //            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        //            messResponse.text = ErrorMessage.E0023;

                        //            return messResponse;
                        //        } else if (model.CommandType == (int)COMMANDTYP.OFFHALFMORNING && TimeSpan.Compare(workTimeInfo.actual_start_time, new TimeSpan(12, 0, 0)) < 0)
                        //        {
                        //            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        //            messResponse.text = ErrorMessage.E0023;

                        //            return messResponse;
                        //        }
                        //        else if (model.CommandType == (int)COMMANDTYP.OFFHALFMORNING && TimeSpan.Compare(workTimeInfo.actual_start_time, new TimeSpan(12, 0, 0)) > 0)
                        //        {
                        //            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        //            messResponse.text = ErrorMessage.E0023;

                        //            return messResponse;
                        //        }
                        //    }

                        var workTimeInfo = workTimeService.GetWorkTimeByAccount(model.RegisterDate.Value, model.AccountId);
                        if (workTimeInfo != null && workTimeService.IsApprovedWorkTime(workTimeInfo.id))
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = ErrorMessage.E0042;

                            return messResponse;
                        }

                        double registorDays = (model.CommandType == (int)COMMANDTYP.OFFALLDAY) ? 1 : 0.5 ;
                        double remainPaidHoliday = paidHolidayService.GetRemainPaidHolidayForAcc(model.AccountId);
                        // Check remain paid holiday numbers
                        if(registorDays > remainPaidHoliday)
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.INCHANNEL;
                            var type = "";

                            //Check type off
                            switch (model.CommandType)
                            {
                                case (int)COMMANDTYP.OFFALLDAY:
                                    type = InfoMessage.I0074;
                                    break;
                                case (int)COMMANDTYP.OFFHALFAFTER:
                                    type = InfoMessage.I0073;
                                    break;
                                case (int)COMMANDTYP.OFFHALFMORNING:
                                    type = InfoMessage.I0072;
                                    break;

                            }
                                messResponse.text = string.Format(InfoMessage.I0031, model.UserName, type, model.RegisterDate.Value.ToShortDateString(), model.reason, possessiveNoun);
                            return messResponse;
                        }
                        RequestManageInputInfo request = new RequestManageInputInfo
                        {
                            requestManageList = new List<RequestManageInputDetail>{(new RequestManageInputDetail
                            {
                                id = 0,
                                account_id = model.AccountId ,
                                application_datetime = model.RegisterDate.Value,
                                paid_holiday_type = model.CommandType,
                                created_account_id = model.AccountId,
                                updated_account_id = model.AccountId,
                                reason_txt = model.reason,
                                oldId = (accountPaidHoliday != null && accountPaidHoliday.id != 0) ? accountPaidHoliday.id.ToString() : null
                            }) }
                        };

                        result = paidHolidayService.RegisterRequestManage(request, company);
                        if (result.Status != ResultStatus.RegistSuccess)
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = InfoMessage.I0069;

                            return messResponse;
                        }
                        else
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.INCHANNEL;
                            var type = "";

                            //Check type off
                            switch (model.CommandType)
                            {
                                case (int)COMMANDTYP.OFFALLDAY:
                                    type = InfoMessage.I0074;
                                    break;
                                case (int)COMMANDTYP.OFFHALFAFTER:
                                    type = InfoMessage.I0073;
                                    break;
                                case (int)COMMANDTYP.OFFHALFMORNING:
                                    type = InfoMessage.I0072;
                                    break;

                            }
                            if(accountPaidHoliday != null && accountPaidHoliday.id != 0)
                                type = InfoMessage.I0074;
                            messResponse.text = string.Format(InfoMessage.I0032, model.UserName, type, model.RegisterDate.Value.ToShortDateString(), model.reason);

                            return messResponse;
                        }
                    }
                }
                else if(model.CommandType == (int)COMMANDTYP.LATE)
                {
                    WorkingRecordService workTimeService = ServiceLocator.Resolve<WorkingRecordService>();
                    ChangedRegularWorkingTimeService regularService = ServiceLocator.Resolve<ChangedRegularWorkingTimeService>();                    

                    TimeSpan defaultLateTimeMorning;
                    TimeSpan defaultLateTimeAfternoon;
                    TimeSpan.TryParse(ConstType.CONST_VALUE.LATED_START_TIME_MORNING, out defaultLateTimeMorning);
                    TimeSpan.TryParse(ConstType.CONST_VALUE.LATED_START_TIME_AFTERNOON, out defaultLateTimeAfternoon);
                    TimeSpan expectStartTime;
                    TimeSpan expectEndTime;
                    AccountPaidHoliday accountPaidHoliday;
                    model.RegisterDate = currentDate;

                    // Check register date is weekend/ holiday
                    if (model.RegisterDate.Value.DayOfWeek == DayOfWeek.Saturday || model.RegisterDate.Value.DayOfWeek == DayOfWeek.Sunday || holidayService.IsHoliday(model.RegisterDate.Value, company.id))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0022;
                        return messResponse;
                    }

                    // Check already  registered working time
                    var workTimeInfo = workTimeService.GetWorkTimeByAccount(model.RegisterDate.Value,model.AccountId);
                    if (workTimeInfo != null && workTimeInfo.actual_start_time != null && workTimeInfo.actual_start_time != new TimeSpan(0, 0, 0))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0025;

                        return messResponse;
                    }
                    // Check if your late time is larger than 09:00
                    if((TimeSpan.Compare(model.LateTime.Value, defaultLateTimeMorning) > 0 && TimeSpan.Compare(TimeSpan.Parse(ConstType.CONST_VALUE.DEFAULT_MID_TIME), model.LateTime.Value) >=0 )|| TimeSpan.Compare(model.LateTime.Value, defaultLateTimeAfternoon) > 0)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0027;

                        return messResponse;
                    }

                    accountPaidHoliday = paidHolidayService.GetRequestByAccount(model.AccountId, model.RegisterDate.Value);
                    
                    if(accountPaidHoliday != null && accountPaidHoliday.id != 0 && accountPaidHoliday.paid_holiday_type == CONST_VALUE.ALL)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0024;

                        return messResponse;
                    }

                    // Check register time is smaller or equal with regular_start_time
                    var regularWorkTime = regularService.GetRegularWorkTimeByAccount(model.AccountId ,model.RegisterDate.Value);
                    var nameDate = model.RegisterDate.Value.ToString("ddd").ToLower();
                    string nameFlag = string.Concat(nameDate ,"_flg");
                    if (regularWorkTime == null )
                    {
                        //TimeSpan.TryParse(ConstType.CONST_VALUE.DEFAULT_START_TIME, out expectStartTime);
                        //TimeSpan.TryParse(ConstType.CONST_VALUE.DEFAULT_END_TIME, out expectEndTime);
                        expectStartTime = company.start_working_time;
                        expectEndTime = company.end_working_time;
                    }
                    else
                    { 
                        var test = regularWorkTime.GetType().GetProperty(nameFlag).GetValue(regularWorkTime, null);
                        //expectStartTime = (StringUtil.ConvertObjectToInteger32(test) == 0) ? TimeSpan.Parse(ConstType.CONST_VALUE.DEFAULT_START_TIME) : regularWorkTime.start_time;
                        //expectEndTime = (StringUtil.ConvertObjectToInteger32(test) == 0) ? TimeSpan.Parse(ConstType.CONST_VALUE.DEFAULT_END_TIME) : regularWorkTime.end_time;
                        expectStartTime = (StringUtil.ConvertObjectToInteger32(test) == 0) ? company.start_working_time : regularWorkTime.start_time;
                        expectEndTime = (StringUtil.ConvertObjectToInteger32(test) == 0) ? company.end_working_time : regularWorkTime.end_time;
                    }

                    if (TimeSpan.Compare(model.LateTime.Value, expectStartTime) <= 0)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0026;

                        return messResponse;
                    }

                    expectEndTime = expectEndTime + model.LateTime.Value - expectStartTime;
                    if(paidHolidayService.HasApplication(model.AccountId,model.RegisterDate.Value, APPROVE_FLG.FLG_ON) > 0)
                    {
                        expectEndTime = model.LateTime.Value + new TimeSpan(4, 0, 0);
                    }

                    if (workTimeInfo !=  null && workTimeInfo.additional_rest_time != null)
                    {
                        expectEndTime = expectEndTime + TimeSpan.FromHours(workTimeInfo.additional_rest_time.Value);
                    }

                    if (TimeSpan.Compare(expectEndTime, TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME)) > 0)
                    {
                        expectEndTime = model.LateTime.Value + new TimeSpan(4, 0, 0);
                        if (workTimeInfo != null && workTimeInfo.additional_rest_time != null)
                        {
                            expectEndTime = expectEndTime + TimeSpan.FromHours(workTimeInfo.additional_rest_time.Value);
                        }
                        expectEndTime = TimeSpan.Compare(expectEndTime, TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME)) > 0 ? TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME) : expectEndTime;
                    }

                    result = workTimeService.UpdateChangeOrLateTime(model.AccountId, model.LateTime.Value.ToString(), expectEndTime.ToString(), currentDate.ToString("yyyy/MM/dd"));
                    if(result.Status != ResultStatus.RegistSuccess)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = InfoMessage.I0068;

                        return messResponse;
                    }
                    
                    messResponse.response_type = SLACK_RESPONSE_TYP.INCHANNEL;
                    messResponse.text = string.Format(InfoMessage.I0033, model.UserName, model.LateTime.Value.ToString(@"hh\:mm"), model.reason);

                    return messResponse;
                }
                else if(model.CommandType == (int) COMMANDTYP.CHANGE)
                {
                    WorkingRecordService workTimeService = ServiceLocator.Resolve <WorkingRecordService>();

                    // Check register date is weekend/ holiday
                    if (model.RegisterDate.Value.DayOfWeek == DayOfWeek.Saturday || model.RegisterDate.Value.DayOfWeek == DayOfWeek.Sunday || holidayService.IsHoliday(model.RegisterDate.Value, company.id))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0022;
                        return messResponse;
                    }

                    var workTimeInfo = workTimeService.GetWorkTimeByAccount(model.RegisterDate.Value, model.AccountId);
                    //if(workTimeInfo == null || workTimeInfo.actual_start_time == null 
                    //    || TimeSpan.Compare(workTimeInfo.actual_start_time,new TimeSpan(0,0,0)) == 0
                    //    || workTimeService.IsApprovedWorkTime(workTimeInfo.id))
                    if (workTimeInfo != null && (workTimeService.IsApprovedWorkTime(workTimeInfo.id) || workTimeService.IsSelfCheck(workTimeInfo.id)))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0030;

                        return messResponse;
                    }

                    result = workTimeService.UpdateChangeOrLateTime(model.AccountId, model.ExpectStartTime.ToString(), model.ExpectEndTime.ToString(), model.RegisterDate.Value.ToString("yyyy/MM/dd"));
                    if (result.Status != ResultStatus.RegistSuccess)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = InfoMessage.I0068;

                        return messResponse;
                    }

                    messResponse.response_type = SLACK_RESPONSE_TYP.INCHANNEL ;
                    messResponse.text = string.Format(InfoMessage.I0034, model.UserName, possessiveNoun, model.RegisterDate.Value.ToShortDateString(), model.ExpectStartTime.Value.ToString(@"hh\:mm"), model.ExpectEndTime.Value.ToString(@"hh\:mm"), model.reason);

                    return messResponse;

                }
                else if (model.CommandType == (int)COMMANDTYP.REST)
                {
                    WorkingRecordService workTimeService = ServiceLocator.Resolve<WorkingRecordService>();
                    ChangedRegularWorkingTimeService regularService = ServiceLocator.Resolve<ChangedRegularWorkingTimeService>();
                    
                    TimeSpan startWorkingTime;
                    TimeSpan endWorkingTime;
                    TimeSpan expectEndTime;
                    TimeSpan tmpExpectEndTime;
                    

                    // Check register date is weekend/ holiday
                    if (model.RegisterDate.Value.DayOfWeek == DayOfWeek.Saturday || model.RegisterDate.Value.DayOfWeek == DayOfWeek.Sunday || holidayService.IsHoliday(model.RegisterDate.Value, company.id))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0022;
                        return messResponse;
                    }

                    var workTimeInfo = workTimeService.GetWorkTimeByAccount(model.RegisterDate.Value, model.AccountId);
                    if (workTimeInfo != null  && (workTimeService.IsApprovedWorkTime(workTimeInfo.id) || workTimeService.IsSelfCheck(workTimeInfo.id)))
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0031;

                        return messResponse;
                    }
                    var bool_holiday = paidHolidayService.HasApplication(model.AccountId, model.RegisterDate.Value, APPROVE_FLG.FLG_ON) > 0;
                    //Check total rest time > 4 incase have has account_paid_holiday
                    if (model.RestHours > 4 && bool_holiday)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = ErrorMessage.E0035;
                        return messResponse;
                    }

                    if (workTimeInfo != null)
                    {
                        //IF account has paid holiday, set time need to to in this day = 1/2 default , and no have any rest_time = 0 
                        var default_rest_time = bool_holiday ? TimeSpan.FromHours(double.Parse(CONST_VALUE.HALF_REST_TIME)) : TimeSpan.FromHours(double.Parse(CONST_VALUE.DEFAULT_REST_TIME));
                        var time_need_to_work = bool_holiday ? TimeSpan.Parse(CONST_VALUE.DEFAULT_HALF_TIME_WORK) : TimeSpan.Parse(CONST_VALUE.DEFAULT_START_TIME);
                        //Get start working time
                        startWorkingTime = (workTimeInfo.actual_start_time != null && TimeSpan.Compare(workTimeInfo.expect_start_time, workTimeInfo.actual_start_time) >= 0) ? workTimeInfo.expect_start_time : workTimeInfo.actual_start_time;

                        //tmpExpectEndTime = workTimeInfo.expect_end_time + TimeSpan.FromHours(model.RestHours);
                        tmpExpectEndTime = workTimeInfo.expect_start_time + time_need_to_work + TimeSpan.FromHours(model.RestHours) + default_rest_time;
                        //if (workTimeInfo.additional_rest_time != null)
                        //{
                        //    tmpExpectEndTime = tmpExpectEndTime - TimeSpan.FromHours(workTimeInfo.additional_rest_time.Value);
                        //}
                        expectEndTime = TimeSpan.Compare(tmpExpectEndTime, TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME)) <= 0 ? tmpExpectEndTime : TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME);

                        //Get end working time 
                        endWorkingTime = (workTimeInfo.actual_end_time != null && TimeSpan.Compare(workTimeInfo.actual_end_time, new TimeSpan(0, 0, 0)) != 0 && TimeSpan.Compare(expectEndTime, workTimeInfo.actual_end_time) <= 0) ? workTimeInfo.actual_end_time : expectEndTime;

                        var realWorkingTime = (endWorkingTime - startWorkingTime).TotalHours - default_rest_time.TotalHours - model.RestHours;
                        if (realWorkingTime < 0)
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = ErrorMessage.E0036;
                            return messResponse;
                        }
                    }
                    else
                    {
                        // Check register time is smaller or equal with regular_start_time
                        var regularWorkTime = regularService.GetExpectTime(model.AccountId, model.RegisterDate.Value, company);
                        startWorkingTime = regularWorkTime.start_time;
                        tmpExpectEndTime = regularWorkTime.end_time + TimeSpan.FromHours(model.RestHours);
                        endWorkingTime = TimeSpan.Compare(tmpExpectEndTime, TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME)) <= 0 ? tmpExpectEndTime : TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME);
                        expectEndTime = TimeSpan.Compare(tmpExpectEndTime, TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME)) <= 0 ? tmpExpectEndTime : TimeSpan.Parse(CONST_VALUE.DEFAULT_MAX_END_WORKINGTIME);

                    }
                    
                    //if(endWorkingTime.ToString() == ConstType.CONST_VALUE.NONE_TIME)
                    //{
                    //    endWorkingTime = workTimeInfo.expect_end_time;
                    //}

                    //var realWorkingTime = (endWorkingTime - startWorkingTime).TotalHours - workTimeInfo.rest_time - workTimeInfo.additional_rest_time - model.RestHours;
                    //if(realWorkingTime < 0)
                    //{
                    //    messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    //    messResponse.text = "Your additional rest time larger your real working time. Please check again.";
                    //    return messResponse;
                    //}

                    //Check time from < start working time and time to > end working time 
                    foreach (TimeFromto tmp in model.TimeFromTo)
                    {
                        if (tmp.TimeFrom < startWorkingTime)
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = ErrorMessage.E0033;
                            return messResponse;
                        }

                        if (tmp.TimeTo > endWorkingTime)
                        {
                            messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                            messResponse.text = ErrorMessage.E0034;
                            return messResponse;
                        }
                    }

                    result = workTimeService.UpdateAdditionalResttime(model.AccountId, model.RestHours, model.RegisterDate.Value.ToString("yyyy/MM/dd"), startWorkingTime.ToString(), expectEndTime.ToString());
                    if (result.Status != ResultStatus.RegistSuccess)
                    {
                        messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                        messResponse.text = InfoMessage.I0068;

                        return messResponse;
                    }
                    
                    switch(model.TimeFromTo.Count)
                    {
                        case 1:
                            messResponse.text = string.Format(InfoMessage.I0035, model.UserName, model.RegisterDate.Value.ToShortDateString(), model.TimeFromTo[0].TimeFrom.Value.ToString(@"hh\:mm"), model.TimeFromTo[0].TimeTo.Value.ToString(@"hh\:mm"),possessiveNoun, startWorkingTime.ToString(@"hh\:mm"), expectEndTime.ToString(@"hh\:mm"),  model.reason);
                            break;
                        case 2:
                            messResponse.text = string.Format(InfoMessage.I0036, model.UserName, model.RegisterDate.Value.ToShortDateString(), model.TimeFromTo[0].TimeFrom.Value.ToString(@"hh\:mm"), model.TimeFromTo[0].TimeTo.Value.ToString(@"hh\:mm"), model.TimeFromTo[1].TimeFrom.Value.ToString(@"hh\:mm"), model.TimeFromTo[1].TimeTo.Value.ToString(@"hh\:mm"), possessiveNoun, startWorkingTime.ToString(@"hh\:mm"), expectEndTime.ToString(@"hh\:mm"), model.reason);
                            break;
                        case 3:
                            messResponse.text = string.Format(InfoMessage.I0037, model.UserName, model.RegisterDate.Value.ToShortDateString(), model.TimeFromTo[0].TimeFrom.Value.ToString(@"hh\:mm"), model.TimeFromTo[0].TimeTo.Value.ToString(@"hh\:mm"), model.TimeFromTo[1].TimeFrom.Value.ToString(@"hh\:mm"), model.TimeFromTo[1].TimeTo.Value.ToString(@"hh\:mm"), model.TimeFromTo[2].TimeFrom.Value.ToString(@"hh\:mm"), model.TimeFromTo[2].TimeTo.Value.ToString(@"hh\:mm"), possessiveNoun, startWorkingTime.ToString(@"hh\:mm"), expectEndTime.ToString(@"hh\:mm"), model.reason);
                            break;

                    }
                    messResponse.response_type = SLACK_RESPONSE_TYP.INCHANNEL;

                    return messResponse;
                }
                else if (model.CommandType == (int)COMMANDTYP.HELP)
                { 
                    messResponse.response_type = SLACK_RESPONSE_TYP.EPHEMERAL;
                    messResponse.text = InfoMessage.I0030_1;
                    messResponse.attachments.Add(new HelperContent { text = InfoMessage.I0030_2 });
                    messResponse.attachments.Add(new HelperContent { text = InfoMessage.I0030_3 });
                    messResponse.attachments.Add(new HelperContent { text = InfoMessage.I0030_4 });
                    messResponse.attachments.Add(new HelperContent { text = InfoMessage.I0030_5 });
                }

                return messResponse;
            }
            catch(Exception ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
                return messResponse;
            }
        }
    }
}

﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.DataAccessLayer;
using AM.Interface;
using AM.Model.api;
using AM.Model.Common;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer
{
    public class AnnouncementService : BaseService
    {
        /// <summary>
        /// Get announcement information
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get announcement information processing",
             AfterMessage = "Finish get announcement information processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<Announcement> GetAnnouncement(int companyId)
        {
            return ServiceLocator.Resolve<IAnnouncement>().GetAnnouncement(companyId);
        }

        /// <summary>
        /// Delete an announcement by id
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete announcement processing",
             AfterMessage = "Finish delete announcement processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo DeleteAnnouncement(Announcement model)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DeleteFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0005
            };
            try
            {
                if (ServiceLocator.Resolve<IAnnouncement>().DeleteAnnouncement(model))
                {
                    resultInfo.Status = ResultStatus.DeleteSuccess;
                    resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                    resultInfo.Message = InfoMessage.I0048;
                }

                return resultInfo;
            }
            catch (Exception ex)
            {
                resultInfo.Message = ex.Message;
                return resultInfo;
            }
        }

        /// <summary>
        /// Recover an announcement by id
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start recover announcement processing",
            AfterMessage = "Finish recover announcement processing",
            Severity = TraceEventType.Information,
            Categories = new string[] { "General" },
            IncludeParameters = false)]
        [Transaction()]
        public ResultInfo RecoverAnnouncement(Announcement model)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DeleteFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0004
            };
            try
            {
                if (ServiceLocator.Resolve<IAnnouncement>().RecoverAnnouncement(model))
                {
                    resultInfo.Status = ResultStatus.DeleteSuccess;
                    resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                    resultInfo.Message = InfoMessage.I0049;
                }

                return resultInfo;
            }
            catch (Exception ex)
            {
                resultInfo.Message = ex.Message;
                return resultInfo;
            }
        }

        /// <summary>
        /// Insert/Update an announcement 
        /// </summary>
        /// <param name="model">Announcement object</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start insert/update an announcement processing",
            AfterMessage = "Finish insert/update an announcement processing",
            Severity = TraceEventType.Information,
            Categories = new string[] { "General" },
            IncludeParameters = false)]
        [Transaction()]
        public ResultInfo Merge(Announcement model)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.EditFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0007
            };
            try
            {
                if (ServiceLocator.Resolve<IAnnouncement>().Merge(model))
                {
                    resultInfo.Status = ResultStatus.EditSuccess;
                    resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                    resultInfo.Message = InfoMessage.I0008;
                }

                return resultInfo;
            }
            catch (Exception ex)
            {
                resultInfo.Message = ex.Message;
                return resultInfo;
            }

        }

        /// <summary>
        /// Get approve waiting info
        /// </summary>
        /// <param name="account_id"></param>
        /// <param name="flag_admin"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get approve waiting information processing",
            AfterMessage = "Finish get approve waiting information processing",
            Severity = TraceEventType.Information,
            Categories = new string[] { "General" },
            IncludeParameters = false)]
        public List<ApproveWaitingInfo> GetApproveWaiting(int account_id, bool flag_admin, string company_id)
        {
            return ServiceLocator.Resolve<IAnnouncement>().GetApproveWaiting(account_id, flag_admin, company_id);
        }

        /// <summary>
        /// Post message to slack
        /// </summary>
        /// <param name="email">login user email</param>
        /// <param name="title">title</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start post message to slack to inform a new announcement processing",
            AfterMessage = "Finish post message to slack to inform a new announcement processing",
            Severity = TraceEventType.Information,
            Categories = new string[] { "General" },
            IncludeParameters = false)]
        public async Task<ResultInfo> PostMessageToSlack(string email,string title,Company company_info)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistSuccess,
                Message = InfoMessage.I0010_Code
            };
            // Get token
            var cts = new CancellationTokenSource();
            string messageSlack = string.Empty;
            var attendanceSetting = new AttendanceSettings();
            var tokenDal = new TokenDal(attendanceSetting);
            HttpClient client = new HttpClient();

            try
            {
                messageSlack = string.Format(AttendanceSettings.AnnouncementMessage, email, title);

                // Init para
                DataPostMessageToSlack dataPost = new DataPostMessageToSlack();
                dataPost.text = messageSlack;
                //dataPost.channel = AttendanceSettings.AnnouncementChanel;
                dataPost.channel = company_info.announcement_channel;
                //dataPost.user_name = AttendanceSettings.UserNameSlack;
                dataPost.username = company_info.post_message_username;
                //dataPost.icon_url = AttendanceSettings.IconUrlSlack;
                dataPost.icon_url = company_info.company_logo;
                //var objPara = new { data = new List<DataPostMessageToSlack>() { dataPost } };

                // Convert to json
                var jsonParameter = JsonConvert.SerializeObject(dataPost);

                int timeoutSlack = StringUtil.ConvertObjectToInteger32(ConfigurationManager.AppSettings["SlackTimeOut"]);
                cts.CancelAfter(timeoutSlack);
                //cts.CancelAfter(new TimeSpan(0, 0, CONST_VALUE.SLACK_TIME_OUT));

                var token = tokenDal.GetTokenPostMessageSlack(CONST_VALUE.ACTION_NAME);
                //jsonParameter = jsonParameter.Replace('[', ' ');
                //jsonParameter = jsonParameter.Replace(']', ' ');
                var content = new StringContent(jsonParameter, Encoding.UTF8, CONST_VALUE.CONTENT_TYPE);
                //web hook url
                //string url = string.Format(AttendanceSettings.GetPostMessageToSlackUrl + "action={0}&token={1}&time={2}", CONST_VALUE.ACTION_NAME, token.AccessToken, token.UnixTime;
                var res = await client.PostAsync(new Uri(company_info.slack_webhook_url), content, cts.Token);
                var jsonRes = res.Content.ReadAsStringAsync().Result;
                if (jsonRes != CONST_VALUE.STATUS_SLACK_SUCCESS)
                {
                    return resultInfo;
                }
                
                // Sucessfull
                resultInfo.Message = InfoMessage.I0009;
            }
            catch (OperationCanceledException)
            {
                resultInfo.Message = InfoMessage.I0010_Code;
            }
            catch (Exception)
            {
                //resultInfo.Message = ex.Message;
            }

            cts = null;
            // Return
            return resultInfo;
        }
    }
}

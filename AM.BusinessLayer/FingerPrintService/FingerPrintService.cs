﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.ConstData;
using AM.Model.Tables;
using System;
using System.Collections.Generic;
using AM.Core.PolicyInjection;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer.FingerPrintService
{
    public class FingerPrintService : BaseService
    {
        /// <summary>
        /// Get List Finger Print
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list finger print information processing",
             AfterMessage = "Finish get list finger print information processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<FingerPrintResponse> GetFingerPrint(string company_id = CONST_VALUE.SESSION_COMPANY_DEFAULT)
        {
            return ServiceLocator.Resolve<IFingerPrint>().FingerPrintLst(company_id);
        }

        /// <summary>
        /// Register a new finger print
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register/update a finger print processing",
                        AfterMessage = "Finish register/update a finger print processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo InsertFingerPrint(FingerPrint fingerPrint, string account_id)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0012
            };

            var dalFingerPrint = ServiceLocator.Resolve<IFingerPrint>();
            var dalAccount = ServiceLocator.Resolve<IAccount>();
            var account = new Account();

            try
            {
                // Check fingerPrintId exist or not
                //if (dalFingerPrint.checkExistFingerId(fingerPrint.id))
                //{
                //    resultInfo.Message = "Finger print id already exist";
                //    return resultInfo;
                //}
                //else
                //{

                if (!String.IsNullOrEmpty(account_id))
                {
                    account = dalAccount.GetDataById(StringUtil.ConvertObjectToInteger32(account_id));
                    if (account == null)
                    {
                        resultInfo.Message = InfoMessage.I0013;
                        return resultInfo;
                    }
                }

                if (!dalFingerPrint.Insert(fingerPrint))
                    return resultInfo;

                if (!String.IsNullOrEmpty(account_id))
                {
                    account.fingerprint_id = fingerPrint.id;
                    account.updated_date = fingerPrint.updated_date;
                    account.updated_account_id = fingerPrint.updated_account_id;

                    if (!dalAccount.Update(account))
                        return resultInfo;
                }
                    
                resultInfo.Status = ResultStatus.RegistSuccess ;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0057;
                //}
                
            }
            catch(Exception ex)
            {
                resultInfo.Message = ex.Message;
            }

            return resultInfo;
        }

        /// <summary>
        /// Delete Finger Print Id
        /// </summary>
        /// <param name="FingerPrintId">FingerPrintId</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete a finger print by id processing",
                        AfterMessage = "Finish delete a finger print by id processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo DeleteFingerPrint(int FingerPrintId)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DeleteFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0058
            };

            var dalFingerPrint = ServiceLocator.Resolve<IFingerPrint>();
            try
            {
                // Check fingerPrintId exist or not
                if (dalFingerPrint.checkExistFingerId(FingerPrintId))
                {
                    if(!dalFingerPrint.DeleteFingerPrint(FingerPrintId))
                        return resultInfo;

                    resultInfo.Status = ResultStatus.DeleteSuccess;
                    resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                    resultInfo.Message = InfoMessage.I0059;
                }
                else
                {
                    resultInfo.Message = InfoMessage.I0060;
                }
            }
            catch(Exception ex)
            {
                resultInfo.Message = ex.Message;
            }

            return resultInfo;
        }
    }
}

﻿using AM.Core.Util;
using System;
using System.Configuration;
using System.Net;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer
{
    public class TimedWebClient : WebClient
    {
        // Timeout in milliseconds, default = 10,000 msec
        public int Timeout { get; set; }

        public TimedWebClient()
        {
            this.Timeout = StringUtil.ConvertObjectToInteger32(ConfigurationManager.AppSettings["SlackTimeOut"]);
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var objWebRequest = base.GetWebRequest(address);
            objWebRequest.Timeout = this.Timeout;
            return objWebRequest;
        }
    }
}

﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer.ConfirmationService
{
    public class ConfirmationService : BaseService
    {
        /// <summary>
        /// Update confirmation.check_status
        /// </summary>
        /// <param name="application_id">application_id</param>
        /// <param name="account_id">account_id</param>
        /// <param name="check_status">check_status</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update check status when approving processing",
             AfterMessage = "Finish update check status when approving processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateCheckStatus(Confirmation model)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.EditFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0003
            };
            try
            {
                var dalConfirm = ServiceLocator.Resolve<IConfirmation>();
                if (!dalConfirm.UpdateCheckStatus(model))
                    return resultInfo;

                resultInfo.Status = ResultStatus.EditSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0055;
            }
            catch (System.Exception ex)
            {
                resultInfo.Message = ex.Message;
            }

            // return
            return resultInfo;
        }

        /// <summary>
        /// Register confirmation
        /// </summary>
        /// <param name="listConfirmAccount">List of account id confirm</param>
        /// <param name="account_id">Accounut Id</param>
        /// <param name="application_id">Application Id</param>
        /// <param name="is_reject_selfcheck">Is reject selfcheck</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register confirmation processing",
             AfterMessage = "Finish register confirmation processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo RegisterConfirmation(IList<int> listConfirmAccount, int account_id, int application_id, bool is_reject_selfcheck = false)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
            };
            try
            {
                var dalConfirm = ServiceLocator.Resolve<IConfirmation>();
                var memberInfo = ServiceLocator.Resolve<IAccount>().GetDataById(account_id);
                DateTime dateNow = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);
                int application_sequence = dalConfirm.GetMaxApplicationSequence(application_id);

                // Check is_reject_selfcheck = true
                if (is_reject_selfcheck)
                {
                    // Get max application_sequence
                    application_sequence = application_sequence + 1;
                }
                for (int index = 0; index < listConfirmAccount.Count; index++)
                {
                    Confirmation confirmData = new Confirmation
                    {
                        application_id = application_id,
                        account_id = listConfirmAccount[index],
                        confirmed_datetime = null,
                        application_form_id = (byte)ConfirmationType.WorkingTime,
                        check_sequence = Convert.ToByte(index + 1),
                        check_status = 0,
                        created_account_id = account_id,
                        created_date = dateNow,
                        updated_account_id = account_id,
                        updated_date = dateNow,
                        deleted_date = null,
                        deleted_account_id = null,
                        application_sequence = application_sequence
                    };

                    if (!dalConfirm.Insert(confirmData))
                        return resultInfo;
                }

                resultInfo.Status = ResultStatus.RegistSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0056;
            }
            catch (Exception ex)
            {
                resultInfo.Message = ex.Message;
            }

            return resultInfo;
        }

        /// <summary>
        /// Get list of notes when reject
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns>List of notes when reject</returns>
        [LogCallHandler(BeforeMessage = "Start get list of notes of rejected working time processing",
             AfterMessage = "Finish get list of notes of rejected working time processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<ConfirmationResponse> GetNotesWhenReject(string companyId)
        {
            return ServiceLocator.Resolve<IConfirmation>().GetNotesWhenReject(companyId);
        }

        /// <summary>
        /// Get max application sequence
        /// </summary>
        /// <param name="application_id">Application Id</param>
        /// <returns>max applicatioin sequence</returns>
        [LogCallHandler(BeforeMessage = "Start get max application sequence by application id processing",
             AfterMessage = "Finish get max application sequence by application id processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public int GetMaxApplicationSequence(int application_id)
        {
            return ServiceLocator.Resolve<IConfirmation>().GetMaxApplicationSequence(application_id);
        }

        /// <summary>
        /// Get list of notes when reject
        /// </summary>
        /// <returns>List of notes when reject</returns>
        [LogCallHandler(BeforeMessage = "Start get list of notes of rejected request processing",
             AfterMessage = "Finish get list of notes of rejected request processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<ConfirmationResponse> GetCheckNotesWhenReject(string companyId)
        {
            return ServiceLocator.Resolve<IConfirmation>().GetCheckNotesWhenReject(companyId);
        }
    }
}

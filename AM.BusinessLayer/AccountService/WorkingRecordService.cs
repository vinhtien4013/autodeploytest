﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.DataAccessLayer;
using AM.Interface;
using AM.Model;
using AM.Model.api;
using AM.Model.Common;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer.AccountService
{
    public class WorkingRecordService : BaseService
    {
        /// <summary>
        /// Get Working time information of account in the month
        /// </summary>
        /// <param name="account_id">account id</param>
        /// <param name="month">month</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get working time information by account id and month processing",
                       AfterMessage = "Finish get working time information by account id and month processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public List<WorkingTimeResponse> GetWorkingTime(int account_id, DateTime month,Company company_info)
        {
            return ServiceLocator.Resolve<IWorkingtimeRecord>().GetWorkingTime(account_id, month, company_info);
        }

        /// <summary>
        /// Get data working time record in db by Id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get working time information by id processing",
               AfterMessage = "Finish get working time information by id processing",
               Severity = TraceEventType.Information,
               Categories = new string[] { "General" },
               IncludeParameters = false)]
        public WorkingtimeRecord GetDataById(int id)
        {
            return ServiceLocator.Resolve<IWorkingtimeRecord>().GetdataById(id);
        }

        /// <summary>
        /// Update list working time info
        /// </summary>
        /// <param name="listInfo">List of WorkingTimeRecord</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update working time information processing",
               AfterMessage = "Finish update working time information processing",
               Severity = TraceEventType.Information,
               Categories = new string[] { "General" },
               IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateWorkingTime(List<WorkingtimeRecord> listInfo, int companyId)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.EditFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0019
            };

            if (listInfo.Count > 0)
            {
                var holidayLst = ServiceLocator.Resolve<IHoliday>().GetHolidayByYear(listInfo[0].register_date.Year, companyId);
                var dalWorkiingTime = ServiceLocator.Resolve<IWorkingtimeRecord>();
                var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, companyId.ToString(),null);
                DateTime nowTime = AzureUtil.LocalDateTimeNow(company.time_zone_id);
                WorkingtimeRecord data = new WorkingtimeRecord();
                AccountService accountService = ServiceLocator.Resolve<AccountService>();
                var overTimeService = ServiceLocator.Resolve<IWorkingovertimeRecord>();
                ConfirmationService.ConfirmationService confirmService = ServiceLocator.Resolve < ConfirmationService.ConfirmationService>();

                var accountInfo = accountService.GetDataById(listInfo[0].account_id);
                var staff_id = accountInfo.staff_id;
                int insertedId = 0;

                for (int i = 0; i < listInfo.Count; i++)
                {
                    data = listInfo[i];
                    if(!(data.deleted_account_id == 0 || data.deleted_account_id == null))
                    {
                        dalWorkiingTime.Delete(data);
                    }
                    else
                    {
                        WorkingovertimeRecord overTimeModel = new WorkingovertimeRecord()
                        {
                            total_over_timework = data.overtime_record.total_over_timework,
                            created_date = nowTime,
                            created_account_id = data.updated_account_id,
                            updated_date = nowTime,
                            updated_account_id = data.updated_account_id
                        };

                        //Check weekendday and holiday
                        if (listInfo[i].actual_end_time != null && IsOvertimeDay(holidayLst, listInfo[i].register_date))

                            //Total overtime
                            overTimeModel.total_over_timework = listInfo[i].actual_end_time.Subtract(listInfo[i].actual_start_time).TotalHours - listInfo[i].rest_time - StringUtil.ConvertObjectToDouble(listInfo[i].additional_rest_time);

                        if (overTimeModel.total_over_timework > 0 && (data.overtime_record_id == null || !overTimeService.IsExist(data.overtime_record_id.Value)))
                        {
                            // Insert working over time
                            int overTimeId = overTimeService.Insert(overTimeModel);
                            if(overTimeId <= 0)
                                return resultInfo; 
                            // Overtime id
                            data.overtime_record_id = overTimeId;
                        }
                        else
                        {
                            if (data.overtime_record_id != null)
                            {
                                // id
                                overTimeModel.id = data.overtime_record_id.Value;
                                // Update working over time
                                if (!overTimeService.Update(overTimeModel))
                                    return resultInfo;
                            }
                        }

                        // Update workingtime_record table
                        //if (data.id != 0)
                        //{
                        //    insertedId = data.id;
                        //    if (!dalWorkiingTime.Update(data))
                        //        return resultInfo;
                        //}
                        //else
                        //{
                        //    // Insert workingtime_record table
                        //    insertedId = dalWorkiingTime.Insert(data);
                        //    if (insertedId <= 0)
                        //        return resultInfo;
                        //}
                        insertedId = dalWorkiingTime.Insert(data);
                        if (insertedId <= 0)
                            return resultInfo;
                        TimeSpan tmpTime = new TimeSpan(0, 0, 0);
                        // Insert data for table confirmation
                        if (TimeSpan.Compare(data.actual_start_time, tmpTime) == 1 && TimeSpan.Compare(data.actual_end_time, tmpTime) == 1)
                        {
                            // Get list account id for confimation
                            var lstCheckAccId = accountService.GetListCheckAccountId(accountInfo.id, accountInfo.division_id);

                            if (lstCheckAccId.Count > 0)
                            {
                                lstCheckAccId = lstCheckAccId.Distinct().ToList();
                                // Merge confirmation info
                                resultInfo = confirmService.RegisterConfirmation(lstCheckAccId, data.updated_account_id, insertedId, data.is_reject_selfcheck);
                                if (resultInfo.Status != ResultStatus.RegistSuccess)
                                    return resultInfo;
                            }
                        }
                    }    
                   
                }

                resultInfo.Status = ResultStatus.EditSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0018;
            }

            return resultInfo;
        }

        /// <summary>
        /// Get list of check attendance
        /// </summary>
        /// <param name="register_date">register_date</param>
        /// <param name="company_id">company_id</param>
        /// <returns></returns>
        public List<CheckAttendanceResponse> GetCheckAttendance(string register_date, string company_id)
        {
            return ServiceLocator.Resolve<IWorkingtimeRecord>().GetCheckAttendance(register_date, company_id);
        }

        /// <summary>
        /// Register working information for check in
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register working information for check in processing",
              AfterMessage = "Finish register working information for check in processing",
              Severity = TraceEventType.Information,
              Categories = new string[] { "General" },
              IncludeParameters = false)]
        [Transaction()]
        public ResultInfo RegisterCheckinInfo(int account_id, DateTime dateNow, Company company)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0025
            };

            var dalWorkiingTime = ServiceLocator.Resolve<IWorkingtimeRecord>();
            var actualTime = DateTimeUtil.RoundingTime(dateNow, (int)RoundingTimeTypeEnum.UP).ToString("HH:mm");

            // Update time start working
            if (dalWorkiingTime.UpdateWorkingTimeGoing(dateNow, actualTime, account_id, company) < 0)
                return resultInfo;

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0024;

            return resultInfo;
        }

        /// <summary>
        /// Update working time information for check out
        /// </summary>
        /// <param name="account_id"></param>
        /// <param name="CheckConfirmLst"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update working information for check out processing",
              AfterMessage = "Finish update working information for check out processing",
              Severity = TraceEventType.Information,
              Categories = new string[] { "General" },
              IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateCheckOutInfo(int account_id, WorkingtimeRecord workingtime, IList<int> CheckConfirmLst, Company company)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = ErrorMessage.E0015
            };

            var dalWorkiingTime = ServiceLocator.Resolve<IWorkingtimeRecord>();
            var ConfirmService = ServiceLocator.Resolve < ConfirmationService.ConfirmationService>();
            DateTime dateNow = AzureUtil.LocalDateTimeNow(company.time_zone_id);
            var actualTime = DateTimeUtil.RoundingTime(dateNow, (int)RoundingTimeTypeEnum.DOWN);
            TimeSpan startTime = (TimeSpan.Compare(workingtime.expect_start_time, workingtime.actual_start_time) < 0) ? workingtime.actual_start_time : workingtime.expect_start_time;
            // actual end time
            TimeSpan actualendTime = new TimeSpan(actualTime.Hour, actualTime.Minute, 0);
            TimeSpan endtime = (TimeSpan.Compare(workingtime.expect_end_time, actualendTime) < 0) ? workingtime.expect_end_time : actualendTime;

            // caculate them real working time in date
            TimeSpan workTime = endtime.Subtract(startTime);
            //var restTime = CONST_VALUE.DEFAULT_REST_TIME;
            var restTime = Math.Round((company.end_lunch_time - company.start_lunch_time).TotalHours, 2).ToString();
            if (workTime.Hours < 5)
            {
                restTime = CONST_VALUE.HALF_REST_TIME;
            }

            // Update working over time record
            WorkingovertimeRecord overTimeModel = new WorkingovertimeRecord()
            {
                total_over_timework = workTime.TotalHours,
                created_date = dateNow,
                created_account_id = account_id,
                updated_date = dateNow,
                updated_account_id = account_id
            };
            var holidayLst = ServiceLocator.Resolve<IHoliday>().GetHolidayByYear(dateNow.Year, company.id);
            int overtime_record_id = 0;
            var overTimeService = ServiceLocator.Resolve <IWorkingovertimeRecord>();
            if (IsOvertimeDay(holidayLst, dateNow))
            {
                // Insert working over time
                overtime_record_id = overTimeService.Insert(overTimeModel);
                if (overtime_record_id <= 0)
                {
                    return resultInfo;
                }
            }
            
            // Update time leaving
            int updatedId = dalWorkiingTime.UpdateWorkingTimeLeaving(dateNow, actualendTime, account_id, restTime, overtime_record_id);
            if (updatedId < 0)
                return resultInfo;

            if (CheckConfirmLst.Count > 0)
            {
                // Create confirmation info 
                resultInfo = ConfirmService.RegisterConfirmation(CheckConfirmLst, account_id, updatedId);
                if (resultInfo.Status != ResultStatus.RegistSuccess)
                    return resultInfo;
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0027;

            return resultInfo;
        }

        /// <summary>
        /// Post message to slack
        /// </summary>
        /// <param name="checkInOut">1: Check in, 2: check out</param>
        /// <param name="dateNow">DateTime.Now</param>
        /// <param name="staffName">Staff name</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start post message to slack for check in/out processing",
             AfterMessage = "Finish post message to slack for check in/out processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public async Task<ResultInfo> PostMessageToSlack(CheckInOutEnum checkInOut, DateTime dateNow, string staffName,Company company_info)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistSuccess,
                Message = InfoMessage.I0026_Code,
            };
            // Get token
            var cts = new CancellationTokenSource();
            string messageSlack = string.Empty;
            var attendanceSetting = new AttendanceSettings();
            var tokenDal = new TokenDal(attendanceSetting);
            HttpClient client = new HttpClient();

            try
            {
                // Check in (going)
                if (checkInOut == CheckInOutEnum.CheckIn)
                {
                    messageSlack = string.Format(AttendanceSettings.MessageCheckInSlack, dateNow.ToString(), staffName);
                }
                else // Check out (leaving)
                {
                    messageSlack = string.Format(AttendanceSettings.MessageCheckOutSlack, dateNow.ToString(), staffName);
                }

                // Init para
                DataPostMessageToSlack dataPost = new DataPostMessageToSlack();
                dataPost.text = messageSlack;
                //dataPost.channel = AttendanceSettings.ChannelSlack;
                dataPost.channel = company_info.attendance_channel;
                //dataPost.user_name = AttendanceSettings.UserNameSlack;
                dataPost.username = company_info.post_message_username;
                //dataPost.icon_url = AttendanceSettings.IconUrlSlack;
                dataPost.icon_url = company_info.company_logo;
                //var objPara = new { data = new List<DataPostMessageToSlack>() { dataPost } };

                // Convert to json
                var jsonParameter = JsonConvert.SerializeObject(dataPost);
               
                int timeoutSlack = StringUtil.ConvertObjectToInteger32(ConfigurationManager.AppSettings["SlackTimeOut"]);
                cts.CancelAfter(timeoutSlack);
                //cts.CancelAfter(new TimeSpan(0,0,CONST_VALUE.SLACK_TIME_OUT));

                var token = tokenDal.GetTokenPostMessageSlack(CONST_VALUE.ACTION_NAME);
                //jsonParameter = jsonParameter.Replace('[', ' ');
                //jsonParameter = jsonParameter.Replace(']', ' ');
                var content = new StringContent(jsonParameter, Encoding.UTF8, CONST_VALUE.CONTENT_TYPE);
                //string url = string.Format(AttendanceSettings.GetPostMessageToSlackUrl + "action={0}&token={1}&time={2}", CONST_VALUE.ACTION_NAME, token.AccessToken, token.UnixTime);
                var res = await client.PostAsync(new Uri(company_info.slack_webhook_url), content, cts.Token);
                var jsonRes = res.Content.ReadAsStringAsync().Result;
                if (jsonRes != CONST_VALUE.STATUS_SLACK_SUCCESS)
                {
                    return resultInfo;
                }
                // Sucessfull
                resultInfo.Message = InfoMessage.I0001;
            }
            catch (OperationCanceledException)
            {
                resultInfo.Message = InfoMessage.I0026_Code;
            }
            catch(Exception)
            {
                //resultInfo.Message = ex.Message;
            }

            cts = null;
            // Return
            return resultInfo;
        }

        /// <summary>
        /// Check leaving time after going time at least 5 minutes 
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check leaving time after going time at least 5 minutes processing",
             AfterMessage = "Finish check leaving time after going time at least 5 minutes processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public bool CheckTimeLeaving(DateTime dateNow, TimeSpan startTime)
        {
            var dalWorkiingTime = ServiceLocator.Resolve<IWorkingtimeRecord>();
            //DateTime dateNow = AzureUtil.VietnamDateTimeNow();

            var actualTime = DateTimeUtil.RoundingTime(dateNow, (int)RoundingTimeTypeEnum.DOWN);
            TimeSpan endtime = new TimeSpan(actualTime.Hour, actualTime.Minute, 0);
            //var workingTimeInfo = dalWorkiingTime.GetWorkTimeInfo(dateNow, account_id);

            if (TimeSpan.Compare(startTime, endtime) != (-1))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get working time information in dateNow of account_id
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get working time information in dateNow of account_id processing",
             AfterMessage = "Finish get working time information in dateNow of account_id processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public WorkingtimeRecord GetWorkTimeByAccount(DateTime dateNow, int account_id)
        {
            var dalWorkiingTime = ServiceLocator.Resolve<IWorkingtimeRecord>();
            return dalWorkiingTime.GetWorkTimeByAccount(dateNow, account_id);
        }

        /// <summary>
        /// Check the working time record be approved or not 
        /// </summary>
        /// <param name="id">workingtime_record.id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check the working time record be approved or not processing",
             AfterMessage = "Finish check the working time record be approved or not processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public bool IsApprovedWorkTime(int id)
        {
            return ServiceLocator.Resolve<IWorkingtimeRecord>().IsApprovedWorkTime(id);
        }

        /// <summary>
        ///  Update working time for registering additional rest time request via Slack
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="additionalRestTime">additional rest time</param>
        /// <param name="updatedDate">updated date</param>
        /// <param name="expectStartTime">expect start time</param>
        /// <param name="expectEndTime">expect end time</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update working time for registering additional rest time request via Slack processing",
             AfterMessage = "Finish update working time for registering additional rest time request via Slack processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateAdditionalResttime(int accountId, double additionalRestTime, string updatedDate, string expectStartTime, string expectEndTime)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0044
            };
             if (! ServiceLocator.Resolve<IWorkingtimeRecord>().UpdateAdditionalResttime(accountId, additionalRestTime, updatedDate, expectStartTime, expectEndTime))
            {
                return resultInfo;
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0045;

            return resultInfo;
        }

        /// <summary>
        /// Is Over time day
        /// </summary>
        /// <param name="holidayLst"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check whether the date is over day processing",
             AfterMessage = "Finish check whether the date is over day processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        private bool IsOvertimeDay(List<Holiday> holidayLst, DateTime date)
        {
            bool result = false;

            // Check isWeekendDay
            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                result = true;
            else
            {
                // Check isHoliday 
                var lst = holidayLst.Where(m => m.holiday_date_from <= date && m.holiday_date_to >= date).ToList();
                if (lst.Count > 0) result = true;
            }
            return result;
        }

        /// <summary>
        /// Update working time when have change regular time/late request
        /// </summary>
        /// <param name="accountId">account_id</param>
        /// <param name="expectStartTime">expect start time</param>
        /// <param name="expectEndTime">expect end time</param>
        /// <param name="updatedDate">updated date</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update working time for registering late/change request via Slack processing",
             AfterMessage = "Finish update working time for registering late/change request via Slack processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateChangeOrLateTime(int accountId, string expectStartTime,string expectEndTime ,string updatedDate)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0046
            };

            if(!ServiceLocator.Resolve<IWorkingtimeRecord>().UpdateChangeOrLateTime(accountId, expectStartTime, expectEndTime, updatedDate))
            {
                return resultInfo;
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0047;

            return resultInfo;
        }

        /// <summary>
        ///  Check the working time is selfcheck or not
        /// </summary>
        /// <param name="id">working time record id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check the workingtime record self check or not processing",
             AfterMessage = "Finish check the workingtime record self check or not processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public bool IsSelfCheck(int id)
        {
            return ServiceLocator.Resolve<IWorkingtimeRecord>().IsSelfCheck(id);
        }

        /// <summary>
        ///  Get list staff working status
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start getting list staff working status processing",
             AfterMessage = "Finish getting list staff working status processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<StaffWKStatusResponse> GetStaffWKStatus(string id,int range,Company company)
        {
            return ServiceLocator.Resolve<IWorkingtimeRecord>().GetStaffWKStatus(id,range,company);
        }
    }
}

﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model;
using AM.Model.api;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer.AccountService
{
    public class AccountService : BaseService
    {
        /// <summary>
        /// Get Account List by company
        /// </summary>
        /// <returns> List active account</returns>
        [LogCallHandler(BeforeMessage = "Start get list account processing",
                      AfterMessage = "Finish get list account processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public List<EditAccountModel> GetAccount(string company_id = CONST_VALUE.SESSION_COMPANY_DEFAULT)
        {
            return ServiceLocator.Resolve<IAccount>().GetAccount(company_id);
        }

        /// <summary>
        /// search Account List by input
        /// </summary>
        /// <returns> List active account</returns>
        [LogCallHandler(BeforeMessage = "Start search list account with condition processing",
                     AfterMessage = "Finish search list account with condition processing",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        public List<EditAccountModel> GetAccountByCondition(ParameterSearchAccount searchInput)
        {
            return ServiceLocator.Resolve<IAccount>().GetAccountByCondition(searchInput);
        }

        /// <summary>
        /// Get Member Information by Email when loggin by google
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get account information by email when logging by google processing",
                     AfterMessage = "Finish get account information by email when logging by google processing",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        public MemberInfoModel GetMemberInfoByGoogle(string email, string token = null)
        {
            return ServiceLocator.Resolve<IAccount>().GetMemberInfoByGoogle(email, token);
        }

        /// <summary>
        /// Get Member by slack_user_id
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get account information by email when logging by google processing",
                     AfterMessage = "Finish get account information by email when logging by google processing",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        public MemberInfoModel GetMemberInfoBySlackUserId(string slack_user_id, string token)
        {
            return ServiceLocator.Resolve<IAccount>().GetMemberInfoBySlackUserId(slack_user_id, token);
        }

        /// <summary>
        /// Get Member Information by Email when loggin by manual
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get account information  when logging by manual  processing",
                     AfterMessage = "Finish get account information when logging by manual processing",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        public MemberInfoManualModel GetMemberInfoByManual(string email,string password, string token = null)
        {
            return ServiceLocator.Resolve<IAccount>().GetMemberInfoByManual(email, password, token);
        }
        /// <summary>
        /// Update account with finger print ID
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update account processing",
                       AfterMessage = "Finish update account processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateAccountInfo(List<Account> accountLst)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0020
            };
            var serviceAccount = ServiceLocator.Resolve<IAccount>();
            var serviceFingerPrint = ServiceLocator.Resolve<IFingerPrint>();
            string oldpassword = "";
            foreach (var account in accountLst)
            {
                oldpassword = account.password;
                // Account info already exist in database
                if (account.id != 0)
                {
                    if (!string.IsNullOrEmpty(account.password))
                    {
                         account.password = BCrypt.Net.BCrypt.HashString(account.password);
                    }
                    if (!serviceAccount.Update(account))
                    {
                        return resultInfo;
                    }                   
                }
                else // register a new staff
                {
                    account.language = CONST_VALUE.DEFAULT_LANGUAGE;
                    if(account.password != null)
                    {
                        account.password = BCrypt.Net.BCrypt.HashString(account.password);
                    }    
                   
                    if (serviceAccount.Insert(account) <= 0)
                    {
                        return resultInfo;
                    }
                }
                account.password = oldpassword;
            }
            
            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0001;

            return resultInfo;
        }
        /// <summary>
        /// Update Account Language
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update account language processing",
                       AfterMessage = "Finish update account language processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateAccountLanguage(int id, string language)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0020
            };
            var serviceAccount = ServiceLocator.Resolve<IAccount>();
            if (!serviceAccount.UpdateAccountLanguage(id,language))
                return resultInfo;

                resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0001;

            return resultInfo;
        }

        /// <summary>
        /// Delete account with finger print ID
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete account by finger print ID processing",
                       AfterMessage = "Finish delete account by finger print ID processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        [Transaction()]
        public ResultInfo DeleteAccount(int FingerPrintId, string token = null)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DeleteFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0041
            };
            var Accountdal = ServiceLocator.Resolve<IAccount>();
            // get infomation of account by fingerprint_id
            var account = GetDataByFingerPrintId(FingerPrintId);
            // If exist account then delete
            if (account != null)
            {
                if (!Accountdal.Delete(account))
                {
                    return resultInfo;
                }
            }

            resultInfo.Status = ResultStatus.DeleteSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0002;

            return resultInfo;
        }

        /// <summary>
        /// Get data by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get data account by id processing",
                       AfterMessage = "Finish get data account by id processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public MemberInfoModel GetDataById(int id)
        {
            return ServiceLocator.Resolve<IAccount>().GetDataById(id);
        }

        /// <summary>
        /// Get data by finger print id
        /// </summary>
        /// <param name="fingerprint_id">fingerprint_id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get data account by finger print id processing",
                       AfterMessage = "Finish get data account by finger print id processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public MemberInfoModel GetDataByFingerPrintId(int fingerprint_id, string token = null)
        {
            return ServiceLocator.Resolve<IAccount>().GetDataByFingerPrintId(fingerprint_id, token);
        }

        /// <summary>
        /// Get list account for check working time
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="divisionId">division id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list account approve for workingtime/request processing",
                       AfterMessage = "Finish get list account approve for workingtime/request processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public IList<int> GetListCheckAccountId(int accountId, int divisionId)
        {
            List<int> lstCheckAccId = new List<int>();
            var listDivGroup = ServiceLocator.Resolve<DivisionService.DivisionService>().GetListCheckAccount(divisionId);
            // Add self check
            lstCheckAccId.Add(accountId);
            if (listDivGroup != null && listDivGroup.Count > 0)
            {
                bool adminFlg = listDivGroup.Where(m => m.id == divisionId).ToList().FirstOrDefault().admin_div_flg;
                for (int i = 0; i < listDivGroup.Count; i++)
                {
                    if (i != 0 && i == listDivGroup.Count - 1 && !adminFlg)
                    {
                        lstCheckAccId.Add(0);
                    }
                    else
                    {
                        lstCheckAccId.Add(listDivGroup[i].responsible_staff_id);
                    }
                }
            }
            //var group_login_id = string.Empty;
            //bool adminFlg = false;

            //var brwAccounts = ServiceLocator.Resolve<CommonService>().GetBriswellVietNamAccount(companyId);
            //if (brwAccounts != null)
            //{
            //    var staffInfo = brwAccounts.data.Where(bw => bw.staff_id.ToLower().ToString() == staff_id.ToLower()).ToList();
            //    if (staffInfo != null && staffInfo.Count > 0)
            //    {
            //        // Group id
            //        group_login_id = staffInfo.FirstOrDefault().group_id.ToString();
            //    }
            //}

            //// Get all group parent by group_id
            //var groupsParent = ServiceLocator.Resolve<CommonService>().GetAllAccountBySuperGroupId(group_login_id, companyId);
            //if (groupsParent != null)
            //{
            //    //Get all list account
            //    List<EditAccountModel> lstAccount = ServiceLocator.Resolve<AccountService>().GetAccountList();
            //    if (lstAccount != null && lstAccount.Count > 0)
            //    {
            //        // Check member login is manager? ====
            //        group_data gdLogin = groupsParent.Where(x => x.group_id == group_login_id).FirstOrDefault();
            //        if (gdLogin != null)
            //        {
            //            // Login is not manager
            //            //if (gdLogin.responsible_person_staff_id != staff_id)
            //            //{
            //            // Add yourself
            //            var accLogin = lstAccount.Where(x => x.staff_id == staff_id).FirstOrDefault();
            //            if (accLogin != null)
            //            {
            //                lstCheckAccId.Add(accLogin.id);
            //            }
            //            //}
            //        }

            //        // Loop parent account ====
            //        foreach (var item in groupsParent)
            //        {
            //            if(StringUtil.ConvertObjectToBool(item.admin_div_flg) == true)
            //            {
            //                adminFlg = true;
            //            }
            //            // Get Account object by responsible_person_staff_id
            //            var objAccount = lstAccount.Where(x => x.staff_id == item.responsible_person_staff_id).FirstOrDefault();
            //            if (objAccount != null)
            //            {
            //                // Not the last check
            //                // or if it is the  last check : group_level = 0 and admin_div_flg = 1
            //                if (!string.Equals(item.group_level, ADMIN_DIV_FLG.NOTIN_ADMIN_GROUP) || (string.Equals(item.group_level, ADMIN_DIV_FLG.NOTIN_ADMIN_GROUP) && adminFlg))
            //                    lstCheckAccId.Add(objAccount.id);
            //                else
            //                    // if the member not in admin group => the last check is one of member in admin group 
            //                    // Login is not manager
            //                    if (item.responsible_person_staff_id != staff_id)
            //                    {
            //                        lstCheckAccId.Add(0);
            //                    }
            //            }
            //        }
            //    }
            //}

            return lstCheckAccId;
        }

        ///// <summary>
        ///// Get staff name via staff_id
        ///// </summary>
        ///// <param name="staff_id">staff_id</param>
        ///// <returns></returns>
        //[LogCallHandler(BeforeMessage = "Start get staff name by staff id processing",
        //               AfterMessage = "Finish get staff name by staff id processing",
        //               Severity = TraceEventType.Information,
        //               Categories = new string[] { "General" },
        //               IncludeParameters = false)]
        //public string GetStaffName(string staff_id)
        //{
        //    string staff_name = string.Empty;

        //    var brwAccounts = ServiceLocator.Resolve<CommonService>().GetBriswellVietNamAccount();
        //    if (brwAccounts != null)
        //    {
        //        var staffInfo = brwAccounts.data.Where(bw => bw.staff_id.ToLower().ToString() == staff_id.ToLower()).ToList();
        //        if (staffInfo != null && staffInfo.Count > 0)
        //        {
        //            // staff_name
        //            staff_name = staffInfo.FirstOrDefault().staff_name;
        //        }
        //    }
        //    return staff_name;
        //}

        /// <summary>
        /// Check the staff is responsible staff or not
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check staff is responsible staff processing",
                       AfterMessage = "Finish check staff is responsible staff processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public bool IsResponsibleStaff(string accountId, string companyId)
        {
            return ServiceLocator.Resolve<IAccount>().IsResponsibleStaff(accountId, companyId);
        }

        /// <summary>
        /// Update password quen forgot password
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update password processing",
                       AfterMessage = "Finish update password processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateAccountPassword(string password, int id,int update_account_id)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0042
            };
            if(ServiceLocator.Resolve<IAccount>().UpdateAccountPassword(password, id, update_account_id))
            {
                resultInfo.Status = ResultStatus.RegistSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0043;

            }
            return resultInfo;
        }
        public static string CreateRanDomPassword(int length)
        {
            const string lower = "abcdefghijklmnopqrstuvwxyz";
            const string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string number = "1234567890";
            const string special = "!@#$%^&*";

            var middle = length / 2;
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                if (middle == length)
                {
                    res.Append(number[rnd.Next(number.Length)]);
                }
                else if (middle - 1 == length)
                {
                    res.Append(special[rnd.Next(special.Length)]);
                }
                else
                {
                    if (length % 2 == 0)
                    {
                        res.Append(lower[rnd.Next(lower.Length)]);
                    }
                    else
                    {
                        res.Append(upper[rnd.Next(upper.Length)]);
                    }
                }
            }
            return res.ToString();
        }

       
        /// <summary>
        /// Get data by personId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get data account by personId processing",
                       AfterMessage = "Finish get data account by personId processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public MemberInfoModel GetDataByPersonId(string personId,string token)
        {
            return ServiceLocator.Resolve<IAccount>().GetDataByPersonId(personId, token);
        }

        /// <summary>
        /// Call face detect
        /// </summary>
        /// <param name="face_image">face_image</param>       
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start calling face detect processing",
                       AfterMessage = "Finish calling face detect processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public async Task<List<FaceDetectResponse>> FaceDetectByImg(string face_image)
        {
            List<FaceDetectResponse> value = new List<FaceDetectResponse>();
            FaceDetectResponse result = new FaceDetectResponse();
            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            HttpClient client = new HttpClient();
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            //Convert to binary and add content
            var binaryImg = Convert.FromBase64String(face_image);
            var content = new ByteArrayContent(binaryImg);
            content.Headers.ContentType = new MediaTypeHeaderValue(CONST_VALUE.BINARY_CONTENT_TYPE);
            // Call api detect face
            var apiUrl = AttendanceSettings.apimEndPoint + "/detect";
            Uri apim = new Uri(apiUrl);
            var res = await client.PostAsync(apim, content);
            // if success return personId           
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            if (!res.IsSuccessStatusCode)
            {
                result = JsonConvert.DeserializeObject<FaceDetectResponse>(jsonRes);
                result.status = res.StatusCode;
                value.Add(result);
                return value;
            }
            value = JsonConvert.DeserializeObject<List<FaceDetectResponse>>(jsonRes);
            if (value.Count == 0)
            {
                return value;
            }
            else
            {
                value.First().status = res.StatusCode;
            }
            // Return
            return value;
        }

        /// <summary>
        /// Call face identify
        /// </summary>
        /// <param name="faceId">faceId</param>       
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start calling face identify processing",
                       AfterMessage = "Finish calling face identify processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public async Task<List<FaceIdentifyResponse>> IdentifyFaceByFaceId(string faceId,string groupId)
        {
            List<FaceIdentifyResponse> value = new List<FaceIdentifyResponse>();
            FaceIdentifyResponse result = new FaceIdentifyResponse();
            // Get access key  
            var face_api_key = AttendanceSettings.apimKey;
            HttpClient client = new HttpClient();
            // Add header
            client.DefaultRequestHeaders.Add(CONST_VALUE.Face_API_KEY, face_api_key);
            //Add content
            List<string> faceIds = new List<string>();
            faceIds.Add(faceId);
            var body = new FaceIdentifyRequest
            {
                faceIds = faceIds,
                personGroupId = groupId,
                maxNumOfCandidatesReturned = CONST_VALUE.MAX_NUM_CANDIDATE_RETURN,
                confidenceThreshold = CONST_VALUE.CONFIDENCE_THRESHOLD
            };
            //convert body to json string
            var json = JsonConvert.SerializeObject(body);
            var content = new StringContent(json, Encoding.UTF8, CONST_VALUE.CONTENT_TYPE);
            // Call api identify face
            var apiUrl = AttendanceSettings.apimEndPoint + "/identify";
            Uri apim = new Uri(apiUrl);
            var res = await client.PostAsync(apim, content);
            // if success return object           
            var jsonRes = res.Content.ReadAsStringAsync().Result;
            if (!res.IsSuccessStatusCode)
            {
                result = JsonConvert.DeserializeObject<FaceIdentifyResponse>(jsonRes);
                result.status = res.StatusCode;
                value.Add(result);
                return value;
            }
            value = JsonConvert.DeserializeObject<List<FaceIdentifyResponse>>(jsonRes);
            if (value.Count == 0)
            {
                return value;
            }
            else
            {
                value.First().status = res.StatusCode;
            }
            // Return
            return value;
        }

        /// <summary>
        /// Get account list that alr existed face_image
        /// </summary>
        /// <returns> List active face_image</returns>
        [LogCallHandler(BeforeMessage = "Start getting list account with face_image processing",
                     AfterMessage = "Finish getting list account with face_image condition processing",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        public List<Account> ListAccountImages()
        {
            return ServiceLocator.Resolve<IAccount>().ListAccountImages();
        }
    }
}

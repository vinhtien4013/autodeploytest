﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System.Diagnostics;

namespace AM.BusinessLayer
{
    public class OverTimeService : BaseService
    {
        /// <summary>
        /// Insert working over time
        /// </summary>
        /// <param name="overtime_record_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start insert a working over time information processing",
                        AfterMessage = "Finish insert a working over time information processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public int Insert(WorkingovertimeRecord model)
        {
            var overTimDal = ServiceLocator.Resolve<IWorkingovertimeRecord>();
            return overTimDal.Insert(model);
        }

        /// <summary>
        /// Update working over time
        /// </summary>
        /// <param name="overtime_record_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update a working over time information processing",
                       AfterMessage = "Finish update a working over time information processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        [Transaction()]
        public bool Update(WorkingovertimeRecord model)
        {
            var overTimDal = ServiceLocator.Resolve<IWorkingovertimeRecord>();
            return overTimDal.Update(model);
        }

        /// <summary>
        /// The overtime was in existence
        /// </summary>
        /// <param name="overtime_record_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check if working over time is exist processing",
                       AfterMessage = "Finish update a working over time information processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public bool IsExist(int overtime_record_id)
        {
            var overTimDal = ServiceLocator.Resolve<IWorkingovertimeRecord>();
            return overTimDal.IsExist(overtime_record_id);
        }
    }
}

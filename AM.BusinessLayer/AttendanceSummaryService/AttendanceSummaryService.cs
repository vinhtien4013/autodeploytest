﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AM.BusinessLayer.AttendanceSummaryService
{
    public class AttendanceSummaryService : BaseService 
    {
        /// <summary>
        /// Get list data Attendance summary of all staff by month, year
        /// </summary>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list attendance summary processing",
             AfterMessage = "Finish get list attendance summary processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<AttendanceSummary> GetAttendanceSummary(int month, int year,string account_id,string staff_id,int company_id)
        {
            var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, company_id.ToString(),null);
            var now = AzureUtil.LocalDateTimeNow(company.time_zone_id);
            int endDate = DateTime.DaysInMonth(year, month);
            if (now.Month == month)
                endDate = now.Day;
            return ServiceLocator.Resolve<ISummary>().GetAttendanceSummary(endDate, month, year, account_id,staff_id, company_id);
        }

        /// <summary>
        /// Get list data Attendance summary detail of all staff by month, year
        /// </summary>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list attendance summary detail processing",
             AfterMessage = "Finish get list attendance summary detail processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<AttendanceSummaryDetail> GetAttendanceSummaryDetail(int month, int year,string account_id,string staff_id, int company_id)
        {
            var company = ServiceLocator.Resolve<CompanyService>().GetCompanyByCondition(null, company_id.ToString(),null);
            var now = AzureUtil.LocalDateTimeNow(company.time_zone_id);
            int endDate = DateTime.DaysInMonth(year, month);
            if (now.Month == month)
                endDate = now.Day;
            return ServiceLocator.Resolve<ISummary>().GetAttendanceSummaryDetail(endDate, month, year, account_id, staff_id, company_id);
        }
    }
}

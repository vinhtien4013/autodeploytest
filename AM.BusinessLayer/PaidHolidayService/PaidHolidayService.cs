﻿using AM.BusinessLayer.AccountService;
using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model;
using AM.Model.Const;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer
{
    public class PaidHolidayService : BaseService
    {
        /// <summary>
        /// Get remain paid holiday by account id
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get remain paid holiday by account id processing",
                        AfterMessage = "Finish get remain paid holiday by account id processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public double GetRemainPaidHolidayForAcc(int account_id)
        {
            double remainPaidHoliday = 0;

            remainPaidHoliday = ServiceLocator.Resolve<IAccountPaidHoliday>().GetRemainPaidHolidayForAcc(account_id);

            return remainPaidHoliday;
        }

        /// <summary>
        /// Get List Account Paid Holiday
        /// </summary>
        /// <param name="account_id">Account login id</param>
        /// <param name="staff_id">staff id</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list paid holiday processing",
                        AfterMessage = "Finish get list paid holiday processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<PaidHolidayResponse> GetPaidHoliday(string account_id, string staff_id, string companyId)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().GetPaidHoliday(account_id, staff_id, companyId);
        }

        /// <summary>
        /// Check Monthly Addition
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check whether monthly addition processing",
                        AfterMessage = "Finish check whether monthly addition processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public int CheckMonthlyAddition(string companyId)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().CheckMonthlyAddition(companyId);
        }

        /// <summary>
        /// Update Account Paid Holiday
        /// </summary>
        /// <param name="accPaidHolidayLst">List AccountPaidHoliday</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update account paid holiday request processing",
                        AfterMessage = "Finish update account paid holiday request processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateAccountPaidHoliday(List<AccountPaidHoliday> accPaidHolidayLst, int processeing_status)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.EditFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0023
            };

            var dalPaidHoliday = ServiceLocator.Resolve<IAccountPaidHoliday>();

            for (int i = 0; i < accPaidHolidayLst.Count; i++)
            {
                // Insert
                if (accPaidHolidayLst[i].id == 0)
                {
                    if (dalPaidHoliday.Insert(accPaidHolidayLst[i]) < 0)
                        return resultInfo;
                }
                else
                {
                    // Delete
                    if (accPaidHolidayLst[i].deleted_account_id != null && accPaidHolidayLst[i].deleted_account_id > 0)
                    {
                        if (!dalPaidHoliday.Delete(accPaidHolidayLst[i]))
                            return resultInfo;
                    }
                    // Update
                    else
                    {
                        if (!dalPaidHoliday.UpdatePaidHolidayDetail(accPaidHolidayLst[i]))
                            return resultInfo;
                    }
                }
            }

            resultInfo.Status = ResultStatus.EditSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0062;

            return resultInfo;
        }

        /// <summary>
        ///  Monthly addition for all account
        /// </summary>
        /// <param name="updateAccount"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start monthly addition for all account processing",
                        AfterMessage = "Finish monthly addition for all account processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateMonthlyAddition(int updateAccount, string companyId)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.EditFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0023
            };

            var dalPaidHoliday = ServiceLocator.Resolve<IAccountPaidHoliday>();
            if (!dalPaidHoliday.MonthlyAddition(updateAccount, companyId))
                return resultInfo;

            resultInfo.Status = ResultStatus.EditSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0062;

            return resultInfo;
        }

        /// <summary>
        /// Get list Request Manage
        /// </summary>
        /// <param name="account_id">id of user login</param>
        /// <param name="date_from">data from in view</param>
        /// <param name="date_to">date to in view</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list request manage processing",
                        AfterMessage = "Finish get list request manage processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<PaidHolidayRequestManageResponse> GetRequestManage(string id, string account, string date_from, string date_to, string approve_status)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().GetRequestManage(id, account, date_from, date_to, approve_status);
        }

        /// <summary>
        /// Get List Request Manage By Id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get request by id processing",
                        AfterMessage = "Finish get request by id processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public AccountPaidHoliday GetRequestManageById(int id)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().GetRequestManageById(id);
        }

        /// <summary>
        /// Register a new record for Request Manage / Update a exit record
        /// </summary>
        /// <param name="lstRequestManage">List Account Paid Holiday</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register/update a request processing",
                        AfterMessage = "Finish register/update a request processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo RegisterRequestManage(RequestManageInputInfo RequestManageInfo, Company company)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0021
            };

            var dalRequestManage = ServiceLocator.Resolve<IAccountPaidHoliday>();
            var dalConfirmation = ServiceLocator.Resolve<IConfirmation>();
            var dalWorkingTime = ServiceLocator.Resolve<IWorkingtimeRecord>();
            IList<int> lstConfirm = new List<int>();
            var accountId = RequestManageInfo.requestManageList.Where(m => (m.id == 0) || (m.deleted_date == null && m.oldId != "" && m.oldId != "0")).ToList().Select(p => p.account_id).FirstOrDefault();

            var accountInfo = ServiceLocator.Resolve<AccountService.AccountService>().GetDataById(RequestManageInfo.requestManageList[0].account_id);
            var dateNow = AzureUtil.LocalDateTimeNow(accountInfo.time_zone_id);
            if (accountId > 0)
            {
                // Get list account id for confimation
                lstConfirm = ServiceLocator.Resolve<AccountService.AccountService>().GetListCheckAccountId(accountId, accountInfo.division_id);
            }

            foreach (var item in RequestManageInfo.requestManageList)
            {
                List<Confirmation> confirmLst = new List<Confirmation>();
                
                
                 var _paidholidayData=new AccountPaidHoliday()
                {
                    id = item.id,
                    account_id = item.account_id,
                    application_datetime = item.application_datetime,
                    reason_txt = item.reason_txt,
                    paid_holiday_type = item.paid_holiday_type,
                    registor_days = (item.paid_holiday_type == CONST_VALUE.ALL) ? CONST_VALUE.REGISTOR_ALL : CONST_VALUE.REGISTOR_HALF,
                    created_account_id = item.created_account_id,
                    created_date = dateNow,
                    updated_account_id = item.updated_account_id,
                    updated_date = dateNow,
                    deleted_account_id = null,
                    deleted_date = null
                };

                if (item.id != 0 && (item.deleted_date != null || String.IsNullOrEmpty(item.oldId) || StringUtil.ConvertObjectToInteger32(item.oldId) == 0))
                {
                    // Check delete row
                    if (item.deleted_date != null)
                    {
                        // Delete account_paid_holiday table with processing_status = 0 and Delete confirmation table with application_form_id = 2
                        AccountPaidHoliday requestInfo = new AccountPaidHoliday();
                        requestInfo.id = item.id;
                        requestInfo.deleted_account_id = item.updated_account_id;
                        requestInfo.deleted_date = item.deleted_date;

                        resultInfo = DeleteRequestManage(requestInfo);
                        if (resultInfo.Status != ResultStatus.DeleteSuccess)
                        {
                            return resultInfo;
                        }
                    }
                    else
                    {
                        if (item.paid_holiday_type == (int)COMMANDTYP.OFFALLDAY)
                        {
                            var workingTime = new WorkingtimeRecord()
                            {
                                account_paid_holiday_id = 0,
                                account_id = item.account_id,
                                register_date = item.application_datetime,
                                deleted_account_id = item.updated_account_id,
                                deleted_date = dateNow
                            };

                            if (!dalWorkingTime.Delete(workingTime))
                                return resultInfo;
                        }
                        if (!dalRequestManage.Update(_paidholidayData))
                            return resultInfo;
                        if (!dalWorkingTime.UpdateWorkingAfterApproved(_paidholidayData, company.start_working_time.ToString(), company.end_working_time.ToString()))
                            return resultInfo;
                    }
                }
                else
                {
                    // delete old request
                    // Update working time record in case the request be approved
                    // Apply only for case the user want to change from half day request to all day request 
                    if (item.paid_holiday_type != (int)COMMANDTYP.OFFALLDAY && !String.IsNullOrEmpty(item.oldId) && StringUtil.ConvertObjectToInteger32(item.oldId) != 0)
                    {
                        
                        AccountPaidHoliday oldRequest = new AccountPaidHoliday()
                        {
                            id = StringUtil.ConvertObjectToInteger32(item.oldId),
                            deleted_account_id = item.updated_account_id,
                            deleted_date = dateNow
                        };
                        
                        if (!dalRequestManage.Delete(oldRequest))
                            return resultInfo;

                        var workingTime = new WorkingtimeRecord()
                        {
                            account_paid_holiday_id = StringUtil.ConvertObjectToInteger32(item.oldId),
                            account_id = item.account_id,
                            register_date = item.application_datetime,
                            deleted_account_id = item.updated_account_id,
                            deleted_date = dateNow
                        };
                        
                        if (!dalWorkingTime.Delete(workingTime))
                            return resultInfo;

                        if (item.id != 0)
                        {
                            oldRequest.id = item.id;
                            if (!dalRequestManage.Delete(oldRequest))
                                return resultInfo;

                            workingTime.account_paid_holiday_id = item.id;
                            if (!dalWorkingTime.Delete(workingTime))
                                return resultInfo;
                        }

                        _paidholidayData.id = 0;
                        _paidholidayData.paid_holiday_type = CONST_VALUE.ALL;
                        _paidholidayData.registor_days = CONST_VALUE.REGISTOR_ALL;
                    }
                    else if (item.paid_holiday_type == (int)COMMANDTYP.OFFALLDAY)
                    {
                        var workingTime = new WorkingtimeRecord()
                        {
                            account_paid_holiday_id = 0,
                            account_id = item.account_id,
                            register_date = item.application_datetime,
                            deleted_account_id = item.updated_account_id,
                            deleted_date = dateNow
                        };

                        if (!dalWorkingTime.Delete(workingTime))
                            return resultInfo;
                    }

                    int id = dalRequestManage.Insert(_paidholidayData);
                  
                    if (id < 0)
                        return resultInfo;
                    else
                    {
                        var check_sequence = 0;
                        if (lstConfirm.Count > 0)
                        {
                            lstConfirm = lstConfirm.Distinct().ToList();
                            lstConfirm.Remove(item.account_id);
                            foreach (var account_check in lstConfirm)
                            {
                                check_sequence++;
                                var confirm = new Confirmation()
                                {
                                    application_id = id,
                                    application_form_id = (byte)ConfirmationType.PaidHoliday,
                                    account_id = account_check,
                                    check_sequence = (byte)check_sequence,
                                    check_status = 0,
                                    application_sequence = 1,
                                    confirmed_datetime = null,
                                    reason = null,
                                    created_account_id = item.account_id,
                                    created_date = dateNow,
                                    updated_account_id = item.account_id,
                                    updated_date = dateNow
                                };

                                if (!dalConfirmation.Insert(confirm))
                                    return resultInfo;
                            }
                        }

                        _paidholidayData.id = id;
                        if (!dalWorkingTime.UpdateWorkingAfterApproved(_paidholidayData, company.start_working_time.ToString(), company.end_working_time.ToString()))
                            return resultInfo;
                    }
                }
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = InfoMessage.I0067;

            return resultInfo;
        }

        /// <summary>
        /// Delete a exit record for Request Manage
        /// </summary>
        /// <param name="requestManage">object AccountPaidHodiday</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete a request processing",
                        AfterMessage = "Finish delete a request processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo DeleteRequestManage(AccountPaidHoliday requestManage)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DeleteFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0063
            };

            AccountPaidHoliday requestInfo = GetRequestManageById(requestManage.id);
            var dalConfirmation = ServiceLocator.Resolve<IConfirmation>();
            var checkHasApproved = true;

            // Get account info by id
            int accId = requestManage.deleted_account_id ?? 0;
            var accInfo = ServiceLocator.Resolve<AccountService.AccountService>().GetDataById(accId);
            if (accInfo != null && accInfo.admin_flg == true)
            {
                checkHasApproved = false;
            }
            else
            {
                checkHasApproved = dalConfirmation.checkHasApproved(requestManage.id);
            }

            if (requestInfo == null)
            {
                resultInfo.Status = ResultStatus.DeleteNotTargetData;
                resultInfo.Message = InfoMessage.I0064;
            }
            else if (checkHasApproved == true)
            {
                resultInfo.Status = ResultStatus.DeleteFailed;
                resultInfo.Message = InfoMessage.I0065;
            }
            else
            {
                var dalRequestManage = ServiceLocator.Resolve<IAccountPaidHoliday>();
                if (!dalRequestManage.Delete(requestManage))
                    return resultInfo;
                var dalWorkingtimeRecord = ServiceLocator.Resolve<IWorkingtimeRecord>();
                if (!dalWorkingtimeRecord.UpdateWorkingAfterDeleted(requestManage.id, StringUtil.ConvertObjectToDateTime2(requestManage.deleted_date), StringUtil.ConvertObjectToInteger32(requestManage.deleted_account_id)))
                    return resultInfo;
                resultInfo.Status = ResultStatus.DeleteSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0066;
            }

            return resultInfo;
        }

        /// <summary>
        /// GEt list paid holiday detail
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list account paid holiday by account id processing",
                       AfterMessage = "Finish get list account paid holiday by account id processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public List<AccountPaidHoliday> GetListPaidHolidayDetail(string account_id)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().GetListPaidHolidayDetail(account_id);
        }

        /// <summary>
        /// Get paid holiday detail by account id and date
        /// </summary>
        /// <param name="account_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get paid holiday request detail by account id and date processing",
                      AfterMessage = "Finish get paid holiday request detail by account id and date processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public AccountPaidHoliday GetRequestByAccount(int accountId, DateTime applicationDate)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().GetRequestByAccount(accountId, applicationDate);
        }

        /// <summary>
        /// Check has appplication on application_date with account_id
        /// </summary>
        /// <param name="account_id">account_id</param>
        /// <param name="application_date">application_datetime</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check if the day is account paid holiday of account processing",
                      AfterMessage = "Finish check if the day is account paid holiday of account processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        public int HasApplication(int account_id, DateTime application_date, string isApproved_flg = null)
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().CheckApplication(account_id, application_date, isApproved_flg);
        }

        /// <summary>
        /// Delete Monthly On This Month
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete monthly addition in this month processing",
                      AfterMessage = "Finish delete monthly addition in this month processing",
                      Severity = TraceEventType.Information,
                      Categories = new string[] { "General" },
                      IncludeParameters = false)]
        [Transaction()]
        public bool DeleteMonthly()
        {
            return ServiceLocator.Resolve<IAccountPaidHoliday>().DeleteMonthly();
        }

        /// <summary>
        /// Convert data from list input to model
        /// </summary>
        /// <param name="requestLst">List request manage for register</param>
        /// <param name="data"> data input</param>
        /// <returns></returns>
        private bool ConverttoModel(RequestManageInputDetail data, ref AccountPaidHoliday paidHolidayData)
        {
            var memberInfo = ServiceLocator.Resolve<IAccount>().GetDataById(data.updated_account_id);
            var nowTime = AzureUtil.LocalDateTimeNow(memberInfo.time_zone_id);
            //confirmLst = new List<Confirmation>();

            paidHolidayData = new AccountPaidHoliday()
            {
                id = data.id,
                account_id = data.account_id,
                application_datetime = data.application_datetime,
                reason_txt = data.reason_txt,
                paid_holiday_type = data.paid_holiday_type,
                registor_days = (data.paid_holiday_type == CONST_VALUE.ALL) ? CONST_VALUE.REGISTOR_ALL : CONST_VALUE.REGISTOR_HALF,
                created_account_id = data.created_account_id,
                created_date = nowTime,
                updated_account_id = data.updated_account_id,
                updated_date = nowTime,
                deleted_account_id = null,
                deleted_date = null
            };

            return true;
        }

        
    }
}

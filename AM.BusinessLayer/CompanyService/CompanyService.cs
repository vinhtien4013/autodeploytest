﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using static AM.Model.Const.ConstType;

namespace AM.BusinessLayer
{
    public class CompanyService: BaseService
    {
        /// <summary>
        /// Get list company
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list company processing",
                       AfterMessage = "Finish get list company processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public List<Company> GetCompany ()
        {
            return ServiceLocator.Resolve<ICompany>().GetCompany ();
        }

        /// <summary>
        /// Get company information by token
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get company information by token processing",
                       AfterMessage = "Finish get company information by token processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public Company GetCompanyByCondition(string token,string id,string slack_token)
        {
            return ServiceLocator.Resolve<ICompany>().GetCompanyByCondition(token,id, slack_token);
        }

        /// <summary>
        /// Get company information by slack_token
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get company information by slack_token processing",
                       AfterMessage = "Finish get company information by slack_token processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public Company GetCompanyBySlackInfo(string slack_token)
        {
            return ServiceLocator.Resolve<ICompany>().GetCompanyBySlackInfo(slack_token);
        }

        /// <summary>
        /// Insert/ Update/ Delete the company in list
        /// </summary>
        /// <param name="companyLst"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update list company processing",
                     AfterMessage = "Finish update list company processing",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateCompany(List<Company> companyLst)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0028
            };
            try
            {
                foreach (var company in companyLst)
                {
                    // Delete company
                    if (company.deleted_date != null)
                    {
                        if (!ServiceLocator.Resolve<ICompany>().DeleteCompany(company))
                        {
                            return resultInfo;
                        }
                    }
                    else
                    {
                        // Insert company
                        if(company.id == 0)
                        {
                            int companyId = ServiceLocator.Resolve<ICompany>().InsertCompany(company);
                            if (companyId <= 0)
                            {
                                return resultInfo;
                            }

                            // Create default division
                            var division = new Division()
                            {
                                div_name = CONST_VALUE.DEFAULT_DIVISION,
                                div_code = CONST_VALUE.DEFAULT_DIV_CODE,
                                div_range_parent = 1,
                                div_range_from = 1,
                                div_range_to = 2,
                                responsible_staff_id = 0,
                                admin_div_flg = true,
                                description = CONST_VALUE.DEFAULT_DIVISION,
                                company_id = companyId,
                                created_date = company.updated_date,
                                created_account_id = company.updated_account_id,
                                updated_date = company.updated_date,
                                updated_account_id = company.updated_account_id,
                                deleted_date = null,
                                deleted_account_id = null
                            };

                            // Insert default division
                            int divisionId = ServiceLocator.Resolve<IDivision>().AddDivision(division);
                            if (divisionId <= 0)
                            {
                                return resultInfo;
                            }
                            // Create master mail
                            var account = new Account()
                            {
                                fingerprint_id = null,
                                staff_id = CONST_VALUE.DEFAULT_STAFF_ID,
                                staff_name = CONST_VALUE.DEFAULT_STAFF_NAME,
                                email = ConfigurationManager.AppSettings["MasterMail"].ToString(),
                                sex = SEX.MALE,
                                admin_flg = true,
                                division_id = divisionId,
                                joined_date = company.updated_date,
                                resign_date = company.updated_date,
                                created_date = company.updated_date,
                                created_account_id = company.updated_account_id,
                                updated_date = company.updated_date,
                                updated_account_id = company.updated_account_id,
                                deleted_date = null,
                                deleted_account_id = null,
                                password = BCrypt.Net.BCrypt.HashString(ConfigurationManager.AppSettings["MasterPassWord"].ToString()),
                                language = CONST_VALUE.DEFAULT_LANGUAGE
                            };

                            // Insert master mail
                            int accountId = ServiceLocator.Resolve<IAccount>().Insert(account);

                            if (accountId <= 0)
                            {
                                return resultInfo;
                            }

                            // Update responsible staff for default division
                            division.updated_date = company.updated_date;
                            division.responsible_staff_id = accountId;
                            division.group_id = divisionId;

                            if (!ServiceLocator.Resolve<IDivision>().UpdateDivision(division))
                            {
                                return resultInfo;
                            }
                        }
                        else
                        {
                            // Update company
                            if (!ServiceLocator.Resolve<ICompany>().UpdateCompany(company))
                            {
                                return resultInfo;
                            }
                        }
                    }

                }

                resultInfo.Status = ResultStatus.RegistSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0001;

            }
            catch(Exception ex)
            {
                LoggingUtil.WriteExceptionLog(ex.Message);
            }

            return resultInfo;
        }

        /// <summary>
        /// Create random pasword
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string CreateRanDomPassword(int length)
        {
            const string lower = "abcdefghijklmnopqrstuvwxyz";
            const string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string number = "1234567890";
            const string special = "!@#$%^&*";

            var middle = length / 2;
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                if (middle == length)
                {
                    res.Append(number[rnd.Next(number.Length)]);
                }
                else if (middle - 1 == length)
                {
                    res.Append(special[rnd.Next(special.Length)]);
                }
                else
                {
                    if (length % 2 == 0)
                    {
                        res.Append(lower[rnd.Next(lower.Length)]);
                    }
                    else
                    {
                        res.Append(upper[rnd.Next(upper.Length)]);
                    }
                }
            }
            return res.ToString();
        }
    }
}

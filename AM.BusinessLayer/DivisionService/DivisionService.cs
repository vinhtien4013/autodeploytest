﻿using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model;
using AM.Model.Tables;
using AM.Model.ConstData;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using AM.Core.PolicyInjection;
using System.Diagnostics;
using AM.Model.api.Brw;
using AM.Model.Const;
using System.Linq;

namespace AM.BusinessLayer.DivisionService
{
    public class DivisionService : BaseService
    {
        /// <summary>
        /// Get Division List For Combobox
        /// </summary>
        /// <returns> List active divison by company</returns>
        [LogCallHandler(BeforeMessage = "Start get division list for combobox processing",
                        AfterMessage = "Finish get division list for combobox processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<Division> GetDivisionListIdAndName(string company_id)
        {
            return ServiceLocator.Resolve<IDivision>().GetDivisionListIdAndName(company_id);
        }

        /// <summary>
        /// Get list division
        /// </summary>
        /// <param name="month">month</param>
        /// <param name="year">year</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get  division processing",
                        AfterMessage = "Finish get  division processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<DivisionResponse> GetDivison(int? data, int company_id)
        {

            return ServiceLocator.Resolve<IDivision>().GetDivison(data, company_id);
        }

        /// <summary>
        /// Add new division
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register a new division processing",
                        AfterMessage = "Finish register a new division processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public bool AddDivision(Division model)
        {
            return ServiceLocator.Resolve<IDivision>().AddDivision(model) > 0;
        }

        /// <summary>
        ///  Delete a division
        /// </summary>
        /// <param name="id"></param>
        /// <param name="account_id"></param>
        /// <param name="company_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete a division processing",
                        AfterMessage = "Finish delete a division processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public bool DeleteDivision(int id, int account_id, int company_id)
        {
            return ServiceLocator.Resolve<IDivision>().DeleteDivision(id, account_id, company_id);
        }

        /// <summary>
        /// Get division by company id
        /// </summary>
        /// <param name="company_id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start  get division by company id processing",
                        AfterMessage = "Finish   get division by company id processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<DivisionMenu> GetDivisionByCompanyId(int company_id)
        {
            return ServiceLocator.Resolve<IDivision>().GetDivisionByCompanyId(company_id);
        }

        /// <summary>
        /// Check delete division
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check delete division processing",
                        AfterMessage = "Finish check delete division processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public int CheckDeleteDivision(int id)
        {
            return ServiceLocator.Resolve<IDivision>().CheckDeleteDivision(id);
        }

        /// <summary>
        /// Update a division
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update a division processing",
                        AfterMessage = "Finish update a division processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public bool Update(Division model)
        {
            return ServiceLocator.Resolve<IDivision>().UpdateDivision(model);
        }

        /// <summary>
        /// Update list of division
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update list of division processing",
                        AfterMessage = "Finish update list of division processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateDivision(List<Division> RequestInfo)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0029
            };
            string checkExistDivCode = CheckExistDivCode(RequestInfo);
            if (checkExistDivCode != ConstType.CheckExistDivision.Pass)
            {
                resultInfo.Status = ResultStatus.DataNotExists;
                resultInfo.Message = checkExistDivCode;
                return resultInfo;
            }

            foreach (var item in RequestInfo)
            {
                //Delete mode
                if (item.modify_mode == ConstType.ModifyMode.DeleteMode)
                {
                    if (!DeleteDivision(item.group_id, item.created_account_id, item.company_id))
                    {
                        return resultInfo;
                    };
                }
                //Add mode
                else if (item.modify_mode == ConstType.ModifyMode.InSertMode)
                {
                    if (!AddDivision(item))
                        return resultInfo;
                }
                else
                {
                    if (!Update(item))
                    {
                        return resultInfo;
                    }
                }
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = "Register Request Manage Success";

            return resultInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get group data  processing",
                        AfterMessage = "Finish  get group data processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public BRWGroup GetGroupData(string companyId)
        {
            BRWGroup group = new BRWGroup();
            var lstdata = ServiceLocator.Resolve<IDivision>().GetGroupData(companyId);
            group.data = lstdata;

            return group;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get group data  processing",
                        AfterMessage = "Finish  get group data processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<Division> GetListCheckAccount(int divisionId)
        {
            return ServiceLocator.Resolve<IDivision>().GetListCheckAccount(divisionId);
        }

        public string CheckExistDivCode(List<Division> lstDvision)
        {
            var lstIdUpdate = string.Join(",", lstDvision.Select(x => x.group_id));
            foreach (var item in lstDvision)
            {
                if (item.modify_mode == ConstType.ModifyMode.InSertMode || item.modify_mode == ConstType.ModifyMode.UpdateMode)
                {
                    if (ServiceLocator.Resolve<IDivision>().CheckExistDivCode(item.div_code, item.company_id, lstIdUpdate))
                        return item.div_code;

                }
            }
            return ConstType.CheckExistDivision.Pass;
        }
    }
}

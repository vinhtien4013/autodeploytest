﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Common.Properties;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AM.BusinessLayer
{
    public class HolidayService : BaseService
    {
        /// <summary>
        /// Get List Holiday By Year
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list holiday by year processing",
                        AfterMessage = "Finish get list holiday by year processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public List<Holiday> GetHolidayByYear(int year, int companyId)
        {
            return ServiceLocator.Resolve<IHoliday>().GetHolidayByYear(year, companyId);
        }

        /// <summary>
        /// Get List Holiday By Id
        /// </summary>
        /// <param name="year">Year</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get holiday information by id processing",
                        AfterMessage = "Finish get holiday information by id processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        public Holiday GetHolidayById(int id)
        {
            return ServiceLocator.Resolve<IHoliday>().GetHolidayById(id);
        }

        /// <summary>
        /// Register a new record for Holiday / Update a exit record
        /// </summary>
        /// <param name="Lstholiday">List Holiday</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start register/update holiday processing",
                        AfterMessage = "Finish register/update holiday processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdateHoliday(List<Holiday> Lstholiday)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.RegistFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0011
            };

            var dalHoliday = ServiceLocator.Resolve<IHoliday>();

            foreach(var item in Lstholiday)
            {
                if (item.id != 0)
                {
                    if (item.deleted_date == null)
                    {
                        if (!dalHoliday.Update(item))
                            return resultInfo;
                    }
                    else
                    {
                        if (!dalHoliday.Delete(item))
                            return resultInfo;
                    }
                }
                else
                {
                    if (!dalHoliday.Insert(item))
                        return resultInfo;
                }
            }

            resultInfo.Status = ResultStatus.RegistSuccess;
            resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            resultInfo.Message = "Register Holiday Success";

            return resultInfo;
        }

        /// <summary>
        /// Delete a exit record for Holiday
        /// </summary>
        /// <param name="Lstholiday">List Holiday</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start delete a holiday processing",
                        AfterMessage = "Finish delete a holiday processing",
                        Severity = TraceEventType.Information,
                        Categories = new string[] { "General" },
                        IncludeParameters = false)]
        [Transaction()]
        public ResultInfo DeleteHoliday(Holiday holiday)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.DeleteFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0015
            };

            Holiday holidayInfo = GetHolidayById(holiday.id);

            if (holidayInfo == null)
            {
                resultInfo.Status = ResultStatus.DeleteNotTargetData;
                resultInfo.Message = InfoMessage.I0014;
            }
            else
            {
                var dalHoliday = ServiceLocator.Resolve<IHoliday>();

                if (!dalHoliday.Delete(holiday))
                    return resultInfo;
                 
                resultInfo.Status = ResultStatus.DeleteSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0061;
            }

            return resultInfo;
        }

        /// <summary>
        /// Check the date is holiday or not
        /// </summary>
        /// <param name="date"></param>
        /// <param name="companyId">company id</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start check if the date is holiday processing",
                       AfterMessage = "Finish check if the date is holiday processing",
                       Severity = TraceEventType.Information,
                       Categories = new string[] { "General" },
                       IncludeParameters = false)]
        public bool IsHoliday(DateTime date, int companyId)
        {
            return ServiceLocator.Resolve<IHoliday>().IsHoliday(date, companyId);
        }
    }
}

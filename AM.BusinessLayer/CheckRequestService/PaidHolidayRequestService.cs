﻿using AM.Core.PolicyInjection;
using AM.Core.ServiceLocator;
using AM.Interface;
using AM.Model;
using AM.Model.ConstData;
using AM.Model.Tables;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AM.BusinessLayer
{
    public class PaidHolidayRequestService : BaseService
    {
        /// <summary>
        /// Get list information for screen Paid Holiday Check Request
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="accountId">account_id</param>
        /// <param name="applyFrom">date_from</param>
        /// <param name="applyTo">date To</param>
        /// <param name="status">status</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list check paid holiday information processing",
             AfterMessage = "Finish get list check paid holiday information processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        public List<CheckPaidHolidayResponse> GetPaidHolidayCheckRequest(string id, string accountId, string applyFrom, string applyTo, string status, string companyId)
        {
            var dalAccountPaidHoliday = ServiceLocator.Resolve<IAccountPaidHoliday>();
            List<CheckPaidHolidayResponse> listRequest = new List<CheckPaidHolidayResponse>();

            try
            {
                listRequest = dalAccountPaidHoliday.GetPaidHolidayCheckRequest(id, accountId, applyFrom, applyTo, status, companyId);
            }
            catch (Exception)
            {
            }

            return listRequest;
        }

        /// <summary>
        /// Update check Status relevant to approve Paid Holiday
        /// </summary>
        /// <param name="lstConfirm">list of Confirmation</param>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start update check Status relevant to approve paid holiday request processing",
             AfterMessage = "Finish update check Status relevant to approve paid holiday request processing",
             Severity = TraceEventType.Information,
             Categories = new string[] { "General" },
             IncludeParameters = false)]
        [Transaction()]
        public ResultInfo UpdatePaidHolidayCheck(Collection<Confirmation> lstConfirm, Company company)
        {
            ResultInfo resultInfo = new ResultInfo
            {
                Status = ResultStatus.EditFailed,
                InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.RequestRollBack,
                Message = InfoMessage.I0022
            };

            try
            {
                var dalConfirm = ServiceLocator.Resolve<IConfirmation>();
                foreach(var item in lstConfirm)
                {
                    if (!dalConfirm.UpdatePaidHolidayCheckStatus(item, company.start_working_time.ToString(), company.end_working_time.ToString()))
                        return resultInfo;
                }

                resultInfo.Status = ResultStatus.EditSuccess;
                resultInfo.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
                resultInfo.Message = InfoMessage.I0054;

            }
            catch (Exception ex)
            {
                resultInfo.Message = ex.Message;
            }

            return resultInfo;
        }
    }
}

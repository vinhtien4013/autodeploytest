﻿using System;
using System.Net.Http;
using AM.Core.PolicyInjection;
using AM.DataAccessLayer;
using AM.Model.api;
using AM.Model.ConstData;

/// <summary>
/// 基幹サービス
/// </summary>
/// <remarks>
/// 処理結果を定義したサービス
/// 各サービスはこのサービスを継承してください
/// </remarks>
namespace AM.BusinessLayer
{
    public class BaseService : MarshalByRefObject
    {
        #region "ResultStatus: 処理結果定義"
        public class ResultInfo : IResultModel
        {
            public string InnerStatus { get; set; }
            public string Status { get; set; }
            public string Message { get; set; }
            public ResultInfo()
            {
                //正常終了用に初期化
                this.Status = ResultStatus.RegistSuccess;
                this.InnerStatus = Core.PolicyInjection.ResultConst.InnerStatus.Success;
            }           
        }
        protected HttpClient client = new HttpClient();
        #endregion
    }
}

﻿using AM.Interface;
using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AM.BusinessLayer.SendGrid
{
    public class SendGridService : IEmailService
    {
        public string SendEmail(string toEmail, string subject, string content)
        {
            string FROM = ConfigurationManager.AppSettings["FromMail"];
            string FROMNAME = ConfigurationManager.AppSettings["NameFromMail"];
            string TO = toEmail;
            string SMTP_USERNAME = ConfigurationManager.AppSettings["SmtpUsername"];
            string SMTP_PASSWORD = ConfigurationManager.AppSettings["SmtpPassword"];
            string HOST = ConfigurationManager.AppSettings["HostSmtp"];
            int PORT = 587;
            // Create and build a new MailMessage object
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.From = new MailAddress(FROM, FROMNAME);
            message.To.Add(new MailAddress(TO));
            message.Subject = subject;
            message.Body = content;
            // Comment or delete the next line if you are not using a configuration set

            using (var client = new SmtpClient(HOST, PORT))
            {
                // Pass SMTP credentials
                client.Credentials =
                    new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                // Enable SSL encryption
                client.EnableSsl = true;

                // Try to send the message. Show status in console.
                try
                {
                    client.Send(message);
                    return "Success";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
               
            }
        }

    }
}

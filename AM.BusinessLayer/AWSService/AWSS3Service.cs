﻿using AM.Core.ServiceLocator;
using AM.Core.Util;
using AM.Interface;
using AM.Model.api;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Practices.EnterpriseLibrary.Logging.PolicyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace AM.BusinessLayer.AWSService
{
    public class AWSS3Service : BaseService
    {
        private IAmazonS3 s3Client = GetS3Client();

        /// <summary>
        /// Init S3 Client
        /// </summary>
        /// <returns></returns>
        public static IAmazonS3 GetS3Client()
        {
            //NameValueCollection appConfig = ConfigurationManager.AppSettings;
            AmazonS3Config config = new AmazonS3Config();
            //default Southeast Singapore
            config.RegionEndpoint = RegionEndpoint.APSoutheast1;
            //init s3Client
            AmazonS3Client s3Client = new AmazonS3Client(AttendanceSettings.awsAccessKEy, AttendanceSettings.awsSecretKEy, config);
            return s3Client;
        }

        /// <summary>
        /// get List Object
        /// </summary>
        /// <returns></returns>
        public ListObjectsResponse GetListObject()
        {
            ListObjectsRequest listRequest = new ListObjectsRequest
            {
                BucketName = ConfigurationManager.AppSettings["awsDefaultBucketName"],
                Prefix = ConfigurationManager.AppSettings["awsDefaultFolderName"]
            };

            ListObjectsResponse listResponse;
            do
            {
                // Get a list of objects
                listResponse = s3Client.ListObjects(listRequest);
                // Set the marker property
                listRequest.Marker = listResponse.NextMarker;
            } while (listResponse.IsTruncated);
            return listResponse;
        }

        /// <summary>
        /// get list file image expire, not include default.jpg
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public List<S3Object> GetListFileExpire(int day = 2592000)
        {
            var listALlObject = GetListObject();
            var listFileExpire = new List<S3Object>();
            string[] prefixArr;
            bool checkFormat;
            string file_name;
            string defaultImg = ConfigurationManager.AppSettings["defaultImg"];
            var expireDay = ConfigurationManager.AppSettings["expireDay"];
            day = Int32.Parse(expireDay);
            //get file jpg png
            string[] formats = new string[] { ".jpg", ".png" };
            foreach (var item in listALlObject.S3Objects)
            {
                prefixArr = item.Key.Split('/');
                file_name = prefixArr.Last();
                checkFormat = formats.Any(i => file_name.EndsWith(i, StringComparison.OrdinalIgnoreCase));
                /*Get object expire
                Get object in sub-folder only
                Get object is valid img format
                Get object is not default image*/
                if ((DateTime.Now - item.LastModified).TotalSeconds > day && prefixArr.Length > 2 && checkFormat && !file_name.Equals(defaultImg))
                {
                    listFileExpire.Add(item);
                }
            }
            return listFileExpire;
        }

        /// <summary>
        /// Get list delete object that expire and unused from account table
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start get list key unnecessary image for delete processing",
              AfterMessage = "Finish get list key unnecessary image for delete processing",
              Severity = TraceEventType.Information,
              Categories = new string[] { "General" },
              IncludeParameters = false)]
        public DeleteObjectsRequest GetListDeleteKey()
        {
            DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest
            {
                BucketName = ConfigurationManager.AppSettings["awsDefaultBucketName"]
            };

            //list expire
            var listExpire = GetListFileExpire();
            //list active image in used
            var listAccountImage = ServiceLocator.Resolve<IAccount>().ListAccountImages();
            //return list expire and not used
            //var result = new List<string>();
            var expireImgAccountList = new List<S3Object>();
            if (listExpire.Count > 0 && listAccountImage.Count > 0)
            {
                expireImgAccountList = listExpire.Where(x => !listAccountImage.Any(s => s.face_image.Equals(x.Key.Split('/').Last()))).ToList();
            }
            if (expireImgAccountList.Count > 0)
            {
                foreach (var item in expireImgAccountList)
                {
                    multiObjectDeleteRequest.AddKey(item.Key);
                }
            }
            return multiObjectDeleteRequest;
        }

        /// <summary>
        /// Delete multi objects expire
        /// </summary>
        /// <returns></returns>
        [LogCallHandler(BeforeMessage = "Start processing of Batch DeleteUnnecessaryFaceImageWebjob",
                     AfterMessage = "Finish processing of Batch DeleteUnnecessaryFaceImageWebjob",
                     Severity = TraceEventType.Information,
                     Categories = new string[] { "General" },
                     IncludeParameters = false)]
        public bool MultiObjectDelete() 
        {
            ////get list need to be deleted
            //var deleteKeys = GetListDeleteKey();
            //DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest
            //{
            //    BucketName = ConfigurationManager.AppSettings["awsDefaultBucketName"]
            //};
            //if (deleteKeys.Count > 0)
            //{
            //    foreach (var item in deleteKeys)
            //    {
            //        multiObjectDeleteRequest.AddKey(item);
            //    }
            //    DeleteObjectsResponse response = s3Client.DeleteObjects(multiObjectDeleteRequest);
            //    if (response.DeleteErrors.Count > 0)
            //    {
            //        return false;
            //    }
            //}
            try
            {
                //get list need to be deleted
                var multiObjectDeleteRequest = GetListDeleteKey();

                DeleteObjectsResponse response = s3Client.DeleteObjects(multiObjectDeleteRequest);
                if (response.DeleteErrors.Count > 0)
                {
                    foreach (DeleteError deleteError in response.DeleteErrors)
                    {
                        LoggingUtil.WriteLog("Error deleting item " + deleteError.Key);
                        LoggingUtil.WriteLog("Message - " + deleteError.Message);
                    }

                    return false;
                }

                foreach (DeletedObject deletedObject in response.DeletedObjects)
                {
                    LoggingUtil.WriteLog("Deleted item " + deletedObject.Key);
                }

                LoggingUtil.WriteLog("API DeleteUnnecessaryFaceImage delete " + response.DeletedObjects.Count + "images successful");

                return true;
            }
            catch (DeleteObjectsException doe)
            {
                // Catch error and list error details
                DeleteObjectsResponse errorResponse = doe.Response;

                foreach (DeletedObject deletedObject in errorResponse.DeletedObjects)
                {
                    LoggingUtil.WriteLog("Deleted item " + deletedObject.Key);
                }
                foreach (DeleteError deleteError in errorResponse.DeleteErrors)
                {
                    LoggingUtil.WriteLog("Error deleting item " + deleteError.Key);
                    LoggingUtil.WriteLog("Message - " + deleteError.Message);
                }
                return false;
            }
        }
    }
}